

::settings
nrfutil settings generate --family NRF52 --application ..\_build\collar_app.hex --application-version 1 --bootloader-version 1 --bl-settings-version 1 collar_setting.hex

::merge
mergehex.exe -m nrf52_sdk.hex nrf52_boot.hex -o sd_boot.hex
mergehex.exe -m sd_boot.hex ..\_build\collar_app.hex -o sd_boot_app.hex
mergehex.exe -m sd_boot_app.hex collar_setting.hex -o collarapp.hex

del sd_boot.hex
del sd_boot_app.hex
::del .\nrf52_setting.hex

