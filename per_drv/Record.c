/*
 * @Author: jiaqi.chen 
 * @Date: 2018-02-01 13:59:30 
 * @Last Modified by: jiaqi.chen
 * @Last Modified time: 2018-02-01 16:38:19
 */
#include "Record.h"
#include "Partition.h"
#include "nrf_log.h"
#include "W25Q.h"
#include "behavior.h"
#include "user_app_origin.h"
#include "user_app_env.h"
#include "user_app_estrus.h"
#include "user_app_warning.h"

/* Defines -------------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
/**
* acc origin index
*/
static Part_Index_t originIdx = {
  .read = 0,
  .write = 0,
  .count = 0,
  .sequence = 0xffffffff,
};

static Part_Info_t originPart = {
  .base = ACC_ORIGIN_BASE_SECOTR * W25Q_SECTOR_SIZE,
  .itemSize = sizeof(acc_origin_record_t),
  .capacity = (ACC_ORIGIN_NUM_SECTORS - 1) * W25Q_SECTOR_SIZE / sizeof(acc_origin_record_t),
  .index = &originIdx,
  .lock = 0,
};
/**
* behaveior index
*/
static Part_Index_t behaviorIdx = {
  .read = 0,
  .write = 0,
  .count = 0,
  .sequence = 0xffffffff,
};

static Part_Info_t behaviorPart = {
  .base = BEHAVIOR_BASE_SECOTR * W25Q_SECTOR_SIZE,
  .itemSize = sizeof(Behavior_Record_t),
  .capacity = (BEHAVIOR_NUM_SECTORS - 1) * W25Q_SECTOR_SIZE / sizeof(Behavior_Record_t),
  .index = &behaviorIdx,
  .lock = 0,
};
/**
* environment index
*/
static Part_Index_t envIdx = {
  .read = 0,
  .write = 0,
  .count = 0,
  .sequence = 0xffffffff,
};

static Part_Info_t envPart = {
  .base = ENV_BASE_SECOTR * W25Q_SECTOR_SIZE,
  .itemSize = sizeof(env_record_t),
  .capacity = (ENV_NUM_SECTORS - 1) * W25Q_SECTOR_SIZE / sizeof(env_record_t),
  .index = &envIdx,
  .lock = 0,
};

/**
* estrus index
*/
static Part_Index_t estrusIdx = {
  .read = 0,
  .write = 0,
  .count = 0,
  .sequence = 0xffffffff,
};

static Part_Info_t estrusPart = {
  .base = ESTRUS_BASE_SECOTR * W25Q_SECTOR_SIZE,
  .itemSize = sizeof(estrus_record_t),
  .capacity = (ESTRUS_NUM_SECTORS - 1) * W25Q_SECTOR_SIZE / sizeof(estrus_record_t),
  .index = &estrusIdx,
  .lock = 0,
};
/**
* warning index
*/
static Part_Index_t warningIdx = {
  .read = 0,
  .write = 0,
  .count = 0,
  .sequence = 0xffffffff,
};

static Part_Info_t warningPart = {
  .base = WARNING_BASE_SECOTR * W25Q_SECTOR_SIZE,
  .itemSize = sizeof(warning_record_t),
  .capacity = (WARNING_NUM_SECTORS - 1) * W25Q_SECTOR_SIZE / sizeof(warning_record_t),
  .index = &warningIdx,
  .lock = 0,
};

static Part_Info_t* partTable[] = {
    
  &originPart,
  &behaviorPart,
  &envPart,
  &estrusPart,
  &warningPart,
};

#define LOG_TAG NRF_LOG_INFO("%s(%d)<%s>",__FILE__,__LINE__,__FUNCTION__);



/**
 * @brief Write an error record
 *
 * @param time
 * @param type
 * @param data
 *
 * @return true if success
 */
/*
bool Record_Error(uint32_t time, uint32_t type, uint32_t data) {
    records_error_t err;
    err.time_stamp = time;
    err.version = 0x01;
    err.type = type;
    err.data = data;

  return Record_Write(E_RECORDS_TYPE_ERROR, (uint8_t*)&err);
}
*/

/**
 * @brief Get the size of a data item
 *
 * @param type
 *
 * @return size in byte
 */
uint32_t Record_Size(uint32_t type) {
  uint16_t ret = 0;

  switch(type) {
    case RECORD_TYPE_BEHAVIOR:
      ret = sizeof(Behavior_Record_t);
      break;
#if 0
    case RECORDS_TYPE_STATUS:
      ret = sizeof(records_status_t);
      break;
    case RECORDS_TYPE_ERROR:
      ret = sizeof(records_status_t);
      break;
#endif
    default:
      break;
  }
  return ret;
}

/**
 * @brief Count record
 *
 * @param type
 *
 * @return number of items
 */
uint32_t Record_Count(uint32_t type) {
  return Part_Count(partTable[type]);
}

//uint8_t buf[1024];
//void Record_Browse(uint32_t type) {
//  const Part_Info_t *part = partTable[type];
//  uint32_t address = part->base;
//  uint32_t batch = sizeof(buf) / part->itemSize * part->itemSize;

//  if(!W25Q_Lock()) {
//    return;
//  }

//  for(int i = 0; i < 4; i++) {
//    W25Q_Read(address, sizeof(buf), buf);
//    address += sizeof(buf);
//  }
//
//  for(int i = 0; i < 160; i++) {
//    W25Q_Read(address, batch, buf);
//    address += batch;
//  }
//  W25Q_Unlock();
//}

/**
 * @brief Read record
 *
 * @param type
 * @param pOut
 * @param outBufSize
 *
 * @return number of items read
 */
uint32_t Record_Read(uint32_t type, uint8_t* pOut, uint32_t outBufSize) 
{
  return Part_Read(partTable[type], pOut, outBufSize);
}

/**
 * @brief Get record
 *
 * @param type
 * @param offset
 * @param pOut
 *
 * @return true if success
 */
bool Record_Get(uint32_t type, uint32_t offset, uint8_t* pOut) {
  return Part_Get(partTable[type], offset, pOut);
}

/**
 * @brief Peek record
 *
 * @param type
 * @param pOut
 * @param outBufSize
 *
 * @return number of items read
 */
uint32_t Record_Peek(uint32_t type, uint8_t* pOut, uint32_t outBufSize) {
  return Part_Peek(partTable[type], pOut, outBufSize);
}

/**
 * @brief Write record
 *
 * @param type
 * @param pData
 *
 * @return true if success
 */
bool Record_Write(uint32_t type, uint8_t* pData) 
{
  return Part_Write(partTable[type], pData);
}

/**
 * @brief Delete record
 *
 * @param type
 * @param nbItems
 *
 * @return true if success
 */
bool Record_Delete(uint32_t type, uint32_t nbItems) {
  return Part_Delete(partTable[type], nbItems);
}

/**
 * @brief Erase all records
 *
 * @return true if success
 */
/*
bool Record_Erase(void) {
  for(int type = 0; type < E_RECORDS_TYPE_MAX; type++) {
      Part_Erase(partTable[type]);
  }
  return true;
}



void records_test()
{
    records_odba_t odba[16 * 5];
    static uint8_t count = 0;
    odba[0].time_stamp = 0xffff0000;
    odba[0].dba_x = 10;
    odba[0].dba_y = 12;
    odba[0].dba_z = 13 + count;
    //Record_Write(E_RECORDS_TYPE_ODBA,(uint8_t *)(&odba[0]));

    //NRF_LOG_INFO("Untrans count is: %d",Record_Count(E_RECORDS_TYPE_ODBA));
    return;



 

    //Record_Read(E_RECORDS_TYPE_ODBA,(uint8_t *)odba, 16 * (count + 5));
    //NRF_LOG_INFO("%d",Record_Count(E_RECORDS_TYPE_ODBA));
    return ;

    for(int i = 0; i < 5 + count; i++)
    {
        NRF_LOG_INFO("time stamp is :%08x", odba[i].time_stamp);
        NRF_LOG_INFO("dbax is :%d", odba[i].dba_x);
        NRF_LOG_INFO("dbay is :%d", odba[i].dba_y);
        NRF_LOG_INFO("dbaz is :%d", odba[i].dba_z);
    }
    count ++;
    return;
    for(int i = 0; i < E_RECORDS_TYPE_MAX; i++)
    {

        NRF_LOG_INFO("**********RECORDS Info*******%d", i);
        Part_Info_t * part = *(partTable + i); 
        NRF_LOG_INFO("Base is: %08x", part->base);
        NRF_LOG_INFO("Capacity is: %d", part->capacity);
        NRF_LOG_INFO("Item size is: %d", part->itemSize);
        Part_Index_t * p_index = part->index;
        NRF_LOG_INFO("Write is: %08x", p_index->write);
        NRF_LOG_INFO("Read is: %08x", p_index->read);
        NRF_LOG_INFO("Countis: %d", p_index->count);
        NRF_LOG_INFO("Sequence is: %d", p_index->sequence);
    }

}
*/

