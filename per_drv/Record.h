/*
 * @Author: jiaqi.chen 
 * @Date: 2018-02-01 13:59:40 
 * @Last Modified by: jiaqi.chen
 * @Last Modified time: 2018-02-01 15:29:16
 */
#ifndef __RECORDS_H__
#define __RECORDS_H__
#include <stdint.h>
#include <stdbool.h>
#include "W25Q.h"
//one sector has 4096 bytes, 4KB
//#define APP_SECTOR_BYTES            (DRV_GD25Q127_ONE_SECTOR_HAS_BYTES)
#define APP_DATA_BASE_SECTOR        10

#define ACC_ORIGIN_BASE_SECOTR      (APP_DATA_BASE_SECTOR+1)
#define ACC_ORIGIN_NUM_SECTORS      3200//3200*4096//12M bytes , 16* 200 sectors for origin acc datas

#define BEHAVIOR_BASE_SECOTR        (ACC_ORIGIN_NUM_SECTORS + ACC_ORIGIN_BASE_SECOTR+1)
#define BEHAVIOR_NUM_SECTORS        256     //4KB * 100 spaces

#define ENV_BASE_SECOTR             (BEHAVIOR_BASE_SECOTR + BEHAVIOR_NUM_SECTORS+1)
#define ENV_NUM_SECTORS             256     //400K spaces

#define ESTRUS_BASE_SECOTR          (ENV_BASE_SECOTR + ENV_NUM_SECTORS+1)
#define ESTRUS_NUM_SECTORS          256     //400K spaces

#define WARNING_BASE_SECOTR          (ESTRUS_BASE_SECOTR + ESTRUS_NUM_SECTORS+1)
#define WARNING_NUM_SECTORS         200     //200K spaces


typedef struct _records_odba
{
    uint32_t time_stamp;
    uint32_t dba_x;
    uint32_t dba_y;
    uint32_t dba_z;
    uint32_t meandl_x;
    uint32_t meandl_y;
    uint32_t meandl_z;
}records_odba_t;

typedef struct _records_error
{
    uint32_t time_stamp;
    uint32_t version;
    uint32_t type;
    uint32_t data;
}records_error_t;

typedef struct __records_status
{
    uint32_t time_stamp;
    uint32_t battery_level;
    uint32_t state;
}records_status_t;

typedef enum 
{
    RECORD_TYPE_ACC_ORIGIN = 0,
    RECORD_TYPE_BEHAVIOR,    
    RECORD_TYPE_ENV,
    RECORD_TYPE_ESTRUS,  
    RECORD_TYPE_WARNING,
    RECORD_TYPE_MAX,
    
}records_type_t;

//void records_test();

uint32_t Record_Size(uint32_t type);
bool Record_Error(uint32_t time, uint32_t type, uint32_t data);
uint32_t Record_Count(uint32_t type);
bool Record_Write(uint32_t type, uint8_t* pData);
bool Record_Delete(uint32_t type, uint32_t nbItems);
uint32_t Record_Read(uint32_t type, uint8_t* pOut, uint32_t outBufSize);
uint32_t Record_Peek(uint32_t type, uint8_t* pOut, uint32_t outBufSize);
#endif

