/* Automatically generated nanopb header */
/* Generated by nanopb-0.3.6-dev at Fri May 25 16:43:00 2018. */

#ifndef PB_SETTING_PB_H_INCLUDED
#define PB_SETTING_PB_H_INCLUDED
#include <pb.h>

#include "IdentityMsg.pb.h"

/* @@protoc_insertion_point(includes) */
#if PB_PROTO_HEADER_VERSION != 30
#error Regenerate this file with the current version of nanopb generator.
#endif

#ifdef __cplusplus
extern "C" {
#endif

/* Struct definitions */
typedef struct {
    bool has_EnvSampleMode;
    int32_t EnvSampleMode;
    bool has_EnvSampleInterval;
    int32_t EnvSampleInterval;
    bool has_BehaviorSampleMode;
    int32_t BehaviorSampleMode;
    bool has_BehaviorSampleInterval;
    int32_t BehaviorSampleInterval;
    bool has_GPSSampleMode;
    int32_t GPSSampleMode;
    bool has_GPSSampleInterval;
    int32_t GPSSampleInterval;
    bool has_CommunicationMode;
    int32_t CommunicationMode;
    bool has_CommunicationInterval;
    int32_t CommunicationInterval;
    pb_callback_t CommunicationTimeTable;
    bool has_ResetDevice;
    int32_t ResetDevice;
    pb_callback_t PowerOffTime;
    bool has_PowerOffMode;
    int32_t PowerOffMode;
    bool has_OTAFirmwareVersion;
    int32_t OTAFirmwareVersion;
    pb_callback_t OTAFirmwareID;
    bool has_OTAForceUpgrade;
    int32_t OTAForceUpgrade;
    pb_callback_t OTAServerHost;
    bool has_OTAServerPort;
    int32_t OTAServerPort;
    bool has_SMSMode;
    int32_t SMSMode;
    bool has_SMSInterval;
    int32_t SMSInterval;
    bool has_AlarmMode;
    int32_t AlarmMode;
    pb_callback_t OriginTime;
    bool has_OriginMode;
    int32_t OriginMode;
    bool has_EstrusSampleMode;
    int32_t EstrusSampleMode;
    bool has_EstrusSampleInterval;
    int32_t EstrusSampleInterval;
/* @@protoc_insertion_point(struct:protocol_setting_t) */
} protocol_setting_t;

typedef struct {
    protocol_identity_msg_t Iden;
/* @@protoc_insertion_point(struct:protocol_setting_req_t) */
} protocol_setting_req_t;

typedef struct {
    uint32_t Begin;
    uint32_t End;
/* @@protoc_insertion_point(struct:protocol_time_range_t) */
} protocol_time_range_t;

typedef struct {
    protocol_identity_msg_t Iden;
    bool has_SettingInfo;
    protocol_setting_t SettingInfo;
/* @@protoc_insertion_point(struct:protocol_setting_rsp_t) */
} protocol_setting_rsp_t;

/* Default values for struct fields */
extern const int32_t protocol_setting_env_sample_mode_default;
extern const int32_t protocol_setting_env_sample_interval_default;
extern const int32_t protocol_setting_behavior_sample_mode_default;
extern const int32_t protocol_setting_behavior_sample_interval_default;
extern const int32_t protocol_setting_g_p_s_sample_mode_default;
extern const int32_t protocol_setting_g_p_s_sample_interval_default;
extern const int32_t protocol_setting_communication_mode_default;
extern const int32_t protocol_setting_communication_interval_default;
extern const int32_t protocol_setting_reset_device_default;
extern const int32_t protocol_setting_power_off_mode_default;
extern const int32_t protocol_setting_o_t_a_firmware_version_default;
extern const int32_t protocol_setting_o_t_a_force_upgrade_default;
extern const int32_t protocol_setting_o_t_a_server_port_default;
extern const int32_t protocol_setting_s_m_s_mode_default;
extern const int32_t protocol_setting_s_m_s_interval_default;
extern const int32_t protocol_setting_alarm_mode_default;
extern const int32_t protocol_setting_origin_mode_default;
extern const int32_t protocol_setting_estrus_sample_mode_default;
extern const int32_t protocol_setting_estrus_sample_interval_default;

/* Initializer values for message structs */
#define PROTOCOL_SETTING_REQ_INIT_DEFAULT        {PROTOCOL_IDENTITY_MSG_INIT_DEFAULT}
#define PROTOCOL_TIME_RANGE_INIT_DEFAULT         {0, 0}
#define PROTOCOL_SETTING_INIT_DEFAULT            {false, 0, false, 0, false, 0, false, 0, false, 0, false, 0, false, 1, false, 0, {{NULL}, NULL}, false, 0, {{NULL}, NULL}, false, 0, false, 0, {{NULL}, NULL}, false, 0, {{NULL}, NULL}, false, 0, false, 0, false, 0, false, 0, {{NULL}, NULL}, false, 0, false, 0, false, 0}
#define PROTOCOL_SETTING_RSP_INIT_DEFAULT        {PROTOCOL_IDENTITY_MSG_INIT_DEFAULT, false, PROTOCOL_SETTING_INIT_DEFAULT}
#define PROTOCOL_SETTING_REQ_INIT_ZERO           {PROTOCOL_IDENTITY_MSG_INIT_ZERO}
#define PROTOCOL_TIME_RANGE_INIT_ZERO            {0, 0}
#define PROTOCOL_SETTING_INIT_ZERO               {false, 0, false, 0, false, 0, false, 0, false, 0, false, 0, false, 0, false, 0, {{NULL}, NULL}, false, 0, {{NULL}, NULL}, false, 0, false, 0, {{NULL}, NULL}, false, 0, {{NULL}, NULL}, false, 0, false, 0, false, 0, false, 0, {{NULL}, NULL}, false, 0, false, 0, false, 0}
#define PROTOCOL_SETTING_RSP_INIT_ZERO           {PROTOCOL_IDENTITY_MSG_INIT_ZERO, false, PROTOCOL_SETTING_INIT_ZERO}

/* Field tags (for use in manual encoding/decoding) */
#define PROTOCOL_SETTING_ENVSAMPLEMODE_TAG       1
#define PROTOCOL_SETTING_ENVSAMPLEINTERVAL_TAG   2
#define PROTOCOL_SETTING_BEHAVIORSAMPLEMODE_TAG  4
#define PROTOCOL_SETTING_BEHAVIORSAMPLEINTERVAL_TAG 5
#define PROTOCOL_SETTING_GPSSAMPLEMODE_TAG       7
#define PROTOCOL_SETTING_GPSSAMPLEINTERVAL_TAG   8
#define PROTOCOL_SETTING_COMMUNICATIONMODE_TAG   10
#define PROTOCOL_SETTING_COMMUNICATIONINTERVAL_TAG 11
#define PROTOCOL_SETTING_COMMUNICATIONTIMETABLE_TAG 12
#define PROTOCOL_SETTING_RESETDEVICE_TAG         16
#define PROTOCOL_SETTING_POWEROFFTIME_TAG        17
#define PROTOCOL_SETTING_POWEROFFMODE_TAG        18
#define PROTOCOL_SETTING_OTAFIRMWAREVERSION_TAG  22
#define PROTOCOL_SETTING_OTAFIRMWAREID_TAG       23
#define PROTOCOL_SETTING_OTAFORCEUPGRADE_TAG     24
#define PROTOCOL_SETTING_OTASERVERHOST_TAG       25
#define PROTOCOL_SETTING_OTASERVERPORT_TAG       26
#define PROTOCOL_SETTING_SMSMODE_TAG             27
#define PROTOCOL_SETTING_SMSINTERVAL_TAG         28
#define PROTOCOL_SETTING_ALARMMODE_TAG           29
#define PROTOCOL_SETTING_ORIGINTIME_TAG          33
#define PROTOCOL_SETTING_ORIGINMODE_TAG          34
#define PROTOCOL_SETTING_ESTRUSSAMPLEMODE_TAG    35
#define PROTOCOL_SETTING_ESTRUSSAMPLEINTERVAL_TAG 36
#define PROTOCOL_SETTING_REQ_IDEN_TAG            1
#define PROTOCOL_TIME_RANGE_BEGIN_TAG            1
#define PROTOCOL_TIME_RANGE_END_TAG              2
#define PROTOCOL_SETTING_RSP_IDEN_TAG            1
#define PROTOCOL_SETTING_RSP_SETTINGINFO_TAG     2

/* Struct field encoding specification for nanopb */
extern const pb_field_t protocol_setting_req_fields[2];
extern const pb_field_t protocol_time_range_fields[3];
extern const pb_field_t protocol_setting_fields[25];
extern const pb_field_t protocol_setting_rsp_fields[3];

/* Maximum encoded size of messages (where known) */
#define PROTOCOL_SETTING_REQ_SIZE                (6 + PROTOCOL_IDENTITY_MSG_SIZE)
#define PROTOCOL_TIME_RANGE_SIZE                 12
/* PROTOCOL_SETTING_SIZE depends on runtime parameters */
#define PROTOCOL_SETTING_RSP_SIZE                (12 + PROTOCOL_IDENTITY_MSG_SIZE + PROTOCOL_SETTING_SIZE)

/* Message IDs (where set with "msgid" option) */
#ifdef PB_MSGID

#define SETTING_MESSAGES \


#endif

#ifdef __cplusplus
} /* extern "C" */
#endif
/* @@protoc_insertion_point(eof) */

#endif
