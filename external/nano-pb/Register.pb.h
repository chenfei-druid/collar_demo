/* Automatically generated nanopb header */
/* Generated by nanopb-0.3.6-dev at Fri May 25 16:43:01 2018. */

#ifndef PB_REGISTER_PB_H_INCLUDED
#define PB_REGISTER_PB_H_INCLUDED
#include <pb.h>

#include "IdentityMsg.pb.h"

/* @@protoc_insertion_point(includes) */
#if PB_PROTO_HEADER_VERSION != 30
#error Regenerate this file with the current version of nanopb generator.
#endif

#ifdef __cplusplus
extern "C" {
#endif

/* Struct definitions */
typedef struct {
    protocol_identity_msg_t Iden;
    pb_callback_t IMSI;
    pb_callback_t IMEI;
    pb_callback_t MAC;
    bool has_DeviceType;
    int32_t DeviceType;
    bool has_HardwareVersion;
    int32_t HardwareVersion;
    bool has_FirmwareVersion;
    int32_t FirmwareVersion;
    bool has_Status;
    int32_t Status;
    bool has_BatteryVoltage;
    int32_t BatteryVoltage;
    bool has_BatteryPower;
    int32_t BatteryPower;
    bool has_SignalStrength;
    int32_t SignalStrength;
    bool has_BitErrorRate;
    int32_t BitErrorRate;
    bool has_RadioAccessTechnology;
    int32_t RadioAccessTechnology;
    bool has_NetworkOperator;
    uint32_t NetworkOperator;
/* @@protoc_insertion_point(struct:protocol_register_req_t) */
} protocol_register_req_t;

typedef struct {
    protocol_identity_msg_t Iden;
    uint32_t Timestamp;
    bool has_SimType;
    int32_t SimType;
/* @@protoc_insertion_point(struct:protocol_register_rsp_t) */
} protocol_register_rsp_t;

/* Default values for struct fields */
extern const int32_t protocol_register_req_device_type_default;
extern const int32_t protocol_register_req_hardware_version_default;
extern const int32_t protocol_register_req_firmware_version_default;
extern const int32_t protocol_register_req_status_default;
extern const int32_t protocol_register_req_battery_voltage_default;
extern const int32_t protocol_register_req_battery_power_default;
extern const int32_t protocol_register_req_signal_strength_default;
extern const int32_t protocol_register_req_bit_error_rate_default;
extern const int32_t protocol_register_req_radio_access_technology_default;
extern const uint32_t protocol_register_req_network_operator_default;
extern const int32_t protocol_register_rsp_sim_type_default;

/* Initializer values for message structs */
#define PROTOCOL_REGISTER_REQ_INIT_DEFAULT       {PROTOCOL_IDENTITY_MSG_INIT_DEFAULT, {{NULL}, NULL}, {{NULL}, NULL}, {{NULL}, NULL}, false, 0, false, 0, false, 0, false, 0, false, 0, false, 0, false, 0, false, 0, false, 0, false, 0u}
#define PROTOCOL_REGISTER_RSP_INIT_DEFAULT       {PROTOCOL_IDENTITY_MSG_INIT_DEFAULT, 0, false, 0}
#define PROTOCOL_REGISTER_REQ_INIT_ZERO          {PROTOCOL_IDENTITY_MSG_INIT_ZERO, {{NULL}, NULL}, {{NULL}, NULL}, {{NULL}, NULL}, false, 0, false, 0, false, 0, false, 0, false, 0, false, 0, false, 0, false, 0, false, 0, false, 0}
#define PROTOCOL_REGISTER_RSP_INIT_ZERO          {PROTOCOL_IDENTITY_MSG_INIT_ZERO, 0, false, 0}

/* Field tags (for use in manual encoding/decoding) */
#define PROTOCOL_REGISTER_REQ_IDEN_TAG           1
#define PROTOCOL_REGISTER_REQ_IMSI_TAG           2
#define PROTOCOL_REGISTER_REQ_IMEI_TAG           3
#define PROTOCOL_REGISTER_REQ_MAC_TAG            4
#define PROTOCOL_REGISTER_REQ_DEVICETYPE_TAG     5
#define PROTOCOL_REGISTER_REQ_HARDWAREVERSION_TAG 6
#define PROTOCOL_REGISTER_REQ_FIRMWAREVERSION_TAG 7
#define PROTOCOL_REGISTER_REQ_STATUS_TAG         8
#define PROTOCOL_REGISTER_REQ_BATTERYVOLTAGE_TAG 9
#define PROTOCOL_REGISTER_REQ_BATTERYPOWER_TAG   10
#define PROTOCOL_REGISTER_REQ_SIGNALSTRENGTH_TAG 11
#define PROTOCOL_REGISTER_REQ_BITERRORRATE_TAG   12
#define PROTOCOL_REGISTER_REQ_RADIOACCESSTECHNOLOGY_TAG 13
#define PROTOCOL_REGISTER_REQ_NETWORKOPERATOR_TAG 14
#define PROTOCOL_REGISTER_RSP_IDEN_TAG           1
#define PROTOCOL_REGISTER_RSP_TIMESTAMP_TAG      2
#define PROTOCOL_REGISTER_RSP_SIMTYPE_TAG        3

/* Struct field encoding specification for nanopb */
extern const pb_field_t protocol_register_req_fields[15];
extern const pb_field_t protocol_register_rsp_fields[4];

/* Maximum encoded size of messages (where known) */
/* PROTOCOL_REGISTER_REQ_SIZE depends on runtime parameters */
#define PROTOCOL_REGISTER_RSP_SIZE               (23 + PROTOCOL_IDENTITY_MSG_SIZE)

/* Message IDs (where set with "msgid" option) */
#ifdef PB_MSGID

#define REGISTER_MESSAGES \


#endif

#ifdef __cplusplus
} /* extern "C" */
#endif
/* @@protoc_insertion_point(eof) */

#endif
