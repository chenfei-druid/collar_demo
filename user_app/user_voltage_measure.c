
#include "app_voltage_measure.h"

#define     SAMPLES_OF_BUFFER   1

adc_value_t adc_value;

uint16_t ble_send_vol_data(ble_volmeass_t * p_volmeass, uint8_t *p_Volbuf, uint8_t count)
{
    ble_gatts_hvx_params_t params;
	uint8_t i;
    uint16_t len;
	uint32_t err_code;
	uint8_t  volValue[20];
	
	  
	volValue[0] = count;
    for(i=0;i<count*2;i++)
    {    
        volValue[i] = *(p_Volbuf+i);
    }
	len = count*2;

	if (p_volmeass->conn_handle != BLE_CONN_HANDLE_INVALID)
    {
        memset(&params, 0, sizeof(params));
        params.type = BLE_GATT_HVX_NOTIFICATION;
        params.handle = p_volmeass->mpu_char_handles.value_handle;
        params.p_data = volValue;
        params.p_len = &len;
    
        return sd_ble_gatts_hvx(p_volmeass->conn_handle, &params);
	}
	else
    {
		err_code = NRF_ERROR_INVALID_STATE;
	}
    return err_code;  
}

void saadc_callback(nrf_drv_saadc_evt_t const * p_event)
{
    //uint16_t vol_value;
    if (p_event->type == NRF_DRV_SAADC_EVT_DONE)
    {
        ret_code_t err_code;
     
        err_code = nrf_drv_saadc_buffer_convert(p_event->data.done.p_buffer, SAMPLES_IN_BUFFER);
        APP_ERROR_CHECK(err_code);
			  
        adc_value.buf[adc_value.cnt*2]   = (uint8_t)(p_event->data.done.p_buffer[0] >> 8);
		adc_value.buf[adc_value.cnt*2+1] = (uint8_t)(p_event->data.done.p_buffer[0]);
		adc_value.cnt++;
        vol_value=p_event->data.done.p_buffer[0]*3600/1024;   //mV
        vol_value /=10;     //100V
        ble_send_vol_data(&m_volmeass, &vol_value,adc_value.cnt);
		adc_value.cnt = 0;
    }
}
void saadc_init(void)
{
    ret_code_t err_code;
    nrf_saadc_channel_config_t channel_config =
            NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_AIN2);
    err_code = nrf_drv_saadc_init(NULL, saadc_callback);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_saadc_channel_init(0, &channel_config);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_saadc_buffer_convert(m_buffer_pool[0],SAMPLES_IN_BUFFER);
    APP_ERROR_CHECK(err_code);
    
    err_code = nrf_drv_saadc_buffer_convert(m_buffer_pool[1],SAMPLES_IN_BUFFER);
    APP_ERROR_CHECK(err_code);
	
	  adc_value.cnt = 0;
}

