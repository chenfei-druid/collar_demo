/** 
 * hal_acc_origin.c
 *
 * @group:       collar project
 * @author:      Chenfei
 * @create time: 2018/4/20
 * @version:     V0.0
 *
 */
 #include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdint.h>
#include "freertos_platform.h"
#include "drv_acc_KX022.h"
#include "Record.h"
#include "user_app_origin.h"
#include "user_app_log.h"
//#include "hal_rtc.h"
#include "alarm_clock.h"
#include "Define.pb.h"
#include "pb_encode.h"
#include "pb_decode.h"
#include "user_data_pkg.h"
#include "user_send_data.h"
#include "user_app_identity.h"
#include "SimpleRsp.pb.h"
#include "alarm_clock.h"
#include "SEGGER_RTT.h"
#include "user_pb_callback.h"
#include "ble_application.h"

//static      TaskHandle_t                acc_origin_handle;
//static      xSemaphoreHandle            binary_semaphore_origin = NULL;
static      xSemaphoreHandle            binary_semaphore_wait_response = NULL;
//static      QueueHandle_t               queue_acc_origin;
            
//static uint8_t hal_acc_buf[HAL_ACC_POINT_SAMPLES*DRV_ACC_kx022_ONE_XYZ_DATA_LEN] = {0};
                    //30s datas 
//static  acc_origin_point_t              m_acc_point_t[ACC_ORIGIN_SAMPLE_CACHE_POINT];     //25Hz, 10S, max size is FQ*S
//static  uint32_t                        acc_point_cnt = 0;
//static  int32_t                         msg_index = 0;
//static  bool                            rsp_flag = 0;   
static  acc_sample_time_range_t   m_time_range_t = {
                                                        //.time = {0},
                                                        .handle = {0},
                                                        .count = 0,
                                                    };
static  acc_sample_time_flag      user_time_type[] = {
                                                        ACC_SAMPLE_START_TYPE,
                                                        ACC_SAMPLE_END_TYPE 
                                                     }; 
static  acc_origin_sample_status    g_sample_status ={.start_sample = 0,
                                                      .stop_sample = 0,  
                                                      };
/***********************************************defined functions**********************************************/
/**
* @functionname : user_acc_alarm_handle 
* @description  : when time out of alarm begin or end clock, it will call the handler, user will start sample or stop sample at here
* @input        : none
* @output       : none 
* @return       : none
*/                                                      
void user_acc_alarm_handle(uint32_t time_stamp, void * arg)
{
    uint8_t *p = arg;
    //alarm_app_type_s time_flag = *p;
    uint8_t time_flag = *p;
    if(time_flag >= ACC_SAMPLE_MAX_TYPE)
        return;
    if(time_flag == ALARM_TYPE_ACC_SAMPLE_START)
    {
        //start sample      
        g_sample_status.start_sample = 1;
        g_sample_status.stop_sample  = 0;
        //hal_acc_start_collect();
        user_acc_set();
        DBG_LOG("start sample origin data, timestamp = %d\r\n",hal_rtc2_get_unix_time());
    }
    else if(time_flag == ALARM_TYPE_ACC_SAMPLE_END)
    {
        //end sample
        //DBG_LOG("time to stop sample acc data, timestamp = %d\r\n",hal_rtc2_get_unix_time());
        g_sample_status.start_sample = 0;
        g_sample_status.stop_sample  = 1;
        //hal_acc_stop_collect();
        user_acc_set();
        DBG_LOG("stop sample origin data, timestamp = %d\r\n",hal_rtc2_get_unix_time());
    }
}
/**
* @functionname : user_acc_disable_sample 
* @description  : function for disable all origin accelerater data sample beging time and end time, 
*                 must disable all sample alarm clock before every setting alarm
* @input        : none
* @output       : none 
* @return       : disable status
*/
static bool user_acc_disable_sample_alarm(void)
{
    //distroy the alarm has exist
    DBG_LOG("\r\n***************disable all origin sample alarm clock****************\r\n");
    if(m_time_range_t.count > 0)
    {
        if(m_time_range_t.count > ACC_TIME_RANGE_PAIR_NUM_MAX)
        {
            DBG_LOG("\r\nthe count > max , set count to max\r\n");
            m_time_range_t.count = ACC_TIME_RANGE_PAIR_NUM_MAX;
        }
        while(m_time_range_t.count)
        {
            if(alarm_clock_distroy(m_time_range_t.handle[m_time_range_t.count--].start) == -1)
            {
                DBG_LOG("\r\ndisable origin start alarmclock number %d is failed\r\n",m_time_range_t.handle[m_time_range_t.count].start);
                return false;
            }
            if(alarm_clock_distroy(m_time_range_t.handle[m_time_range_t.count--].stop) == -1)
            {
                DBG_LOG("\r\ndisable origin stop alarmclock number %d is failed\r\n",m_time_range_t.handle[m_time_range_t.count].stop);
                return false;
            }
        }
    }
    m_time_range_t.count = 0;
    memset(m_time_range_t.handle,0,sizeof(m_time_range_t.handle));
    //memset(m_time_range_t.time,0,sizeof(m_time_range_t.time));
    return true;
}
/**
* @functionname : user_set_acc_sample_alarm 
* @description  : function for setting the origin accelerater data sample beging time and end time 
*               
* @input        : p_time_range_t: pointer to repeated time struct @ref user_time_range_t
*               : alarm_mode: @ref alarm_clock_run_type_t
*               : time_pair_num: the sample time-pair(begin and end time) numbers
* @output       : none 
* @return       : time pair numbers
*/
static int32_t user_set_acc_sample_alarm(user_time_range_t      const * p_time_range_t, 
                               alarm_clock_run_type_t const   alarm_mode, 
                               uint8_t                        time_pair_num
                              )
{
    if(p_time_range_t == NULL)
    {
        DBG_LOG("\r\nset origin alarm clock, pointer is null\r\n");
        return 0;
    }
    if(alarm_mode >= ALARM_TYPE_RUN_EVERY_MAX)
    {
        DBG_LOG("\r\nset origin alarm clock, alarm_mode invalid = %d\r\n",alarm_mode);
        return 0;
    }
    if(time_pair_num > ACC_TIME_RANGE_PAIR_NUM_MAX)
    {
        DBG_LOG("\r\nset origin alarm clock, time pair number is invalid = %d\r\n",time_pair_num);
        return 0;
    }
    alarm_clock_config_t    acc_alarm_config;
    
    //must desable
    if(user_acc_disable_sample_alarm() != true)
    {
        DBG_LOG("\r\nset origin alarm clock, disable all alarm failed\r\n");
        return 0;
    }
    
    if(time_pair_num > ACC_TIME_RANGE_PAIR_NUM_MAX)
    {
        DBG_LOG("\r\nset origin alarm clock, give alarm pair is exceed max\r\n");
        time_pair_num = ACC_TIME_RANGE_PAIR_NUM_MAX;
    }
    acc_alarm_config.callback   = user_acc_alarm_handle;
    acc_alarm_config.mode       = alarm_mode; 
    m_time_range_t.count = 0;
    //set new alarm  
    
    for(uint8_t i=0; i<time_pair_num; i++)
    {
        //set start alarm
        acc_alarm_config.time_stamp = p_time_range_t[i].Begin;     
        acc_alarm_config.param      = &user_time_type[ACC_SAMPLE_START_TYPE];        
        m_time_range_t.handle[m_time_range_t.count].start = alarm_clock_create_alarm(&acc_alarm_config);        //create alarm begin time
        alarm_clock_enable(m_time_range_t.handle[m_time_range_t.count].start);
        
        DBG_LOG("\r\nset origin alarm clock, number %d begin alarm time is %d\r\n",i,p_time_range_t[i].Begin);
        //set stop alarm
        acc_alarm_config.time_stamp = p_time_range_t[i].End;     
        acc_alarm_config.param      = &user_time_type[ACC_SAMPLE_END_TYPE];        
        m_time_range_t.handle[m_time_range_t.count].stop = alarm_clock_create_alarm(&acc_alarm_config);        //create alarm begin time
        alarm_clock_enable(m_time_range_t.handle[m_time_range_t.count].stop);
        m_time_range_t.count++;
        DBG_LOG("\r\nset origin alarm clock, number %d end alarm time is %d\r\n",i,p_time_range_t[i].End);
    }
    return time_pair_num;
}
#if 0
static int32_t user_set_alarm_test(user_time_range_t      const * p_time_range_t, 
                                    alarm_clock_run_type_t const   alarm_mode, 
                                    uint8_t                        time_pair_num
                              )
{
    alarm_clock_config_t    acc_alarm_config;
    uint32_t handle;
    
    
    acc_alarm_config.callback   = user_acc_alarm_handle;
    acc_alarm_config.mode       = ALARM_TYPE_RUN_EVERY_MINUTE;    
    acc_alarm_config.time_stamp = 20;
    acc_alarm_config.param      = &user_time_type[ACC_SAMPLE_START_TYPE];
    handle = alarm_clock_create_alarm(&acc_alarm_config);
    alarm_clock_enable(handle);
    
    acc_alarm_config.callback   = user_acc_alarm_handle;
    acc_alarm_config.mode       = ALARM_TYPE_RUN_EVERY_MINUTE;    
    acc_alarm_config.time_stamp = 40;
    acc_alarm_config.param      = &user_time_type[ACC_SAMPLE_END_TYPE];
    handle = alarm_clock_create_alarm(&acc_alarm_config);
    alarm_clock_enable(handle);
    
    DBG_LOG("\r\nset alarm the time is %d\r\n",hal_rtc2_get_unix_time());
    return 1;
}
#endif
/**
* @functionname : user_acc_set_sameple_time 
* @description  : function for setting the origin accelerater data sample beging time and end time , and mode
*               
* @input        : p_time_range_t: pointer to repeated time struct @ref user_time_range_t
*               : mode: sample time mode, @ref acc_sample_mode
*               : time_pair_num: the sample time-pair(begin and end time) numbers
* @output       : none 
* @return       : time pair numbers
*/
//uint32_t user_acc_set_sameple_time(user_time_range_t const * p_time_range_t, 
//                                   uint32_t const mode, 
 //                                  uint8_t const time_pair_num
//                                  )
uint32_t user_origin_setting(accorigin_setting_t setting)
{
    alarm_clock_run_type_t  alarm_type;
//    uint8_t time_num;

    switch(setting.mode)
    {
        case ACC_SAMPLE_MODE_DISABLE:
            DBG_LOG("\r\norigin set,  mode= ACC_SAMPLE_MODE_DISABLE\r\n\0");
            user_acc_disable_sample_alarm();
            break;
        
        case ACC_SAMPLE_MODE_BASE_ON_UNIXTIME:   //unix timestamp
            DBG_LOG("\r\norigin set,  mode= ACC_SAMPLE_MODE_BASE_ON_UNIXTIME\r\n\0");
            //user_set_acc_sample_alarm(p_time_range_t,ALARM_TYPE_RUN_SIGLE,time_pair_num);
            alarm_type = ALARM_TYPE_RUN_SIGLE;
            break;
        case ACC_SAMPLE_MODE_BASE_ON_MINUTE:
            alarm_type = ALARM_TYPE_RUN_EVERY_MINUTE;
            break;
        case ACC_SAMPLE_MODE_BASE_ON_HOUR:       //the sencond counter base on hour
            DBG_LOG("\r\norigin set,  mode= ACC_SAMPLE_MODE_BASE_ON_HOUR\r\n\0");
            //user_set_acc_sample_alarm(p_time_range_t,ALARM_TYPE_RUN_EVERY_HOUR,time_pair_num);
            alarm_type = ALARM_TYPE_RUN_EVERY_HOUR;
            break;
        case ACC_SAMPLE_MODE_BASE_ON_DAY:       //the sencond counter base on day
            //user_set_acc_sample_alarm(p_time_range_t,ALARM_TYPE_RUN_EVERY_DAY,time_pair_num);
            alarm_type = ALARM_TYPE_RUN_EVERY_DAY;
            break;
        case ACC_SAMPLE_MODE_BASE_ON_MONTH:      //the sencond counter base on month
            DBG_LOG("\r\norigin set,  mode= ACC_SAMPLE_MODE_BASE_ON_MONTH\r\n\0");
            //user_set_acc_sample_alarm(p_time_range_t,ALARM_TYPE_RUN_EVERY_MONTH,time_pair_num);
            alarm_type = ALARM_TYPE_RUN_EVERY_MONTH;
            break;
        default:
            DBG_LOG("\r\norigin set,  mode= invalid\r\n");
            alarm_type = ALARM_TYPE_RUN_EVERY_MAX;
            break;
    }
    return user_set_acc_sample_alarm(setting.range,alarm_type,setting.pair_num);
    //return user_set_alarm_test(setting.range,alarm_type,setting.pair_num);
}
bool user_origin_get_sample_mode(void)
{  
    return g_sample_status.start_sample;
}
/**
* @functionname : app_encode_repeated_var_struct 
* @description  : function for encode the origin records, the record number is filled at head position
* @input        : stream, encode stream,  
*               : fields: the origin fields
*               : src_struct: the origin struct will be encoded
* @output       : none 
* @return       : true if successed
*/
static bool app_encode_repeated_var_struct(pb_ostream_t *stream, const pb_field_t fields[],  void * const *src_struct)
{
    
    bool res;
    acc_origin_encode_t *m_org_head_t = (acc_origin_encode_t *)*src_struct;
    protocol_XYZ_t    *xyz_t        = m_org_head_t->xyz_t;
    //res = pb_encode_tag_for_field(stream, fields);
    //DBG_LOG("encode origin record tag is %s", (res? "successed":"failed"));
    //PRINT_S("encode origin record tag over\r\n\0");
    DBG_LOG("encode origin total point number = %d\r\n\0",m_org_head_t->count);
    for(uint8_t i=0; i<m_org_head_t->count; i++)
    {
        if (!pb_encode_tag_for_field(stream, fields))
        {
            DBG_LOG("encode origin record TAG %d failed\r\n\0",i);
            return false;
        }
        DBG_LOG("encode origin record TAG %d successed\r\n\0",i);
        res = pb_encode_submessage(stream, protocol_XYZ_fields, &xyz_t[i]);
        //DBG_LOG("encode origin record %d is %s", i,(res? "successed":"failed"));
        if(res == false)
        {
            DBG_LOG("encode origin record struct %d failed\r\n\0",i);
            break;
        }       
        DBG_LOG("encode origin record struct %d successec\r\n\0",i);
    }
    //PRINT_S("encode origin record struct over\r\n\0");
    return res;
}
/**
* @functionname : app_encode_repeated_var_string 
* @description  : function for encode a strings data
* @input        : stream, encode stream,  
*               : fields: the origin fields
*               : str: the string data will be encoded
* @output       : none 
* @return       : true if successed
*/
static bool app_encode_repeated_var_string(pb_ostream_t *stream, const pb_field_t *fields,  void * const *str)
{
    bool res;
    res = pb_encode_tag_for_field(stream, fields);
    //DBG_LOG("pb encode tag for string is  %s", (res? "successed":"failed"));
    res = pb_encode_string(stream,*str,strlen(*str));
    DBG_LOG("encode origin string \"%s\" is %s\r\n", *str, (res? "successed":"failed"));
    //PRINT_S("pb encode string over\r\n\0");
    return res;
}
/**
* @functionname : app_decode_repeated_var_string 
* @description  : function for decode a strings data
* @input        : stream, encode stream,  
*               : fields: the origin fields
*               : str: the string data will be encoded
* @output       : none 
* @return       : true if successed
*/
/*
static bool app_decode_repeated_var_string(pb_istream_t *stream, const pb_field_t *field, void **str)
{
//    uint32_t tag;
//    uint32_t *p = *str;
    //pb_wire_type_t wire_type;
    pb_byte_t   cache_buf[stream->bytes_left];
    uint8_t     byte_len = stream->bytes_left;
    //bool eof;
    bool res = false;
    res = pb_read(stream,cache_buf, stream->bytes_left);
    DBG_LOG("\r\ndecode origin data \"%s\"\r\n",cache_buf);
    if(res == true)
    {
        memcpy(*str, cache_buf, byte_len);
        return true;
    }
    else
    {
        *str = NULL;
        return false;
    }
}
*/
/**
* @functionname : app_decode_repeated_var_struct 
* @description  : function for decode a register struct data
* @input        : stream, encode stream,  
*               : fields: the register fields
*               : src_struct: the struct data will be decoded
* @output       : none 
* @return       : true if successed
*/
/*
static int decode_struct_cnt = 0;
static bool app_origin_decode_repeated_var_struct(pb_istream_t *stream, const pb_field_t *field, void **src_struct)
{
    //uint8_t *p = *src_struct;
    //uint32_t tag;
    //pb_wire_type_t wire_type;
    //bool eof;
    protocol_XYZ_t *xyz_t = (protocol_XYZ_t *)(*src_struct);
    bool res = false;
    ////DBG_LOG("decoding struct..., the bytes left is %d\r\n", stream->bytes_left);
    //res = pb_decode_tag(stream, &wire_type, &tag, &eof);
    ////DBG_LOG("pb decode tag is %s\r\n", (res? "successed":"failed"));
    res = pb_decode(stream,protocol_XYZ_fields,&xyz_t[decode_struct_cnt]);
    //DBG_LOG("pb decode struct is %s\r\n", (res? "successed":"failed"));
    DBG_LOG("decode struct count %d\r\n", decode_struct_cnt++); 
    return res;
}
*/
#define test_decode 0
#if test_decode

void user_data_pb_decode_test(uint8_t * const buffer, uint32_t const len)
{
    protocol_origin_req_t   decode_org_req_t;
    protocol_XYZ_t          xyz_t[ACC_ORIGIN_ONCE_UPLOAD_POINT];
    pb_istream_t i_stream ;
    pb_ostream_t            m_stream ;
    acc_origin_encode_t     org_data_t;
    
    bool res;
    uint8_t id[20];
    uint8_t imei[20];
    uint8_t imsi[20];
    uint8_t mac[20];
    
    decode_org_req_t.Iden.DeviceID.funcs.decode  = &app_decode_repeated_var_string;
    decode_org_req_t.Iden.DeviceID.arg           = id;

    decode_org_req_t.XYZInfo.funcs.decode        = & app_origin_decode_repeated_var_struct;
    decode_org_req_t.XYZInfo.arg                 = xyz_t;
    decode_struct_cnt = 0;
    i_stream = pb_istream_from_buffer(buffer,len);
    //DBG_LOG("given buffer size= %d\r\n",  i_stream.bytes_left);
    res = pb_decode(&i_stream,protocol_origin_req_fields,&decode_org_req_t);   
    //DBG_LOG("decode is %s\r\n", (res? "successed":"failed"));  
    DBG_LOG("decode is %s\r\n", (res? "successed":"failed"));    
}

bool decode_test(uint8_t * const p_buf, uint32_t const len)
{
    protocol_simple_rsp_t test_rsp_t;
    uint8_t id[20] = {0};
    bool res;
    pb_istream_t i_stream ;
    DBG_LOG("\r\n-----test decoding......\r\n");    
    for(uint16_t i=0; i< len ;i++)
    {
        DBG_LOG("0x%-2x ", p_buf[i]);
    }
    test_rsp_t.Iden.DeviceID.funcs.decode   = &app_decode_repeated_var_string;
    test_rsp_t.Iden.DeviceID.arg            = id;
    
    i_stream = pb_istream_from_buffer(p_buf,len);
    res = pb_decode(&i_stream,protocol_simple_rsp_fields,&test_rsp_t);
    if(res == true)
    {
        DBG_LOG("\r\ntest --- origin response decode successed\r\n");
        DBG_LOG("decode is successed");
        return true;
    }
    else
    {
        DBG_LOG("origin response decode failed\r\n");
        DBG_LOG("decode is failed");
        return false;
    }
}
#endif
/**
* @functionname : app_encode_origin_data 
* @description  : function for encode data
* @input        : stream, encode stream,  
*               : fields: the origin fields
*               : str: the string data will be encoded
* @output       : none 
* @return       : true if successed
*/
static uint32_t user_origin_encode_data(uint8_t             *const encode_out_buffer,
                                        uint32_t            const buffer_size,
                                        acc_origin_record_t const *const p_record_buf, 
                                        uint32_t            const record_size, 
                                        user_identity_t     const iden
                                       )
{
    if(record_size > ACC_ORIGIN_ONCE_SEND_RECORD)
    {
        //DBG_LOG("given record size %d to encode is too long", record_size);
        DBG_LOG("given record size to encode is too long\r\n\0");
        return 0;
    }
    if(p_record_buf == NULL || encode_out_buffer == NULL)
    {
        //DBG_LOG("buffer pointor is NULL");
        DBG_LOG("buffer pointor is NULL\r\n\0");
        return 0;
    }
    
    int32_t point_num = record_size*ACC_ORIGIN_ONE_RECORD_HAS_POINTS;
      
    protocol_origin_req_t   p_org_req_t;
    pb_ostream_t            m_stream ;
    acc_origin_encode_t     org_data_t;
    protocol_XYZ_t          xyz_t[ACC_ORIGIN_ONCE_SEND_RECORD*ACC_ORIGIN_ONE_RECORD_HAS_POINTS];
    //char p_dev_mac[13] ;
    
    org_data_t.count    =   point_num;        //the head of record item size
    org_data_t.xyz_t    =   xyz_t;              //pointor to record buffer
    
    memset(xyz_t, 0, sizeof(xyz_t));
    memset(&p_org_req_t, 0, sizeof(p_org_req_t));
    
    //get buffer records
    for(uint32_t i=0;i<point_num;i++)
    {
        xyz_t[i].has_X   = true;
        xyz_t[i].has_Y   = true;
        xyz_t[i].has_Z   = true;
        
        xyz_t[i].X       = p_record_buf->p_dt[i].x;
        xyz_t[i].Y       = p_record_buf->p_dt[i].y;
        xyz_t[i].Z       = p_record_buf->p_dt[i].z;
    }
    
    p_org_req_t.Iden = iden.head;
    p_org_req_t.Iden.DeviceID.funcs.encode = &app_encode_repeated_var_string;
    p_org_req_t.Iden.DeviceID.arg          = (char *)iden.dev_id;
    
    p_org_req_t.XYZInfo.funcs.encode       = &app_encode_repeated_var_struct;
    p_org_req_t.XYZInfo.arg                = &org_data_t ; 
    
    m_stream = pb_ostream_from_buffer(encode_out_buffer,buffer_size);
    bool res = pb_encode(&m_stream,protocol_origin_req_fields,&p_org_req_t);
    
    if(res)
    {
        DBG_LOG("encode origin is successed\r\n\0");
        #if test_decode
        user_data_pb_decode_test(out_origin_encode_buffer,m_stream.bytes_written);
        #endif
        return m_stream.bytes_written;
    }
    DBG_LOG("encode origin is failed\r\n\0");
    return 0;
}
/**
* @functionname : app_register_rsp_decode 
* @description  : function for decode register response message
* @input        : p_buf: input by-decode buffer
*               : len: input by-decode data length
* @output       : rgst_rsp_t, decode out struct,  
*               : dev_id: device id buffer 
* @return       : true if successed
*/
//static protocol_simple_rsp_t test_rsp_t;
//static uint8_t id[20] = {0};
/*
static bool app_origin_rsp_decode(protocol_simple_rsp_t * const p_rsp_t, 
                             uint8_t * const dev_id, 
                             uint8_t * const p_buf, 
                             uint32_t const len)
{
    bool res;
    pb_istream_t i_stream ;
        
    p_rsp_t->Iden.DeviceID.funcs.decode   = &app_decode_repeated_var_string;
    p_rsp_t->Iden.DeviceID.arg            = dev_id;
   
    i_stream = pb_istream_from_buffer(p_buf,len);
    res = pb_decode(&i_stream,protocol_simple_rsp_fields,p_rsp_t);
    if(res == true)
    {
        DBG_LOG("origin response decode successed\r\n");
        DBG_LOG("decode is successed");
        return true;
    }
    else
    {
        DBG_LOG("origin response decode failed\r\n");
        DBG_LOG("decode is failed");
        return false;
    }
}
*/
/**
* @function@name:    app_read_origin_record
* @description  :    function for read one record of origin acc data
* @input        :    p_org_dt: pointor to a origin data include x,y,z-axis
* @output       :    none
* @return       :    true if success
*/
/*
static int32_t app_read_origin_record(acc_origin_record_t  * const p_org_dt)
{    
    if(p_org_dt == NULL)
    {
        return 0;
    }
    return Record_Read(RECORD_TYPE_ACC_ORIGIN, (uint8_t *)p_org_dt, sizeof(acc_origin_record_t));
}
*/
/**
* @function@name:    app_read_origin_records
* @description  :    function for read more records of origin acc data
* @input        :    record_num: the record numbers
* @output       :    p_org_dt: pointor to a origin data buf struct,  include x,y,z-axis
* @return       :    true if success
*/
#if 1
static int32_t app_read_origin_records(acc_origin_record_t  *p_org_dt, int32_t record_num)
{
    if(p_org_dt == NULL)
    {
        return 0;
    }
    record_num = Record_Read(RECORD_TYPE_ACC_ORIGIN, (uint8_t *)p_org_dt, record_num*sizeof(acc_origin_record_t));
    
    uint32_t point_num = record_num * ACC_ORIGIN_ONE_RECORD_HAS_POINTS;
    DBG_LOG("\r\n\r\nread origin record number = %d, point = %d\r\n",record_num,point_num);
    for(uint32_t i=0;i<point_num;i++)
    {
        DBG_LOG("data[%d].x = %d, data[%d].y = %d,data[%d].z = %d\r\n",\
                i,p_org_dt->p_dt[i].x,i,p_org_dt->p_dt[i].y,i,p_org_dt->p_dt[i].z);
    }
    return record_num;
}
#else
//for test
uint32_t app_read_origin_records(acc_origin_record_t  * const p_org_dt, uint32_t const record_num)
{
    if(p_org_dt == NULL)
    {
        return 0;
    }
    uint32_t point_num = record_num * ACC_ORIGIN_ONE_RECORD_HAS_POINTS;
    DBG_LOG("\r\n\r\nread origin record number = %d, point = %d\r\n",record_num,point_num);
    for(uint32_t i=0;i<point_num;i++)
    {
        p_org_dt->p_dt[i].x = 0x7eee ;
        p_org_dt->p_dt[i].y = 0x7eee  ;
        p_org_dt->p_dt[i].z = 0x7eee  ;
        DBG_LOG("data[%d].x = %d, data[%d].y = %d,data[%d].z = %d\r\n",\
                i,p_org_dt->p_dt[i].x,i,p_org_dt->p_dt[i].y,i,p_org_dt->p_dt[i].z);
    }
    DBG_LOG("\r\nread over\r\n");
    return record_num;
}
#endif
/**
* @function@name:    hal_acc_save_origin_data
* @description  :    function for save accelerate data into memory as record
* @input        :    p_org_dt: 
* @output       :    none
* @return       :    true if success
*/
//void hal_acc_save_origin_data(acc_origin_data_t *p_org_dt, uint32_t len)
static bool app_save_origin_record(acc_origin_record_t *p_org_dt)
{
    uint8_t *p_dt = (uint8_t *)p_org_dt;
    return Record_Write(RECORD_TYPE_ACC_ORIGIN,p_dt);
}

/**
* @function@name:    hal_acc_save_origin_records
* @description  :    function for save some origin accelerate datas into memory as record
* @input        :    p_org_dt: 
*               :   record_num:
* @output       :    none
* @return       :    true if success
*/
/*
static bool app_origin_save_records(acc_origin_record_t *p_org_dt, uint32_t record_num)
{
//    bool ret;
    if(p_org_dt == NULL)
    {
        DBG_LOG("origin record pointor is null\r\n");
        return false;
    }
    for(uint8_t i=0;i<record_num;i++)
    {
        if(app_save_origin_record(&p_org_dt[i]) == false)
        {
            DBG_LOG("origin datas save failed\r\n");
            return false;
        }
    }
    DBG_LOG("origin datas save successed\r\n");

    return true;
}
*/

/**
* @function@name:    hal_acc_data_complement_to_source
* @description  :    function for save convert the complement to source data
* @input        :    p_org_cmpt: 
*               :    p_org_sr:
*               ��   point_num: data-point numbers
* @output       :    none
* @return       :    true if success
*/
/*
static bool app_data_complement_to_source(acc_origin_point_t * const p_org_cmpt, 
                                                acc_origin_point_t * const p_org_sr, 
                                                uint32_t point_num
                                             )
{
    if(p_org_cmpt == NULL)
    {
        DBG_LOG("hal acc convert of source data is null");
        return 0;
    }
    if(point_num > HAL_ACC_POINT_SAMPLES)
    {
        DBG_LOG("hal acc convertion of point numbers is more than cache size");
        return 0;
    }
    for(uint32_t i=0; i< point_num ; i++)
    {
        if(p_org_sr[i].x & HAL_ACC_AXIS_SIGN_BIT )    //if is negative
        {
            p_org_cmpt[i].x =  ((~((p_org_sr[i].x&HAL_ACC_AXIS_VALUE_MASK) - 1))&HAL_ACC_AXIS_VALUE_MASK) | HAL_ACC_AXIS_SIGN_BIT;
            DBG_LOG("point[i].x = %d", p_org_cmpt[i].x);
        }
        if(p_org_sr[i].y & HAL_ACC_AXIS_SIGN_BIT )    //if is negative
        {
            p_org_cmpt[i].y =  ((~((p_org_sr[i].y&HAL_ACC_AXIS_VALUE_MASK) - 1))&HAL_ACC_AXIS_VALUE_MASK) | HAL_ACC_AXIS_SIGN_BIT;
            DBG_LOG("point[i].y = %d", p_org_cmpt[i].y);
        }
        if(p_org_sr[i].z & HAL_ACC_AXIS_SIGN_BIT )    //if is negative
        {
            p_org_cmpt[i].z =  ((~((p_org_sr[i].z&HAL_ACC_AXIS_VALUE_MASK) - 1))&HAL_ACC_AXIS_VALUE_MASK) | HAL_ACC_AXIS_SIGN_BIT;
            DBG_LOG("point[i].z = %d", p_org_cmpt[i].z);
        }        
    }
    DBG_LOG("hal acc convertion complete");
    return true;
}
*/
/**
* @function@name:    hal_acc_save_origin_datas
* @description  :    function for save origin acc datas, user collect data, then call this funtion
* @input        :    p_org_dt: pointer to origin acc data
*               ��   xyz_point_num: data-point numbers
* @output       :    none
* @return       :    true if success
*/
bool app_origin_save_handle(acc_origin_point_t * const p_org_dt, uint32_t const xyz_point_num,uint32_t time)
{
    //bool ret;
    static int32_t sample_cnt = 0;
    static acc_origin_record_t  origin_record_buf[ACC_ORIGIN_CACHE_RECORDS_MAX] ;  //5 RECORDS
    static int32_t record_cnt = 0;
    
    if(xyz_point_num > ACC_ORIGIN_ONCE_SAMPLE_POINTS)
    {
        DBG_LOG("origin points numbers more than max\r\n");
        return 0;
    }
    //DBG_LOG("sample_cnt = %d\r\n",sample_cnt);
    if(sample_cnt >= ACC_ORIGIN_ONE_RECORD_HAS_SECONDS)
    {
        DBG_LOG("sample_cnt= %d >= 2, erro~~~~~~~~~~~~~r\r\n",sample_cnt);
        sample_cnt = 0;
    }
    if(sample_cnt == 0)
    {
        //DBG_LOG("sample_cnt= 0, get timestamp\r\n");
        //origin_record_buf[record_cnt].timestamp = hal_rtc2_get_unix_time(); 
        origin_record_buf[record_cnt].timestamp = time; 
    }       
    for(uint32_t i=0;i<xyz_point_num;i++)
    {
        //DBG_LOG("cache the record number %d's xyz point %d\r\n",record_cnt,i);
        origin_record_buf[record_cnt].p_dt[i + sample_cnt*ACC_ORIGIN_SECOND_HAS_POINT].x = p_org_dt[i].x;
        origin_record_buf[record_cnt].p_dt[i + sample_cnt*ACC_ORIGIN_SECOND_HAS_POINT].y = p_org_dt[i].y;
        origin_record_buf[record_cnt].p_dt[i + sample_cnt*ACC_ORIGIN_SECOND_HAS_POINT].z = p_org_dt[i].z;
    }
    sample_cnt++;
    if(sample_cnt >= ACC_ORIGIN_ONE_RECORD_HAS_SECONDS)
    {
        //DBG_LOG("sample_cnt %d >=2, cache one record\r\n",sample_cnt);
        sample_cnt =0;    
        record_cnt++;
        //DBG_LOG("record_cnt = %d\r\n",record_cnt);
        if(record_cnt >= ACC_ORIGIN_CACHE_RECORDS_MAX)
        {
            //DBG_LOG("save all cached record\r\n",record_cnt);
            for(uint8_t i=0;i<record_cnt;i++)
            {
                if(app_save_origin_record(&origin_record_buf[i]) == false)
                {
                    DBG_LOG("origin datas save failed\r\n");
                    return false;
                }   
                DBG_LOG("origin datas save successed\r\n");
            }
            record_cnt = 0;
            memset(origin_record_buf,0,sizeof(origin_record_buf));
        }
    }
    return true;
}

/**
* @createtime   :2018-5-12
* @function@name:    app_origin_instruction_parse
* @description  :    function for parse response, judge the device id, and msgtoken
* @input        :    none
* @output       :    none
* @return       :    true if success
*/
static uint32_t app_origin_instruction_parse(protocol_simple_rsp_t const *const p_msg_t, char const * const p_id)
{
    if(p_msg_t == NULL)
    {
        DBG_LOG("pointor is NULL");
        return 0;
    }
    uint32_t msgtoken = 0;
    char device_id[13] = {0};
    app_iden_get_device_id_char(device_id);
    uint16_t give_random = p_msg_t->Iden.MsgToken & 0x0ffff;
    if(p_id[0] != '\0')
    {
        DBG_LOG("\r\norigin parse : give dev id is: %s\r\n",p_id);
        if(memcmp(p_id,device_id,12) != 0)
        {
            DBG_LOG("\r\norigin parse: error, device id is: %s\r\n",device_id);            
            return 0;
        }        
        msgtoken = app_get_msg_token(p_id,give_random);
    }
    else
    {
        msgtoken = app_get_msg_token(device_id,give_random);
    }
    if(msgtoken != p_msg_t->Iden.MsgToken)
    {
        DBG_LOG("\r\norigin parse: check msgtoken is error\r\n");
        return 0;
    }
    DBG_LOG("Iden.DeviceID = %s\r\n",p_id);    
    DBG_LOG("register MsgIndex = %u\r\n",p_msg_t->Iden.MsgIndex);
    DBG_LOG("register MsgToken = %x\r\n",p_msg_t->Iden.MsgToken);
    DBG_LOG("\r\n-----------------------origin response parse successful----------------------\r\n\0");
        
    return true;
}
/**
* @function@name:    app_origin_rsp_parse_handle
* @description  :    function for parse response after upload origin acc data
* @input        :    p_buf:  given buffer for response
*               :    len:  response len
* @output       :    none
* @return       :    true if success
*/
bool app_origin_rsp_parse_handle(uint8_t const * const p_rsp, int32_t const len)
{
    protocol_simple_rsp_t     p_msg_t;
    char dev_id[15];
    if(p_rsp == NULL || len <= 0)
    {
        DBG_LOG("\r\norigin rsp buffer pointor NULL, or len <= 0\r\n\0");
        return false;
    }
    if(user_app_decode_simple_rsp(&p_msg_t,dev_id,p_rsp,len) == false)
    {
        DBG_LOG("\r\norigin response decode failed\r\n",dev_id);
        return false;
    }
    if(app_origin_instruction_parse(&p_msg_t,dev_id) != true)
    {
        DBG_LOG("\r\norigin response parse failed\r\n",dev_id);
        return false;
    }  

    xSemaphoreGive(binary_semaphore_wait_response);
    return true;
}
static bool user_origin_wait_rsp(void)
{

    if(xSemaphoreTake( binary_semaphore_wait_response, 5 * 1000) == pdPASS)
    {
        DBG_LOG("receive origin response successed\r\n\0");
        return true;
    }
    return false;
}
/**
* @function@name:    app_start_upload_origin_acc_data
* @description  :    function for read data from memory, then send them,until the memory is empty
* @input        :    none
* @output       :    none
* @return       :    true if success
*/

//bool app_origin_upload_data(void)
bool app_origin_upload_data(uint8_t * encode_buf, uint32_t encode_buf_size, 
                  uint8_t * send_buf, uint32_t send_buf_size
                 )
{
    if(!ble_get_connect_status())
    {
        DBG_LOG("\r\nble not connect\r\n");
        return false;
    }
    if(encode_buf == NULL || send_buf == NULL)
    {
        DBG_LOG("\r\n pointer encode_buf or send_buf is null\r\n");
        return false;
    }
    int32_t                 record_size = ACC_ORIGIN_ONCE_SEND_RECORD;    //one record of 50 points once send
    acc_origin_record_t     *origin_record_buf =NULL;//[ACC_ORIGIN_ONCE_SEND_RECORD];
    
    //uint16_t    encode_buf_size = 1000;
    //uint16_t    send_buf_size = 1000;
    //uint8_t     *p_encode_buf = (uint8_t *)malloc(sizeof(uint8_t)*encode_buf_size);
    //uint8_t     *send_buf = (uint8_t *)malloc(sizeof(uint8_t)*send_buf_size);
    
    bool                    ret = 0;
            uint8_t         frame_seq = 0;
            int32_t         len_temp = 0;
            //uint8_t         send_buf[1500] = {0};
            //uint8_t         p_encode_buf[1500] = {0};
            origin_record_buf = (acc_origin_record_t *)send_buf;
            uint8_t         func_code = USER_COMMON_CODE;  //0X01
            user_identity_t     iden;
    binary_semaphore_wait_response = xSemaphoreCreateBinary();
    if(binary_semaphore_wait_response == NULL)
    {
        DBG_LOG("\r\n>>>>>>>> upload origin: binary semaphore create failed <<<<<<<<<\\r\n\0");
        return false;
    }
    xSemaphoreTake( binary_semaphore_wait_response, 0);
    //Record_Delete(RECORD_TYPE_BEHAVIOR,(ACC_ORIGIN_NUM_SECTORS*DRV_GD25Q127_ONE_SECTOR_HAS_BYTES) / sizeof(acc_origin_record_t));
    do
    {
        frame_seq++;
        if(!frame_seq)
        {
            frame_seq++;
        }   
        DBG_LOG("\r\n upload origin pachage sequence  = %d\r\n\0",frame_seq);
        record_size = app_read_origin_records(origin_record_buf,ACC_ORIGIN_ONCE_SEND_RECORD);
        DBG_LOG("\r\n read origin record, record number = %d\r\n\0",record_size);
        if(record_size > 0)
        {
            app_get_indetity_msg(&iden.head);
            app_iden_get_device_id_char(iden.dev_id);
            //encode data

            len_temp = user_origin_encode_data(encode_buf,
                                                encode_buf_size,
                                                origin_record_buf,
                                                record_size,
                                                iden
                                                );
            DBG_LOG("\r\n encode origin data, len = %d\r\n\0",len_temp);                              
            if(len_temp > 0)
            {
                //package encoded data
                len_temp = user_ble_data_packge(send_buf, send_buf_size, encode_buf, len_temp, 
                                                PROTOCOL_HEADER_TYPE_TYPE_ORIGIN_REQ,
                                                frame_seq,func_code);                 
                DBG_LOG("\r\norigin data package, len = %d\r\n\0",len_temp);
                if(len_temp > 0)
                {
                    //transmit data                    
                    DBG_LOG("\r\norigin transmit data, len = %d\r\n",len_temp);
                    ret = user_send_data_by_ble(send_buf,len_temp);  
                    if(ret)
                    {
                        ret = user_origin_wait_rsp();
                        if(ret)
                        {                                                       
                            if(Record_Delete(RECORD_TYPE_ACC_ORIGIN,record_size) == true)  
                            {
                                DBG_LOG("delet origin sample %d is successful\r\n\r\n",record_size);
                            }
                            else
                            {
                                DBG_LOG("delet origin sample %d is failed\r\n\r\n",record_size);
                            }
                        }
                    }
                }                    
            }
        }
    //}while(record_len && ret);
    }while(record_size && len_temp && ret);
    DBG_LOG("origin data upload over\r\n\0");
    vSemaphoreDelete( binary_semaphore_wait_response );
    //free(send_buf);
    //free(p_encode_buf);
    return true;
}


void app_origin_parse_rsp(uint8_t *p_buf, int32_t len)
{
    
}
/**
* @function@name:    user_origin_transmit_task
* @description  :    origin transmit task, if received a semaphore, then startup transmit all data
* @input        :    none
* @output       :    none
* @return       :    none
*/
/*
static void user_origin_transmit_task(void *arg)
{
    xSemaphoreTake( binary_semaphore_origin, 200 );
    while(1)
    {
        if(xSemaphoreTake( binary_semaphore_origin, portMAX_DELAY ) == pdPASS)
        {
        //enable transmit data;
            //app_origin_save_handle();
        }
    }
}
*/
/*
static void user_origin_detect_task(void *arg)
{
    acc_origin_point_t  p_acc_pt[HAL_ACC_POINT_SAMPLES];
    uint8_t *p_buf = (uint8_t *)p_acc_pt;
    uint8_t  temp;
    uint8_t  pt_cnt = 0;
    while(1)
    {
        pt_cnt = 0;
        if(xQueueReceive(queue_acc_origin, &temp , 2000) == pdPASS)
        {
            //first buffer data is a length of accelerate points which has x,y,z-axis
            if(temp == HAL_ACC_POINT_SAMPLES)
            {
                while(xQueueReceive(queue_acc_origin, &p_buf , 0) != errQUEUE_EMPTY)
                {
                    p_buf[pt_cnt++] = temp;
                }
                hal_acc_save_origin_datas(p_buf,pt_cnt);
            }
        }
        if(xSemaphoreTake( binary_semaphore_origin, 2000 ) == pdPASS)
        {
            //enable transmit data;
            
        }
    }
}
*/
/*
// create a origin transmit task, and semaphore
void user_create_origin_transmit_task(void)
{
    BaseType_t ret;  
    binary_semaphore_origin = xSemaphoreCreateBinary(  );
    vSemaphoreCreateBinary(  );
    //binary_semaphore_wait_response = xSemaphoreCreateBinary();
    //queue_acc_origin = xQueueCreate( HAL_ACC_POINT_SAMPLES, sizeof(acc_origin_point_t) );
    
    ret = xTaskCreate( user_origin_transmit_task, "acc_origin", 128, NULL, 1, &acc_origin_handle );
    if(pdPASS != ret)
    {
        APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
    }  
}

// this function for sending a semaphore to task
void user_send_semaph_to_orgtrans_task(void)
{
    static BaseType_t semaphore = pdFALSE;
    xSemaphoreGiveFromISR(binary_semaphore_origin,&semaphore);
}
// this function for sending some acc data to task
void user_send_queue_acc_to_orgtrans_task(uint8_t *p_acc_dt, uint32_t bytes_len)
{
    for(uint8_t i=0; i<bytes_len; i++)
    {
         while(xQueueSendToBack(queue_acc_origin, &p_acc_dt[i], 5) != pdPASS);
    }
}
*/
//end

