
/**
 * hal_rf.c
 * Date:    2018/3/28
 * Author:  Chenfei
 * 
 * @brief neck strap project.
 * by  nrf_52832 freertos platform
 *
 */
#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "nrf.h"
#include "app_error.h"
#include "FreeRTOS.h"
#include "nrf_sdh_freertos.h"
#include "semphr.h"
#include "app_timer.h"
#include "peer_manager.h"
#include "bsp_btn_ble.h"
#include "task.h"
#include "timers.h"
#include "fds.h"
#include "nrf_drv_clock.h"
#include "ble_sys_cfg.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "user_task.h"
#include "user_app.h"
#include "nrf_drv_spi.h"
#include "hal_rf.h"
#include "drv_rf_si4432.h"


static      TaskHandle_t    rf_handle;
static      volatile bool   spi_xfer_done; 
/**
 * @brief SPI user event handler.
 * @param event
 */


static void hal_rf_handler_task(void *arg)
{

   drv_si4432_power_down();
   vTaskDelete(rf_handle);
}
void hal_creat_rf_handler_task(void)
{
    BaseType_t ret;    
    ret = xTaskCreate( hal_rf_handler_task, "rf", 128, NULL, 1, &rf_handle );
    if(pdPASS != ret)
    {
        APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
    }  
}

