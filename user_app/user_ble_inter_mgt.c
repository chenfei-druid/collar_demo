/** 
 * user_ble_inter_mgt.c
 *
 * @group:       collar project
 * @author:      Chenfei
 * @create time: 2018/4/22
 * @version:     V0.0
 *
 */

#include "freertos_platform.h"
#include "user_ble_inter_mgt.h"
#include "user_app_origin.h"
#include "user_app_bhv.h"
#include "user_app_log.h"
#include "user_app_register.h"
#include "user_data_pkg.h"
#include "crc16.h"
#include "user_analyse_commu_cmd.h"
#include "ble_application.h"
#include "ble_rts.h"
#include "user_app.h"
#include "user_app_env.h"
#include "user_app_estrus.h"
#include "user_app_warning.h"
#include "user_task.h"
#include "ble.h"
#include "ble_hci.h"

//#include "SEGGER_RTT.h"
//#include "SEGGER_RTT_Conf.h"

#define     UPLOAD_ENCODE_BUF          500
#define     UPLOAD_SEND_BUF          500

static      xSemaphoreHandle    binary_semaphore_ble_transmit;
static      xSemaphoreHandle    binary_semaphore_ble_transmit_enable;
static      xSemaphoreHandle    binary_semaphore_ble_transmit_disable;

static      QueueHandle_t       queue_ble_rx;
//static      QueueHandle_t       queue_ble_tx;
//static      TaskHandle_t        ble_rx_handle;    
static      TaskHandle_t        ble_tx_handle;



/*
uint32_t user_ble_get_rx_data(uint8_t *p_buf)
{
    uint8_t ret_len;
    if(len > sizeof(data_buff))
    {
        ret_len = 0;
    }
    memcpy(p_buf,data_buff,ret_len);
    len = 0;
    return ret_len;
}
*/
/*
static void user_ble_receive_task(void *arg)
{
      //valid ble max length is 247-3
    uint32_t            ble_rec_len = 0;
    uint8_t             rec_temp;
    //static uint8_t     inst_msg[255];
//    static uint8_t     rsp_data[255];
    //static uint8_t     ble_rec_buf[255];
    xQueueReceive(queue_ble_rx, &rec_temp , 200);
    while(1)
    {        
        
        ble_rec_len = 0;
        if(xQueueReceive(queue_ble_rx, &rec_temp , portMAX_DELAY) == pdPASS)
        {
            //ble_rec_buf[ble_rec_len] = rec_temp;
        }
        uint16_t ble_rx_queue_len = rec_temp;
        while(xQueueReceive(queue_ble_rx, &rec_temp , 0) != errQUEUE_EMPTY)
        {
                ble_rec_buf[ble_rec_len++] = rec_temp;
        }     
        
            // add validity judge  later/////////
            
            int32_t temp_len = 0;
            if(ble_rec_len > 0 && ble_rx_queue_len == ble_rec_len)  //given length and receive length must be equal
            {
                temp_len = user_ble_combine_split_frame(rsp_data, ble_rec_buf, ble_rec_len);
                if(temp_len > 0)
                {
                    temp_len = user_ble_get_valid_msg(inst_msg,rsp_data,temp_len);
                    if(temp_len > 0)
                    {
                        user_parse_send_queue(inst_msg,temp_len);
                    }
                }
            }
            
    }
}
*/
static void user_ble_transmit_task(void *arg)
{
    static uint8_t * encode_buf = NULL;
    static uint8_t * send_buf = NULL;
     
    //static uint8_t  encode_buf[512];
    //static uint8_t  send_buf [512];
    if(binary_semaphore_ble_transmit == NULL)
    {
        binary_semaphore_ble_transmit = xSemaphoreCreateBinary();
    }
    xSemaphoreTake( binary_semaphore_ble_transmit, 0);
    while(1)
    {
    
        DBG_LOG("ble_tx_handle = 0x%x\r\n\0",ble_tx_handle);
        if(xSemaphoreTake( binary_semaphore_ble_transmit, 3600*1000 ) == pdPASS)
        {
            //enable transmit data;
            if(ble_get_connect_status() && ble_get_notify_status())
            {
                vPortFree(encode_buf);               
                encode_buf = pvPortMalloc(UPLOAD_ENCODE_BUF);
                vPortFree(send_buf);
                send_buf = pvPortMalloc(UPLOAD_SEND_BUF);
                user_app_send_led_queue(1000);
                //send register
                //user_suspend_nb_timer_transmit_task();
                DBG_LOG("\r\n*****************it's time to register through ble*****************************\r\n");
                if(app_register(encode_buf,UPLOAD_ENCODE_BUF, send_buf, UPLOAD_SEND_BUF,COMMUNICATION_INTERFACE_TYPE_BLE) == true)
                {    
                    DBG_LOG("*********************it's time to setting require**************************\r\n");
                    user_app_setting_require(encode_buf,UPLOAD_ENCODE_BUF, send_buf, UPLOAD_SEND_BUF,COMMUNICATION_INTERFACE_TYPE_BLE);
                    DBG_LOG("\r\n*****************it's time to upload environment data through ble********************\r\n");
                    user_env_upload_data(encode_buf,UPLOAD_ENCODE_BUF, send_buf, UPLOAD_SEND_BUF,COMMUNICATION_INTERFACE_TYPE_BLE);
                    DBG_LOG("\r\n*****************it's time to upload behavior data through ble********************\r\n");
                    user_bhv_upload_data(encode_buf,UPLOAD_ENCODE_BUF, send_buf, UPLOAD_SEND_BUF,COMMUNICATION_INTERFACE_TYPE_BLE);            
                    DBG_LOG("\r\n*****************it's time to upload estrus data through ble****************************\r\n"); 
                    user_estrus_upload_data(encode_buf,UPLOAD_ENCODE_BUF, send_buf, UPLOAD_SEND_BUF,COMMUNICATION_INTERFACE_TYPE_BLE);
                    DBG_LOG("\r\n*****************it's time to upload warning data through ble****************************\r\n"); 
                    user_warning_infor_upload(encode_buf,UPLOAD_ENCODE_BUF, send_buf, UPLOAD_SEND_BUF,COMMUNICATION_INTERFACE_TYPE_BLE);
                    DBG_LOG("\r\n*****************it's time to upload origin data through ble***************************\r\n");
                    app_origin_upload_data(encode_buf,UPLOAD_ENCODE_BUF, send_buf, UPLOAD_SEND_BUF);
                    
                }
                DBG_LOG("+++++++++++++++++++++++++++ble transmit complete++++++++++++++++++++++++++\r\n");
                //user_resume_nb_timer_transmit_task();
                user_app_send_led_queue(0);
                vPortFree(encode_buf);
                vPortFree(send_buf);
                sd_ble_gap_disconnect(0, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
                //NVIC_SystemReset();                 
            }           
        }   
        //SEGGER_RTT_printf(0,"++++++++++ ble transmit complete +++++++++++\r\n");        
    }
}

/**
* @function@name:    user_creat_uart_handler_task
* @description  :    Function for creating a task for ble transmit and receive data
* @input        :    none:                     
* @output       :    none
* @return       :    none
*/
/*
void user_creat_ble_receive_task(void)
{
    queue_ble_rx = xQueueCreate( 50, sizeof(char) );
    BaseType_t ret;    
    ret = xTaskCreate( user_ble_receive_task, "brx", 256, NULL, 1, &ble_rx_handle );
    if(pdPASS != ret)
    {
        APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
    }  
}
*/
void user_creat_ble_transmit_task(void)
{
    binary_semaphore_ble_transmit = xSemaphoreCreateBinary();
    BaseType_t ret;    
    ret = xTaskCreate( user_ble_transmit_task, "btx", 512, NULL, 1, &ble_tx_handle );
    if(pdPASS != ret)
    {
        APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
    }  
    //DBG_LOG("ble_tx_handle = 0x%x\r\n\0",ble_tx_handle);
}

void user_give_ble_semaphore(semaphore_ble_type type)
{
    static BaseType_t semaphore = pdFALSE;
    switch(type)
    {
        case BLE_TRANSMIT_ENABLE:
            xSemaphoreGiveFromISR(binary_semaphore_ble_transmit_enable,&semaphore);
            break;
        case BLE_TRANSMIT_DISABLE:
            xSemaphoreGiveFromISR(binary_semaphore_ble_transmit_disable,&semaphore);
            break;
        default:break;
    } 
}

void user_ble_send_queue_to_task(uint8_t *p_queue, uint8_t len)
{
    while(xQueueSendToBack(queue_ble_rx, &len, 10) != pdPASS);
    for(uint8_t i=0;i<len;i++)
        {
            while(xQueueSendToBack(queue_ble_rx, &p_queue[i], 10) != pdPASS);
        }
}
void user_ble_notify_enable(void)
{
    if(binary_semaphore_ble_transmit == NULL)
    {
        binary_semaphore_ble_transmit = xSemaphoreCreateBinary();
    }
    BaseType_t semaphore = pdFALSE;
    xSemaphoreGiveFromISR(binary_semaphore_ble_transmit,&semaphore);
    //xSemaphoreGive(binary_semaphore_ble_transmit);
    DBG_LOG("\r\nble connect and notify transmit task.......\r\n");
}
void user_ble_notify_disable(void)
{
    user_give_ble_semaphore(BLE_TRANSMIT_DISABLE);
}
void user_suspend_ble_trans_task(void)
{
    DBG_LOG("\r\nsuspend_ble_transmit_task\r\n");
    vTaskSuspend(ble_tx_handle);
    DBG_LOG("\r\nsuspend task ok\r\n");
}
void user_resume_ble_trans_task(void)
{
    DBG_LOG("\r\nresume_ble_transmit_task\r\n");
    vTaskResume(ble_tx_handle);
    DBG_LOG("\r\nresume task ok\r\n");
}

