
#ifndef __PERIPHERAL_INTERFACE_CONFIG_
#define __PERIPHERAL_INTERFACE_CONFIG_

void drv_acc_kx022_int_enable(void);

void user_acc_interrupt_config(void);
void user_acc_interrupt_interface_enable(void);
void user_acc_interrupt_interface_disable(void);

#endif
