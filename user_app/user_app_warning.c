/** 
 * user_app_warning.c
 *
 * @group:       collar project
 * @author:      Chenfei
 * @create time: 2018/5/18
 * @version:     V0.0
 * 
 *
 */
/*****************************************includes*******************************************/
#include <math.h>
#include "freertos_platform.h"
#include "string.h"
//#include "time.h"
#include "Record.h"
//#include "hal_rtc.h"
//#include "alarm_clock.h"
#include "user_pb_callback.h"
#include "Define.pb.h"
#include "pb_encode.h"
#include "pb_decode.h"
#include "SimpleRsp.pb.h"
#include "user_data_pkg.h"
#include "user_send_data.h"
#include "user_battery_mgt.h"
#include "user_illumination.h"
#include "user_adc_common.h"
#include "user_app_temp.h"
#include "user_app_log.h"
#include "drv_nb_modul.h"
#include "user_app_identity.h"
#include "user_analyse_commu_cmd.h"
#include "user_app_warning.h"
#include "user_app_env.h"
#include "user_app_estrus.h"
#include "Warning.pb.h"
#include "drv_nb_modul.h"
#include "ble_application.h"
/*******************************************defines*******************************************/
#define     test_decode     0
#define     S               1000
typedef struct
{
    int32_t             count;
    protocol_warning_t     *record;
}warning_encode_t;

#if test_decode
typedef struct
{
    int32_t             count;
    protocol_env_t     *record;
}warning_decode_t;
#endif
static      TaskHandle_t            warning_detect_handle;
static      xSemaphoreHandle        warning_wait_response_semaphore = NULL;

static      warning_setting_t       m_set = {.mode = WARNING_MODE_DISABLE};

/*****************************************functions*******************************************/
/**
    WARNING_TYPE_BOOT         		    = 2,        // startup warning
	WARNING_TYPE_LOW_BATTERY   		    = 3,        // low battery warning
	WARNING_TYPE_STATIC       		    = 4,        // 静止报警
	//WARNING_TYPE_GEO_FENCE_IN   		= 5,        // ??????
	//WARNING_TYPE_GEO_FENCE_OUT  		= 6,        // ??????
	WARNING_TYPE_DEVICE_REMOVE       	= 7,        // 设备拆除报警
	WARNING_TYPE_ACTIVITY_EXCESS        = 8,        // 运动过量报警
	WARNING_TYPE_TEMPERATURE_EXCESS     = 9,        // 温度超标报警
	//WARNING_TYPE_DEVICE_DESTROY       	= 10,        // 开盖报警
*/
void user_warning_mode_set(warning_setting_t *p_set)
{
    m_set.mode = p_set->mode;
}
/**
* @createtime   :    2018-5-12
* @function@name:    user_env_save_one_record
* @description  :    function for save one env record into memory
* @input        :    p_rec_t:  one env record data @ref env_record_t
* @output       :    none
* @return       :    true if success
*/
static bool user_warning_save_one_record(warning_record_t const p_rec_t)
{
    return Record_Write(RECORD_TYPE_WARNING,(uint8_t *)&p_rec_t);
}
/**
* @function@name:    user_warnig_read_records
* @description  :    Function for getting environment record from memory
* @input        :    none
* @output       :    none
* @return       :    record size
* @createtime   :   2018-5-21
**/
#if 0
static int32_t user_warnig_read_records(warning_record_t * const p_record_t, int32_t const record_size)
{
   if(p_record_t == NULL)
    {
        DBG_LOG("\r\nwarning read record: pointer is null\r\n",i);
        return 0;
    }
    int32_t ret = record_size; 
    for(uint32_t i=0;i<record_size;i++)
    {
        p_record_t[i].type = WARNING_TYPE_LOW_BATTERY;
        p_record_t[i].env.battery_percent = 50;
        p_record_t[i].env.battery_voltage = 3950;
        p_record_t[i].env.illumination    = 230;
        p_record_t[i].env.temperature     = 250;
        p_record_t[i].timestamp   = hal_rtc2_get_unix_time();
    }
    DBG_LOG("warning read record number = %d\r\n", ret);
    
    for(uint32_t i=0;i<ret;i++)
    {
        DBG_LOG("\r\nnumber %d record:\r\n",i);
        DBG_LOG("timestamp = %d\r\n", p_record_t[i].timestamp);        
        DBG_LOG("bat percent = %d%\r\n", p_record_t[i].env.battery_percent);
        DBG_LOG("bat vol = %d\r\n", p_record_t[i].env.battery_voltage);
        DBG_LOG("lux = %d\r\n", p_record_t[i].env.illumination);    
        DBG_LOG("temperature = %d℃\r\n", p_record_t[i].env.temperature); 
        DBG_LOG("warning = %d\r\n", p_record_t[i].type); 
    }
    return ret;
}
#else
/**
* @function@name:    user_warnig_read_records
* @description  :    Function for getting environment record from memory
* @input        :    none
* @output       :    none
* @return       :    record size
* @createtime   :   2018-5-21
**/
int32_t user_warnig_read_records(warning_record_t * const p_record_t, int32_t const record_size,trans_interface_type chn_type)
{
    if(p_record_t == NULL)
    {
        DBG_LOG("\r\nwarning read record: pointer is null\r\n");
        return 0;
    }
    int32_t ret;
    if(chn_type == COMMUNICATION_INTERFACE_TYPE_BLE)
    {
        ret = Record_Read(RECORD_TYPE_WARNING, (uint8_t *)p_record_t, record_size * sizeof(warning_record_t));      
    }
    else
    {   
        ret = Record_Peek(RECORD_TYPE_WARNING, (uint8_t *)p_record_t, sizeof(warning_record_t)); 
        if(ret > 1)
        {
            ret = 1;
        }
    }
    DBG_LOG("warning read record number = %d\r\n", ret);
    if(ret > WARNING_ONCE_UPLOAD_RECORD_NUM)
    {
        ret = 1;
    }
    for(uint32_t i=0;i<ret;i++)
    {
        DBG_LOG("\r\nnumber %d record:\r\n",i);
        DBG_LOG("timestamp = %d\r\n", p_record_t[i].timestamp);        
        DBG_LOG("bat percent = %d%\r\n", p_record_t[i].env.battery_percent);
        DBG_LOG("bat vol = %d\r\n", p_record_t[i].env.battery_voltage);
        DBG_LOG("lux = %d\r\n", p_record_t[i].env.illumination);    
        DBG_LOG("temperature = %d℃\r\n", p_record_t[i].env.temperature); 
        DBG_LOG("warning = %d\r\n", p_record_t[i].type);     
    }
    return ret;
}
#endif
/**
* @function@name:    user_warnig_peek_newest_record
* @description  :    Function for reading a newest warning record
* @input        :    none
* @output       :    none
* @return       :    record size
* @createtime   :   2018-5-21
**/
/*
static int32_t user_warnig_peek_newest_record(warning_record_t * const p_record_t)
{
    if(p_record_t == NULL)
    {
        DBG_LOG("\r\nwarning read record: pointer is null\r\n");
        return 0;
    }
    int32_t ret;
    ret = Record_Peek(RECORD_TYPE_WARNING, (uint8_t *)p_record_t, sizeof(warning_record_t));  
    if(ret > 1)
    {
        ret = 1;
    }
    DBG_LOG("warning read record number = %d\r\n", ret);

    DBG_LOG("timestamp = %d\r\n", p_record_t->timestamp);        
    DBG_LOG("bat percent = %d%\r\n", p_record_t->env.battery_percent);
    DBG_LOG("bat vol = %d\r\n", p_record_t->env.battery_voltage);
    DBG_LOG("lux = %d\r\n", p_record_t->env.illumination);    
    DBG_LOG("temperature = %d℃\r\n", p_record_t->env.temperature); 
    DBG_LOG("warning = %d\r\n", p_record_t->type);     
    return ret;
}
*/
/**
* @functionname : user_env_encode_repeated_var_struct 
* @description  : function for encode the environment records, the record number is filled at head position
* @input        : stream, encode stream,  
*               : fields: the env fields
*               : src_struct: the env struct will be encoded
* @output       : none 
* @return       : true if successed
*/
static bool user_warning_encode_repeated_var_struct(pb_ostream_t *stream, const pb_field_t fields[],  void * const *src_struct)
{
    
    bool res;
    warning_encode_t *m_head_t = (warning_encode_t *)*src_struct;
    protocol_warning_t    *record_t        = m_head_t->record;
    for(uint8_t i=0; i<m_head_t->count; i++)
    {
        if (!pb_encode_tag_for_field(stream, fields))
        {
            DBG_LOG("encode warning tag failed\r\n\0");
            return false;
        }
        res = pb_encode_submessage(stream, protocol_warning_fields, &record_t[i]);
        //DBG_LOG("encode behavior record %d is %s", i,(res? "successed":"failed"));
        if(res == false)
        {
            DBG_LOG("encode warning struct failed\r\n\0");
            break;
        }     
        DBG_LOG("encode warning struct number %d of %d successed\r\n\0",i,m_head_t->count);
    }
    DBG_LOG("encode warning struct is %s\r\n", res? "successed":"failed");
    return res;
}
/**
* @functionname : user_warning_encode_data 
* @description  : function for encode data, must do it before package
* @input        : buffer_size: encode_out_buffer's max size,  
*               : p_record_buf: warning records pointer
*               : record_size: warning record numbers
* @output       : encode_out_buffer: the encode complete data buff 
* @return       : encoded data byte size
*/
static int32_t user_warning_encode_data(uint8_t         * const encode_out_buffer,
                                        uint32_t        const buffer_size,
                                        warning_record_t    * const p_record_buf, 
                                        uint32_t        const record_size,  
                                        user_identity_t const iden)
{
    if(encode_out_buffer == NULL || p_record_buf == NULL)
    {
        DBG_LOG("\r\nenv encode pointer is null\r\n");
        return 0;
        
    }
    
    protocol_warning_req_t    pro_warning_req_t;
    protocol_warning_t        encode_record_t[record_size];
    warning_encode_t          v_warning_encode_t;
    
    //char p_dev_mac[13] ;
    memset(encode_record_t, 0, sizeof(encode_record_t));
    pro_warning_req_t.Iden = iden.head;
    
    v_warning_encode_t.count    = record_size;
    v_warning_encode_t.record   = encode_record_t;
    
    //fill record data
    for(int32_t i=0; i<record_size; i++)
    {
        /*
        encode_record_t[i].EnvInfo.has_AmbientLight = 1;
        encode_record_t[i].EnvInfo.has_BatteryPower = 1;
        encode_record_t[i].EnvInfo.has_BatteryVoltage = 1;        
        encode_record_t[i].EnvInfo.has_ChargeCurrent = false;        
        encode_record_t[i].EnvInfo.has_ChargeVoltage = false;
        encode_record_t[i].EnvInfo.has_InnerHumidity = false;
        encode_record_t[i].EnvInfo.has_InnerPressure = false;
        encode_record_t[i].EnvInfo.has_InnerTemperature = 1;        
        encode_record_t[i].EnvInfo.has_Timestamp = 1;
                
        encode_record_t[i].EnvInfo.AmbientLight     = p_record_buf[i].env.illumination;
        encode_record_t[i].EnvInfo.BatteryPower     = p_record_buf[i].env.battery_percent;
        encode_record_t[i].EnvInfo.BatteryVoltage   = p_record_buf[i].env.battery_voltage;
        encode_record_t[i].EnvInfo.InnerTemperature = p_record_buf[i].env.temperature;
        encode_record_t[i].EnvInfo.Timestamp        = p_record_buf[i].env.timestamp;     
        */
        encode_record_t[i].has_EnvInfo = 1;
        encode_record_t[i].has_StatusType = 1;
        encode_record_t[i].has_Timestamp = 1;        
                
        encode_record_t[i].StatusType  = (protocol_warning_type_t)p_record_buf[i].type;
        encode_record_t[i].Timestamp   = p_record_buf[i].timestamp;     
    }
 
    pro_warning_req_t.Iden.DeviceID.funcs.encode    = &user_app_encode_repeated_var_string;
    pro_warning_req_t.Iden.DeviceID.arg             = (char *)iden.dev_id;
    
    pro_warning_req_t.WarningInfo.funcs.encode          = &user_warning_encode_repeated_var_struct;
    pro_warning_req_t.WarningInfo.arg                   = &v_warning_encode_t;
    
    pb_ostream_t            m_stream ;    
    m_stream = pb_ostream_from_buffer(encode_out_buffer,buffer_size);
    bool res = pb_encode(&m_stream,protocol_warning_req_fields,&pro_warning_req_t);
    if(res)
    {
        DBG_LOG("encode warning is successed\r\n\0");
        #if test_decode
        user_data_pb_decode_test(encode_out_buffer,m_stream.bytes_written);
        #endif
        return m_stream.bytes_written;
    }
    DBG_LOG("encode warning is failed\r\n\0");
    return 0;
}
/**
* @createtime   :2018-5-12
* @function@name:    app_warning_instruction_parse
* @description  :    function for parse response, judge the device id, and msgtoken
* @input        :    none
* @output       :    none
* @return       :    true if success
*/
static uint32_t user_warning_instruction_parse(protocol_simple_rsp_t const *const p_msg_t, char const * const p_id)
{
    if(p_msg_t == NULL)
    {
        DBG_LOG("pointor is NULL\r\n");
        return 0;
    }
    uint32_t msgtoken = 0;
    char device_id[13] = {0};
    app_iden_get_device_id_char(device_id);
    uint16_t give_random = p_msg_t->Iden.MsgToken & 0x0ffff;
    if(p_id[0] != '\0')
    {
        DBG_LOG("\r\nwarning parse : give dev id is: %s\r\n",p_id);
        if(strcmp(p_id,device_id) != 0)
        {
            DBG_LOG("\r\nwarning parse: error, device id is: %s\r\n",device_id);            
            return 0;
        }        
        msgtoken = app_get_msg_token(p_id,give_random);
    }
    else
    {
        msgtoken = app_get_msg_token(device_id,give_random);
    }
    if(msgtoken != p_msg_t->Iden.MsgToken)
    {
        DBG_LOG("\r\nwarning parse: check msgtoken is error\r\n");
        return 0;
    }
    DBG_LOG("Iden.DeviceID = %s\r\n",p_id);    
    DBG_LOG("warning MsgIndex = %u\r\n",p_msg_t->Iden.MsgIndex);
    DBG_LOG("warning MsgToken = %x\r\n",p_msg_t->Iden.MsgToken);
    DBG_LOG("\r\n-----------------------warning response parse successful----------------------\r\n\0");
        
    return true;
}
/**
* @functionname : app_warning_rsp_parse_handle 
* @description  : function for encode data, must do it before package
* @input        : p_rsp: response message from platform,  
*               : len: response message byte length
* @output       : none 
* @return       : true if right response
*/
bool app_warning_rsp_parse_handle(uint8_t *p_rsp, int32_t len)
{
    protocol_simple_rsp_t     p_msg_t;
    char dev_id[15] = {0};
    if(p_rsp == NULL || len <= 0)
    {
        DBG_LOG("env rsp buffer pointor NULL, or len <= 0\r\n\0");
        return false;
    }
    if(user_app_decode_simple_rsp(&p_msg_t,dev_id,p_rsp,len) == false)
    {
        DBG_LOG("\r\nenv response decode failed\r\n",dev_id);
        return false;
    }
    if(user_warning_instruction_parse(&p_msg_t,dev_id) != true)
    {
        DBG_LOG("\r\nenv response parse failed\r\n",dev_id);
        return false;
    }   
    xSemaphoreGive(warning_wait_response_semaphore);
    return true;
}
/**
* @functionname : user_warning_wait_rsp_nb 
* @description  : function for waiting response from platform, until timeout of 5S
* @input        : none
* @output       : none 
* @return       : true if right response
*/
static bool user_warning_wait_rsp_nb(trans_interface_type chn_type)
{
    uint32_t wait_response_timeout;
    if(chn_type == COMMUNICATION_INTERFACE_TYPE_NB)
    {
        wait_response_timeout = WAIT_SERVER_RESPONSE;
    }
    else
    {
        wait_response_timeout = 5;
    }
    DBG_LOG("waiting parse warning response\r\n\0");
    if(xSemaphoreTake( warning_wait_response_semaphore, wait_response_timeout*S ) == pdPASS)    //wait 10s
    {
        DBG_LOG("receive warning response successed\r\n\0");\
        return true;
    }
    DBG_LOG("waiting rsp timeout, no response received\r\n\0");
    return false;
}

/**
* @functionname : user_warning_analyse 
* @description  : function for analyse the warning type according to warning information 
* @input        : warning_infor_s: the detected information of the device
* @output       : none 
* @return       : warning type @ref warning_type
*/
static bool user_warning_analyse(warning_infor_t const warning_infor_s)
{
    
    warning_record_t            warning_req_msg_s;
    warning_req_msg_s.env       = warning_infor_s.env;
    warning_req_msg_s.timestamp = hal_rtc2_get_unix_time();
    bool status = true;
    if(warning_infor_s.env.battery_percent < WARNING_LOW_BATTERY_PERCENT_DEFAULT)
    {
        warning_req_msg_s.type      = WARNING_TYPE_LOW_BATTERY;
        user_warning_save_one_record(warning_req_msg_s);
        DBG_LOG("\r\nwarning detect: warning type is low battery..............\r\n\0");
        status = false; 
        //return WARNING_TYPE_LOW_BATTERY;
    }
    else if(warning_infor_s.env.temperature > WARNING_EXCESS_TEMP_VALUE_DEFAULT)
    {
        warning_req_msg_s.type      = WARNING_TYPE_TEMPERATURE_EXCESS;
        user_warning_save_one_record(warning_req_msg_s);
        DBG_LOG("\r\nwarning detect: warning type is excess temperature.............\r\n\0");
        status = false;
        //return WARNING_TYPE_TEMPERATURE_EXCESS;
    }
    else if(warning_infor_s.activity <= WARNING_STATIC_VALUE_DEFAULT)
    {
        warning_req_msg_s.type      = WARNING_TYPE_STATIC;
        user_warning_save_one_record(warning_req_msg_s);
        DBG_LOG("\r\nwarning detect: warning type is static............\r\n\0");
        status = false;
        //return WARNING_TYPE_STATIC;
    }
    else if(warning_infor_s.activity >= WARNING_EXCESS_ACTIVITY_VALUE_DEFAULT)
    {
        warning_req_msg_s.type      = WARNING_TYPE_ACTIVITY_EXCESS;
        user_warning_save_one_record(warning_req_msg_s);
        DBG_LOG("\r\nwarning detect: warning type is activity excessed...............\r\n\0");
        status = false;
        //return WARNING_TYPE_ACTIVITY_EXCESS;
    }
    else
    {
        warning_req_msg_s.type      = WARNING_TYPE_UNKNOWN;
        user_warning_save_one_record(warning_req_msg_s);
        DBG_LOG("\r\n.................warning detect: no no no no no no warning..........\r\n\0");
        status = true;
        //return WARNING_TYPE_UNKNOWN;
    }
    return status;
}
/**
* @functionname : user_warning_infor_detect 
* @description  : function for detect device information, if abnormal @ref warning_type, save and upload the warning information
* @input        : none
* @output       : none 
* @return       : none
*/
static bool user_warning_infor_detect(void)
{
    warning_infor_t    warning_infor_s;
    DBG_LOG("\r\nwarning detect\r\n\0");
    DBG_LOG("\r\nwarning detect: get a env information\r\n\0");
    user_env_get_sample(&warning_infor_s.env);
    DBG_LOG("\r\nwarning detect: get behavior\r\n\0");
    //warning_infor_s.bhv = user_bhv_get_action();
    DBG_LOG("\r\nwarning detect: get activity\r\n\activity = %d\r\n",warning_infor_s.bhv);
    warning_infor_s.activity    = user_estrus_get_activity();
    //warning_type    warn_type;
    return user_warning_analyse(warning_infor_s);
    /*
    warn_type = user_warning_analyse(warning_infor_s);
    if(warn_type != true)
    {       
        //upload warning information through
        DBG_LOG("\r\nwarning detect: upload warning\r\n\0");
        user_nb_send_semphore_to_tx_task();
        return 1;
    }
    return 0;
    */
}
/**
* @functionname : user_warning_infor_upload 
* @description  : function for uploading the warning data, if detected an abnormal status, the task will triger uploading, 
*               : or will be uploaded by communiation time if has one or more warning record on memory
* @input        : none
* @output       : none 
* @return       : true if right response
*/
//bool user_warning_infor_upload(trans_interface_type chn_type)
bool user_warning_infor_upload(uint8_t * encode_buf, uint32_t encode_buf_size, 
                  uint8_t * send_buf, uint32_t send_buf_size,  
                  trans_interface_type chn_type
                 )
{
    if(chn_type >= COMMUNICATION_INTERFACE_TYPE_MAX || chn_type <= COMMUNICATION_INTERFACE_TYPE_MIN)
    {
        return false;
    }
    if(chn_type == COMMUNICATION_INTERFACE_TYPE_BLE)
    {
        if(!ble_get_connect_status())
        {
            return false;
        }
    }
    int32_t             warning_record_size = WARNING_ONCE_UPLOAD_RECORD_NUM;      //send 125 point of xyz each send time
    warning_record_t    *warning_record_buf = (warning_record_t *)send_buf;//[WARNING_ONCE_UPLOAD_RECORD_NUM] = {0};
    user_identity_t iden;
    //bool        wait_ret = 0;
    int32_t     frame_seq = 0;
    //int32_t     len_temp = 0;
    //uint8_t     send_buf[256] = {0};
    //uint8_t     p_encode_buf[256] = {0};
    //uint8_t     *send_buf = (uint8_t *)malloc(256*sizeof(uint8_t));
    //uint8_t     *p_encode_buf = (uint8_t *)malloc(256*sizeof(uint8_t));
    DBG_LOG("\r\nsend_buf : 0x%x, p_encode_buf: 0x%x\r\n",send_buf,encode_buf);
    //char        p_dev_mac[DEVICE_ID_MAX_LEN];
    uint8_t     func_code = USER_COMMON_CODE;  //0X01
    int32_t     byte_len = 0;
    bool        res = false;
    memset(&iden,0,sizeof(iden));
    warning_wait_response_semaphore = xSemaphoreCreateBinary();
    if(warning_wait_response_semaphore == NULL)
    {
        DBG_LOG("\r\n>>>>>>>> upload warning: binary semaphore create failed <<<<<<<<<\\r\n\0");
        return false;
    }
    xSemaphoreTake( warning_wait_response_semaphore, 0);
    ////FOR TEST
    //user_warning_infor_detect();
    do
    {
        warning_record_size = user_warnig_read_records(warning_record_buf,WARNING_ONCE_UPLOAD_RECORD_NUM,chn_type);        
        DBG_LOG("\r\nread warning records len = %d\r\n",warning_record_size);
        if(warning_record_size > 0)
        {
            frame_seq++;
            if(!frame_seq)
            {
                frame_seq++;
            }
            app_get_indetity_msg(&iden.head);
            //iden.head.MsgIndex++;
            app_iden_get_device_id_char(iden.dev_id);
            //encode data
            byte_len = user_warning_encode_data(encode_buf,
                                            encode_buf_size,
                                            warning_record_buf,
                                            warning_record_size,
                                            iden
                                            );
            DBG_LOG("warning encode len = %d\r\n\0",byte_len);                                
            if(byte_len>0)  
            {                
                //package encoded data
                byte_len = user_data_packge(send_buf, send_buf_size, encode_buf, byte_len, 
                                            PROTOCOL_HEADER_TYPE_TYPE_WARNING_REQ,
                                            frame_seq,func_code,chn_type); 
                DBG_LOG("\r\nwarning data package len = %d\r\n\0",byte_len);
                if(byte_len > 0)
                {                    
                    //int retry = 0;
                    //do
                    //{                        
                        //transmit data
                        res = user_send_data(send_buf,byte_len,chn_type);
                        if(res == true)
                        {
                            res = user_warning_wait_rsp_nb(chn_type);
                        }
                        if(res)
                        {
                            if(chn_type == COMMUNICATION_INTERFACE_TYPE_BLE)
                            {
                                DBG_LOG("\r\ndelete warning record numbers = %d ",warning_record_size);
                                if(Record_Delete(RECORD_TYPE_WARNING,warning_record_size))
                                {
                                    DBG_LOG("successful\r\n\r\n",warning_record_size);
                                }
                                else
                                {
                                    DBG_LOG("failed\r\n\r\n",warning_record_size);
                                }
                            }
                        }
                    //}while(!res && retry++ < 2);
                }
            }
        }        
    }while(warning_record_size && res && byte_len);
    DBG_LOG("warning data upload over\r\n\0");
    vSemaphoreDelete( warning_wait_response_semaphore );
    //free(send_buf);
    //free(p_encode_buf);
    return res;
}
/**
* @functionname warning_detect_handler
* @brief        function for detect warning information, if exceed with battary power, temperateure......  
*
*/
static void warning_detect_handler(void)
{
    if((m_set.mode > WARNING_MODE_DISABLE) && (m_set.mode < WARNING_MODE_UNKNOWN_MAX))
    {
        if(user_warning_infor_detect() != true)
        {
            //upload warning information through
            switch(m_set.mode)
            {
                case WARNING_MODE_GPRS:
                    DBG_LOG("\r\nwarning detect: upload by gprs\r\n\0");
                    user_nb_send_queue_to_task(NB_TRANS_TASK_RUN_TYPE_WARNING);
                    break;
                case WARNING_MODE_SHORT_MESSAGE:
                    break;
                case WARNING_MODE_GPRS_AND_MESSAGE:
                    break;
                default:
                    DBG_LOG("\r\nwarning detect: invalid mode\r\n\0");
                    break;
            }            
        }
    }
    else
    {
        DBG_LOG("\r\nwarning detect: disable or invalid mode\r\n\0");
    }
}
void user_warning_detect_task(void *arg)
{
    //portTickType xLastWakeTime;
    //xLastWakeTime = xTaskGetTickCount();
    while(1)
    {      
        DBG_LOG("\r\nwarning_detect_handle = 0x%x\r\n\0",warning_detect_handle);
        vTaskDelay( 3600*S );
          
        //user_set_rewrite_timestamp();
        warning_detect_handler();
    }
}
void user_creat_warning_detect_task(void)
{
    BaseType_t ret;  
    ret = xTaskCreate( user_warning_detect_task, "warn", 512, NULL, 1, &warning_detect_handle );
    if(pdPASS != ret)
    {
        APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
    }  
    //DBG_LOG("\r\nwarning_detect_handle = 0x%x\r\n\0",warning_detect_handle);
}
void user_suspend_warning_detct_task(void)
{
    DBG_LOG("\r\nsuspend warning detect task\r\n\0");
    vTaskSuspend(warning_detect_handle);
    DBG_LOG("\r\nsuspend ok\r\n\0");
}
void user_resume_warning_detct_task(void)
{
    DBG_LOG("\r\nresume warning detect task\r\n\0");
    vTaskResume(warning_detect_handle);
    DBG_LOG("\r\nresume ok\r\n\0");
}
//end
