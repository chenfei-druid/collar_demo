#ifndef USER_ACC_ORIGIN_H
#define USER_ACC_ORIGIN_H

#include <stdint.h>
#include "drv_acc_KX022.h"
#include "user_app_set.h"
#include "alarm_clock.h"

#define     HAL_ACC_POINT_SAMPLES           DRV_KX022_FIFO_SAMPLES
#define     HAL_ACC_ONE_POINT_DATA_LEN      DRV_kx022_ONE_XYZ_DATA_LEN
#define     HAL_ACC_DATA_RATE               DRV_KX022_OUT_DATA_RATE_DEFALT
#define     HAL_ACC_AXIS_SIGN_BIT           DRV_KX022_SIGN_BIT      //each axis data's sign bit
#define     HAL_ACC_AXIS_VALUE_MASK         DRV_KX022_VALUE_MASK    //the max valid value of each axis
#define     ACC_TIME_RANGE_PAIR_NUM_MAX     24
typedef enum
{
    ACC_SAMPLE_START_TYPE = 0,
    ACC_SAMPLE_END_TYPE,
    ACC_SAMPLE_MAX_TYPE
}acc_sample_time_flag;

typedef enum
{
    ACC_TASK_MSG_TYPE_MIN = 0,
    ACC_TASK_MSG_TYPE_COLLECT,
    ACC_TASK_MSG_TYPE_SET,
    ACC_TASK_MSG_TYPE_MAX
}acc_task_msg_type;

typedef enum
{
    ACC_SAMPLE_MODE_DISABLE = 0,
    ACC_SAMPLE_MODE_BASE_ON_UNIXTIME,   //unix timestamp
    ACC_SAMPLE_MODE_BASE_ON_MINUTE,
    ACC_SAMPLE_MODE_BASE_ON_HOUR,       //the sencond counter base on hour
    ACC_SAMPLE_MODE_BASE_ON_DAY,        //the sencond counter base on day
    ACC_SAMPLE_MODE_BASE_ON_MONTH,      //the sencond counter base on month
    ACC_SAMPLE_MODE_MAX
}acc_sample_mode;

typedef enum
{
    ACC_OUT_DATA_RATE_12_5H = 0,
    ACC_OUT_DATA_RATE_25H,
    ACC_OUT_DATA_RATE_50H,
    ACC_OUT_DATA_RATE_100H,
    ACC_OUT_DATA_RATE_200H,
    ACC_OUT_DATA_RATE_400H,
    ACC_OUT_DATA_RATE_800H,
    ACC_OUT_DATA_RATE_1600H,
    ACC_OUT_DATA_RATE_0_781H,
    ACC_OUT_DATA_RATE_1_563H,
    ACC_OUT_DATA_RATE_3_125H,
    ACC_OUT_DATA_RATE_6_25H,
}drv_out_data_rate_t;


typedef struct drv_kx022_point_xyz_s acc_origin_point_t;

//#if defined(DRV_ACC_KX022_SAMPLE_BITS) && (DRV_ACC_KX022_SAMPLE_BITS == ACC_KX022_SAMPLES_8_BIT)
/*
#if DRV_ACC_KX022_SAMPLES_8_BIT
typedef struct
{
    int8_t     x;
    int8_t     y;
    int8_t     z;
}acc_origin_point_t;

//#elif defined(DRV_ACC_KX022_SAMPLE_BITS) && (DRV_ACC_KX022_SAMPLE_BITS == ACC_KX022_SAMPLES_16_BIT)
#else
typedef struct
{
    int16_t     x;
    int16_t     y;
    int16_t     z;
}acc_origin_point_t;
#endif
*/
typedef struct
{
    uint8_t         point_buf[HAL_ACC_POINT_SAMPLES*HAL_ACC_ONE_POINT_DATA_LEN];
    uint32_t        point_nums;
}acc_snr_data_t;

/*
typedef struct  
{
    uint32_t            timestamp;
    acc_origin_point_t  buf[HAL_ACC_POINT_SAMPLES];
    
}acc_origin_record_t;
*/
void hal_acc_init(void);
void user_creat_acc_data_collect_task(void);
/*
bool user_set_acc_sample_alarm(user_time_range_t      const * p_time_range_t, 
                               alarm_clock_run_type_t const   alarm_mode, 
                               uint8_t                        time_pair_num
                              );
*/
/*
uint32_t user_origin_setting(user_time_range_t const * p_time_range_t, 
                                   uint32_t const mode, 
                                   uint8_t const time_pair_num
                                  );
*/
void user_acc_send_collect_semaphore(void);
void user_acc_send_setting_semaphore(void);
bool hal_acc_get_status(void); 
bool hal_acc_start_collect(void);
bool hal_acc_stop_collect(void);
//void user_acc_send_queue_to_task(uint8_t type);
//void user_acc_send_queue_to_task(uint8_t *p_buf,uint8_t len);
void user_acc_set(void);
void user_acc_trans_data(uint8_t *p_buf, uint8_t len);
void user_creat_test_acc_data_task(void);
    
#endif	//ORIGIN


