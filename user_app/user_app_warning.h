#ifndef USER_APP_WARNING_H
#define USER_APP_WARNING_H

#include "stdint.h"
#include "stdbool.h"
#include "user_app_env.h"
#include "user_app_bhv.h"
#include "behavior.h"

#define     WARNING_LOW_BATTERY_VALUE_DEFAULT       3600
#define     WARNING_LOW_BATTERY_PERCENT_DEFAULT     10
#define     WARNING_STATIC_VALUE_DEFAULT            5
#define     WARNING_EXCESS_ACTIVITY_VALUE_DEFAULT   1000
#define     WARNING_EXCESS_TEMP_VALUE_DEFAULT       800
#define     WARNING_DEV_DES_LUX_VALUE_DEFAULT       5000

#define     WARNING_ONCE_UPLOAD_RECORD_NUM          6//(224 / sizeof(warning_record_t))
#define     WARNING_DETECT_INTERVAL_DEFAULT         3600//SECOND(3600)        

typedef enum  
{
	WARNING_TYPE_UNKNOWN      		    = 1,        // ??????
	WARNING_TYPE_BOOT         		    = 2,        // startup warning
	WARNING_TYPE_LOW_BATTERY   		    = 3,        // low battery warning
	WARNING_TYPE_STATIC       		    = 4,        // 静止报警
	//WARNING_TYPE_GEO_FENCE_IN   		= 5,        // ??????
	//WARNING_TYPE_GEO_FENCE_OUT  		= 6,        // ??????
	//WARNING_TYPE_DEVICE_REMOVE       	= 7,        // 设备拆除报警
	WARNING_TYPE_ACTIVITY_EXCESS        = 8,        // 运动过量报警
	WARNING_TYPE_TEMPERATURE_EXCESS     = 9,        // 温度超标报警
	//WARNING_TYPE_DEVICE_DESTROY       	= 10,        // 开盖报警
    WARNING_TYPE_MAX,
}warning_type;

typedef enum
{
    WARNING_MODE_DISABLE      		    = 0,
    WARNING_MODE_GPRS,
    WARNING_MODE_SHORT_MESSAGE,
    WARNING_MODE_GPRS_AND_MESSAGE,
    WARNING_MODE_UNKNOWN_MAX
}warning_mode;
typedef struct
{
    uint32_t        timestamp;
    warning_type    type;
    env_record_t    env;
}warning_record_t;

typedef struct
{
    uint32_t        timestamp;
    warning_type    type;
    env_record_t    env;    
}warning_req_msg_t;
typedef struct
{
    int32_t             activity;
    env_record_t        env;
    Behavior_Record_t   bhv;  
}warning_infor_t;

typedef struct
{
    warning_mode    mode;    
}warning_setting_t;


bool app_warning_rsp_parse_handle(uint8_t *p_rsp, int32_t len);
//bool user_warning_infor_upload(trans_interface_type chn_type);
bool user_warning_infor_upload(uint8_t * encode_buf, uint32_t encode_buf_size, 
                  uint8_t * send_buf, uint32_t send_buf_size,  
                  trans_interface_type chn_type
                 );
void user_creat_warning_detect_task(void);
void user_suspend_warning_detct_task(void);
void user_resume_warning_detct_task(void);
void user_warning_mode_set(warning_setting_t *p_set);
#endif  //USER_APP_WARNING_H
