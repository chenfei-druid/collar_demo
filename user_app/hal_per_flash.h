
#ifndef     HAL_PER_FLASH_H
#define     HAL_PER_FLASH_H

#include <stdint.h>
#include <stdbool.h>

//the partition quantity defined by user
#define     USER_PARTITION_QUANTITY           5       
#define     USER_EACH_TYPE_RECORD_NUMBERS_MAX           50

typedef enum
{
    USER_MEMORY_PARTITION_RECORD_SETTING = 0,
    USER_MEMORY_PARTITION_RECORD_BEHAVIOR,
    USER_MEMORY_PARTITION_RECORD_ENVIRONMENT,
    USER_MEMORY_PARTITION_RECORD_ESTRUS,
    
}part_type_n;

typedef struct
{
    //uint32_t    record_cnt;
    //uint32_t    byte_cnt;
    uint32_t    record_read;
    uint32_t    record_write;
    uint32_t    p_write_offset;            //current write data address
    uint32_t    p_read_offset;             //current read address
    //uint32_t    p_end;
    
    uint8_t     p_record_len[USER_EACH_TYPE_RECORD_NUMBERS_MAX];
    
}hal_record_index_t;
    
typedef struct
{
    uint32_t    p_base;             //the partition base address
    uint32_t    part_size;          //the partition size
    uint16_t    part_type;         //record numbers, it indecates that how many records will be collected
    hal_record_index_t  record_index_t;
}hal_part_handl_t;

typedef struct
{
    uint32_t part_count;
    hal_part_handl_t    part_infor_t[USER_PARTITION_QUANTITY];
}hal_main_index_t;

typedef struct
{
    uint32_t    start_address;
    uint32_t    end_address;
    uint32_t    size;
    uint32_t    part_num;
}hal_flash_part_infor_t;

#define     DEFAULT_RECORD_INDEX_CONFIG(arry_size) \
{                                                  \
    .count = 0,                                    \
    .p_record_len = uint8_t len_arry[arry_size],   \
}   

bool hal_per_flash_write(uint32_t address, uint32_t len, uint8_t * p_data);
bool hal_per_flash_read(uint32_t address, uint32_t len, uint8_t * p_buf);
uint32_t hal_per_part_read_record_infor(uint32_t const part_type, hal_record_index_t * p_rec_index_t);
uint32_t hal_per_part_read_continuous_records(uint32_t const part_type, uint8_t *p_buf, uint32_t record_len);
uint32_t hal_per_part_read_specified_record(uint32_t const part_type, uint8_t *p_buf, uint32_t record_number);
uint32_t hal_per_part_read_specified_records(uint32_t const part_type, uint8_t *p_buf, uint32_t record_number, uint32_t record_len);
uint32_t hal_per_part_write_record(uint32_t const part_type, uint8_t * p_data, uint32_t byte_len);
bool hal_per_flash_partition(void);


#endif      //HAL_PER_FLASH_H


