

/** 
 * user_pb_decode_callback.c
 *
 * @group:       collar project
 * @author:      Chenfei
 * @create time: 2018/5/4
 * @version:     V0.0
 *
 */
 
#include    "user_app_log.h"
#include    "user_pb_callback.h"
#include    "user_app_log.h"
/***********************************************defined functions**********************************************/
 /**
* @functionname : app_encode_repeated_var_string 
* @description  : function for encode a strings data
* @input        : stream, encode stream,  
*               : fields: the origin fields
*               : str: the string data will be encoded
* @output       : none 
* @return       : true if successed
*/
bool user_app_encode_repeated_var_string(pb_ostream_t *stream, const pb_field_t *fields,  void * const *str)
{
    bool res;
    res = pb_encode_tag_for_field(stream, fields);
    //DBG_LOG("pb encode tag for string is  %s", (res? "successed":"failed"));
    res = pb_encode_string(stream,*str,strlen(*str));
    DBG_LOG("encode string \"%s\" is %s\r\n", *str, (res? "successed":"failed"));
    //PRINT_S("pb encode string over\r\n\0");
    return res;
}
/**
* @functionname : app_decode_repeated_var_string 
* @description  : function for decode a strings data
* @input        : stream, encode stream,  
*               : fields: the origin fields
*               : str: the string data will be encoded
* @output       : none 
* @return       : true if successed
*/
bool user_app_decode_repeated_var_string(pb_istream_t *stream, const pb_field_t *field, void **str)
{
    pb_byte_t   cache_buf[stream->bytes_left];
    uint8_t     byte_len = stream->bytes_left;
    bool res = false;
    res = pb_read(stream,cache_buf, stream->bytes_left);
    DBG_LOG("\r\ndecode string \"%s\" %s\r\n",cache_buf,(res? "successed":"failed"));
    if(res == true)
    {
        memcpy(*str, cache_buf, byte_len);
        return true;
    }
    else
    {
        *str = NULL;
        return false;
    }
}
/**
* @functionname : user_app_decode_simple_rsp 
* @description  : function for decode simple response message
* @input        : p_buf: input by-decode buffer
*               : len: input by-decode data length
* @output       : p_rsp_t, decode out struct,  
*               : dev_id: device id buffer 
* @return       : true if successed
*/
bool user_app_decode_simple_rsp(protocol_simple_rsp_t * const p_rsp_t, 
                                        char * const dev_id, 
                                        uint8_t const * const p_buf, 
                                        uint32_t const len)
{
    bool res;
    pb_istream_t i_stream ;
        
    p_rsp_t->Iden.DeviceID.funcs.decode   = &user_app_decode_repeated_var_string;
    p_rsp_t->Iden.DeviceID.arg            = dev_id;
   
    i_stream = pb_istream_from_buffer(p_buf,len);
    res = pb_decode(&i_stream,protocol_simple_rsp_fields,p_rsp_t);
    if(res == true)
    {
        DBG_LOG("simple response decode successed\r\n");

        return true;
    }
    else
    {
        DBG_LOG("simple response decode failed\r\n");
        return false;
    }
}
//end

