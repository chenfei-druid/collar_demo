#ifndef KX022_REGISTORS_H
#define KX022_REGISTORS_H

#define     REGISTER_XHPL           0x00
#define     REGISTER_XHPH           0x01
#define     REGISTER_YHPL           0x02
#define     REGISTER_YHPH           0x03
#define     REGISTER_ZHPL           0x04
#define     REGISTER_ZHPH           0x05
#define     REGISTER_XOUTL          0x06
#define     REGISTER_XOUTH          0x07
#define     REGISTER_YOUTL          0x08
#define     REGISTER_YOUTH          0x09
#define     REGISTER_ZOUTL          0x0A
#define     REGISTER_ZOUTH          0x0B
#define     REGISTER_COTR           0x0C
#define     REGISTER_WHO_AM_I       0x0F
#define     REGISTER_TSCP           0x10
#define     REGISTER_TSPP           0x11
#define     REGISTER_INS1           0x12
#define     REGISTER_INS2           0x13        //This Register tells witch function caused an interrupt
#define     REGISTER_INS3           0x14
#define     REGISTER_STATUS         0x15
#define     REGISTER_INT_REL        0x17
#define     REGISTER_CNTL1          0x18
#define     REGISTER_CNTL2          0x19
#define     REGISTER_CNTL3          0x1A
#define     REGISTER_ODCNTL         0x1B
#define     REGISTER_INC1           0x1C
#define     REGISTER_INC2           0x1D
#define     REGISTER_INC3           0x1E
#define     REGISTER_INC4           0x1F
#define     REGISTER_INC5           0x20
#define     REGISTER_INC6           0x21
#define     REGISTER_TILT_TIMER     0x22
#define     REGISTER_WUFC           0x23
#define     REGISTER_TDTRC          0x24
#define     REGISTER_TDTC           0x25
#define     REGISTER_TTH            0x26
#define     REGISTER_TTL            0x27
#define     REGISTER_FTD            0x28
#define     REGISTER_STD            0x29
#define     REGISTER_TLT            0x2A
#define     REGISTER_TWS            0x2B
#define     REGISTER_ATH            0x30
#define     REGISTER_TILT_ANGLE_LL  0x32
#define     REGISTER_TILT_ANGLE_HL  0x33
#define     REGISTER_HYST_SET       0x34
#define     REGISTER_LP_CNTL        0x35
#define     REGISTER_BUF_CNTL1      0x3A
#define     REGISTER_BUF_CNTL2      0x3B
#define     REGISTER_BUF_STATUS1    0x3C
#define     REGISTER_BUF_STATUS2    0x3D
#define     REGISTER_BUF_CLEAR      0x3E
#define     REGISTER_BUF_READ       0x3F
#define     REGISTER_SELF_TEST      0x60


#define     READ                    0x80
#define     WRITE                   0x00

#endif    //KX022_REGISTORS_H
