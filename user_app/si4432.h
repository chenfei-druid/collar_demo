/**
 * drv_rf_si4432.h
 * Date:    2018/3/28
 * Author:  Chenfei
 * 
 * @brief si4432 registor define.
 *
 */

#define   READ                0         // for SPI RW
#define   WRITE               1

#define   WR_REG              0x80      /*!< �� 7 λΪ�Ĵ�����ַ*/
#define   RD_REG              0x00

#define   DeviceTypeCode_00   0x00      /*!<  Device Type Code */
#define   VersionCode_01      0x01      /*!<  Version Code */

#define   DeviceStatus_02     0x02      /*!<  Device Status Reg*/


#define   InterruptStatus1_03 0x03      /*!<  Interrupt/Status 1 Reg */
#define   InterruptEnable1_05 0x05      /*!<  Interrupt Enable 1 Reg */

// InterruptStatus1 & InterruptEnable1 :
#define   FIFO_ERR            0x80      /*!<  FIFO Underflow/Overflow Error. */
#define   TXFIFO_A_FULL       0x40      /*!<  TX FIFO Almost Full */
#define   TXFIFO_A_EMPTY      0x20      /*!<  TX FIFO Almost Empty */
#define   RXFIFO_A_FULL       0x10      /*!<  RX FIFO Almost Full */
#define   EXT_INT             0x08      /*!<  External Interrupt. */
#define   PACKET_SENT         0x04      /*!<  Packet Sent Interrupt */
#define   PACKET_RECEIVED     0x02      /*!<  Valid Packet Received */
#define   CRC_ERR             0x01      /*!<  CRC Error */

#define   InterruptStatus2_04 0x04      /*!<  Interrupt/Status 2 Reg */
#define   InterruptEnable2_06 0x06      /*!<  Interrupt Enable 2 Reg */
// InterruptStatus2 & InterruptEnable2:
#define   SYNC_DETECTED       0x80
#define   PREAMBLE_VALID      0x40
#define   PREAMBLE_INVALID    0x20
#define   RSSI_OK             0x10
#define   WAKEUP_TIMER        0x08
#define   LOW_BATTERY         0x04
#define   CHIP_READY          0x02
#define   POWER_ON_RESET      0x01

#define   OpModeFuncCtrl1_07  0x07      /*!<   Operating Mode and Function Control 1 Reg */
// OpMode_FuncCtr1 :
#define   SOFT_RESET          0x80      /*!<Software Register Reset Bit.*/
#define   EN_LOWBAT_DETECT    0x40
#define   EN_WAKEUP_TIMER     0x20
#define   OSC_RC              0x00
#define   OSC_32kHz           0x10
#define   TX_ON               0x08      /*!< TX on in Manual Transmit Mode*/
#define   RX_ON               0x04      /*!< RX on in Manual Receiver Mode. */
#define   PLL_ON              0x02      /*!< TUNE Mode (PLL is ON). */
#define   XT_ON               0x01      /*!< READY Mode (Xtal is ON)*/

#define   OpModeFuncCtrl2_08  0x08      /*!< Operating Mode and Function Control 2 Reg */
// OpMode_FuncCtrl2:
#define   RX_MULTI_PACKET     0x10
#define   AUTO_TX             0x08
#define   EN_LOW_DUTY         0x04
#define   RXFIFO_CLEAR_STEP1  0x02
#define   RXFIFO_CLEAR_STEP2  0x00
#define   TXFIFO_CLEAR_STEP1  0x01
#define   TXFIFO_CLEAR_STEP2  0x00


#define   Osc30MHzLoadCap_09  0x09      /*!< 30 MHz Crystal Oscillator Load Capacitance Reg */
// Cint = 1.8 pF + 0.085 pF x xlc[bit6:0] + 3.7 pF x xtalshift [bit7]

#define   Out2MCU_Clock_0A    0x0A      /*!<  Microcontroller Output Clock Reg */
// Out2MCU_Clock :
#define   OUT0CLK_BEFORE_SHUT 0x00
#define   OUT128_BEFORE_SHUT  0x10
#define   OUT256_BEFORE_SHUT  0x20
#define   OUT512_BEFORE_SHUT  0x30
#define   EN_LOW_FREQ_CLK     0x08
#define   OUT_30MHz           0x00
#define   OUT_15MHz           0x01
#define   OUT_10MHz           0x02
#define   OUT_4MHz            0x03
#define   OUT_3MHz            0x04
#define   OUT_2MHz            0x05
#define   OUT_1MHz            0x06
#define   OUT_32768Hz         0x07

#define   GPIOConfig0_0B      0x0B       /*!<  GPIO Configuration 0 Reg */
#define   GPIOConfig1_0C      0x0C       /*!<  GPIO Configuration 1 Reg */
#define   GPIOConfig2_0D      0x0D       /*!<  GPIO Configuration 2 Reg */
// GPIOConfig :
#define   GPIO_DRV_L0         0x00       /*!<  GPIO out Drive Strength  */
#define   GPIO_DRV_L1         0x40
#define   GPIO_DRV_L2         0x80
#define   GPIO_DRV_L3         0xC0

#define   PULLUP_200K         0x20

#define   CLK_OUT             0x00
#define   WAKEUP_OUT          0x01
#define   LOWBAT_OUT          0x02
#define   DIRECT_DIN          0x03
#define   EXTI_FALL           0x04
#define   EXTI_RISE           0x05
#define   EXTI_CHANGE         0x06
#define   ADC_IN              0x07
//#define   RESERVED            0x08
//#define   RESERVED            0x09
#define   DIRECT_DOUT         0x0A
//#define   RESERVED            0x0B
//#define   RESERVED            0x0C
//#define   RESERVED            0x0D
#define   REF_V_OUT           0x0E
#define   DATACLK_OUT         0x0F
#define   TXDATA_IN           0x10
#define   RETRANS_REQ         0x11
#define   TXSTAT_OUT          0x12
#define   TXFIFO_AFULL_OUT    0x13
#define   RXDATA_OUT          0x14
#define   RXSTAT_OUT          0x15
#define   RXFIFO_AFULL_OUT    0x16
#define   ANT1_SWITCH         0x17
#define   ANT2_SWITCH         0x18
#define   VALID_PREAMBLE      0x19
#define   INVALID_PREAMBLE    0x1A
#define   SYNC_DETECTED1      0x1B
#define   CLR_CHENNEL         0x1C
#define   VCC                 0x1D
#define   GND                 0x1E

#define   IOPortConfig_0E     0x0E       /*!< I/O Port Configuration Reg */
// IOPortConfig:
#define   GPIO2_INTFLAG       0x40
#define   GPIO1_INTFLAG       0x20
#define   GPIO0_INTFLAG       0x10
#define   IRQ_ON_SDO          0x08
#define   GPIO2_VAL           0x04
#define   GPIO1_VAL           0x02
#define   GPIO0_VAL           0x01

#define   ADCConfig_0F        0x0F       /*!< ADC Configuration Reg */
// ADCConfig:
#define   ADC_START           0x80

#define   TEMPERATURE         0x00
#define   GPIO0_SINGLE        0x10
#define   GPIO1_SINGLE        0x20
#define   GPIO2_SINGLE        0x30
#define   GPIO0_GPIO1         0x40
#define   GPIO1_GPIO2         0x50
#define   GPIO0_GPIO2         0x60
#define   T0_GND              0x70

#define   REF_1V2             0x00
#define   REF_VDD_DIV3        0x80
#define   REF_VDD_DIV2        0xC0

#define   ADCSensorAmpOffset_10 0x10    /*!<  ADC Sensor Amplifier Offset Reg */
#define   ADCValue_11         0x11      /*!<  ADC Value Reg */

#define   TempCalibration_12  0x12      /*!<  Temperature Sensor Calibration Reg */
#define   TempValueOffset_13  0x13      /*!<  Temperature Value Offset Reg*/

#define   WakeUpTimerPeriod1_14 0x14      /*!<  Wake-Up Timer Period 1 Reg */
#define   WakeUpTimerPeriod2_15 0x15      /*!<  Wake-Up Timer Period 2 Reg */
#define   WakeUpTimerPeriod3_16 0x16      /*!<  Wake-Up Timer Period 3 Reg */

#define   WakeUpTimerValue1_17  0x17      /*!<  Wake-Up Timer Value 1 Reg */
#define   WakeUpTimerValue2_18  0x18      /*!<  Wake-Up Timer Value 2 Reg */

#define   LowDutyModeDuration_19 0x19      /*!<  Low-Duty Cycle Mode Duration Reg  */

#define   LowBatThreshold_1A     0x1A      /*!<  Low Battery Detector Threshold Reg */
#define   BatVoltageLevel_1B     0x1B      /*!<  Battery Voltage Level Reg */

#define   IFFilterBandwidth_1C          0x1C          /*!<  IF Filter Bandwidth Reg*/
#define   AFCLoopGearshiftOverride_1D   0x1D          /*!<  AFC Loop Gearshift Override Reg*/

#define   AFCTimingControl_1E           0x1E          /*!<  AFC Timing Control */
#define   ClkRcvGearshiftOverride_1F    0x1F          /*!<  Clock Recovery Gearshift Override Reg*/

#define   ClkRcvOversamplingRate_20     0x20          /*!<  Clock Recovery Oversampling Rate Reg*/
#define   ClkRcvOffset2_21              0x21          /*!<  Clock Recovery Offset 2  Reg*/
#define   ClkRcvOffset1_22              0x22          /*!<  Clock Recovery Offset 1  Reg*/
#define   ClkRcvOffset0_23              0x23          /*!<  Clock Recovery Offset 0  Reg*/
#define   ClkRcvTimingLoopGain1_24      0x24          /*!<  Clock Recovery Timing Loop Gain 1  Reg*/
#define   ClkRcvTimingLoopGain0_25      0x25          /*!<  Clock Recovery Timing Loop Gain 0  Reg*/

#define   RSSI_Value_26                 0x26          /*!<  Received Signal Strength Indicator */
#define   RSSI_Threshold_27             0x27          /*!<  RSSI Threshold for Clear Channel Indicator */

#define   RSSI_onAntenna1_28            0x28          /*!<  Measured RSSI Value on Antenna 1. */
#define   RSSI_onAntenna2_29            0x29          /*!<  Measured RSSI Value on Antenna 2 */

#define   AFCLimiter_2A                 0x2A          /*!<  AFC Limiter */
#define   AFCCorrectionMSBs_2B          0x2B          /*!<  AFC Correction (MSBs) */
#define   OOKCounterValue1_2C           0x2C          /*!<  OOK Counter Value 1 */
#define   OOKCounterValue2_2D           0x2D          /*!<  OOK Counter Value 2 */
#define   SlicerPeakHolder_2E           0x2E          /*!<  Slicer Peak Holder */
//��0x2f

#define   DataAccessControl_30          0x30          /*!<  Data Access Control */

#define   EZMAC_Status_31               0x31          /*!<  EZMAC Status Reg */
// EZMACStatus:
#define   CRC_ALL_ONE                   0x40
#define   PACKET_SEARCHING              0x20
#define   PACKET_RECEIVING              0x10
#define   PACKET_RECEIVED1              0x08
#define   CRC_ERR1                       0x04
#define   PACKET_TRANSMITTING           0x02
#define   PACKET_SENT1                   0x01

#define   HeaderControl1_32             0x32          /*!<  Header Control 1 */

#define   HeaderControl2_33             0x33          /*!<  Header Control 2 */

#define   PreambleLength_34             0x34          /*!<  Preamble Length */

#define   PreambleDetectionControl1_35  0x35          /*!<  Preamble Detection Control 1 */

#define   SyncWord3_36                  0x36          /*!<  Synchronization Word 3 */
#define   SyncWord2_37                  0x37          /*!<  Synchronization Word 2 */
#define   SyncWord1_38                  0x38          /*!<  Synchronization Word 1 */
#define   SyncWord0_39                  0x39          /*!<  Synchronization Word 0 */

#define   TransHeader3_3A               0x3A          /*!<  Transmit Header 3 */
#define   TransHeader2_3B               0x3B          /*!<  Transmit Header 2 */
#define   TransHeader1_3C               0x3C          /*!<  Transmit Header 1 */
#define   TransHeader0_3D               0x3D          /*!<  Transmit Header 0 */

#define   TxPacketLength_3E             0x3E          /*!<  Tx Packet Length */

#define   CheckHeader3_3F               0x3F          /*!<  Check Header 3 */
#define   CheckHeader2_40               0x40          /*!<  Check Header 2 */
#define   CheckHeader1_41               0x41          /*!<  Check Header 1 */
#define   CheckHeader0_42               0x42          /*!<  Check Header 0 */

#define   HeaderEnable3_43              0x43          /*!<  Header Enable 3 */
#define   HeaderEnable2_44              0x44          /*!<  Header Enable 2 */
#define   HeaderEnable1_45              0x45          /*!<  Header Enable 1 */
#define   HeaderEnable0_46              0x46          /*!<  Header Enable 0 */

#define   ReceivedHeader3_47            0x47          /*!<  Received Header 3 */
#define   ReceivedHeader2_48            0x48          /*!<  Received Header 2 */
#define   ReceivedHeader1_49            0x49          /*!<  Received Header 1 */
#define   ReceivedHeader0_4A            0x4A          /*!<  Received Header 0 */

#define   ReceivedPacketLength_4B       0x4B          /*!<  Received Packet Length */

#define   ADC8Control_4F                0x4F          /*!<  ADC8 Control */


#define   ChannelFilterCoAddr_60        0x60          /*!<  Channel Filter Coefficient Address */
#define   Cry_Osc_POR_Control_62        0x62          /*!<  Crystal Oscillator/Power-on-Reset Control */

#define   AGC_Override1_69              0x69          /*!<  AGC Override 1 */

#define   TX_Power_6D                   0x6D          /*!<   TX Power */

#define   TX_Data_Rate1_6E              0x6E          /*!<  TX Data Rate 1 */
#define   TX_Data_Rate0_6F              0x6F          /*!<  TX Data Rate 0 */

#define   Modulation_Mode_Control1_70   0x70          /*!<  Modulation Mode Control 1 */
#define   Modulation_Mode_Control2_71   0x71          /*!<  Modulation Mode Control 2 */
#define   Frequency_Deviation_72        0x72          /*!<  Frequency Deviation */

#define   Frequency_Offset1_73          0x73          /*!<  Frequency Offset 1 */
#define   Frequency_Offset2_74          0x74          /*!<  Frequency Offset 2 */

#define   FrequencyBandSelect_75        0x75          /*!<   Frequency Band Select */
#define   NominalCarrierFreqHigh_76     0x76          /*!<  Nominal Carrier Frequency high*/
#define   NominalCarrierFreqLow_77      0x77          /*!<  Nominal Carrier Frequency low */

#define   FreqHoppingChannelSelect_79   0x79          /*!<  Frequency Hopping Channel Select */
#define   FrequencyHoppingStepSize_7A   0x7A          /*!<  Frequency Hopping Step Size */

#define   TX_FIFO_Control1_7C           0x7C          /*!<  TX FIFO Control 1 */
#define   TX_FIFO_Control2_7D           0x7D          /*!<  TX FIFO Control 2 */
#define   RX_FIFO_Control_7E            0x7E          /*!<  RX FIFO Control */

#define   FIFO_Access_7F                0x7F          /*!<  FIFO Access */


//////////////value for config registors///////////////

//TX POWER
#define   TX_PWR_RESET                  0x18          /*!<   TX Power Default = min*/
#define   TX_PWR_0                      0
#define   TX_PWR_1                      1
#define   TX_PWR_2                      2
#define   TX_PWR_3                      3
#define   TX_PWR_4                      4
#define   TX_PWR_5                      5
#define   TX_PWR_6                      6
#define   TX_PWR_7                      7
#define   TX_PWR_MID                    0x04          /*!<   TX Power Middle */
#define   TX_PWR_MAX                    0x07          /*!<   TX Power Maxim */

// DeviceStatus:
#define   FIFO_OVER           0x80      /*!<  RX/TX FIFO Overflow Status. */
#define   FIFO_UNDER          0x40      /*!<  RX/TX FIFO Underflow Status. */
#define   RXFIFO_EMPTY        0x20      /*!<  RX FIFO Empty Status. */
#define   HEADER_ERR          0x10      /*!<  Header Error Status. */
#define   FREQ_ERR            0x08      /*!<  Frequency Error Status. */
#define   POWER_STAT          0x03      /*!<  Chip Power State bit1 & bit0 */
// POWER_STAT:
#define   IDLE_STAT           0x00
#define   RX_STAT             0x01
#define   TX_STAT             0x02


#define   TX_PWR_0                      0
#define   TX_PWR_1                      1
#define   TX_PWR_2                      2
#define   TX_PWR_3                      3
#define   TX_PWR_4                      4
#define   TX_PWR_5                      5
#define   TX_PWR_6                      6
#define   TX_PWR_7                      7
#define     HIGH_SPEED              0x00
#define     LOW_SPEED               0x20

#define     DATA_RATE_CTR           LOW_SPEED
#define     DATA_RATE               33554
#define     DATA_RATE_LOW           DATA_RATE&0xff
#define     DATA_RATE_HIGH          (DATA_RATE>>8)&0xff
#define     TX_PWR                  TX_PWR_7


