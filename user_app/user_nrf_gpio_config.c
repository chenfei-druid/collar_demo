/**--------------------------------------------------------------------------------------------------------
** Commpany     :       Druid
** Created by   :		chenfei
** Created date :		2017-8-11
** Version      :	    1.0
** Descriptions :		peripheral interface config
**--------------------------------------------------------------------------------------------------------*/
#include "nrf_drv_gpiote.h"
#include "nrf_gpio.h"
#include "drv_acc_KX022.h"
#include "user_acc_hal.h"
#include "SEGGER_RTT.h"
#include "SEGGER_RTT_Conf.h"



