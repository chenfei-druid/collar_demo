#ifndef     USER_APP_ENV_H
#define     USER_APP_ENV_H

#include    "stdint.h"
#include    "stdbool.h"
#include    "user_data_pkg.h"

#define     ENV_SAMPLE_INTERVAL_DEFAULT     3600
typedef struct
{
    unsigned int    timestamp;
    int             battery_percent;
    int             battery_voltage;
    int             temperature;
    int             illumination;
}env_record_t;

typedef struct
{
    int32_t     interval;
    int32_t     mode;
}env_setting_t;

void user_env_init(void);
void user_creat_env_detect_task(void);
bool user_env_set_sample_interval(int32_t interval);
bool user_env_set_sample_mode(int32_t mode);
//bool user_env_upload_data(trans_interface_type chn_type);
bool user_env_upload_data(uint8_t * encode_buf, uint32_t encode_buf_size, 
                  uint8_t * send_buf, uint32_t send_buf_size,  
                  trans_interface_type chn_type
                 );
void user_enb_give_upload_semaphore(void);
bool user_env_get_sample(env_record_t   * const env_infor);
void user_suspend_env_detect_task(void);
void user_resume_env_detect_task(void);
bool app_env_rsp_parse_handle(uint8_t const *  const p_rsp, int32_t len);
bool user_env_setting(env_setting_t setting);
void user_env_memory_test(void);

#endif      //USER_APP_ENV_H

