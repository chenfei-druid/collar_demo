
/** 
 * user_app_handler.c
 *
 * @group:       neck strap project
 * @author:      Chenfei
 * @create time: 2018/4/16
 * @version:     V0.0
 *
 */
#include "user_app_hal.h"
#include "user_data_struct.h"
#include "alltypes.pb.h"
#include "test_helpers.h"
#include "unittests.h"
#include "SEGGER_RTT.h"
#include "SEGGER_RTT_Conf.h"
#include "Define.pb.h"
#include "hal_per_flash.h"
#include "user_data_struct.h"
#include "Record.h"
#include "behavior.h"
#include "user_app_get_record.h"
#include "user_app_hal.h"
#include "user_data_pkg.h"



bool app_send_data(void * p_buffer, uint32_t length, trans_interface_type tx_type)
{
    if(tx_type == NB_DATA_TYPE)
    {
         
    }
    else if(tx_type == BLE_DATA_TYPE)
    {
        
    }
}
bool app_send_behavior_record_handler(identity_msg p_identity_msg)
{
    uint32_t record_num;
    //Behavior_Record_t       *bhv_records_buf[record_num]; 
    protocol_behavior2_t    *pt_bhv_t[record_num];
    //pkg_prot_frame_t        *p_prot_frame_t;
    uint16_t data_len;
    pkg_final_frame_trans_t *p_final_frame_t;
    
    uint8_t p_record_buf[record_num *sizeof(protocol_behavior2_t)+30];  //sizeof(identity_msg) = 30 bytes
    
    //user_app_get_behavior_record(bhv_records_buf,pt_bhv_t,record_num);
    user_app_get_behavior_record(pt_bhv_t,record_num);
    protocol_behavior2_req_t * p_bhv_req_t;
    
    //p_bhv_req_t->Iden.UUID  =   p_identity_msg.uuid;
    
    user_data_pb_bhv_encode(pt_bhv_t,record_num,p_identity_msg,p_record_buf,&data_len);
    
    user_add_head_to_protbuf(p_final_frame_t->frame, p_record_buf, data_len);
    
    user_package_final_frame(p_final_frame_t,p_final_frame_t->frame,data_len+sizeof(p_final_frame_t->head));
    
    
    return true;
}

