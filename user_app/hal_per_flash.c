/** 
 * hal_per_flash.c
 *
 * @group:       nack_trap project
 * @author:      Chenfei
 * @createdate: 2018/4/8
 * @modifydate: 2018/4/8
 * @version:     V0.0
 *
 * The file is function for saving some user datas into memory, user must config some parameters before use it.
 *
 * Config componets are as below :
 *
 * 1. @ref part_type_n : memory partition type;
 * 2. @ref m_part_index_list : user record index list;
 * 3. each independent partition size ;
 * 
 */
#include "hal_per_flash.h"
#include "W25Q.h"
#include <stdio.h>


#define     USER_MEMORY_PAGE_SIZE               DRV_GD25Q127_ONE_PAGE_HAS_BYTES
#define     USER_MEMORY_SECTOR_SIZE             DRV_GD25Q127_ONE_SECTOR_HAS_BYTES

#define     USER_MEMORY_BASE_SECTOR             0
#define     USER_INDEX_SECTORS                  10

//each independent partition size 
#define     USER_RECORD_BEHAVIOR_SECTORS        25    //one behavior record has sectors, one sector has 4K bytes
#define     USER_RECORD_ENVIRONMENT_SECTORS     5       //
#define     USER_RECORD_ESTRUS_SECTORS          2       //
#define     USER_RECORD_SETTING_SECTORS         2       //

#define     USER_ORIG_DATA_SECTORS              DRV_GD25Q127_TOTAL_SECTORS - \
                                                (USER_INDEX_SECTOR + \
                                                 USER_RECORD_BEHAVIOR_SECTORS + \
                                                 USER_RECORD_ENVIRONMENT_SECTORS + \
                                                 USER_RECORD_ESTRUS_SECTORS + \
                                                 USER_RECORD_SETTING_SECTORS + \
                                                 )


//structor for behavior record, the part size id defined @ref USER_RECORD_BEHAVIOR_SECTORS
hal_part_handl_t        m_bhv_index_t={
                                            .p_base     = 0,
                                            .part_size  = USER_RECORD_BEHAVIOR_SECTORS,
                                            .part_type   = USER_MEMORY_PARTITION_RECORD_BEHAVIOR,
                                            .record_index_t = {0},
                                        }; 

//structor for environment record, the part size id defined @ref USER_RECORD_ENVIRONMENT_SECTORS
hal_part_handl_t        m_env_index_t={
                                            .part_size  = USER_RECORD_ENVIRONMENT_SECTORS,
                                            .part_type   = USER_MEMORY_PARTITION_RECORD_ENVIRONMENT,
                                            .record_index_t = {0},
                                        }; 
//structor for environment record, the part size id defined @ref USER_RECORD_ENVIRONMENT_SECTORS
hal_part_handl_t        m_estrus_index_t={
                                            .part_size = USER_RECORD_ESTRUS_SECTORS,
                                            .part_type   = USER_MEMORY_PARTITION_RECORD_ESTRUS,
                                            .record_index_t = {0},
                                         }; 
//structor for setting parameter, the part size id defined @ref USER_RECORD_SETTING_SECTORS
hal_part_handl_t        m_setting_index_t={
                                            .part_size = USER_RECORD_SETTING_SECTORS,
                                            .part_type   = USER_MEMORY_PARTITION_RECORD_SETTING,
                                            .record_index_t = {0},
                                          }; 
//array for parttion list,
hal_part_handl_t       *m_part_index_list[]={
                                            &m_setting_index_t,
                                            &m_bhv_index_t,
                                            &m_env_index_t,
                                            &m_estrus_index_t
                                            };

bool hal_per_flash_write(uint32_t address, uint32_t len, uint8_t * p_data)
{
    if(W25Q_Lock())
    {
        if(W25Q_Write(address,len,p_data) == len)
        {
            return true;
        } 
        W25Q_Unlock();
    }
    return false;
}

bool hal_per_flash_read(uint32_t address, uint32_t len, uint8_t * p_buf)
{

    if(W25Q_Lock())
    {
        if(W25Q_Read(address,len,p_buf) == len)
        {
            return true;
        } 
        W25Q_Unlock();
    }
    return false;
}
uint32_t hal_per_part_read_record_infor(uint32_t const part_type, hal_record_index_t * p_rec_index_t)
{
    p_rec_index_t->p_read_offset  = m_part_index_list[part_type]->record_index_t.p_read_offset;
    p_rec_index_t->p_write_offset = m_part_index_list[part_type]->record_index_t.p_write_offset;
    p_rec_index_t->record_read    = m_part_index_list[part_type]->record_index_t.record_read;
    p_rec_index_t->record_write   = m_part_index_list[part_type]->record_index_t.record_write;
    return true;
}
/**
* @functionname : hal_per_part_read_specified_record 
* @description  : function for read one record in part type with any filed has stored
* @input        : part_type: partition type, the machine will write the datas according the type
*               : p_buf: a buffer save the data has been read 
* @output       : none 
* @return       : data length read
*/
uint32_t hal_per_part_read_continuous_records(uint32_t const part_type, uint8_t *p_buf, uint32_t record_len)
{
    bool ret;
//    uint32_t len_sum = 0;
    uint32_t read_len= 0;
    uint32_t read_address;
    //assure read bytes length
    if(record_len > (m_part_index_list[part_type]->record_index_t.record_write - m_part_index_list[part_type]->record_index_t.record_read))
    {
        //if cxceed the range has stored
        record_len = m_part_index_list[part_type]->record_index_t.record_write - m_part_index_list[part_type]->record_index_t.record_read;
        read_len = m_part_index_list[part_type]->record_index_t.p_write_offset - m_part_index_list[part_type]->record_index_t.p_read_offset ;
    }
    else
    {
        //the record length is within the range
        read_len = 0;
        for(uint32_t i=0; i<record_len; i++)
        {
            read_len += m_part_index_list[part_type]->record_index_t.p_record_len[i+m_part_index_list[part_type]->record_index_t.record_read];
        }
    }
    //set read start address
    read_address = m_part_index_list[part_type]->p_base + m_part_index_list[part_type]->record_index_t.p_read_offset;
    
    ret = hal_per_flash_read(read_address, read_len, p_buf);
    if(ret != true)
    {
        return 0;
    }
    //update read position 
    m_part_index_list[part_type]->record_index_t.record_read   += record_len;
    m_part_index_list[part_type]->record_index_t.p_read_offset += read_len;
    
    return read_len;
}
/**
* @functionname : hal_per_part_read_specified_record 
* @description  : function for read one record in part type with any filed has stored
* @input        : part_type: partition type, the machine will write the datas according the type
*               : p_buf: a buffer save the data has been read 
* @output       : none 
* @return       : data length read
*/
uint32_t hal_per_part_read_specified_record(uint32_t const part_type, uint8_t *p_buf, uint32_t record_number)
{
    bool ret;
    uint32_t len_sum = 0;
    uint32_t read_len= 0;
    uint32_t read_address;
    //search read start address according to record_number
    for(uint32_t i=0;i<record_number;i++)
    {
        len_sum += m_part_index_list[part_type]->record_index_t.p_record_len[i];
    }
    //set read start address
    read_address = m_part_index_list[part_type]->p_base + len_sum;
    read_len = m_part_index_list[part_type]->record_index_t.p_record_len[record_number];
    ret = hal_per_flash_read(read_address, read_len, p_buf);
    if(ret != true)
    {
        return 0;
    }
    return read_len;
}
/**
* @functionname : hal_per_part_read 
* @description  : function for read record in part type with any filed 
* @input        : part_type: partition type, the machine will write the datas according the type
*               : p_buf: a buffer save the data has been read 
*               : record_len: the record length will be read
* @output       : none 
* @return       : data length read
*/
uint32_t hal_per_part_read_specified_records(uint32_t const part_type, uint8_t *p_buf, uint32_t record_number, uint32_t record_len)
{
    bool ret;
    uint32_t byte_len = 0;
    
    uint32_t len_sum = 0;
    if(m_part_index_list[part_type]->record_index_t.record_write < (record_number + record_len))
    {
        //if record quantity has stored is less than the crecord will be read quantity
        record_len = m_part_index_list[part_type]->record_index_t.record_write - record_number;
    }
    //search read start address according to record_number
    for(uint32_t i=0;i<record_number;i++)
    {
        len_sum += m_part_index_list[part_type]->record_index_t.p_record_len[i];
    }    
    //set read start address
    uint32_t read_address = m_part_index_list[part_type]->p_base + len_sum;
    //search byte length of will be read data from record number
    len_sum = 0;
    for(uint32_t i=0; i<record_len; i++)
    {
        len_sum += m_part_index_list[part_type]->record_index_t.p_record_len[record_number + i];
    }
    byte_len = len_sum;    
    ret = hal_per_flash_read(read_address, byte_len, p_buf);
    if(ret != true)
    {
        return 0;
    }
    return byte_len;
}
/**
* @functionname : hal_per_part_write 
* @description  : function for write a record by any length,user must indicate partition type 
* @input        : part_type: partition type, the machine will write the datas according the type
*               : p_data: pointor to data will be stored
*               : record_len: the data length will be sorted
* @output       : none 
* @return       : true if successed
*/
uint32_t hal_per_part_write_record(uint32_t const part_type, uint8_t * p_data, uint32_t byte_len)
{
    uint32_t ret;
    uint32_t write_address = m_part_index_list[part_type]->p_base + m_part_index_list[part_type]->record_index_t.p_write_offset;
    if((m_part_index_list[part_type]->record_index_t.p_write_offset + byte_len) > m_part_index_list[part_type]->part_size)
    {
        //if exceed the part range
        byte_len = m_part_index_list[part_type]->part_size - m_part_index_list[part_type]->record_index_t.p_write_offset;
    }
    ret = hal_per_flash_write(write_address, byte_len,(uint8_t *)p_data);
    if(ret != true)
    {
            return false;
    }
    //update current record length in  len array
    m_part_index_list[part_type]->record_index_t.p_record_len[m_part_index_list[part_type]->record_index_t.record_write] = byte_len;
    //update record counter
    m_part_index_list[part_type]->record_index_t.record_write++;
    //update record write position
    m_part_index_list[part_type]->record_index_t.p_write_offset += byte_len;
    
    return byte_len;
}
/**
* @functionname : hal_per_flash_partition 
* @description  : function for part the memory by information user indicate @ref hal_part_handl_t, it will be performed
*                 automatic, user only indicate partition type and size in struct hal_part_handl_t, and adding the part
*                 index struct into m_part_index_list
* @input        : none
* @output       : none 
* @return       : true if successed
*/
bool hal_per_flash_partition(void)
{   
    uint32_t part_nums = sizeof((hal_part_handl_t *)m_part_index_list);
    //limit the parts within USER_RECORD_QUANTITY
    if(part_nums > USER_PARTITION_QUANTITY)
    {
        return false;
    }
    uint32_t ret;
    uint32_t address_offset, address;
    address = USER_MEMORY_BASE_SECTOR * USER_MEMORY_SECTOR_SIZE;
    address_offset = (USER_MEMORY_BASE_SECTOR + USER_INDEX_SECTORS) * USER_MEMORY_SECTOR_SIZE; //start address
    W25Q_ChipErase();
    for(uint8_t i=0;i<part_nums;i++)
    {
        m_part_index_list[i]->p_base = address_offset;
        m_part_index_list[i]->record_index_t.p_read_offset = 0;
        m_part_index_list[i]->record_index_t.p_write_offset = 0;
        
        //m_part_index_list[i]->p_read = m_part_index_list[i]->p_base;
        //m_part_index_list[i]->p_write= m_part_index_list[i]->p_base;
        
        ret = hal_per_flash_write(address, sizeof(m_part_index_list[i]),(uint8_t *)m_part_index_list[i]);
        if(ret != true)
        {
            return false;
        }
        address += sizeof(m_part_index_list[i]);
        address_offset += m_part_index_list[i]->part_size;
    }
    return true;
}


