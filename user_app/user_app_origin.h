
#ifndef USER_APP_ORIGIN_H
#define USER_APP_ORIGIN_H

#include <stdint.h>
//#include "drv_acc_KX022.h"
#include "user_acc.h"

#include "Origin.pb.h"

#define APP_ACC_DATA_RATE                   HAL_ACC_DATA_RATE
#define ACC_ORIGIN_ONE_POINT_DATA_LEN       HAL_ACC_ONE_POINT_DATA_LEN

//#define ACC_ORIGIN_POINT_CACHE_MAX_SIZE     30*DRV_ACC_KX022_FIFO_SAMPLES 
#define ACC_ORIGIN_ONCE_SAMPLE_POINTS       25
#define ACC_ORIGIN_SECOND_HAS_POINT         DRV_KX022_OUT_POINT_SIZE_SECOND
#define ACC_ORIGIN_ONE_RECORD_HAS_SECONDS   2
#define ACC_ORIGIN_ONE_RECORD_HAS_POINTS   (1*DRV_KX022_OUT_POINT_SIZE_SECOND)  //20s data
#define ACC_ORIGIN_CACHE_RECORDS_MAX        5     //save all cached records every 5 record, 10s
#define ACC_ORIGIN_ONCE_SEND_RECORD         1
//#define ACC_ORIGIN_ONCE_UPLOAD_POINT        2*DRV_ACC_KX022_OUT_POINT_SIZE_SECOND

typedef struct
{
    //alarm_clock_period_t   time[ACC_TIME_RANGE_PAIR_NUM_MAX];       //start clock and stop clock
    alarm_clock_period_t   handle[ACC_TIME_RANGE_PAIR_NUM_MAX];     //start handle and stop handle
    int8_t                 count;
}acc_sample_time_range_t;


typedef struct 
{
    uint32_t               timestamp;
    acc_origin_point_t     p_dt[ACC_ORIGIN_ONE_RECORD_HAS_POINTS]; //arecord every 2S
}acc_origin_record_t;


typedef struct
{
    uint16_t        count;
    protocol_XYZ_t  *xyz_t;
}acc_origin_encode_t;

typedef struct
{
    bool    start_sample;
    bool    stop_sample;
}acc_origin_sample_status;

typedef struct
{
    user_time_range_t   range[20];            //acc origin collect time range, may has some ranges
    uint32_t            pair_num;
    int32_t             mode;
}accorigin_setting_t;

//bool app_origin_upload_data(void);
bool app_origin_upload_data(uint8_t * encode_buf, uint32_t encode_buf_size, 
                  uint8_t * send_buf, uint32_t send_buf_size
                 );
bool app_origin_save_handle(acc_origin_point_t * const p_buf, uint32_t const point_nums,uint32_t time);
void user_create_origin_transmit_task(void);
void user_send_semaph_to_orgtrans_task(void);
void user_send_queue_acc_to_orgtrans_task(uint8_t *p_acc_dt, uint32_t bytes_len);
bool app_wait_ble_upload_origin_rsp(void);
bool app_origin_rsp_parse_handle(uint8_t const * const p_rsp, int32_t const len);
bool user_origin_get_sample_mode(void);
uint32_t user_origin_setting(accorigin_setting_t setting);

#endif  //HAL_ACC_ORIGIN_H

