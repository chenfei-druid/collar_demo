/** 
 * hal_acc_origin.c
 *
 * @group:       collar project
 * @author:      Chenfei
 * @create time: 2018/4/20
 * @version:     V0.0
 *
 */
 #include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "freertos_platform.h"
#include "user_app_log.h"
//#include "hal_rtc.h"
#include "alarm_clock.h"
#include "Define.pb.h"
#include "pb_encode.h"
#include "pb_decode.h"
//#include "user_data_pkg.h"
#include "user_send_data.h"
#include "user_app_register.h"
#include "IdentityMsg.pb.h"
#include "Register.pb.h"
#include "user_ble_inter_mgt.h"
#include "user_app_identity.h"
#include "ble_application.h"
#include "SEGGER_RTT.h"
#include "user_battery_mgt.h"
#include "drv_nb_modul.h"
#include "user_app_set.h"
//#include "SEGGER_RTT_Conf.h"

//static      TaskHandle_t                register_handle;
//static      xSemaphoreHandle            binary_semaphore_register;
//static      QueueHandle_t               queue_register;
            
//static uint8_t hal_acc_buf[HAL_ACC_POINT_SAMPLES*DRV_ACC_kx022_ONE_XYZ_DATA_LEN] = {0};//30s datas 
//static      app_register_req_t          m_regt_req_t;
//static      app_register_rsp_t          m_regt_rsp_t;

//static      bool    parse_result_flag = 0;
//static      char    imsi[20] = USER_IMSI_CODE;
//static      char    imei[20] = USER_IMEI_CODE;
static      xSemaphoreHandle        binary_semaphore_wait_response;
/***********************************************defined functions**********************************************/

/**
* @functionname : app_encode_repeated_var_string 
* @description  : function for encode a strings data
* @input        : stream, encode stream,  
*               : fields: the origin fields
*               : str: the string data will be encoded
* @output       : none 
* @return       : true if successed
*/
static bool app_register_encode_repeated_var_string(pb_ostream_t *stream, const pb_field_t *fields,  void * const *arg)
{
    bool res;
    pb_byte_t *p_str = *arg; 
    res = pb_encode_tag_for_field(stream, fields);
    ////DBG_LOG("pb encode tag for string is  %s", (res? "successed":"failed"));
    ////DBG_LOG("encode tag for string is  %s\r\n", (res? "successed":"failed"));
    res = pb_encode_string(stream,p_str,strlen((char *)p_str));
    //DBG_LOG("stream->bytes_written = %d\r\n", stream->bytes_written);
    DBG_LOG("encode register string \"%s\" is %s\r\n", p_str, (res? "successed":"failed"));
    return res;
}
/**
* @functionname : app_decode_repeated_var_struct 
* @description  : function for decode a register struct data
* @input        : stream, encode stream,  
*               : fields: the register fields
*               : src_struct: the struct data will be decoded
* @output       : none 
* @return       : true if successed
*/
/*
static bool app_register_decode_repeated_var_struct(pb_istream_t *stream, const pb_field_t *field, void **src_struct)
{
    //uint8_t *p = *src_struct;
    //uint32_t tag;
    //pb_wire_type_t wire_type;
    //bool eof;
    bool res = false;
    ////DBG_LOG("decoding struct..., the bytes left is %d\r\n", stream->bytes_left);
    //res = pb_decode_tag(stream, &wire_type, &tag, &eof);
    ////DBG_LOG("pb decode tag is %s\r\n", (res? "successed":"failed"));
    res = pb_decode(stream,protocol_register_req_fields,*src_struct);
    DBG_LOG("decode register struct is %s\r\n", (res? "successed":"failed"));
    return res;
}
*/
/**
* @functionname : app_decode_repeated_var_string 
* @description  : function for decode a strings data
* @input        : stream, encode stream,  
*               : fields: the origin fields
*               : str: the string data will be encoded
* @output       : none 
* @return       : true if successed
*/
static bool app_register_decode_repeated_var_string(pb_istream_t *stream, const pb_field_t *field, void **str)
{
    //pb_wire_type_t wire_type;
    pb_byte_t   cache_buf[stream->bytes_left];
    uint8_t     byte_len = stream->bytes_left;
    //bool eof;
    bool res = false;
    res = pb_read(stream,cache_buf, stream->bytes_left);    
    DBG_LOG("decode register string \"%s\" is %s\r\n" , cache_buf, (res? "successed":"failed"));
    if(res == true)
    {
        memcpy(*str, cache_buf, byte_len);
        //DBG_LOG("decode string \"%s\" is successeds\r\n", *str);
        return true;
    }
    else
    {
        *str = NULL;
        //DBG_LOG("decode failed\r\n");
        return false;
    }
}

#define test_decode 0
#if test_decode
void user_data_pb_decode_test(uint8_t *buffer, uint32_t len)
{
    pb_istream_t i_stream ;
    bool res;
    uint8_t id[20];
    uint8_t imei[20];
    uint8_t imsi[20];
    uint8_t mac[20];
    
    protocol_register_req_t decodce_regt_req_t;
    
    decodce_regt_req_t.Iden.DeviceID.funcs.decode  = &app_register_decode_repeated_var_string;
    decodce_regt_req_t.Iden.DeviceID.arg           = id;
        
    decodce_regt_req_t.MAC.funcs.decode            = &app_register_decode_repeated_var_string;
    decodce_regt_req_t.MAC.arg                     = mac;
    
    decodce_regt_req_t.IMEI.funcs.decode           = &app_register_decode_repeated_var_string;
    decodce_regt_req_t.IMEI.arg                    = imei;    //dec 15-digit
    
    decodce_regt_req_t.IMSI.funcs.decode           = &app_register_decode_repeated_var_string;
    decodce_regt_req_t.IMSI.arg                    = imsi;   //dec 16-digit
    
    
    
    i_stream = pb_istream_from_buffer(buffer,len);
    //DBG_LOG("given buffer size= %d\r\n",  i_stream.bytes_left);
    res = pb_decode(&i_stream,protocol_register_req_fields,&decodce_regt_req_t);   
    //DBG_LOG("decode is %s\r\n", (res? "successed":"failed"));  
    //DBG_LOG("decode is %s\r\n", (res? "successed":"failed"));    
}
#endif

/**
* @functionname : app_encode_origin_data 
* @description  : function for encode data
* @input        : stream, encode stream,  
*               : fields: the origin fields
*               : str: the string data will be encoded
* @output       : none 
* @return       : true if successed
*/

static uint32_t app_encode_register_msg(uint8_t *const out_buffer, int16_t buf_len, trans_interface_type type)
{
    if(out_buffer == NULL || buf_len <= 0)
    {
        return 0;
    }
    char p_dev_mac[20] = {0};
    char p_dev_id[13] = {0};
    char p_imei[16] = {0};
    char p_imsi[16] = {0};
        
    pb_ostream_t   m_stream ;
    protocol_register_req_t     regt_req_encode_t;
    
    user_get_nb_imei(p_imei);
    user_get_nb_imsi(p_imsi);
    
    //get identity message
    app_get_indetity_msg(&regt_req_encode_t.Iden);
    //get device id(mac)
    app_iden_get_device_id_char(p_dev_id);
    //user_lower_to_upper(p_dev_id);
    app_iden_get_device_id_char(p_dev_mac);
    app_add_colon_to_mac_addr(p_dev_mac);
    DBG_LOG("\r\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~p_dev_mac = %s~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\r\n",p_dev_mac);
    DBG_LOG("\r\n........regt_req_encode_t.Iden.MsgToken = %x..........\r\n",regt_req_encode_t.Iden.MsgToken);
    regt_req_encode_t.has_BatteryPower            = true;
     
    regt_req_encode_t.has_BatteryVoltage          = true;
    regt_req_encode_t.has_BitErrorRate            = false;
    regt_req_encode_t.has_DeviceType              = true;
    regt_req_encode_t.has_FirmwareVersion         = true;
    regt_req_encode_t.has_HardwareVersion         = true;
    regt_req_encode_t.has_NetworkOperator         = false;
    regt_req_encode_t.has_RadioAccessTechnology   = false;
    regt_req_encode_t.has_SignalStrength          = false;
    regt_req_encode_t.has_Status                  = true;
    
    if(type == COMMUNICATION_INTERFACE_TYPE_BLE)
    {
        regt_req_encode_t.Status                      = 0;
    }
    else
    {
        regt_req_encode_t.Status                      = 2;
    }
    regt_req_encode_t.BatteryPower                = drv_get_battary_power_percentage();;
    
    regt_req_encode_t.BatteryVoltage              = drv_get_battery_voltage();;
    
    regt_req_encode_t.DeviceType                  = USER_REGISTER_DEV_TYPE;
    regt_req_encode_t.FirmwareVersion             = USER_REGISTER_FW_VS;
    regt_req_encode_t.HardwareVersion             = USER_REGISTER_HW_VS;       

    //regt_req_encode_t.BitErrorRate                = 0;    //at+csq
    //regt_req_encode_t.NetworkOperator             = 12;   //cops
    //regt_req_encode_t.RadioAccessTechnology       = 34;   //CREG
    //regt_req_encode_t.SignalStrength              = 10;   //CSQ
    
    
    regt_req_encode_t.Iden.DeviceID.funcs.encode  = &app_register_encode_repeated_var_string;
    regt_req_encode_t.Iden.DeviceID.arg           = p_dev_id;
    
    regt_req_encode_t.IMEI.funcs.encode           = &app_register_encode_repeated_var_string;
    regt_req_encode_t.IMEI.arg                    = p_imei;    //dec 15-digit
    
    regt_req_encode_t.IMSI.funcs.encode           = &app_register_encode_repeated_var_string;
    regt_req_encode_t.IMSI.arg                    = p_imsi;   //dec 16-digit
    
    regt_req_encode_t.MAC.funcs.encode            = &app_register_encode_repeated_var_string;
    regt_req_encode_t.MAC.arg                     = p_dev_mac;
    
    m_stream = pb_ostream_from_buffer(out_buffer,buf_len);
    bool res = pb_encode(&m_stream,protocol_register_req_fields,&regt_req_encode_t);
    //memcpy(p_out,enc_buf,m_stream.bytes_written);
    DBG_LOG("\r\nencode register is %s, out size = %d\r\n",  (res? "successed":"failed"),m_stream.bytes_written);
    
#if test_decode  
    user_data_pb_decode_test(out_buffer,m_stream.bytes_written);
#endif  //test_decode
    //DBG_LOG("register out_buffer addr = 0x%x \r\n", out_buffer);
    return m_stream.bytes_written;
}

/**
* @functionname : app_register_rsp_decode 
* @description  : function for decode register response message
* @input        : p_buf: input by-decode buffer
*               : len: input by-decode data length
* @output       : rgst_rsp_t, decode out struct,  
*               : dev_id: device id buffer 
* @return       : true if successed
*/
bool app_register_rsp_decode(protocol_register_rsp_t * const rgst_rsp_t, 
                             char * const dev_id, 
                             uint8_t const * p_buf, 
                             uint32_t const len)
{
    bool res;
    pb_istream_t i_stream ;
    rgst_rsp_t->Iden.DeviceID.funcs.decode   = &app_register_decode_repeated_var_string;
    rgst_rsp_t->Iden.DeviceID.arg            = dev_id;
    
    i_stream = pb_istream_from_buffer(p_buf,len);
    res = pb_decode(&i_stream,protocol_register_rsp_fields,rgst_rsp_t);
    DBG_LOG("register response decode is %s\r\n", (res? "successed":"failed")); 
    if(res == true)
    {
        //DBG_LOG("decode is successed");
        return true;
    }
    else
    {
        //DBG_LOG("decode is failed");
        return false;
    }
}
static uint32_t app_register_instruction_parse(protocol_register_rsp_t const * const p_rgst_rsp_t, char const * const p_id)
{
    if(p_rgst_rsp_t == NULL)
    {
        DBG_LOG("pointor is NULL");
        return 0;
    }
    uint32_t msgtoken = 0;
    char device_id[13] = {0};
    app_iden_get_device_id_char(device_id);
    uint16_t give_random = p_rgst_rsp_t->Iden.MsgToken & 0x0ffff;
    if(p_id[0] != '\0')
    {
        DBG_LOG("register parse : give dev id is: %s\r\n",p_id);
        //if(strcmp(p_id,device_id) != 0)
        if(memcmp(p_id,device_id,12) != 0)
        {
            DBG_LOG("\r\nregister parse: error, device id is: %s\r\n",device_id);            
            return 0;
        }        
        msgtoken = app_get_msg_token(p_id,give_random);
    }
    else
    {
        msgtoken = app_get_msg_token(device_id,give_random);
    }
    if(msgtoken != p_rgst_rsp_t->Iden.MsgToken)
    {
        DBG_LOG("\r\nregister parse: check msgtoken is error,give random = 0x%x\r\n",give_random);
        DBG_LOG("\r\ngiven token = 0x%x, caculate is 0x%x\r\n",p_rgst_rsp_t->Iden.MsgToken, msgtoken);
        return 0;
    }
    alarm_clock_set_time_stamp(p_rgst_rsp_t->Timestamp);
    DBG_LOG("register timestamp = %u\r\n", p_rgst_rsp_t->Timestamp); 
    DBG_LOG("register Iden.DeviceID = %s\r\n",p_id);   
    DBG_LOG("register MsgIndex = %u\r\n",p_rgst_rsp_t->Iden.MsgIndex);
    DBG_LOG("register MsgToken = %x\r\n",p_rgst_rsp_t->Iden.MsgToken);

        
    return true;
}
bool app_register_rsp_parse(void const * const p_buf, int32_t const len)
{

    protocol_register_rsp_t rgst_rsp_t;
    char dev_id[15];
    //uint32_t decode_len;
    memset(dev_id,0,sizeof(dev_id));
    if(p_buf == NULL || len <= 0)
    {
        //DBG_LOG("pointor is NULL");
        return false;
    }
    DBG_LOG("register response decoding...\r\n");
    if(app_register_rsp_decode(&rgst_rsp_t, dev_id, p_buf, len)!=true)
    {    
        DBG_LOG("response decode failed\r\n");
        return false;
    }
    DBG_LOG("register instruction parsing...\r\n");
    if(app_register_instruction_parse(&rgst_rsp_t,dev_id) != true)
    {
        DBG_LOG("register instruction parse failed\r\n");
        return false;
    }
    if((rgst_rsp_t.Timestamp > 1533139200) && (rgst_rsp_t.Timestamp < 2147483647))           //2018/8/2 00:00:00 ~ 2038/1/19 11:14:7
    {
        alarm_clock_set_time_stamp(rgst_rsp_t.Timestamp);    
        user_set_rewrite_timestamp();
    }
    else
    {
        DBG_LOG("-------register given time is invalid--------\r\n");
    }
    DBG_LOG("register instruction parse succesful\r\n");
    BaseType_t semaphore = pdFALSE;
    xSemaphoreGiveFromISR(binary_semaphore_wait_response,&semaphore);
    //parse_result_flag = 1;
    return true;
}

static bool app_register_wait_rsp(trans_interface_type chn_type)
{   
    uint32_t wait_response_timeout;
    if(chn_type == COMMUNICATION_INTERFACE_TYPE_NB)
    {
        wait_response_timeout = WAIT_SERVER_RESPONSE*1000;
    }
    else
    {
        wait_response_timeout = 5*1000;
    }
    DBG_LOG("waiting parse register response\r\n\0");
    if(xSemaphoreTake( binary_semaphore_wait_response, wait_response_timeout ) == pdPASS)    //wait 10s
    {
        DBG_LOG("\r\n>>>>>>>>>>>>>>>>>>>>>>>>>>>register  successed>>>>>>>>>>>>>>>>>>>>>\r\n\0");
        return true;
    }
    DBG_LOG("wait register response timeout, register failed\r\n\0");
    return false;
}
/**
* @function@name:    app_send_register_msg
* @description  :    function for send register message
* @input        :    none
* @output       :    none
* @return       :    send length
*/
//bool app_register(trans_interface_type chn_type)
bool app_register(uint8_t * encode_buf, uint32_t encode_buf_size, 
                  uint8_t * send_buf, uint32_t send_buf_size,  
                  trans_interface_type chn_type
                 )
{
    if(chn_type >= COMMUNICATION_INTERFACE_TYPE_MAX || chn_type <= COMMUNICATION_INTERFACE_TYPE_MIN)
    {
        DBG_LOG("\r\nchannle type is exceeded range\r\n");
        return false;
    }
    if(chn_type == COMMUNICATION_INTERFACE_TYPE_BLE)
    {
        if(!ble_get_connect_status())
        {
             DBG_LOG("\r\nno ble connection\r\n");
            return false;
        }
    }
    if(encode_buf == NULL || send_buf == NULL)
    {
        DBG_LOG("\r\n pointer encode_buf or send_buf is null\r\n");
        return false;
    }
    
    int32_t length;
    //uint8_t encode_buf[100] = {0};
    //uint8_t send_buf[128] = {0};
    //uint8_t encode_buf_len = 100;
    //uint8_t send_buf_len = 128;
    //uint8_t *encode_buf = (uint8_t *)malloc(encode_buf_len*sizeof(uint8_t));
    //uint8_t *send_buf   = (uint8_t *)malloc(send_buf_len*sizeof(uint8_t));
    
    DBG_LOG("\r\nsend_buf : 0x%x, encode_buf: 0x%x\r\n",send_buf,encode_buf);
    bool ret = false;
    //uint8_t retry = 0;
    int32_t     frame_seq = 0;
    uint8_t     func_code = USER_COMMON_CODE;  //0X01
    binary_semaphore_wait_response = xSemaphoreCreateBinary();
    if(binary_semaphore_wait_response == NULL)
    {
        DBG_LOG("\r\n>>>>>>>> register: binary semaphore create failed <<<<<<<<<\\r\n\0");
        return false;
    }
    xSemaphoreTake( binary_semaphore_wait_response, 0 );
    ////DBG_LOG("encoding");
    
    length = app_encode_register_msg(encode_buf,encode_buf_size,chn_type);
    DBG_LOG("\r\nencoded register length = %d\r\n",length);
    if(length <= 0)
    {
        free(send_buf);
        free(encode_buf);
        return 0;
    }
    frame_seq++;
    if(!frame_seq)
    {
        frame_seq++;
    }
    length = user_data_packge(send_buf, send_buf_size, encode_buf, length, 
                                                PROTOCOL_HEADER_TYPE_TYPE_REGISTER_REQ,
                                                frame_seq,func_code,chn_type);  
    DBG_LOG("\r\npackeged register length = %d\r\n",length);
    if(length > 0)
    {
        //do
        //{
            DBG_LOG("\r\nstart send register, the send length = %d\r\n",length);
            ret = user_send_data(send_buf,length,chn_type);
            if(ret)
            {
                ret = app_register_wait_rsp(chn_type);
            }
        //}while(!ret && retry++<2);      //resend 2 times
    }
    DBG_LOG("\r\nregister is %s\r\n",ret? "successed":"failed");
    vSemaphoreDelete( binary_semaphore_wait_response );
    //free(send_buf);
    //free(encode_buf);
    return ret;
}


//end



