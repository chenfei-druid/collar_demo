
#include <stdint.h>
#include <stdbool.h>
#include "nrf_drv_spi.h"
#define SPI0_CS_PIN   PIN_RF_CS  /* nRF52832只能使用GPIO作为片选，所以这个单独定义了SPI CS管脚.*/
//#define SPI_BUFSIZE 8
#define SPI0_INSTANCE  0 /**< SPI instance index. */


typedef struct
{
    uint8_t transm_buff[255];
}spi_transm_s;

typedef enum
{
    read=0,
    write,
}spi_atribute;

/*
 * function: nrf52832 SPI0接口初始化
 * input   : null
 * output  : null
 */
void spi_init(uint8_t instance, nrf_drv_spi_config_t spi_config);

/*
 * function : SPI接口数据传输
 * input    : cmd   : 操作命令，针对不同的外设，或者同一个外设有不同的操作命令
 *            reg   : 寄存器地址
 *            data_p: 要写入或者要读出的数据单元
 *            data_len : 要写入或者要读出的数据单元长度
 *            atr_p : 读写属性，写入：write， 读出：read
 * output   : data_p
 */
bool hal_spi_read_write(uint8_t cmd, uint8_t reg, uint8_t *data_p, uint8_t data_len, spi_atribute atr_p);


