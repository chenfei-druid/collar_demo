
#ifndef     USER_APP_BHV_H
#define     USER_APP_BHV_H

#include <stdint.h>
#include <stdbool.h>
#include "user_acc.h"
//#include "behavior.h"
#include "Behavior2.pb.h"
#include "user_data_pkg.h"
#include "drv_nb_modul.h"

#define     test    0

#define     BHV_ODBA_WINDOW_DEFAULT             51
#if test 
#define     BHV_ODBA_ONE_RECORD_TIME_DEFAULT    30
#else
#define     BHV_ODBA_ONE_RECORD_TIME_DEFAULT    60
#endif

#define     BHV_FRAME_MAX_BYTES                 256

#define     BHV_BASE_SAMPLE_INTERAL_DEF         60  //seconds of once record of behavior
#define     BHV_BASE_RESULT_COUNTER_MAX         BHV_BASE_SAMPLE_INTERAL_DEF/(HAL_ACC_POINT_SAMPLES/HAL_ACC_DATA_RATE)

#define     BHV_RESULT_BUF_MAX                  1  //if a result record every one minute, a time to save every one hour

#define     BHV_BLE_FRAME_UPLOAD_RECORD_SIZE    BHV_FRAME_MAX_BYTES / sizeof(Behavior_Record_t)
#define     BHV_NB_FRAME_UPLOAD_RECORD_SIZE     ((256-32) / sizeof(Behavior_Record_t))
    
#define     BHV_UPLOAD_RECORD_SIZE              5//BHV_NB_FRAME_UPLOAD_RECORD_SIZE

#define     BHV_WAIT_SERVER_RESPONSE            (WAIT_SERVER_RESPONSE)

/************************************************************************************
    optional uint32 Timestamp = 1 [default=0];
    optional int32 ODBAX = 2 [default=0];            // odbax value in 0.0001g
    optional int32 ODBAY = 3 [default=0];            // odbay value in 0.0001g
    optional int32 ODBAZ = 4 [default=0];            // odbaz value in 0.0001g
    optional int32 MeandlX = 5 [default=0];          // mean(|X(i) - X(i-1)|) in 0.0001g
    optional int32 MeandlY = 6 [default=0];          // mean(|Y(i) - Y(i-1)|) in 0.0001g
    optional int32 MeandlZ = 7 [default=0];          // mean(|Z(i) - Z(i-1)|) in 0.0001g
    optional int32 ODBA = 8 [default=0];             // odba value in 0.0001g
*************************************************************************************/
  
//typedef struct  Behavior_Record_t       behavior_record_t;

typedef struct
{
    int32_t             count;
    protocol_behavior2_t   *record;
}bhv_encode_t;
typedef struct
{
    int32_t mode;       //behavior sample mode, 0: closed, 1: time to sample
    int32_t interval;
}bhv_setting_t;

void user_bhv_init(void);
//bool user_bhv_get_odba(acc_origin_point_t * p_record_t, uint32_t pt_num);
void user_send_queue_acc_data_to_bhv_task(acc_origin_point_t *p_acc_dt, uint32_t pt_nums);
void user_creat_bhv_detect_task(void);
int32_t user_bhv_sample_handle(acc_origin_point_t *p_acc_xyz_t, uint32_t xyz_nums,uint32_t time);
//bool user_bhv_upload_data(trans_interface_type chn_type);
bool user_bhv_upload_data(uint8_t * encode_buf, uint32_t encode_buf_size, 
                  uint8_t * send_buf, uint32_t send_buf_size,  
                  trans_interface_type chn_type
                 );
bool app_bhv_rsp_parse_handle(uint8_t const * const p_buf, int32_t const len);
int32_t user_bhv_get_interval(void);
bool user_bhv_set_sample_interval(int32_t interval);
bool user_bhv_set_sample_mode(int32_t mode);
//Behavior_Record_t user_bhv_get_action(void);
int32_t user_bhv_get_sample_mode(void);
bool user_bhv_setting(bhv_setting_t setting);

#endif     // USER_APP_BHV_H

