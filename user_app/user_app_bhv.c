
/** 
 * user_app_bhv.c
 *
 * @group:       collar project
 * @author:      Chenfei
 * @create time: 2018/4/20
 * @version:     V0.0
 * 
 *
 */
#include <math.h>
#include "freertos_platform.h"
#include "string.h"
#include "behavior.h"
#include "time.h"
#include "user_data_struct.h"
#include "Record.h"
#include "user_acc.h"
//#include "hal_rtc.h"
#include "alarm_clock.h"
#include "user_app_bhv.h"
#include "user_app_estrus.h"
//#include "Behavior2.pb.h"
#include "user_pb_callback.h"
#include "Define.pb.h"
#include "pb_encode.h"
#include "pb_decode.h"
#include "SimpleRsp.pb.h"
#include "user_data_pkg.h"
#include "user_send_data.h"
#include "user_app_identity.h"
#include "ble_application.h"

/***********************************************user defined variable************************************************/

//#define     BHV_HOUR_RES_MAX            24

//static      TaskHandle_t                bhv_detect_handle;
//static      xSemaphoreHandle            binary_semaphore_bhv_detect = NULL;
static      xSemaphoreHandle            bhv_semaphore_wait_response = NULL;
            QueueHandle_t               queue_bhv_detect;


//static      uint32_t                    bhv_sample_interval = BHV_BASE_SAMPLE_INTERAL_DEF;
//static      uint32_t                    msg_index = 0;
//static      bool                        bhv_rsp_flag = 0;
static      bhv_setting_t               m_bhv_sample_param = {.mode = 0, .interval = BHV_BASE_SAMPLE_INTERAL_DEF};
//static      int32_t                     bhv_odba = 0;
/** ****************************************************************************
behavior record:
    optional uint32     Timestamp  
    optional int32      ODBAX               // odbax value in 0.0001g
    optional int32      ODBAY               // odbay value in 0.0001g
    optional int32      ODBAZ               // odbaz value in 0.0001g
    optional int32      MeandlX             // mean(|X(i) - X(i-1)|) in 0.0001g
    optional int32      MeandlY             // mean(|Y(i) - Y(i-1)|) in 0.0001g
    optional int32      MeandlZ             // mean(|Z(i) - Z(i-1)|) in 0.0001g
    optional int32      ODBA                // odba value in 0.0001g
    
********************************************************************************/
/***********************************************defined functions**********************************************/
#include "behavior.c" 
static void user_bhv_record_init(int w_size, int peroid)
{   
    Behavior_Init(w_size,peroid);   
}
void user_bhv_init(void)
{
    user_bhv_record_init(BHV_ODBA_WINDOW_DEFAULT, BHV_ODBA_ONE_RECORD_TIME_DEFAULT);
    //m_bhv_res_cnt = 0;
    //memset(m_bhv_res_buf,0,sizeof(m_bhv_res_buf));
}
/**
* @functionname : user_bhv_set_sample_interval 
* @description  : function for set behavior sample interval
* @input        : interval: sample interval, unit is second  
* @output       : none 
* @return       : true if valid
*/
bool user_bhv_set_sample_interval(int32_t interval)
{
    if(interval < 60)
    {
        DBG_LOG("behavior set failed of invalid interval = %d\r\n\0",interval);
        return false;
    }
    if(interval < 60)
    {
        //interval = 60;       //60S
    }        
    m_bhv_sample_param.interval = interval;
    DBG_LOG("\r\nbhv set interval = %d\r\n\0", interval);
    return true;
}
/**
* @functionname : user_bhv_set_sample_mode 
* @description  : function for set behavior sample mode
* @input        : mode: 0, colse, 1: time to sample 
* @output       : none 
* @return       : true if valid
*/
bool user_bhv_set_sample_mode(int32_t mode)
{
    if(mode > 1 || mode < 0)
    {
        DBG_LOG("behavior set failed of invalid mode = %d\r\n\0",mode);
        return false;
    }
    if(m_bhv_sample_param.mode != 1)
    {
        m_bhv_sample_param.mode = mode;
        user_acc_set();
        DBG_LOG("\r\nbhv set mode = %d\r\n\0", mode);
    }
    else
    {
        DBG_LOG("\r\nbhv sample mode has set to 1 not need to set repeated\r\n\0");
    }
    return true;
}
/**
* @functionname : user_bhv_setting 
* @description  : function for set sample parameter, the mode ,and interval time
* @input        : bhv_sample_setting_t: @ref bhv_sample_setting_t
* @output       : none 
* @return       : true if valid
*/
bool user_bhv_setting(bhv_setting_t setting)
{
    if(setting.mode > 1 || setting.mode < 0)
    {
        DBG_LOG("behavior set failed of invalid mode = %d\r\n\0",setting.mode);
        return false;
    }
    if(setting.interval < 60)
    {
        DBG_LOG("behavior set failed of invalid interval = %d\r\n\0",setting.interval);
        return false;
    }
    m_bhv_sample_param.interval = setting.interval;    
    m_bhv_sample_param.mode = setting.mode;
    
    user_acc_set();
    
    DBG_LOG("\r\nbhv set mode = %d\r\n\0", setting.mode);
    DBG_LOG("\r\nbhv set interval = %d\r\n\0", setting.interval);
    //bool ret = true;
    //ret &= user_bhv_set_sample_interval(setting.interval);
    //ret &= user_bhv_set_sample_mode(setting.mode);
    return true;
}
int32_t user_bhv_get_sample_mode(void)
{
    return m_bhv_sample_param.mode;
}
/*
Behavior_Record_t user_bhv_get_action(void)
{
    return m_bhv_res_buf[m_bhv_res_cnt-1];
}
*/
/**
* @functionname : app_encode_repeated_var_struct 
* @description  : function for encode the origin records, the record number is filled at head position
* @input        : stream, encode stream,  
*               : fields: the origin fields
*               : src_struct: the origin struct will be encoded
* @output       : none 
* @return       : true if successed
*/
static bool user_bhv_encode_repeated_var_struct(pb_ostream_t *stream, const pb_field_t fields[],  void * const *src_struct)
{
    
    bool res;
    bhv_encode_t *m_head_t = (bhv_encode_t *)*src_struct;
    protocol_behavior2_t    *record_t        = m_head_t->record;
    for(uint8_t i=0; i<m_head_t->count; i++)
    {
        if (!pb_encode_tag_for_field(stream, fields))
        {
            DBG_LOG("encode tag failed\r\n\0");
            return false;
        }
        res = pb_encode_submessage(stream, protocol_behavior2_fields, &record_t[i]);
        //DBG_LOG("encode behavior record %d is %s", i,(res? "successed":"failed"));
        if(res == false)
        {
            DBG_LOG("encode struct failed\r\n\0");
            break;
        }     
        DBG_LOG("encode behavior struct number %d of %d successed\r\n\0",i,m_head_t->count);
    }
    DBG_LOG("encode bhv struct is %s\r\n", res? "successed":"failed");
    return res;
}
/**
* @functionname : app_decode_repeated_var_struct 
* @description  : function for decode a register struct data
* @input        : stream, encode stream,  
*               : fields: the register fields
*               : src_struct: the struct data will be decoded
* @output       : none 
* @return       : true if successed
*/
/*
static bool user_bhv_decode_repeated_var_struct(pb_istream_t *stream, const pb_field_t *field, void **src_struct)
{
    static int decode_struct_cnt = 0;
    protocol_behavior2_t *bhv_t = (protocol_behavior2_t *)(*src_struct);
    bool res = false;
    res = pb_decode(stream,protocol_behavior2_fields,&bhv_t[decode_struct_cnt]);
    DBG_LOG("decode behavior struct count %d\r\n", decode_struct_cnt++); 
    DBG_LOG("MeandlX = %d, MeandlY = %d, MeandlZ = %d", bhv_t->MeandlX,bhv_t->MeandlY,bhv_t->MeandlZ); 
    DBG_LOG("ODBAX = %d, ODBAY = %d, ODBAZ = %d", bhv_t->ODBAX,bhv_t->ODBAY,bhv_t->ODBAZ); 
    return res;
}
*/
/**
* @function@name:    user_acc_save_bhv_record
* @description  :    function for save one bhv record into memory
* @input        :    p_bhv_dt: 
* @output       :    none
* @return       :    true if success
*/
//void hal_acc_save_origin_data(acc_origin_data_t *p_org_dt, uint32_t len)
static bool user_bhv_save_record(Behavior_Record_t * const p_bhv_dt)
{
    uint8_t *p_dt = (uint8_t *)p_bhv_dt;
    bool ret = Record_Write(RECORD_TYPE_BEHAVIOR,p_dt);
    DBG_LOG("\r\n ##########save one bhv record#############\r\n");
    DBG_LOG("meandl.x = %d\r\nmeandl.y = %d\r\nmeandl.z = %d\r\n",p_bhv_dt->meandl.x,p_bhv_dt->meandl.y,p_bhv_dt->meandl.z);
    DBG_LOG("odba.x = %d\r\nodba.y = %d\r\nodba.z = %d\r\n",p_bhv_dt->odba.x,p_bhv_dt->odba.y,p_bhv_dt->odba.z);
    DBG_LOG("modba = %d\r\n",p_bhv_dt->m_odba);
    DBG_LOG("timestamp = %d\r\n",p_bhv_dt->time);
    return ret;
}

/**
* @function@name:    hal_acc_save_bhv_records
* @description  :    function for save some behavior record datas into memory as record
* @input        :    p_org_dt: 
*               :    record_num:
* @output       :    none
* @return       :    true if success
*/
static bool user_bhv_save_records(Behavior_Record_t *p_bhv_dt, uint32_t record_num)
{
//    bool ret;
    if(p_bhv_dt == NULL)
    {
        DBG_LOG("behavior pointor is null\r\n");
        return false;
    }
    if(record_num > BHV_RESULT_BUF_MAX)
    {
        DBG_LOG("bhv will be saved record_num = %d > buf_max\r\n",record_num);
        return false;
    }
    for(uint8_t i=0;i<record_num;i++)
    {
        if(user_bhv_save_record(&p_bhv_dt[i]) == false)
        {
            DBG_LOG("behavior datas save failed\r\n");
            return false;
        }
    }
    DBG_LOG("behavior datas save successed\r\n");

    return true;
}
/**
* @function@name:    user_bhv_read_one_record
* @description  :    function for read one record of behavior data
* @input        :    none
* @output       :    p_bhv_dt: pointor to out buf @ref Behavior_Record_t
* @return       :    actual records length
*/
uint32_t user_bhv_read_one_record(Behavior_Record_t  * const p_bhv_dt)
{    
    if(p_bhv_dt == NULL)
    {
        return 0;
    }
    return Record_Read(RECORD_TYPE_BEHAVIOR, (uint8_t *)p_bhv_dt, sizeof(Behavior_Record_t));
}
/**
* @function@name:    user_bhv_read_records
* @description  :    function for read some records of behavior data
* @input        :    record_num: want to read record numbers
* @output       :    p_bhv_dt: pointor to out buf @ref Behavior_Record_t
* @return       :    actual records length
*/
static int32_t user_bhv_read_records(Behavior_Record_t  *p_bhv_dt, int32_t record_num)
{
    if(p_bhv_dt == NULL)
    {
        DBG_LOG("pointer is null\r\n");
        return 0;
    }
    if(record_num <= 0 || record_num > BHV_UPLOAD_RECORD_SIZE)
    {
        DBG_LOG("behavior read records invalid record number\r\n",record_num);
        return 0;
    }
    int32_t ret;
    ret = Record_Read(RECORD_TYPE_BEHAVIOR, (uint8_t *)p_bhv_dt, record_num * sizeof(Behavior_Record_t));  
    if(ret > record_num)
    {
        ret = record_num;
    }
    
    DBG_LOG("\r\n\r\nbehavior read record len = %d\r\n", ret);
    for(uint32_t i=0;i<ret;i++)
    {
        DBG_LOG("number %d record:\r\n",i);
        DBG_LOG("meandl.x = %u\r\nmeandl.y = %u\r\nmeandl.z = %u\r\n", p_bhv_dt[i].meandl.x,p_bhv_dt[i].meandl.y,p_bhv_dt[i].meandl.z);
        DBG_LOG("odba.x = %u\r\nodba.y = %u\r\nodba.z = %u\r\n", p_bhv_dt[i].odba.x,p_bhv_dt[i].odba.y,p_bhv_dt[i].odba.z);
        DBG_LOG("modba = %u\r\n", p_bhv_dt[i].m_odba);
        DBG_LOG("timestamp = %u\r\n\r\n", p_bhv_dt[i].time);
    }
    
    return ret;
}
/**
* @function@name:    user_bhv_odba_avg
* @description  :    function for compulate average of odba 
* @input        :    time: unix timestamp
* @output       :    none
* @return       :    none
*/
static Behavior_Record_t user_bhv_odba_avg(Behavior_Record_t * const sum, uint32_t const len)
{
    Behavior_Record_t  result;
    
    result.time     =   hal_rtc2_get_unix_time(); 
    
    result.odba.x   =   sum->odba.x / len;
    result.odba.y   =   sum->odba.y / len;
    result.odba.z   =   sum->odba.z / len; 
    
    result.meandl.x =   sum->meandl.x / len;
    result.meandl.y =   sum->meandl.y / len;
    result.meandl.z =   sum->meandl.z / len; 
    
    result.m_odba   =  sum->m_odba / len; 
    
    sum->m_odba     =   0;

    sum->odba.x     =   0;
    sum->odba.y     =   0;
    sum->odba.z     =   0;
    
    sum->meandl.x   =   0;
    sum->meandl.y   =   0;
    sum->meandl.z   =   0;
    
    return result;
}

/**
* @function@name:    user_bhv_minu_get_result
* @description  :    function for get a minutes odba result, it will capulate a result every one minute
* @input        :    bhv_rec_t: pointor of behavior update result,
*               :    
* @output       :    res_rec_t: pointor to the result cache
* @return       :    true if reached and get a result successed
*/
static bool user_bhv_get_record(Behavior_Record_t *p_bhv_t, Behavior_Record_t  *bhv_rec_t)
{
    static Behavior_Record_t    m_bhv_sum_t = {0};
    static uint32_t             m_record_cnt = 0;    
    
    m_bhv_sum_t.odba.x   += bhv_rec_t->odba.x;
    m_bhv_sum_t.odba.y   += bhv_rec_t->odba.y;
    m_bhv_sum_t.odba.z   += bhv_rec_t->odba.z;
    
    m_bhv_sum_t.meandl.x += bhv_rec_t->meandl.x;
    m_bhv_sum_t.meandl.y += bhv_rec_t->meandl.y;
    m_bhv_sum_t.meandl.z += bhv_rec_t->meandl.z;
    
    m_bhv_sum_t.m_odba   += bhv_rec_t->m_odba;
    m_record_cnt++;

    if(m_record_cnt >= m_bhv_sample_param.interval/BHV_ODBA_ONE_RECORD_TIME_DEFAULT)
    {                        
        *p_bhv_t = user_bhv_odba_avg(&m_bhv_sum_t, m_record_cnt); 
        m_record_cnt = 0;
        memset(&m_bhv_sum_t,0,sizeof(m_bhv_sum_t));
        return true;
    }
    return false;
}
/*
static Behavior_Record_t *user_bhv_get_record(Behavior_Record_t  *bhv_rec_t)
                              //Behavior_Record_t  *res_rec_t  )
{
    static Behavior_Record_t    m_bhv_sum_t = {0};
    static uint32_t             m_record_cnt = 0;
    static Behavior_Record_t    bhv_record = {0};
    
    Behavior_Record_t           *p_bhv = NULL;     
    
    m_bhv_sum_t.odba.x   += bhv_rec_t->odba.x;
    m_bhv_sum_t.odba.y   += bhv_rec_t->odba.y;
    m_bhv_sum_t.odba.z   += bhv_rec_t->odba.z;
    
    m_bhv_sum_t.meandl.x += bhv_rec_t->meandl.x;
    m_bhv_sum_t.meandl.y += bhv_rec_t->meandl.y;
    m_bhv_sum_t.meandl.z += bhv_rec_t->meandl.z;
    
    m_bhv_sum_t.m_odba   += bhv_rec_t->m_odba;
    m_record_cnt++;
    
    if(m_record_cnt >= m_bhv_sample_param.interval/BHV_ODBA_ONE_RECORD_TIME_DEFAULT)
    {                        
        bhv_record = user_bhv_odba_avg(&m_bhv_sum_t, m_record_cnt); 
        //p_bhv = &bhv_record;
        m_record_cnt = 0;
        return &bhv_record;
    }
    return NULL;
}
*/
/**
* @function@name:    hal_acc_odba_hour
* @description  :    function for compulate odba data, if return result, then caculate a odba base result every one minute
* @input        :    time: unix timestamp
* @output       :    none
* @return       :    none
*/
static bool user_bhv_get_odba(Behavior_Record_t *p_bhv_t, acc_origin_point_t * p_record_t, uint32_t pt_num)
{
    Behavior_Record_t   *bhv_res_t;
    bhv_res_t = Behavior_Update((DataPoint_t *)p_record_t, pt_num, hal_rtc2_get_unix_time());
    if(bhv_res_t != NULL)
    {
        //send current one base behavior result to estrus queue to compute estrus result
        return user_bhv_get_record(p_bhv_t,bhv_res_t);
    }
    else
        return false;
}
/*
static Behavior_Record_t * user_bhv_get_odba(acc_origin_point_t * p_record_t, uint32_t pt_num)
{
    Behavior_Record_t   *bhv_res_t;
    bhv_res_t = Behavior_Update((DataPoint_t *)p_record_t, pt_num, hal_rtc2_get_unix_time());
    if(bhv_res_t != NULL)
    {
        //send current one base behavior result to estrus queue to compute estrus result
        return user_bhv_get_record(bhv_res_t);
    }
    else
        return NULL;
}
*/
#define test_decode 0
#if test_decode
static int32_t user_data_pb_decode_test(uint8_t * decode_buf, int32_t len)
{
    protocol_behavior2_req_t    bhv_whl_t;
    protocol_behavior2_t        bhv_info_t;
    Behavior_Record_t           p_record_buf[BHV_ONE_FRAME_UPLOAD_LEN_MAX];
    
    uint8_t id[20];
    
    bhv_whl_t.Iden.DeviceID.funcs.decode    = &user_app_decode_repeated_var_string;
    bhv_whl_t.Iden.DeviceID.arg             = id;
    
    bhv_whl_t.BehaviorInfo.funcs.decode     = &user_bhv_decode_repeated_var_struct;
    bhv_whl_t.BehaviorInfo.arg              = p_record_buf;
    
    pb_istream_t i_stream ;
    i_stream = pb_istream_from_buffer(decode_buf,len);
    bool res = pb_decode(&i_stream,protocol_behavior2_req_fields,&bhv_whl_t);   
    DBG_LOG("\r\nbhv decode %s\r\n",res? "successed":"failed");
    return res;
}
#endif
/**
* @functionname : user_bhv_encode_data 
* @description  : function for encode data
* @input        : stream, encode stream,  
*               : fields: the behavior fields
*               : str: the string data will be encoded
* @output       : none 
* @return       : true if successed
*/
static int32_t user_bhv_encode_data(    uint8_t             *const encode_out_buffer,
                                        uint32_t            const buffer_size,
                                        Behavior_Record_t   const *const p_record_buf, 
                                        uint32_t            const record_size, 
                                        user_identity_t     const iden
                                    )
{
    if(encode_out_buffer == NULL || p_record_buf == NULL)
    {
        DBG_LOG("\r\nbhv encode pointer is null\r\n");
        return 0;        
    }
    
    protocol_behavior2_req_t    pro_bhv_req_t;
    protocol_behavior2_t        encode_record_t[record_size];
    bhv_encode_t                p_bhv_encode_t;
    
    //char p_dev_mac[13] ;
    memset(encode_record_t, 0, sizeof(encode_record_t));
    pro_bhv_req_t.Iden = iden.head;
    
    p_bhv_encode_t.count    = record_size;
    p_bhv_encode_t.record   = encode_record_t;
    
    //fill record data
    for(int32_t i=0; i<record_size; i++)
    {
        encode_record_t[i].has_MeandlX = 1;
        encode_record_t[i].has_MeandlY = 1;
        encode_record_t[i].has_MeandlZ = 1;
        
        encode_record_t[i].has_ODBA = 1;
        
        encode_record_t[i].has_ODBAX = 1;
        encode_record_t[i].has_ODBAY = 1;
        encode_record_t[i].has_ODBAZ = 1;
        
        encode_record_t[i].has_Timestamp = 1;
        
        encode_record_t[i].MeandlX = p_record_buf[i].meandl.x;
        encode_record_t[i].MeandlY = p_record_buf[i].meandl.y;
        encode_record_t[i].MeandlZ = p_record_buf[i].meandl.z;
        
        encode_record_t[i].ODBAX = p_record_buf[i].odba.x;
        encode_record_t[i].ODBAY = p_record_buf[i].odba.y;
        encode_record_t[i].ODBAZ = p_record_buf[i].odba.z;
        
        encode_record_t[i].ODBA  = p_record_buf[i].m_odba;
        
        encode_record_t[i].Timestamp = p_record_buf[i].time;        
    }
    
    pro_bhv_req_t.Iden.DeviceID.funcs.encode    = &user_app_encode_repeated_var_string;
    pro_bhv_req_t.Iden.DeviceID.arg             = (char *)iden.dev_id;
    
    pro_bhv_req_t.BehaviorInfo.funcs.encode     = &user_bhv_encode_repeated_var_struct;
    pro_bhv_req_t.BehaviorInfo.arg              = &p_bhv_encode_t;
    
    pb_ostream_t            m_stream ;    
    //PRINT_S("encoding......\r\n\0");
    m_stream = pb_ostream_from_buffer(encode_out_buffer,buffer_size);
    bool res = pb_encode(&m_stream,protocol_behavior2_req_fields,&pro_bhv_req_t);
       
    //DBG_LOG("the encoded out size is %d",  m_stream.bytes_written);
    if(res)
    {
        DBG_LOG("encode behavior is successed\r\n\0");
        #if test_decode
        user_data_pb_decode_test(encode_out_buffer,m_stream.bytes_written);
        #endif
        return m_stream.bytes_written;
    }
    DBG_LOG("encode behavior is failed\r\n\0");
    return 0;
}

/**
* @function@name:    user_bhv_sample_handle
* @description  :    function for parse response after upload behavior data
* @input        :    p_acc_xyz_t:  pointer to sample of acc xyz data
*               :    xyz_nums:  x-y-z points number
* @output       :    none
* @return       :    true if success
*/
int32_t user_bhv_sample_handle(acc_origin_point_t *p_acc_xyz_t, uint32_t xyz_nums,uint32_t time)
{
    static      Behavior_Record_t   m_bhv_res_buf[BHV_RESULT_BUF_MAX] = {0};
    static      uint16_t            m_bhv_res_cnt = 0;
    if(m_bhv_sample_param.mode == 0)
    {
        DBG_LOG("do not sample bhv data\r\n");
        return 0;
    }
    if(p_acc_xyz_t == NULL || xyz_nums == 0)
    {
        DBG_LOG("\r\nbhv handle, pointer p_acc_xyz_t is null, or xyz_nums == 0\r\n");
        return 0;
    }
    Behavior_Record_t   bhv_res_t;
    if(user_bhv_get_odba(&bhv_res_t, p_acc_xyz_t,xyz_nums) == true)
    {
        bhv_res_t.time = time;
        DBG_LOG("\r\nthe behavior number %d, time = %ud\r\n",m_bhv_res_cnt,bhv_res_t.time);
        DBG_LOG("\r\nmeandl.x = %d, meandl.y = %d, meandl.z = %d\r\n", bhv_res_t.meandl.x,bhv_res_t.meandl.y,bhv_res_t.meandl.z);
        DBG_LOG("\r\nodba.x = %d, odba.y = %d, odba.z = %d\r\n", bhv_res_t.odba.x,bhv_res_t.odba.y,bhv_res_t.odba.z);
        m_bhv_res_buf[m_bhv_res_cnt++] = bhv_res_t;        //the buf cache odba rusult of one minutes
        if(m_bhv_res_cnt >= BHV_RESULT_BUF_MAX)
        {            
            //save all 6-item, if reached maximum numbers points
            if(user_bhv_save_records(m_bhv_res_buf,m_bhv_res_cnt) != true)//save all buffer records into rom memory
            {
                DBG_LOG("\r\nsave behavior records cached failed\r\n");
                //return 0;
            }
            else
            {
                DBG_LOG("\r\nsave %d behavior records cached success\r\n",m_bhv_res_cnt);
            }
            m_bhv_res_cnt = 0;          //clear bhv record buffer
        }        
    }
    return xyz_nums;
}

/**
* @createtime   :2018-5-12
* @function@name:    app_bhv_instruction_parse
* @description  :    function for parse response, judge the device id, and msgtoken
* @input        :    none
* @output       :    none
* @return       :    true if success
*/
static uint32_t app_bhv_instruction_parse(protocol_simple_rsp_t const *const p_msg_t, char const * const p_id)
{
    if(p_msg_t == NULL)
    {
        DBG_LOG("pointor is NULL");
        return 0;
    }
    uint32_t msgtoken = 0;
    char device_id[13] = {0};
    app_iden_get_device_id_char(device_id);
    uint16_t give_random = p_msg_t->Iden.MsgToken & 0x0ffff;
    if(p_id[0] != '\0')
    {
        DBG_LOG("behavior parse : give dev id is: %s\r\n",p_id);
        if(memcmp(p_id,device_id,12) != 0)
        {
            DBG_LOG("\r\nbehavior parse: error, device id is: %s\r\n",device_id);            
            return 0;
        }        
        msgtoken = app_get_msg_token(p_id,give_random);
    }
    else
    {
        msgtoken = app_get_msg_token(device_id,give_random);
    }
    if(msgtoken != p_msg_t->Iden.MsgToken)
    {
        DBG_LOG("\r\nbehavior parse: check msgtoken is error\r\n");
        return 0;
    }
    if(p_msg_t->Iden.has_RspCode)
    {
        if(p_msg_t->Iden.RspCode != 0)
        {
            return 0;
        }
    }
    DBG_LOG("Iden.DeviceID = %s\r\n",p_id);    
    DBG_LOG("register MsgIndex = %u\r\n",p_msg_t->Iden.MsgIndex);
    DBG_LOG("register MsgToken = %x\r\n",p_msg_t->Iden.MsgToken);
    DBG_LOG("\r\n-----------------------behavior response parse successful----------------------\r\n\0");
        
    return true;
}
/**
* @function@name:    app_bhv_rsp_parse
* @description  :    function for parse response after upload behavior data
* @input        :    p_buf:  given buffer for response
*               :    len:  response len
* @output       :    none
* @return       :    true if success
*/
bool app_bhv_rsp_parse_handle(uint8_t const * const p_rsp, int32_t const len)
{
    protocol_simple_rsp_t     p_msg_t;
    char dev_id[13] = {0};
    if(p_rsp == NULL || len <= 0)
    {
        DBG_LOG("\r\nbehavior rsp buffer pointor NULL, or len <= 0\r\n\0");
        return false;
    }
    if(user_app_decode_simple_rsp(&p_msg_t,dev_id,p_rsp,len) == false)
    {
        DBG_LOG("\r\nbehavior response decode failed\r\n",dev_id);
        return false;
    }
    if(app_bhv_instruction_parse(&p_msg_t,dev_id) != true)
    {
        DBG_LOG("\r\nbehavior response parse failed\r\n",dev_id);
        return false;
    }  
    //BaseType_t semaphore = pdFALSE;
    //xSemaphoreGiveFromISR(binary_semaphore_wait_response,&semaphore);
    xSemaphoreGive(bhv_semaphore_wait_response);
    return true;
}

static bool user_bhv_wait_rsp(trans_interface_type chn_type)
{
    uint32_t wait_response_timeout;
    if(chn_type == COMMUNICATION_INTERFACE_TYPE_NB)
    {
        wait_response_timeout = WAIT_SERVER_RESPONSE;
    }
    else
    {
        wait_response_timeout = 5;
    }
    DBG_LOG("waiting parse behavior response\r\n\0");
    if(xSemaphoreTake( bhv_semaphore_wait_response, wait_response_timeout * 1000) == pdPASS)    //wait 10s
    {
        DBG_LOG("receive behavior response successed\r\n\0");\
        return true;
    }
    DBG_LOG("waiting behavior response timeout\r\n\0");
    return false;
}
#if 0
static void user_bhv_save_data_test(void)
{
    
    Behavior_Record_t   bhv_record_test;
    bhv_record_test.meandl.x =  12;
    bhv_record_test.meandl.y = 13;
    bhv_record_test.meandl.z = 14;
    bhv_record_test.odba.x = 22;
    bhv_record_test.odba.y =  23;
    bhv_record_test.odba.z =  24;
    bhv_record_test.m_odba = 10;
    bhv_record_test.time   = hal_rtc2_get_unix_time();
    //Record_Delete(RECORD_TYPE_BEHAVIOR,(BEHAVIOR_NUM_SECTORS-1)*W25Q_SECTOR_SIZE/sizeof(Behavior_Record_t));
    if(user_bhv_save_record(&bhv_record_test) == 0)
    {
        DBG_LOG("save hbv test data failed\r\n\0");
    }
    else
    {
        DBG_LOG("save hbv test data successed\r\n\0");
    }
}
#endif
/**
* @function@name:    user_bhv_upload_data
* @description  :    function for upload behavior data, until no data
* @input        :    none
* @output       :    none
* @return       :    true if success
*/

//bool user_bhv_upload_data(trans_interface_type chn_type)
bool user_bhv_upload_data(uint8_t * encode_buf, uint32_t encode_buf_size, 
                  uint8_t * send_buf, uint32_t send_buf_size,  
                  trans_interface_type chn_type
                 )
{
    if(chn_type >= COMMUNICATION_INTERFACE_TYPE_MAX || chn_type <= COMMUNICATION_INTERFACE_TYPE_MIN)
    {
        return false;
    }
    if(chn_type == COMMUNICATION_INTERFACE_TYPE_BLE)
    {
        if(!ble_get_connect_status())
        {
            return false;
        }
    }
    if(encode_buf == NULL || send_buf == NULL)
    {
        DBG_LOG("\r\n pointer encode_buf or send_buf is null\r\n");
        return false;
    }
    int32_t             record_len = 0;
    Behavior_Record_t   *bhv_record_buf = (Behavior_Record_t *)send_buf;//[BHV_UPLOAD_RECORD_SIZE] = {0};
    bool                ret = 0;
    static  int32_t     frame_seq = 0;
            int32_t     len_temp = 0;
            //uint8_t     send_buf[256] = {0};
            //uint8_t     p_encode_buf[256] = {0};
            //uint8_t     *send_buf = (uint8_t *)malloc(256*sizeof(uint8_t));
            //uint8_t     *p_encode_buf = (uint8_t *)malloc(256*sizeof(uint8_t));
            DBG_LOG("\r\nsend_buf : 0x%x, p_encode_buf: 0x%x\r\n",send_buf,encode_buf);
            uint8_t     func_code = USER_COMMON_CODE;  //0X01
            user_identity_t     iden;
    bhv_semaphore_wait_response = xSemaphoreCreateBinary();  
    if(bhv_semaphore_wait_response == NULL)
    {
        DBG_LOG("\r\n >>>>>>>> upload bhv: binary semaphore create failed <<<<<<<<<\r\n\0");
        return false;
    }
    xSemaphoreTake( bhv_semaphore_wait_response, 0 );   

    do
    {
        frame_seq++;
        if(!frame_seq)
        {
            frame_seq++;
        }   
        DBG_LOG("\r\n upload behavior pachage sequence  = %d\r\n\0",frame_seq);
        record_len = user_bhv_read_records(bhv_record_buf,BHV_UPLOAD_RECORD_SIZE);
        DBG_LOG("\r\n read behavior record, record number = %d\r\n\0",record_len);
        if(record_len > 0)
        {
            app_get_indetity_msg(&iden.head);
            app_iden_get_device_id_char(iden.dev_id);
            //encode data
            len_temp = user_bhv_encode_data(encode_buf,
                                          encode_buf_size,
                                          bhv_record_buf,
                                          record_len,
                                          iden
                                       );
            DBG_LOG("\r\n encode behavior data, len = %d\r\n\0",len_temp);                              
            if(len_temp > 0)
            {
                //package encoded data
                len_temp = user_data_packge(send_buf, send_buf_size, encode_buf, len_temp, 
                                                PROTOCOL_HEADER_TYPE_TYPE_BEHAVIOR2_REQ,
                                                frame_seq,func_code,chn_type);                 
                DBG_LOG("\r\nbehavior data package, len = %d\r\n\0",len_temp);
                if(len_temp > 0)
                {
                    //transmit data                    
                    DBG_LOG("\r\nbehavior transmit data, len = %d\r\n",len_temp);
                    ret = user_send_data(send_buf,len_temp,chn_type);  
                    if(ret)
                    {
                        ret = user_bhv_wait_rsp(chn_type);
                        if(ret)
                        {                            
                            //if(chn_type == COMMUNICATION_INTERFACE_TYPE_BLE)
                            {
                                DBG_LOG("\r\ndelete behavior record numbers = %d ",record_len);
                                if(Record_Delete(RECORD_TYPE_BEHAVIOR,record_len))
                                {
                                    DBG_LOG("successful\r\n\r\n",record_len);                               
                                }
                                else
                                {
                                    DBG_LOG("failed\r\n\r\n",record_len);
                                }
                            }
                        }
                    }
                }                    
            }
        }
    //}while(record_len && ret);
    }while(record_len && len_temp && ret);
    DBG_LOG("behavior data upload over\r\n\0");
    vSemaphoreDelete( bhv_semaphore_wait_response );
    //free(send_buf);
    //free(p_encode_buf);
    return ret;
}
/*
//function for data collect task, it performs the acc data process
static void user_bhv_detect_task(void *arg)
{
    acc_origin_point_t  p_acc_pt[HAL_ACC_POINT_SAMPLES];
    acc_origin_point_t  acc_xyz_temp;
    uint8_t  acc_xyz_point_num;
    uint8_t  pt_cnt = 0;
    while(xQueueReceive(queue_bhv_detect, &acc_xyz_point_num , 0) != errQUEUE_EMPTY);
    while(1)
    {
        pt_cnt = 0;
        //xSemaphoreTake( binary_semaphore_bhv_detect, portMAX_DELAY );
        xQueueReceive(queue_bhv_detect, &acc_xyz_point_num , portMAX_DELAY);
        //examin queue data numbers if equal to gived len
        if(acc_xyz_point_num == uxQueueMessagesWaiting(queue_bhv_detect) && acc_xyz_point_num <= HAL_ACC_POINT_SAMPLES)                   
        //if(acc_xyz_point_num <= HAL_ACC_POINT_SAMPLES)        //number one is data in queue length
        {
            //now receive the valid datas of x,y,z-axis
            while(xQueueReceive(queue_bhv_detect, &acc_xyz_temp , 0) != errQUEUE_EMPTY)
            {
                p_acc_pt[pt_cnt++] = acc_xyz_temp;
            }
            Behavior_Record_t   *bhv_res_t;
            //caculate odba,
            bhv_res_t = user_bhv_get_odba(p_acc_pt,pt_cnt);
            if(bhv_res_t != NULL)
            {
                //save odba result
                m_bhv_res_buf[m_bhv_res_cnt++] = *bhv_res_t;
                if(m_bhv_res_cnt >= BHV_RESULT_BUF_MAX)
                {
                    //save all 6-item, if reached maximum numbers points
                    user_bhv_save_records(m_bhv_res_buf,m_bhv_res_cnt);//save all buffer records into rom memory
                    m_bhv_res_cnt = 0;          //clear bhv record buffer
                }
                //send current one behavior result to estrus queue                
            }
        }
        else
        {
            //clear queue
            while(xQueueReceive(queue_bhv_detect, &acc_xyz_point_num , 0) != errQUEUE_EMPTY);
        }
    }
}
void user_creat_bhv_detect_task(void)
{
    BaseType_t ret;  
    //vSemaphoreCreateBinary( binary_semaphore_bhv_detect );
    binary_semaphore_bhv_detect = xSemaphoreCreateBinary();
    //binary_semaphore_wait_response = xSemaphoreCreateBinary();
    queue_bhv_detect = xQueueCreate( HAL_ACC_POINT_SAMPLES, sizeof(acc_origin_point_t) );
    
    ret = xTaskCreate( user_bhv_detect_task, "bhv", 256, NULL, 1, &bhv_detect_handle );
    if(pdPASS != ret)
    {
        APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
    }  
}
void user_send_queue_acc_data_to_bhv_task(acc_origin_point_t *p_acc_dt, uint32_t pt_nums)
{
    
    for(uint8_t i=0; i<pt_nums; i++)
    {
         while(xQueueSendToBack(queue_bhv_detect, &p_acc_dt[i], 0) != pdPASS);
    }
}
*/
int32_t user_bhv_get_interval(void)
{
    return m_bhv_sample_param.interval;
}
//end functions

