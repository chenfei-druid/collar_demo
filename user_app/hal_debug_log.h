#define DEBUG   0


#if NRF_LOG_ENABLED
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "SEGGER_RTT.h"
#include "SEGGER_RTT_Conf.h"
#endif //NRF_LOG_ENABLED


#if DEBUG == 1
#define  DBG_LOG(...)  NRF_LOG_INFO(__VA_ARGS__)
#else
#define  DBG_LOG(...)  0


#endif  //DEBUG




