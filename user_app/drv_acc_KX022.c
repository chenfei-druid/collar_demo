/** 
 * drv_kx022.c
 *
 * @group:       nack_trap project
 * @author:      Chenfei
 * @createdate: 2017/9/29
 * @modifydate: 2018/7/3
 * @version:     V0.0
 *
 */
#include "app_error.h"
#include "nrf_drv_spi.h"
#include "app_util_platform.h"
//#include "nrf_delay.h"
#include "nrf_gpio.h"
#include "KX022_registors.h"
#include "drv_acc_kx022.h"
#include "user_nrf_gpio_config.h"

#include "nrf_drv_gpiote.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "user_app_log.h"
#include "queue.h"
#include "user_acc.h"

/*******************************************defines*************************************************/

#define     SPI_EVENT_TIMEOUT       500     //mS
#define     LOCK_TIMEOUT            100

static      xSemaphoreHandle        semaphore_wait_event = NULL;
static      xSemaphoreHandle        drv_kx022_interrupt_semaphore = NULL;
static      xSemaphoreHandle        drv_kx022_start_sema = NULL;
static      xSemaphoreHandle        spi_lock = NULL;
static      QueueHandle_t           drv_kx022_queue  = NULL;
static      TaskHandle_t            drv_kx022_handle;
//extern    void user_acc_send_collect_semaphore(void);
static      bool                    drv_kx022_run_status = 0;
            bool                    kx022_int_flag=0;

static volatile bool spi_xfer_done;  /**< Flag used to indicate that SPI instance completed the transfer. */

static nrf_drv_spi_t spi;// = NRF_DRV_SPI_INSTANCE(spi_INSTANCE);  /**< SPI instance. */

static drv_kx022_int_callback   drv_kx022_interrupt_event_callback;

static int16_t factor = 2;
//static uint32_t time_out  = 500;
static uint32_t time_cnt = 0;
/*******************************************functions*************************************************/
static void drv_kx_gpio_config(void)
{
    nrf_gpio_cfg_output(DRV_KX022_SPI_CS_PIN); 
}
static void drv_kx022_cs_low(void)
{
    nrf_gpio_cfg_output(DRV_KX022_SPI_CS_PIN);
    nrf_gpio_pin_clear(DRV_KX022_SPI_CS_PIN);
    nrf_spim_enable(spi.p_registers);
}
static void drv_kx022_cs_high(void)
{
    nrf_gpio_cfg_output(DRV_KX022_SPI_CS_PIN);
    nrf_gpio_pin_set(DRV_KX022_SPI_CS_PIN);
    nrf_spim_disable(spi.p_registers);
}
/**
* @function@name:    drv_kx022_send_queue
* @description  :    function for transmit queue of xyz buf
* @input        :    p_buf, xyz buffer pointer
*                    len, xyz sample number
* @output       :    none
* @return       :    none
*/
static void drv_kx022_send_queue(uint8_t *p_buf, uint8_t len)
{
    if(p_buf == NULL || len == 0)
    {
        return;
    }
    //user_acc_trans_data(p_buf,len);
    xQueueSendToBack(drv_kx022_queue, &len, 1);
    for(uint8_t i=0;i<len;i++)
    {
        xQueueSendToBack(drv_kx022_queue, p_buf++, 0);
    }    
}
/**
* @function@name:    drv_kx022_read_cached_sample
* @description  :    function for receive kx022 xyz data from drv_kx022_queue
* @input        :    tiemout_ms, the wait max time of mS
*                    buf_size, out buffer max size of byte
* @output       :    p_xyz, xyz buffer pointer
* @return       :    xyz point numbers
*/
uint32_t drv_kx022_read_cached_sample(uint8_t *p_xyz, uint16_t buf_size, uint32_t tiemout_ms)
{
    uint8_t      queue;
    uint8_t     *p_buf = (uint8_t *)p_xyz;
    uint8_t     len_cnt =0;
    if(xQueueReceive(drv_kx022_queue, &queue , tiemout_ms) == pdPASS)
    {
        /*uint8_t waitsize = uxQueueMessagesWaiting(drv_kx022_queue);
        if(queue != waitsize)
        {
            xQueueReset(drv_kx022_queue);
            return 0;
        }
        */
        while(xQueueReceive(drv_kx022_queue, &queue , 0) != errQUEUE_EMPTY)
        {
            p_buf[len_cnt++] = queue;
            //if(len_cnt >= DRV_KX022_FIFO_SAMPLES*DRV_kx022_ONE_XYZ_DATA_LEN)
            if(len_cnt >= buf_size)
            {
                xQueueReset(drv_kx022_queue);
                break;
            }
        }
        DBG_LOG("read fifo point numbers = %d, sample time = %d\r\n",len_cnt / DRV_kx022_ONE_XYZ_DATA_LEN, time_cnt++);
    }    
    return len_cnt / DRV_kx022_ONE_XYZ_DATA_LEN;
}
/**
 * @brief SPI user event handler.
 * @param event
 */
static void spi_event_handler(nrf_drv_spi_evt_t const * p_event,void * p_context)
{
    spi_xfer_done = true;
    //xSemaphoreGive(semaphore_wait_event);     
    static BaseType_t semaphore = pdFALSE;
    xSemaphoreGiveFromISR(semaphore_wait_event,&semaphore);
    //DBG_LOG("kx022: spi event\r\n");
}

/**
* @function@name:    drv_kx022_spi_tx
* @description  :    function for send server data to kx022
* @input        :    p_cmd, command for kx022
*                    length, command length of bytes
* @output       :    none
* @return       :    true if read successed, false if read failed
*/
static bool drv_kx022_spi_tx(const uint8_t * p_cmd, uint8_t const length)
{
    uint32_t ret = 0;
    if(p_cmd == NULL)
    {
        return false;
    }
    if(semaphore_wait_event == NULL)
    {   
        semaphore_wait_event = xSemaphoreCreateBinary();
        xSemaphoreTake( semaphore_wait_event, 1);
    }
    
    ret = nrf_drv_spi_transfer(&spi,p_cmd,length,NULL,0);      
    if(ret != NRF_SUCCESS)
    {
        DBG_LOG("kx022: spi transmit failed\r\n");
        return false;
    }    
    if(xSemaphoreTake( semaphore_wait_event, SPI_EVENT_TIMEOUT) != pdPASS)
    {
        /* Send received value back to the caller */
        DBG_LOG("kx022: spi transmit wait timeout\r\n");
        return false;
    }
    //DBG_LOG("kx022: spi transmit successed\r\n");
    return true;
}
/**
* @function@name:    drv_kx022_spi_tx
* @description  :    function for send server data to kx022
* @input        :    length, bytes will be read
* @output       :    p_data, out data 
* @return       :    true if read successed, false if read failed
*/
static bool drv_kx022_spi_rx(uint8_t * const p_data, uint8_t length)
{
    uint32_t ret = 0;
    if(p_data == NULL)
    {
        return false;
    }
    if(semaphore_wait_event == NULL)
    {   
        semaphore_wait_event = xSemaphoreCreateBinary();
        xSemaphoreTake( semaphore_wait_event, 1);
    }
    ret = nrf_drv_spi_transfer(&spi,NULL,0,p_data ,length);
    if(ret != NRF_SUCCESS)
    {
        DBG_LOG("kx022: spi receive failed\r\n");
        return false;
    }
    if(xSemaphoreTake( semaphore_wait_event, SPI_EVENT_TIMEOUT) != pdPASS)
    {
        /* Send received value back to the caller */
        DBG_LOG("kx022: spi receive timeout\r\n");
        return false;
    }
    //DBG_LOG("kx022: spi receive successed\r\n");
    return true;
}
/**
* @function@name:    drv_kx022_read_registor
* @description  :    read one registor's data through spi interface from kx022
* @input        :    Reg, registor address
* @output       :    Data, data from registor
* @return       :    true if read successed, false if read failed
*/

bool drv_kx022_read_registor(uint8_t const reg, uint8_t * p_data)
{
    drv_kx022_cs_low();
    uint8_t cmd = reg | READ;
    if(drv_kx022_spi_tx(&cmd,1) != true)
    {        
        DBG_LOG("kx022: read reg tx failed\r\n");
        return false;
    }
    if(drv_kx022_spi_rx(p_data,1)!= true)
    {
        DBG_LOG("kx022: read reg data failed\r\n");
        return false;
    }   
    drv_kx022_cs_high();
    
    return true;
}
/**
* @function@name:    drv_kx022_write_registor
* @description  :    write one registor through spi interface for kx022
* @input        :    WriteAddr, registor address, Data, the data will be wroten into regestor
* @output       :    none
* @return       :    true if read successed, false if read failed
*/

static bool drv_kx022_write_registor(uint8_t addr, uint8_t data)
{
    uint8_t tx_buf[2] = {addr | WRITE, data};
    drv_kx022_cs_low();
    if(drv_kx022_spi_tx(tx_buf,2) != true)
    {
        DBG_LOG("kx022: write reg tx failed\r\n");
        return false;
    }
    drv_kx022_cs_high();
    return true;
}
/**
* @function@name:    kx022_read_xyz_axiz_data
* @description  :    read one set data of X-axiz, Y-axiz, Z-axiz, if 8-bit samples, read 3 bytes, if 16-bit samples, read 6 bytes
* @input        :    length, fifo data length
* @output       :    fifo_buff, the fifo data save into it
* @return       :    true if read successed, false if read failed
*/
static bool drv_kx022_read_xyz_axiz_data(drv_kx022_point_xyz_t * const xyz_buff)
{

    uint8_t tx_buf[2] = {REGISTER_XOUTL | READ,0};
    if(drv_kx022_spi_tx(tx_buf,1) != true)
    {
        DBG_LOG("kx022: read xyz tx failed\r\n");
        return false;
    }
    if(drv_kx022_spi_rx((uint8_t *)xyz_buff,DRV_kx022_ONE_XYZ_DATA_LEN)!= true)
    {
        DBG_LOG("kx022: read xyz rx failed\r\n");
        return false;
    }   
    return true;      
}
bool drv_kx022_read_one_xyz_data(drv_kx022_point_xyz_t * const xyz_buff)
{
    drv_kx022_cs_low();
    
    bool ret = drv_kx022_read_xyz_axiz_data(xyz_buff);
    
    drv_kx022_cs_high();
    
    return ret;
}
/*****************************************************************************************
* @function@name:    drv_kx022_get_fifo_data
* @description  :    read the fifo data of kx022, the numbers = KX022_FIFO_SAMPLES
* @input        :    length, fifo data length
* @output       :    fifo_buff, the fifo data save into it
* @return       :    true if read successed, false if read failed
******************************************************************************************/
static bool drv_kx022_get_fifo_data(drv_kx022_point_xyz_t * const fifo_buff,uint8_t points)
{   
    uint8_t tx_buf[2] = {REGISTER_BUF_READ | READ,0};
    drv_kx022_cs_low();
    if(drv_kx022_spi_tx(tx_buf,1) != true)
    {
        DBG_LOG("kx022: get fifo tx failed\r\n");
        return false;
    }
    if(drv_kx022_spi_rx((uint8_t *)fifo_buff,DRV_kx022_ONE_XYZ_DATA_LEN * points)!= true)
    {
        DBG_LOG("kx022: get fifo receive failed\r\n");
        return false;
    }   
    drv_kx022_cs_high();
    //DBG_LOG("kx022: get fifo data successed\r\n");
    return true;      
}
/**
* @function@name:    drv_kx022_get_range_factor
* @description  :    function for get the factor of different range, 2G/4G/8G
* @input        :    none
* @output       :    factor
* @return       :    return factor
*/
int16_t drv_kx022_get_range_factor(uint8_t range)
{    
    int16_t ret;
    switch(range)
    {
        case DRV_KX022_2G:
            ret = 1;       //if 2G range, the factor is 1
            break;
        case DRV_KX022_4G:
            ret = 2;       //if 4G range, the factor is 2
            break;
        case DRV_KX022_8G:
            ret = 4;       //if 8G range, the factor is 4
            break;
        default:
            ret = 0;       //error
            break;
    }
    return ret; 
}
/**
* @function@name:    drv_kx022_convert_complement_to_source_code
* @description  :    read the fifo data of kx022, the numbers = KX022_FIFO_SAMPLES
* @input        :    src_buff, complement code data
*               :    points, xyz points will be converted
* @output       :    dst_buff, source code data
* @return       :    true if read successed, false if read failed
*/
uint32_t drv_kx022_convert_complement_to_source_code(drv_kx022_point_xyz_t * const dst_buff, \
                                                     uint8_t const points
                                                    )
{
    if(dst_buff == NULL)
    {
        DBG_LOG("hal acc convert of source data is null\r\n");
        return 0;
    }
    if(points > DRV_KX022_FIFO_SAMPLES)
    {
        DBG_LOG("hal acc convertion of point numbers is more than cache size\r\n");
        return 0;
    }     
    int16_t div = 16;
    for(uint32_t i=0; i< points ; i++)
    {               
        dst_buff[i].x = factor * dst_buff[i].x  / div;       
        dst_buff[i].y = factor * dst_buff[i].y  / div;        
        dst_buff[i].z = factor * dst_buff[i].z  / div;
    }
    //DBG_LOG("\r\nhal acc convertion complete\r\n");
    return points;
}
/*****************************************************************************************
* @function@name:    drv_kx022_get_sample_points
* @description  :    read the fifo data of kx022, the numbers = KX022_FIFO_SAMPLES
* @input        :    length, fifo data length
* @output       :    fifo_buff, the fifo data save into it
* @return       :    true if read successed, false if read failed
******************************************************************************************/
/*static uint32_t drv_kx022_get_sample_points(void)
{    
    drv_kx022_point_xyz_t fifo_buff[DRV_KX022_FIFO_BUFFER_MAX] = {0};
    uint8_t point_numbers;
    uint8_t *p_fifo_buf = (uint8_t *)fifo_buff;
    
    drv_kx022_cs_low();
    point_numbers = drv_kx022_get_fifo_points();
    if(point_numbers > DRV_KX022_FIFO_BUFFER_MAX)
    {
        point_numbers = DRV_KX022_FIFO_BUFFER_MAX;
    }
    //DBG_LOG("kx022 read samples numbers = %d\r\n",point_numbers);
    uint32_t ret = drv_kx022_get_fifo_data(fifo_buff,point_numbers);
 
    if(ret > 0)
    {       
        ret = drv_kx022_convert_complement_to_source_code(fifo_buff,point_numbers);
    }
    drv_kx022_cs_high();
    drv_kx022_clear_interrupt_source();
    drv_kx022_send_queue(p_fifo_buf,ret*DRV_kx022_ONE_XYZ_DATA_LEN);
    return ret;
}
*/
uint32_t drv_kx022_get_sample_points(drv_kx022_point_xyz_t * const fifo_buff,uint8_t points)
{    
    if(fifo_buff == NULL)
    {
        DBG_LOG("driver kx022 get sample pointer is null\r\n");
        return 0;
    }
    if(points == 0)
    {
        DBG_LOG("driver kx022 get sample number is 0\r\n");
        return 0;
    }   
    uint32_t ret = drv_kx022_get_fifo_data(fifo_buff,points);
    if(ret > 0)
    {       
        ret = drv_kx022_convert_complement_to_source_code(fifo_buff,points);
    }
    

    return ret;
}
static uint32_t drv_kx022_get_sample(void)
{
    acc_origin_point_t  xyz_buf[DRV_KX022_FIFO_SAMPLES];
    uint8_t point_numbers;

    point_numbers = drv_kx022_get_fifo_points();   
    
    if(point_numbers > DRV_KX022_FIFO_SAMPLES)
    {
        point_numbers = DRV_KX022_FIFO_SAMPLES;            
    }
    if(point_numbers != 0)
    {
         point_numbers = drv_kx022_get_sample_points(xyz_buf,point_numbers); 
    }                              
    drv_kx022_clear_interrupt_source();  
    //DBG_LOG("read fifo point numbers = %d\r\n",point_numbers);
    //transmit xyz data into queue 
    drv_kx022_send_queue((uint8_t *)xyz_buf,point_numbers*DRV_kx022_ONE_XYZ_DATA_LEN);
    return point_numbers;
    
}
/***************************************BUF_ODCNTL(addr:0x1B) config************************************************/

/**
* @function@name:    drv_kx022_set_filter_bypass_mode
* @description  :    Function for set filter bypass mode,
* @input        :    able: 0-filtering applied, 1-filter bypassed
* @output       :    none
* @return       :    none
*/
static bool drv_kx022_set_filter_bypass_mode(bool mode)
{
    uint8_t value;
    if(drv_kx022_read_registor(REGISTER_ODCNTL,&value) != true)
    {
        return false;
    }
    value &= ~0x80;
    value |= (uint8_t)(mode << 7);
    if(drv_kx022_write_registor(REGISTER_ODCNTL,value) != true)
    {
        return false;
    }
    
    //read back to veriry
    uint8_t temp = 0;
    if(drv_kx022_read_registor(REGISTER_ODCNTL,&temp) != true)
    {
        return false;
    }
    if(temp != value)
    {
        DBG_LOG("kx022: set_filter_bypass write value verify failed\r\n");
        return false;
    }
    
    return true;
}
/**
* @function@name:    drv_kx022_set_low_pass_filter
* @description  :    Function for set low-pass filter roll off control,
* @input        :    able: 0-filter corner frequency set to ODR/9, 1-filter corner frequency set to ODR/2
* @output       :    none
* @return       :    none
*/
static bool drv_kx022_set_low_pass_filter_freq(drv_kx022_filter_freq_t mode)
{
    uint8_t value;
    if(mode > DRV_KX022_FILT_FREQ_ODR_0)
    {
        return false;
    }
    if(mode == DRV_KX022_FILT_FREQ_ODR_0)
    {
        //bypass filter
        if(drv_kx022_set_filter_bypass_mode(DRV_KX022_FILTER_BYPASSED) != true)
        {
            return false;
        }
        return true;        
    }
    else
    {
        //applied filter
        if(drv_kx022_set_filter_bypass_mode(DRV_KX022_FILTER_APPLIED) != true)
        {
            return false;
        }
        if(drv_kx022_read_registor(REGISTER_ODCNTL,&value) != true)
        {
            return false;
        }
        value &= ~0x40;
        value |= (uint8_t)(mode << 6);
        if(drv_kx022_write_registor(REGISTER_ODCNTL,value) != true)
        {
            return false;
        }       
        //read back to veriry
        uint8_t temp = 0;
        if(drv_kx022_read_registor(REGISTER_ODCNTL,&temp) != true)
        {
            return false;
        }
        if(temp != value)
        {
            DBG_LOG("kx022: set filter write value verify failed\r\n");
            return false;
        }
    }    
    return true;
}

/**
* @function@name:    drv_kx022_set_out_data_rate
* @description  :    Function for set output data rate,
* @input        :    rate: acceleration output data rate, @ref drv_kx022_out_data_rate_t
* @output       :    none
* @return       :    none
*/
static bool drv_kx022_set_out_data_rate(drv_kx022_out_data_rate_t rate)
{
    uint8_t value;
    if(drv_kx022_read_registor(REGISTER_ODCNTL,&value) != true)
    {
        return false;
    }
    value &= ~0x0f;
    value |= (uint8_t)(0x0f & rate);
    if(drv_kx022_write_registor(REGISTER_ODCNTL,value) != true)
    {
        return false;
    }
    
    //read back to veriry
    uint8_t temp = 0;
    if(drv_kx022_read_registor(REGISTER_ODCNTL,&temp) != true)
    {
        return false;
    }
    if(temp != value)
    {
        DBG_LOG("kx022: set out data rate write value verify failed\r\n");
        return false;
    }
    
    return true;
}

/***************************************Register INC4(addr:0x1F) config************************************************/

/**
* @function@name:    drv_kx022_set_interrupt_report_int1
* @description  :    Function for set interrupt report on interrupt pin int1, 

*                    0 | BFI1 | WMI1 | DRDYI1 | Reserved | TDTI1 | WUFI1 | TPI1  

* @input        :    report_flag, @ref drv_kx022_int_report_t
                    BFI1 -Buffer full interrupt reported on physical interrupt pin INT1
                    WMI1 - Watermark interrupt reported on physical interrupt pin INT1
                    DRDYI1- Data ready interrupt reported on physical interrupt pin INT1
                    TDTI1 - Tap/Double Tap interrupt reported on physical interrupt pin INT1
                    WUFI1- Wake-Up (motion detect) interrupt reported on physical interrupt pin INT1
                    TPI1-Tilt position interrupt reported on physical interrupt pin INT1
* @output       :    none
* @return       :    none
*/
static bool drv_kx022_set_interrupt_report_int1(drv_kx022_int_report_t report_flag)
{
    uint8_t value = (uint8_t)(1 << report_flag);
   
    if(drv_kx022_write_registor(REGISTER_INC4,value)!= true)   
    {
        return false;
    }
    //read back to veriry
    uint8_t temp = 0;
    if(drv_kx022_read_registor(REGISTER_INC4,&temp) != true)
    {
        return false;
    }
    if(temp != value)
    {
        DBG_LOG("kx022: set interrupt report int1 veriry value verify failed\r\n");
        return false;
    }
    return true;
}

/***************************************Register INC1(addr:0x1B) config************************************************/

/**
* @function@name:    drv_kx022_enable_disable_int_pin
* @description  :    Function for enables/disables the physical interrupt pin
* @input        :    polarity: 0-physical interrupt pin is disabled, 
*                              1-physical interrupt pin is enabled
* @output       :    none
* @return       :    none
*/
static bool drv_kx022_enable_int_pin(bool able)
{
    uint8_t value;
    if(drv_kx022_read_registor(REGISTER_INC1,&value)!= true)   
    {
        return false;
    }
    value &= ~0x20;
    value |= (uint8_t)(able << 5);
    if(drv_kx022_write_registor(REGISTER_INC1,value)!= true)   
    {
        return false;
    }
    //read back to veriry
    uint8_t temp = 0;
    if(drv_kx022_read_registor(REGISTER_INC1,&temp) != true)
    {
        return false;
    }
    if(temp != value)
    {
        DBG_LOG("kx022: enable int pin value verify failed\r\n");
        return false;
    }
    return true;
}
/**
* @function@name:    drv_kx022_set_int_pin_polarity
* @description  :    Function for set the polarity of the physical interrupt pin,
* @input        :    polarity: 0-polarity of the physical interrupt pin is active low, 
*                              1-polarity of the physical interrupt pin is active high
* @output       :    none
* @return       :    none
*/
static bool drv_kx022_set_int_pin_active_polarity(drv_kx022_int_active_t polarity)
{
    uint8_t value;
    if(drv_kx022_read_registor(REGISTER_INC1,&value)!= true)   
    {
        return false;
    }
    value &= ~0x10;
    value |= (uint8_t)(polarity << 4);
    if(drv_kx022_write_registor(REGISTER_INC1,value)!= true)   
    {
        return false;
    }
    //read back to veriry
    uint8_t temp = 0;
    if(drv_kx022_read_registor(REGISTER_INC1,&temp) != true)
    {
        return false;
    }
    if(temp != value)
    {
        DBG_LOG("kx022: set int pin active veriry value verify failed\r\n");
        return false;
    }
    return true;
}
/**
* @function@name:    drv_kx022_set_interrupt_response_type
* @description  :    Function for set the response of the physical interrupt pin,
* @input        :    polarity: 0-the physical interrupt pin latches until it is cleared by reading INT_REL, 
*                              1- the physical interrupt pin will transmit one pulse with a period of 50 us
* @output       :    none
* @return       :    none
*/
static bool drv_kx022_set_interrupt_response_type(bool mode)
{
    uint8_t value;
    if(drv_kx022_read_registor(REGISTER_INC1,&value)!= true)   
    {
        return false;
    }
    value &= ~0x08;
    value |= (uint8_t)(mode << 3);
    if(drv_kx022_write_registor(REGISTER_INC1,value)!= true)   
    {
        return false;
    }
    //read back to veriry
    uint8_t temp = 0;
    if(drv_kx022_read_registor(REGISTER_INC1,&temp) != true)
    {
        return false;
    }
    if(temp != value)
    {
        DBG_LOG("kx022: set interrupt type int1 veriry value verify failed\r\n");
        return false;
    }
    return true;
}

/*********************************Register CNTL1(addr:0x18) config********************************************/
/**
* @function@name:    drv_kx022_into_standby_mode
* @description  :    Function for setting kx022 into standby mode, the current is 0.9uA in this mode
* @input        :    none
* @output       :    none
* @return       :    none
*/
static bool drv_kx022_into_standby_mode(void)
{
//    uint8_t value;
    uint8_t value;
    if(drv_kx022_read_registor(REGISTER_INC1,&value)!= true)   
    {
        return false;
    }
    value &= 0x7f;
    if(drv_kx022_write_registor(REGISTER_CNTL1,value) != true)                        //change into standby mode
    {
        return false;
    }
    //for veriry
    uint8_t temp = 0;
    if(drv_kx022_read_registor(REGISTER_CNTL1,&temp) != true)
    {
        return false;
    }
    if(temp != value)
    {
        DBG_LOG("kx022: standby mode write vaule failed\r\n");
        return false;
    }
    return true;
}

/**
* @function@name:    drv_kx022_into_operating_mode
* @description  :    Function for setting kx022 into operating mode,
*                    CNTL1.PC1 controls the operating mode of the KX022. When in RES = 0, please allow 1.2/ODR
*                    delay time when transistioning from stand-by PC1 = 0 to operating mode PC1 = 1 to
*                    allow new settings to load.
*                    0 = stand-by mode
*                    1 = operating mode
* @input        :    none
* @output       :    none
* @return       :    none
*/
static bool drv_kx022_into_operating_mode(void)
{
    uint8_t value;
    if(drv_kx022_read_registor(REGISTER_CNTL1,&value)!= true)   
    {
        return false;
    }
    value |= 0x80;
    if(drv_kx022_write_registor(REGISTER_CNTL1,value)!= true)   
    {
        return false;
    } 
    //read back to veriry
    uint8_t temp = 0;
    if(drv_kx022_read_registor(REGISTER_CNTL1,&temp) != true)
    {
        return false;
    }
    if(temp != value)
    {
        DBG_LOG("kx022: set operating mode veriry value verify failed\r\n");
        return false;
    }
    return true;
}

/**
* @function@name:    drv_kx022_set_performance_mode
* @description  :    Function for determines the performance mode of the KX022,
* @input        :    mode: 0 = low current.
*                          1 = high current. Bandwidth (Hz) = ODR/2
* @output       :    none
* @return       :    none
*/
static bool drv_kx022_set_power_mode(drv_kx022_perform_mode_t mode)
{
    uint8_t value;
    if(drv_kx022_read_registor(REGISTER_CNTL1,&value)!= true)   
    {
        return false;
    }
    value &= ~0x40;
    value |= (uint8_t)(mode << 6);
    if(drv_kx022_write_registor(REGISTER_CNTL1,value)!= true)   
    {
        return false;
    }
    //read back to veriry
    uint8_t temp = 0;
    if(drv_kx022_read_registor(REGISTER_CNTL1,&temp) != true)
    {
        return false;
    }
    if(temp != value)
    {
        DBG_LOG("kx022: set power mode veriry value verify failed\r\n");
        return false;
    }
    return true;
}
/**
* @function@name:    drv_kx022_set_data_ready_interrupt
* @description  :    Function for enables the reporting of the availability of new acceleration data as an interrupt,
* @input        :    mode: 0 = availability of new acceleration data is not reflected as an interrupt
*                          1 = availability of new acceleration data is reflected as an interrupt
* @output       :    none
* @return       :    none
*/
static bool drv_kx022_reflect_data_ready_interrupt(bool mode)
{
    uint8_t value;
    if(drv_kx022_read_registor(REGISTER_CNTL1,&value)!= true)   
    {
        return false;
    }
    value &= ~0x20;
    value |= (uint8_t)(mode << 5);
    if(drv_kx022_write_registor(REGISTER_CNTL1,value)!= true)   
    {
        return false;
    }
    //read back to veriry
    uint8_t temp = 0;
    if(drv_kx022_read_registor(REGISTER_CNTL1,&temp) != true)
    {
        return false;
    }
    if(temp != value)
    {
        DBG_LOG("kx022: set reflect data ready interrupt value verify failed\r\n");
        return false;
    }
    return true;
}
/**
* @function@name:    drv_kx022_select_range
* @description  :    Function for selects the acceleration range of the accelerometer outputs,
* @input        :    mode: 0 = availability of new acceleration data is not reflected as an interrupt
*                          1 = availability of new acceleration data is reflected as an interrupt
* @output       :    none
* @return       :    none
*/
static bool drv_kx022_select_range(drv_kx022_range_t range)
{
    uint8_t value;
    if(drv_kx022_read_registor(REGISTER_CNTL1,&value)!= true)   
    {
        return false;
    }
    value &= ~0x18;
    value |= (uint8_t)(range << 3);
    if(drv_kx022_write_registor(REGISTER_CNTL1,value)!= true)   
    {
        return false;
    }
    
    //read back to veriry
    uint8_t temp = 0;
    if(drv_kx022_read_registor(REGISTER_CNTL1,&temp) != true)
    {
        return false;
    }
    if(temp != value)
    {
        DBG_LOG("kx022: select range value verify failed\r\n");
        return false;
    }
    return true;
}
/**
* @function@name:    drv_kx022_enable_wakeup
* @description  :    Function for enables the Wake Up (motion detect) function,
* @input        :    mode: 0 = Wake Up function disabled
*                          1 = Wake Up function enabled
                    CNTL1.WUFE enables the Wake Up (motion detect) function. 0= disabled, 1= enabled. Note that to
                    change the value of this bit, the PC1 bit must first be set to �0�.
                    0 = Wake Up function disabled
                    1 = Wake Up function enabled
* @output       :    none
* @return       :    none
*/
static bool drv_kx022_enable_wakeup(bool mode )
{
    uint8_t value;
    if(drv_kx022_read_registor(REGISTER_CNTL1,&value)!= true)   
    {
        return false;
    }
    value &= ~0x02;
    value |= (uint8_t)(mode << 1);
    if(drv_kx022_write_registor(REGISTER_CNTL1,value)!= true)   
    {
        return false;
    }
    //read back to veriry
    uint8_t temp = 0;
    if(drv_kx022_read_registor(REGISTER_CNTL1,&temp) != true)
    {
        return false;
    }
    if(temp != value)
    {
        DBG_LOG("kx022: enable wakeup value verify failed\r\n");
        return false;
    }
    return true;
}

/*********************************Register BUF_CNTL1(addr:0x3A) config********************************************/

/**
* @function@name:    drv_kx022_set_fifo_samples_threshold
* @description  :    Function for set fifo samples numbers,determines the number of samples that will trigger a
*                    watermark interrupt or will be saved prior to a trigger event. When BUF_RES=1, the
*                    maximum number of samples is 41; when BUF_RES=0, the maximum number of samples is 84
* @input        :    sample_nums: number of samples that will trigger a watermark interrupt 
* @output       :    none
* @return       :    none
*/
static bool drv_kx022_set_fifo_samples_threshold(uint8_t sample_nums)
{
    if(drv_kx022_write_registor(REGISTER_BUF_CNTL1,sample_nums)!= true)   
    {
        return false;
    }   
    //read back to veriry
    uint8_t temp = 0;
    if(drv_kx022_read_registor(REGISTER_BUF_CNTL1,&temp) != true)
    {
        return false;
    }
    if(temp != sample_nums)
    {
        DBG_LOG("kx022: set fifo samples value verify failed\r\n");
        return false;
    }
    return true;
}

/***************************************BUF_CNTL2(addr:0x3B) config************************************************/
/**
* @function@name:    drv_kx022_set_sample_buffer_activa
* @description  :    Function for enable or disable sample buffer,
* @input        :    able: 0-sample buffer inactive, 1-sample buffer active
* @output       :    none
* @return       :    none
*/
static bool drv_kx022_set_sample_buffer_active(bool able)
{
    uint8_t value;
    if(drv_kx022_read_registor(REGISTER_BUF_CNTL2,&value)!= true)   
    {
        return false;
    }
    value &= ~0x80;
    value |= (uint8_t)(able << 7);
    if(drv_kx022_write_registor(REGISTER_BUF_CNTL2,value)!= true)   
    {
        return false;
    }  
    uint8_t temp = 0;
    if(drv_kx022_read_registor(REGISTER_BUF_CNTL2,&temp) != true)
    {
        return false;
    }
    if(temp != value)
    {
        DBG_LOG("kx022: set sample buffer active: value verify failed\r\n");
        return false;
    }
    return true;
}
/**
* @function@name:    drv_kx022_set_data_samples_bits
* @description  :    Function for set output data rate,
* @input        :    sample_bit:
*                    BUF_CNTL2.BRES determines the resolution of the acceleration data samples collected by the sample buffer.
*                    BUF_RES = 0-8-bit samples are accumulated in the buffer
                     BUF_RES = 1-16-bit samples are accumulated in the buffer
* @output       :    none
* @return       :    none
*/
static bool drv_kx022_set_data_samples_bits(drv_kx022_sample_bits_t sample_bit)
{
    uint8_t value;
    if(drv_kx022_read_registor(REGISTER_BUF_CNTL2,&value)!= true)   
    {
        return false;
    }
    value &= ~0x40;    
    value |= (uint8_t)(sample_bit << 6);  
    if(drv_kx022_write_registor(REGISTER_BUF_CNTL2,value)!= true)   
    {
        return false;
    } 
    uint8_t temp = 0;
    if(drv_kx022_read_registor(REGISTER_BUF_CNTL2,&temp) != true)
    {
        return false;
    }
    if(temp != value)
    {
        DBG_LOG("kx022: set sample bits value verify failed\r\n");
        return false;
    }
    return true;
}
/**
* @function@name:    drv_kx022_set_buffer_full_interrupt
* @description  :    Function for enable or disable sample buffer,
* @input        :    able: 0-buffer full interrupt disabled, 1-buffer full interrupt updated in INS2
* @output       :    none
* @return       :    none
*/
static bool drv_kx022_set_buffer_full_interrupt(bool able) 
{
    uint8_t value;
    if(drv_kx022_read_registor(REGISTER_BUF_CNTL2,&value)!= true)   
    {
        return false;
    }
    value &= ~0x20;
    value |= (uint8_t)(able << 5);
    if(drv_kx022_write_registor(REGISTER_BUF_CNTL2,value)!= true)   
    {
        return false;
    }   
    uint8_t temp = 0;
    if(drv_kx022_read_registor(REGISTER_BUF_CNTL2,&temp) != true)
    {
        return false;
    }
    if(temp != value)
    {
        DBG_LOG("kx022: set buffer full interrupt value verify failed\r\n");
        return false;
    }
    return true;
}
/**
* @function@name:    drv_kx022_set_fifo_mode
* @description  :    Function for set fifo mode,
* @input        :    mode:
*                     0  0   FIFO         The buffer collects 84 sets of 8-bit low resolution values or 41
*                                        sets of 16-bit high resolution values and then stops collecting
*                                        data, collecting new data only when the buffer is not full.
*
*                    0  1    Stream        The buffer holds the last 84 sets of 8-bit low resolution values
*                                        or 41 sets of 16-bit high resolution values. Once the buffer is
*                                        full, the oldest data is discarded to make room for newer data.
*
*                    1  0    Trigger       When a trigger event occurs, the buffer holds the last data set
*                                         of SMP[6:0] samples before the trigger event and then continues to 
*                                        collect data until full. New data is collected only when the buffer is not full.
*
*                    1  1    FILO          The buffer holds the last 84 sets of 8-bit low resolution values
*                                        or 41 sets of 16-bit high resolution values. Once the buffer is
*                                        full, the oldest data is discarded to make room for newer data.
*                                        Reading from the buffer in this mode will return the most
*                                        recent data first.

* @output       :    none
* @return       :    none
*/
static bool drv_kx022_set_fifo_mode(uint8_t mode)
{
    uint8_t value;
    if(drv_kx022_read_registor(REGISTER_BUF_CNTL2,&value)!= true)   
    {
        return false;
    }
    value &= ~0x03;
    value |= (mode & 0x03);
    if(drv_kx022_write_registor(REGISTER_BUF_CNTL2,value)!= true)   
    {
        return false;
    }
    uint8_t temp = 0;
    if(drv_kx022_read_registor(REGISTER_BUF_CNTL2,&temp) != true)
    {
        return false;
    }
    if(temp != value)
    {
        DBG_LOG("kx022: set fifo mode value verify failed\r\n");
        return false;
    }
    return true;
}
/**
* @function@name:    drv_kx022_software_reset
* @description  :    Function for initializing software reset, which performs the RAM reboot routine
*                    This bit will remain 1 until the RAM reboot routine is finished.
* @input        :    none
* @output       :    none
* @return       :    none
*/
bool drv_kx022_software_reset(void)
{
    uint8_t value;
    if(drv_kx022_read_registor(REGISTER_CNTL2,&value)!= true)   
    {
        return false;
    }
    value |= 0x80;
    if(drv_kx022_write_registor(REGISTER_CNTL2,value)!= true)   
    {
        return false;
    }
    vTaskDelay( 100 ); 
    return true;
}

/**
* @function@name:    drv_kx022_clear_buffer
* @description  :    Function for clear all buffer samples, and status information, latched buffer status
*                    information and the entire sample buffer are cleared when any data is written to this register
* @input        :    none
* @output       :    none
* @return       :    none
*/
bool drv_kx022_clear_buffer(void)
{
    if(drv_kx022_write_registor(REGISTER_BUF_CLEAR,0)!= true)   
    {
        return false;
    }
    return true;
}

/**
* @function@name:    drv_kx022_clear_interrupt_source
* @description  :    Function for clear all buffer samples, and status information, latched buffer status
*                    information and the entire sample buffer are cleared when any data is written to this register
* @input        :    none
* @output       :    none
* @return       :    none
*/
bool drv_kx022_clear_interrupt_source(void)
{
    uint8_t temp;
    if(drv_kx022_read_registor(REGISTER_INT_REL,&temp)!= true)   
    {
        return false;
    }
    return true;
}

/**
* @function@name:    drv_kx022_read_int_flag
* @description  :    read the interrupt flag of kx022
* @input        :    none
* @output       :    interrupt flag
* @return       :    interrupt flag
*/
uint8_t drv_kx022_read_int_flag(void)
{
    uint8_t flag = 0;
    drv_kx022_read_registor(REGISTER_INS2,&flag);
    return flag;
}
/**
* @function@name:    drv_kx022_read_fifo_data_numbers
* @description  :    read the interrupt flag of kx022
* @input        :    none
* @output       :    interrupt flag
* @return       :    interrupt flag
*/
uint8_t drv_kx022_get_fifo_points(void)
{
    uint8_t data_numbers = 0;
    if(drv_kx022_read_registor(REGISTER_BUF_STATUS1,&data_numbers)!= true)   
    {
        return 0;
    }
    return data_numbers / DRV_kx022_ONE_XYZ_DATA_LEN;
}
//function for judge the kx022 id
bool drv_kx022_read_id(void)
{
    uint8_t whoami;  
    //drv_kx022_cs_low();
    drv_kx022_read_registor(REGISTER_WHO_AM_I,&whoami);
    //drv_kx022_read_registor(0XFE,&whoami);
    if(whoami != 0x14)               //KX022 ID = 0x14, is there a KX022 exist
    {       
        DBG_LOG("kx022: id=0x%x is not exist\r\n",whoami);
        return false;
    } 
    DBG_LOG("kx022: id=0x%x\r\n",whoami);
    return true;
}
//function for config mcu interrupt io
static void drv_acc_int_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    //DBG_LOG("\r\n.......kx022: interrupt.......\r\n");
    //acc interrupt/
    if((pin == DRV_KX022_INIT1_PIN) && (action == NRF_GPIOTE_POLARITY_LOTOHI))
    {
        //uint8_t temp = DRV_KX022_TASK_TYPE_READ_SAMPLE;
        static portBASE_TYPE TaskWoken = pdFALSE;
        xSemaphoreGiveFromISR(drv_kx022_interrupt_semaphore,&TaskWoken);
    }   
}

//function for enable the acc kx022 interrupt under nrf52832 platform
void drv_kx022_soc_int_enable(void)
{
    nrf_drv_gpiote_in_event_enable(DRV_KX022_INIT1_PIN, true);
    //DBG_LOG("acc interrupt enable");  
}

//function for disable the acc kx022 interrupt
void drv_kx022_soc_int_disable(void)
{
    nrf_drv_gpiote_in_event_disable(DRV_KX022_INIT1_PIN);
    //DBG_LOG("acc interrupt disable");
}

//function for initializing acc kx022 interruput
void drv_acc_interrupt_config(void)
{

    nrf_drv_gpiote_in_config_t config_acc = GPIOTE_CONFIG_IN_SENSE_LOTOHI(false);
    config_acc.pull = NRF_GPIO_PIN_NOPULL;
        
    nrf_drv_gpiote_in_init(DRV_KX022_INIT1_PIN, &config_acc, drv_acc_int_handler);
    //nrf_drv_gpiote_in_init(DRV_KX022_INIT1_PIN, &config_acc, handler);
    drv_kx022_soc_int_enable();
    
    kx022_int_flag = 0; 

}


/**
* @function@name:    drv_kx022_get_spi_instance
* @description  :    get instace for spi interface
* @input        :    none
* @output       :    none
* @return       :    none
*/
bool drv_kx022_get_spi_instance(uint8_t const spi_instance)
{
    bool ret ;
    switch(spi_instance)
    {
        case 0:
            spi.p_registers  = NRF_DRV_SPI_PERIPHERAL(0);         
            spi.irq          = CONCAT_3(SPI, 0, _IRQ);            
            spi.drv_inst_idx = CONCAT_3(SPI, 0, _INSTANCE_INDEX); 
            spi.use_easy_dma = SPI_N_USE_EASY_DMA(0) ;
            ret = true;
            break;
        case 1:
            spi.p_registers  = NRF_DRV_SPI_PERIPHERAL(1);         
            spi.irq          = CONCAT_3(SPI, 1, _IRQ);            
            spi.drv_inst_idx = CONCAT_3(SPI, 1, _INSTANCE_INDEX); 
            spi.use_easy_dma = SPI_N_USE_EASY_DMA(1) ;
            ret = true;
            break;
        case 2:
            spi.p_registers  = NRF_DRV_SPI_PERIPHERAL(2);         
            spi.irq          = CONCAT_3(SPI, 2, _IRQ);            
            spi.drv_inst_idx = CONCAT_3(SPI, 2, _INSTANCE_INDEX); 
            spi.use_easy_dma = SPI_N_USE_EASY_DMA(2) ;
            ret = true;
            break;
        default:
            DBG_LOG("kx022: invalid spi instance\r\n");  
            ret = false;
            break;
    }
    return ret;
}
/**
* @function@name:    spi_init
* @description  :    initializing the spi interface for acceleration-sensor
* @input        :    none
* @output       :    none
* @return       :    none
*/
 static bool spi_init(uint8_t const spi_instance)
{ 
    if(spi_lock == NULL)
    {
        spi_lock = xSemaphoreCreateBinary();
        xSemaphoreGive(spi_lock);
    }
    if(xSemaphoreTake( spi_lock, LOCK_TIMEOUT) != pdPASS )
    {
        DBG_LOG("\r\nspi has initialized, not need to initialized again\r\n");     
        return false;
    }
    drv_kx022_get_spi_instance(spi_instance);
    nrf_drv_spi_config_t spi_config = NRF_DRV_SPI_DEFAULT_CONFIG;    
    //spi_config.ss_pin       =   DRV_KX022_SPI_CS_PIN;//; 
    spi_config.ss_pin       =   NRF_DRV_SPI_PIN_NOT_USED;
    spi_config.sck_pin      =   DRV_KX022_SPI_SCK_PIN;   //
    spi_config.miso_pin     =   DRV_KX022_SPI_MISO_PIN;
    spi_config.mosi_pin     =   DRV_KX022_SPI_MOSI_PIN;
    spi_config.mode         =   NRF_DRV_SPI_MODE_0;   
        
       
    if(nrf_drv_spi_init(&spi, &spi_config,spi_event_handler,NULL) != NRF_SUCCESS)
    {
        xSemaphoreGive(spi_lock);
        DBG_LOG("kx022: spi init failed\r\n");
        return false;
    }   
    if(semaphore_wait_event == NULL)
    {   
        semaphore_wait_event = xSemaphoreCreateBinary();
        xSemaphoreTake( semaphore_wait_event, 1);
    }
    
    //nrf_gpio_cfg_output(DRV_KX022_SPI_MOSI_PIN);
    //nrf_gpio_pin_set(DRV_KX022_SPI_MOSI_PIN);
    nrf_gpio_cfg_output(DRV_KX022_SPI_CS_PIN);
    drv_kx022_cs_high(); 
    
    DBG_LOG("kx022: spi init successed\r\n");
    return true;
}
/**
* @function@name:    spi_uninit
* @description  :    uninitializing the spi interface for acceleration-sensor
* @input        :    none
* @output       :    none
* @return       :    none
*/
static bool spi_uninit(void)
{
    /*
    if(spi_lock == NULL)
    {
        spi_lock = xSemaphoreCreateBinary();
        xSemaphoreGive(spi_lock);
    }
    if(xSemaphoreTake( spi_lock, LOCK_TIMEOUT) == pdPASS )
    {
        DBG_LOG("\r\nspi has uninitialized, not need to uninitialized again\r\n");
        return false;
    }
    */
    nrf_drv_spi_uninit(&spi) ;
    xSemaphoreGive(spi_lock);
    return true;   
}

/**
* @function@name:    drv_kx022_stop_fifo_sample
* @description  :    stop the acceleration-sensor sample
* @input        :    none
* @output       :    none
* @return       :    none
*/
bool drv_kx022_stop_fifo_sample(void)
{
    spi_init(DRV_KX022_SPI_INSTANCE);
    if(semaphore_wait_event == NULL)
    {   
        semaphore_wait_event = xSemaphoreCreateBinary();
        xSemaphoreTake( semaphore_wait_event, 1);
    }   
    drv_kx022_soc_int_disable(); 
    //DBG_LOG("kx022: software reset\r\n");
    drv_kx022_software_reset();
    vTaskDelay(5);
    //DBG_LOG("kx022: standby\r\n");
    drv_kx022_into_standby_mode();
    //DBG_LOG("kx022: disable interrupt\r\n");
    drv_kx022_enable_int_pin(false);   
    //DBG_LOG("kx022: clear interrupt\r\n"); 
    drv_kx022_clear_interrupt_source();    
    drv_kx022_run_status = 0;
    xSemaphoreGive(drv_kx022_start_sema);
    DBG_LOG("kx022: stopped sample\r\n");
    return true;
}
/**
* function for sleep kx022
*/
bool drv_kx022_into_sleep_mode(void)
{
    if(drv_kx022_stop_fifo_sample()!= true)   
    {
        return false;
    }
    drv_kx022_into_standby_mode();
    spi_uninit(); 
    xSemaphoreGive(spi_lock);
    return true;
}

/**
* @function@name:    drv_kx022_config
* @description  :    initializing acceleration-sensor kx022, spi interface
* @input        :    none
* @output       :    none
* @return       :    true if ID is exist, unless is false
*/
bool drv_kx022_config(drv_kx022_cfg_t * const p_cfg_t, uint8_t const spi_instance)
{   
    //spi = NRF_DRV_SPI_INSTANCE(spi_instance);
    if(drv_kx022_get_spi_instance(spi_instance) != true)
    {
        return false;
    }
    if(spi_init(spi_instance) != true)
    {
        return false;
    }
    drv_kx022_software_reset();
    vTaskDelay(5);
    if(drv_kx022_read_id() == false)
    {
        //DBG_LOG("kx022: invalid kx022 id\r\n");
        return false;
    }           
    if(drv_kx022_into_standby_mode()!= true)   
    {
        DBG_LOG("kx022: set standby mode failed\r\n");
        return false;
    }
    if(drv_kx022_set_out_data_rate(p_cfg_t->out_rate) != true)
    {
        DBG_LOG("kx022: set out data rate failed\r\n");
        return false;
    }
    if(drv_kx022_set_interrupt_report_int1(p_cfg_t->int_report) != true)
    {
        DBG_LOG("kx022: set interrupt int1 failed\r\n");
        return false;
    }
    factor = drv_kx022_get_range_factor(p_cfg_t->range);
    if(drv_kx022_select_range(p_cfg_t->range) != true)
    {
        DBG_LOG("kx022: select range failed\r\n");
        return false;
    }
    if(drv_kx022_set_fifo_mode(p_cfg_t->fifo_mode) != true)
    {
        DBG_LOG("kx022: set fifo mode failed\r\n");
        return false;
    }
    if(p_cfg_t->pwr_mode == DRV_KX022_HIGH_CURRENT)
    {
    if(drv_kx022_set_low_pass_filter_freq(p_cfg_t->filt_mode) != true)
    {
        DBG_LOG("kx022: set filter failed\r\n");
        return false;
    }
    }
    if(drv_kx022_set_int_pin_active_polarity(p_cfg_t->int_act) != true)
    {
        DBG_LOG("kx022: set pin active mode failed\r\n");
        return false;
    }
    if(drv_kx022_set_interrupt_response_type(p_cfg_t->int_type) != true)
    {
        DBG_LOG("kx022: set interrupt type failed\r\n");
        return false;
    }
    if(drv_kx022_set_power_mode(p_cfg_t->pwr_mode) != true)
    {
        DBG_LOG("kx022: set power mode failed\r\n");
        return false;
    } 
      
    if(drv_kx022_set_data_samples_bits(p_cfg_t->sample_bits)!= true) 
    {
        DBG_LOG("kx022: set sample bits failed\r\n");
        return false;
    }
    if(drv_kx022_set_fifo_samples_threshold(p_cfg_t->fifo_points)!= true) 
    {
        DBG_LOG("kx022: set fifo sample point number failed\r\n");
        return false;
    }
    if(drv_kx022_set_sample_buffer_active(true)!= true)
    {
        DBG_LOG("kx022: set sample buffer active failed\r\n");
        return false;
    }    
    if(drv_kx022_clear_buffer()!= true) 
    {
        DBG_LOG("kx022: clear buffer failed\r\n");
        return false;
    }
    if(drv_kx022_clear_interrupt_source()!= true) 
    {
        DBG_LOG("kx022: clear interrupt failed\r\n");
        return false;
    }
    if(drv_kx022_enable_int_pin(true)!= true) 
    {
        DBG_LOG("kx022: enable_int_pin failed\r\n");
        return false;
    }
    if(drv_kx022_into_operating_mode()!= true)   
    {
        DBG_LOG("kx022: set operate mode failed\r\n");
        return false;
    }    
    
    return true;
}
/**
* @function@name:    drv_kx022_measure_fifo_start
* @description  :    start the acceleration-sensor measurement
* @input        :    none
* @output       :    none
* @return       :    none
*/
bool drv_kx022_start_fifo_sample(void)
//bool drv_kx022_start_fifo_sample(nrf_drv_gpiote_evt_handler_t const handler)
{
    DBG_LOG("\r\nkx022 starting......\r\n");
    if(drv_kx022_start_sema == NULL) 
    {
        drv_kx022_start_sema = xSemaphoreCreateBinary();
        xSemaphoreGive(drv_kx022_start_sema);
    }
    if(xSemaphoreTake( drv_kx022_start_sema, LOCK_TIMEOUT) != pdPASS )
    {
        DBG_LOG("\r\nkx022 has started, not need to start again\r\n");
        if(nrf_gpio_pin_out_read(DRV_KX022_INIT1_PIN) == 1)
        {
            portBASE_TYPE kx022woken = pdFALSE;
            xSemaphoreGiveFromISR(drv_kx022_interrupt_semaphore,&kx022woken);
        }
        return false;
    }
    drv_kx022_cfg_t config = DRV_KX022_CONFIG_DEFAULT;
    
    //config.gpio_int_handler   = NULL;
    
    if(drv_kx022_config(&config,DRV_KX022_SPI_INSTANCE)!= true)   
    {
        DBG_LOG("kx022: init failed\r\n");
        xSemaphoreGive(drv_kx022_start_sema);
        //drv_kx022_software_reset();
        return false;
    }
    drv_acc_interrupt_config();    
    drv_kx022_run_status = 1;
    time_cnt = 0;
    //nrf_gpio_cfg_output(DRV_KX022_SPI_CS_PIN);
    nrf_spim_disable(spi.p_registers);
    //spi_uninit();
    DBG_LOG("kx022 started measurement of fifo and enable interrupt\r\n");
    return true;
    
}
/**
* function for data collect task, it performs the acc data process
*/
void drv_kx022_task(void *arg)
{
    //vTaskDelay( 5000 );     
    //static uint8_t      queue;
    uint8_t temp;
    xSemaphoreTake( drv_kx022_interrupt_semaphore, 100);
    xQueueReset(drv_kx022_queue);
    while(1)
    {
        
        if((xSemaphoreTake(drv_kx022_interrupt_semaphore, portMAX_DELAY) == pdPASS))
        {
            temp = drv_kx022_get_sample();
            //DBG_LOG("\r\nkx022 read samples numbers = %d\r\n",temp);
        }        
    }
}
/**
* @function@name:    user_creat_acc_data_collect_task
* @description  :    Function for creat the acc sample task 
* @input        :    none
* @output       :    none
* @return       :    none
*/
void drv_kx022_creat_task(void)
{
    BaseType_t ret;  
    drv_kx022_queue = xQueueCreate( 256, sizeof(uint8_t) );
    drv_kx022_interrupt_semaphore = xSemaphoreCreateBinary();
    ret = xTaskCreate( drv_kx022_task, "kx22", 256, NULL, 1, &drv_kx022_handle );
    if(pdPASS != ret)
    {
        APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
    }  
    //xQueueReset(drv_kx022_queue);
}

void drv_kx022_fifo_sample_start_type(void)
{
    uint8_t type = DRV_KX022_TASK_TYPE_FIFO_SAMPLE_START;
    while(xQueueSendToBack(drv_kx022_queue, &type, 10) != pdPASS);
}
void drv_kx022_fifo_sample_stop_type(void)
{
    uint8_t type = DRV_KX022_TASK_TYPE_FIFO_SAMPLE_STOP;
    while(xQueueSendToBack(drv_kx022_queue, &type, 10) != pdPASS);
}

