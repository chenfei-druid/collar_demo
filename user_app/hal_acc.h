
#ifndef __USER_ACC_HAL_
#define __USER_ACC_HAL_

#include <stdint.h>
#include "drv_acc_KX022.h"

#define     USER_MOVING_MEAN_DEPTH      11      //滑动平均值深度

#define     HAL_ACC_ONE_POINT_DATA_LEN      DRV_ACC_kx022_ONE_XYZ_DATA_LEN

typedef struct
{
    uint8_t     fifo_buff[256];
    uint16_t    fifo_buff_length;
}drv_acc_fifo_data;

typedef struct
{
    uint8_t     data_s[256];
    uint32_t    data_len;
}hal_snr_data;

typedef struct
{

#if (HAL_ACC_ONE_POINT_DATA_LEN == 6)             //如果是12位数据，那么一个样本三轴就是6个字节
    
    int16_t     samples_x_axis[50];
    int16_t     samples_y_axis[50];
    int16_t     samples_z_axis[50];
    
#elif (HAL_ACC_ONE_POINT_DATA_LEN == 3)             //8位数据
    
    int8_t     samples_x_axis[90];
    int8_t     samples_y_axis[90];
    int8_t     samples_z_axis[90];
    
#endif

    uint8_t     samples_x_len;
    uint8_t     samples_y_len;
    uint8_t     samples_z_len;
    uint8_t    samples_number;
}g_sensor_xyz_s;

void hal_acc_init(void);
void hal_acc_odba_alg_init(void);
void hal_acc_start_collect(void);
void hal_acc_odba_hour(uint32_t time);


#endif  //__USER_ACC_HAL_
