#ifndef     DRV_SENSE_CAP_H
#define     DRV_SENSE_CAP_H

#include <stdint.h>
#include <stdbool.h>

#define     DRV_SNS_CAP_ADC_CHN         NRF_SAADC_INPUT_AIN3
#define     DRV_SNS_CAP_LEVEL_PIN       6

#define     DRV_DEV_FALL_THRESHOLD      1200

void drv_sns_cap_init(void);
void drv_sns_cap_uninit(void);
bool drv_sns_cap_vol_msr(uint16_t * const level);


#endif  //DRV_SENSE_CAP_H

