
/** 
 * user_app_estrus.c
 *
 * @group:       collar project
 * @author:      Chenfei
 * @create time: 2018/4/21
 * @version:     V0.0
 *
 */
#include <math.h>
#include "freertos_platform.h"
#include "string.h"
#include "behavior.h"
#include "time.h"
#include "user_data_struct.h"
#include "Record.h"
#include "user_acc.h"
//#include "hal_rtc.h"
#include "alarm_clock.h"
#include "user_app_estrus.h"
//#include "user_app_bhv.h"
#include "user_pb_callback.h"
#include "user_data_pkg.h"
#include "user_send_data.h"
#include "user_app_log.h"
#include "drv_nb_modul.h"
#include "Env.pb.h"
#include "user_app_identity.h"
#include "Define.pb.h"
#include "pb_encode.h"
#include "pb_decode.h"
#include "SimpleRsp.pb.h"
#include "user_analyse_commu_cmd.h"
#include "user_app_identity.h"
#include "ble_application.h"

/***********************************************user defined variable************************************************/

#define     ESTRUS_AVERAGING_COUNTER_MAX    60//60 ODBAs to averaging //ESTRUS_RESULT_TIME_MAX / BHV_RESULT_TIME_MAX   //record counter

#define     ESTRUS_SAMPLE_INTERVAL_DEFAULT  600

#define     ESTRUS_RESULT_BUF_MAX           1       //save record every record data generated
#define     ESTRUS_THRESHOLD_DEFAULT        256

//static      int32_t                         estrus_threshold = ESTRUS_THRESHOLD_DEFAULT;
//static      xSemaphoreHandle                binary_semaphore_estrus_transmit = NULL;
//static      xSemaphoreHandle                binary_semaphore_estrus_continue_transmit = NULL;
static      xSemaphoreHandle                binary_semaphore_wait_est_response = NULL;
static      TaskHandle_t                    estrus_detect_handle;
            QueueHandle_t                   queue_estrus_detect;
            
static      estrus_setting_t                m_estrus_sample_param = {.mode = 0,
                                                                     .interval = ESTRUS_SAMPLE_INTERVAL_DEFAULT,
                                                                     .estrus_threshold = ESTRUS_THRESHOLD_DEFAULT,
                                                                    };  
static      int32_t                         activity = 0;
/** ****************************************************************************
estrus record:
            optional uint32     Timestamp               
            optional int32      Activity                //
            optional int32      ODBA 
            optional int32      EstrusThreshold 
    
********************************************************************************/
/***********************************************defined functions**********************************************/
#include "behavior.c"                                                                    
static void user_estrus_record_init(int w_size, int peroid)
{   
    Behavior_Init(w_size,peroid);   
}
void user_estrus_init(void)
{
    user_estrus_record_init(ESTRUS_ODBA_WINDOW_DEFAULT,ESTRUS_BASE_ODBA_RESULT_TIME);
    //m_estrus_record_buf_cnt = 0;
    //memset(m_estrus_record_buf,0,sizeof(m_estrus_record_buf));
}

/**
* @functionname : user_estrus_setting 
* @description  : function for set sample parameter, the mode ,and interval time
* @input        : bhv_sample_setting_t: @ref bhv_sample_setting_t
* @output       : none 
* @return       : true if valid
*/
bool user_estrus_setting(estrus_setting_t setting)
{
    if(setting.mode > 1 || setting.mode < 0)
    {
        DBG_LOG("\r\nestrus set failed of invalid mode = %d\r\n\0",setting.mode);
        return false;
    }
    if(setting.interval < 60)
    {
        DBG_LOG("\r\nestrus set failed of invalid interval = %d\r\n\0",setting.interval);
        return false;
    }
    m_estrus_sample_param.interval = setting.interval;
    m_estrus_sample_param.mode = setting.mode;
    m_estrus_sample_param.estrus_threshold  = setting.estrus_threshold;
    user_acc_set();
    
    DBG_LOG("\r\nestrus set mode = %d\r\n\0", setting.mode);
    DBG_LOG("\r\nestrus set interval = %d\r\n\0", setting.interval);
    DBG_LOG("\r\nestrus set threshold = %d\r\n\0", setting.estrus_threshold);
    //bool ret = true;
    //ret &= user_estrus_set_sample_interval(setting.interval);
    //ret &= user_estrus_set_sample_mode(setting.mode);
    //return ret;
    return true;
}
int32_t user_estrus_get_sample_mode(void)
{
    return m_estrus_sample_param.mode;
}
int32_t user_estrus_get_activity(void)
{
    return activity;
}

/**
* @functionname : user_estrus_encode_repeated_var_struct 
* @description  : function for encode the environment records, the record number is filled at head position
* @input        : stream, encode stream,  
*               : fields: the env fields
*               : src_struct: the env struct will be encoded
* @output       : none 
* @return       : true if successed
*/
static bool user_estrus_encode_repeated_var_struct(pb_ostream_t *stream, const pb_field_t fields[],  void * const *src_struct)
{
    
    bool res;
    estrus_encode_t *m_head_t = (estrus_encode_t *)*src_struct;
    protocol_estrus_t    *record_t        = m_head_t->record;
    for(uint8_t i=0; i<m_head_t->count; i++)
    {
        if (!pb_encode_tag_for_field(stream, fields))
        {
            DBG_LOG("encode estrus tag failed\r\n\0");
            return false;
        }
        res = pb_encode_submessage(stream, protocol_estrus_fields, &record_t[i]);
        if(res == false)
        {
            DBG_LOG("encode estrus struct failed\r\n\0");
            break;
        }     
        DBG_LOG("encode estrus struct number %d of %d successed\r\n\0",i,m_head_t->count);
    }
    DBG_LOG("encode estrus struct is %s\r\n", res? "successed":"failed");
    return res;
}
/**
* @function@name:    user_acc_save_bhv_record
* @description  :    function for save one estrus record into memory
* @input        :    p_bhv_dt: 
* @output       :    none
* @return       :    true if success
*/
//void hal_acc_save_origin_data(acc_origin_data_t *p_org_dt, uint32_t len)
static bool app_save_estrus_record(estrus_record_t const *p_est_dt)
{
    uint8_t *p_dt = (uint8_t *)p_est_dt;
    return Record_Write(RECORD_TYPE_ESTRUS,p_dt);
}
/**
* @function@name:    hal_acc_save_estrus_records
* @description  :    function for save some estrus record datas into memory as record
* @input        :    p_org_dt: 
*               :    record_num:
* @output       :    none
* @return       :    true if success
*/
static bool app_save_estrus_records(estrus_record_t *p_est_dt, uint32_t const record_num)
{
//    bool ret;
    if(p_est_dt == NULL)
    {
        DBG_LOG("estrus pointor is null\r\n");
        return false;
    }
    for(uint8_t i=0;i<record_num;i++)
    {
        if(app_save_estrus_record(&p_est_dt[i]) == false)
        {
            DBG_LOG("estrus datas save failed\r\n");
            return false;
        }
    }
    DBG_LOG("estrus datas save successed\r\n");

    return true;
}

/**
* @function@name:    user_env_read_records
* @description  :    Function for getting environment record from memory
* @input        :    none
* @output       :    none
* @return       :    record size
* @createtime   :2018-5-12
**/
#define     test_read   0
#if test_read
static int32_t user_estrus_read_records(estrus_record_t *p_est_dt, int32_t const record_size)
{
    if(p_est_dt == NULL)
    {
        DBG_LOG("\r\nestrus record read pointer is null\r\n");
        return 0;
    }
    if(record_size <= 0 || record_size > ESTRUS_ONCE_UPLOAD_RECORD_NUM)
    {
        DBG_LOG("\r\nestrus read given record size is invalid\r\n");
        return 0;
    }
    int32_t ret = record_size; 
    for(uint32_t i=0;i<record_size;i++)
    {
        p_est_dt[i].odba        = 3965;
        p_est_dt[i].activity    = 123;
        p_est_dt[i].est_thres   = 256;
        p_est_dt[i].timestamp   = hal_rtc2_get_unix_time();
    }
    DBG_LOG("estrus read record number = %d\r\n", ret);
    
    for(uint32_t i=0;i<ret;i++)
    {
        DBG_LOG("\r\nnumber %d record:\r\n",i);
        DBG_LOG("timestamp = %d\r\n", p_est_dt[i].timestamp);        
        DBG_LOG("ODBA every hour\r\n", p_est_dt[i].odba);
        DBG_LOG("activity = %d\r\n", p_est_dt[i].activity);
        DBG_LOG("estrus threshold = %d\r\n", p_est_dt[i].est_thres);     
    }
    return ret;
}
#else
static int32_t user_estrus_read_records(estrus_record_t *p_est_dt, int32_t const record_size)
{
    if(p_est_dt == NULL)
    {
        DBG_LOG("\r\nestrus record read pointer is null\r\n");
        return 0;
    }
    if(record_size <= 0 || record_size > ESTRUS_ONCE_UPLOAD_RECORD_NUM)
    {
        DBG_LOG("\r\nestrus read given record size is invalid\r\n");
        return 0;
    }
    int32_t ret;
    ret = Record_Read(RECORD_TYPE_ESTRUS, (uint8_t *)p_est_dt, record_size * sizeof(estrus_record_t));  
    DBG_LOG("\r\nestrus read record number = %d\r\n", ret);
    
    for(uint32_t i=0;i<ret;i++)
    {
        DBG_LOG("number %d record:\r\n",i);
        DBG_LOG("timestamp = %u\r\n", p_est_dt[i].timestamp);        
        DBG_LOG("ODBA = %u\r\n", p_est_dt[i].odba);
        DBG_LOG("activity = %u\r\n", p_est_dt[i].activity);
        DBG_LOG("estrus threshold = %u\r\n", p_est_dt[i].est_thres);      
    }
    return ret;
}
#endif  //test_read

#if test_decode
static int32_t user_data_pb_decode_test(uint8_t * decode_buf, int32_t len)
{
    protocol_estrus_req_t    estrus_req_t;
    protocol_estrus_t        estrus_info_t;
    protocol_estrus_t        p_record_buf[USER_ENV_ONCE_UPLOAD_RECORD_NUM] = {0};
    estrus_decode_t             p_decode_t;
    uint8_t id[20] = {0};
    
    p_decode_t.count = 0;
    p_decode_t.record = p_record_buf;
    
    estrus_req_t.Iden.DeviceID.funcs.decode    = &user_app_decode_repeated_var_string;
    estrus_req_t.Iden.DeviceID.arg             = id;
    
    estrus_req_t.EnvInfo.funcs.decode     = &user_env_decode_repeated_var_struct;
    estrus_req_t.EnvInfo.arg              = &p_decode_t;
    
    pb_istream_t i_stream ;
    i_stream = pb_istream_from_buffer(decode_buf,len);
    bool res = pb_decode(&i_stream,protocol_estrus_req_fields,&estrus_req_t);   
    DBG_LOG("\r\nestrus decode %s\r\n",res? "successed":"failed");
    return res;
}
#endif
/**
* @functionname : user_bhv_encode_data 
* @description  : function for encode data
* @input        : stream, encode stream,  
*               : fields: the estrus fields
*               : str: the string data will be encoded
* @output       : none 
* @return       : true if successed
*/
static int32_t user_estrus_encode_data( uint8_t         *const encode_out_buffer,
                                        uint32_t        const buffer_size,
                                        estrus_record_t *const p_record_buf, 
                                        uint32_t        const record_size,  
                                        user_identity_t const iden
                                    )
{
    if(encode_out_buffer == NULL || p_record_buf == NULL)
    {
        DBG_LOG("\r\nestrus encode pointer is null\r\n");
        return 0;        
    }
    
    protocol_estrus_req_t   pro_estrus_req_t;
    protocol_estrus_t       encode_record_t[record_size];
    estrus_encode_t         v_estrus_encode_t;
    
    memset(encode_record_t, 0, sizeof(encode_record_t));
    pro_estrus_req_t.Iden = iden.head;
    
    v_estrus_encode_t.count    = record_size;
    v_estrus_encode_t.record   = encode_record_t;
    
    //fill record data
    for(int32_t i=0; i<record_size; i++)
    {
        encode_record_t[i].has_Activity = 1;
        encode_record_t[i].has_ODBA = 1;
        encode_record_t[i].has_Timestamp = 1;              
        encode_record_t[i].has_EstrusThreshold = 1;
        
        encode_record_t[i].Activity     = p_record_buf[i].activity;
        encode_record_t[i].ODBA         = p_record_buf[i].odba;
        encode_record_t[i].Timestamp    = p_record_buf[i].timestamp;  
        encode_record_t[i].EstrusThreshold  = m_estrus_sample_param.estrus_threshold;        
    }
 
    pro_estrus_req_t.Iden.DeviceID.funcs.encode    = &user_app_encode_repeated_var_string;
    pro_estrus_req_t.Iden.DeviceID.arg             = (char *)iden.dev_id;
    
    pro_estrus_req_t.EstrusInfo.funcs.encode          = &user_estrus_encode_repeated_var_struct;
    pro_estrus_req_t.EstrusInfo.arg                   = &v_estrus_encode_t;
    
    pb_ostream_t            m_stream ;    
    m_stream = pb_ostream_from_buffer(encode_out_buffer,buffer_size);
    bool res = pb_encode(&m_stream,protocol_estrus_req_fields,&pro_estrus_req_t);
    if(res)
    {
        DBG_LOG("encode estrus is successed\r\n\0");
        #if test_decode
        user_data_pb_decode_test(encode_out_buffer,m_stream.bytes_written);
        #endif
        return m_stream.bytes_written;
    }
    DBG_LOG("encode estrus is failed\r\n\0");
    return 0;
}

/**
* @createtime   :2018-5-12
* @function@name:    app_estrus_instruction_parse
* @description  :    function for parse response, judge the device id, and msgtoken
* @input        :    none
* @output       :    none
* @return       :    true if success
*/
static uint32_t app_bhv_instruction_parse(protocol_simple_rsp_t const *const p_msg_t, char const * const p_id)
{
    if(p_msg_t == NULL)
    {
        DBG_LOG("pointor is NULL");
        return 0;
    }
    uint32_t msgtoken = 0;
    char device_id[13] = {0};
    app_iden_get_device_id_char(device_id);
    uint16_t give_random = p_msg_t->Iden.MsgToken & 0x0ffff;
    if(p_id[0] != '\0')
    {
        DBG_LOG("\r\nestrus parse : give dev id is: %s\r\n",p_id);
        //if(strcmp(p_id,device_id) != 0)
        if(memcmp(p_id,device_id,12) != 0)
        {
            DBG_LOG("\r\nestrus parse: error, device id is: %s\r\n",device_id);            
            return 0;
        }        
        msgtoken = app_get_msg_token(p_id,give_random);
    }
    else
    {
        msgtoken = app_get_msg_token(device_id,give_random);
    }
    if(msgtoken != p_msg_t->Iden.MsgToken)
    {
        DBG_LOG("\r\nestrus parse: check msgtoken is error\r\n");
        return 0;
    }
    DBG_LOG("Iden.DeviceID = %s\r\n",p_id);    
    DBG_LOG("register MsgIndex = %u\r\n",p_msg_t->Iden.MsgIndex);
    DBG_LOG("register MsgToken = %x\r\n",p_msg_t->Iden.MsgToken);
    DBG_LOG("\r\n-----------------------estrus response parse successful----------------------\r\n\0");
        
    return true;
}
/**
* @function@name:    app_estrus_rsp_parse_handle
* @description  :    function for parse response after upload estrus data
* @input        :    p_buf:  given buffer for response
*               :    len:  response len
* @output       :    none
* @return       :    true if success
*/
bool app_estrus_rsp_parse_handle(uint8_t *p_rsp, int32_t len)
{
    protocol_simple_rsp_t     p_msg_t;
    char dev_id[13] = {0};
    if(p_rsp == NULL || len <= 0)
    {
        DBG_LOG("\r\nestrus rsp buffer pointor NULL, or len <= 0\r\n\0");
        return false;
    }
    if(user_app_decode_simple_rsp(&p_msg_t,dev_id,p_rsp,len) == false)
    {
        DBG_LOG("\r\nestrus response decode failed\r\n",dev_id);
        return false;
    }
    if(app_bhv_instruction_parse(&p_msg_t,dev_id) != true)
    {
        DBG_LOG("\r\nestrus response parse failed\r\n",dev_id);
        return false;
    }  
    xSemaphoreGive(binary_semaphore_wait_est_response);
    return true;
}
static bool user_estrus_wait_rsp_nb(trans_interface_type chn_type)
{
    uint32_t wait_response_timeout;
    if(chn_type == COMMUNICATION_INTERFACE_TYPE_NB)
    {
        wait_response_timeout = WAIT_SERVER_RESPONSE*1000;
    }
    else
    {
        wait_response_timeout = 5000;
    }
    DBG_LOG("waiting estrus response\r\n\0");
    if(xSemaphoreTake( binary_semaphore_wait_est_response, wait_response_timeout ) == pdPASS)    //wait 5s
    {
        DBG_LOG("receive estrus response successed\r\n\0");\
        return true;
    }
    DBG_LOG("wait response estrus timeout\r\n\0");
    return false;
}
/**
* @function@name:    user_estrus_cache_record
* @description  :    function for cache a record, if buffer full, save them into flash  
* @input        :    time: unix timestamp
* @output       :    none
* @return       :    none
*/
int32_t user_estrus_cache_record(estrus_record_t record)
{
    static  estrus_record_t     m_estrus_record_buf[ESTRUS_RESULT_BUF_MAX] = {0};
    static  uint32_t            m_estrus_record_buf_cnt = 0;
    uint32_t size = m_estrus_record_buf_cnt;
    m_estrus_record_buf[m_estrus_record_buf_cnt].odba      = record.odba;
    //get activity
    m_estrus_record_buf[m_estrus_record_buf_cnt].activity  = record.activity;
    //get timestamp
    m_estrus_record_buf[m_estrus_record_buf_cnt].timestamp = record.timestamp;
    m_estrus_record_buf[m_estrus_record_buf_cnt].est_thres = record.est_thres;
    
    m_estrus_record_buf_cnt++;
    DBG_LOG("\r\ncached one estrus middle result odba, cnt = %d\r\n",m_estrus_record_buf_cnt);
    if(m_estrus_record_buf_cnt >= ESTRUS_RESULT_BUF_MAX) //ESTRUS_RESULT_BUF_MAX is max record can be cached in ram memory
    {
        if(app_save_estrus_records(m_estrus_record_buf, m_estrus_record_buf_cnt) != true)
        {
            DBG_LOG("\r\nsave estrus records cached failed----------------\r\n");
            return 0;
        }
        DBG_LOG("\r\nsave estrus records cached successed----------------\r\n");
        for(uint32_t i=0;i<m_estrus_record_buf_cnt;i++)
        {
            DBG_LOG("m_estrus_record_buf[%d].timestamp = %d\r\n",i,m_estrus_record_buf[i].timestamp);
            DBG_LOG("m_estrus_record_buf[%d].odba = %d\r\n",i,m_estrus_record_buf[i].odba);
            DBG_LOG("m_estrus_record_buf[%d].activity = %d\r\n",i,m_estrus_record_buf[i].activity);
            DBG_LOG("m_estrus_record_buf[%d].est_thres = %d\r\n",i,m_estrus_record_buf[i].est_thres);
        }
        m_estrus_record_buf_cnt = 0;
        memset(m_estrus_record_buf,0,sizeof(m_estrus_record_buf));
    }  
    return size;
}

/**
* @function@name:    app_estrus_generate_record
* @description  :    function for get estrus result, it will calculate a result every 60 odba value, or user set
*                    calculate once every minute result which is as a parameter 
* @input        :    p_minu_res_t: it is a result of minute odba,
*               :    
* @output       :    p_hour_res_t: result of hour odba
* @return       :    true if reached and get a result successed
*/

static bool app_estrus_generate_record(int32_t const odba,uint32_t time)
{
    
    static int32_t  estrus_odba_sum = 0;
    static int32_t  estrus_base_result_cnt = 0;
           int32_t  estrus_act_cnt  = 0; 
    
    estrus_odba_sum += odba;
    
    if(odba > m_estrus_sample_param.estrus_threshold)
    {
        estrus_act_cnt++;
    }
    estrus_base_result_cnt++;
    
    if(estrus_base_result_cnt >= m_estrus_sample_param.interval / ESTRUS_BASE_ODBA_RESULT_TIME)    //default base odba time = 60S
    {        
        DBG_LOG("generate estrus record number %d, odba sum = %d, time is %d\r\n", estrus_base_result_cnt,estrus_odba_sum,hal_rtc2_get_unix_time());
        estrus_record_t record;
        
        record.odba         = estrus_odba_sum / estrus_base_result_cnt;         //middle result odba
        record.activity     = 100 * estrus_act_cnt / estrus_base_result_cnt;    //percent
        //record.timestamp    = hal_rtc2_get_unix_time();
        record.timestamp    = time;
        record.est_thres    = m_estrus_sample_param.estrus_threshold;
        
        user_estrus_cache_record(record);
        
        estrus_odba_sum = 0;
        estrus_act_cnt  = 0;
        estrus_base_result_cnt = 0;  
        
        return true;
    }
    return false;
}
/**
* @function@name:    user_bhv_sample_handle
* @description  :    function for parse response after upload behavior data
* @input        :    p_acc_xyz_t:  pointer to sample of acc xyz data
*               :    xyz_nums:  x-y-z points number
* @output       :    none
* @return       :    true if success
*/
void user_estrus_sample_handle(acc_origin_point_t * const p_acc_xyz_t, uint32_t xyz_nums,uint32_t time)
{
    if(m_estrus_sample_param.mode == 0)
    {
        //the sample mode is closed
        DBG_LOG("do not sample bhv data\r\n");
        return;
    }
    if(p_acc_xyz_t == NULL || xyz_nums == 0)
    {
        DBG_LOG("\r\nbhv handle, pointer p_acc_xyz_t is null, or xyz_nums == 0\r\n");
    }
    Behavior_Record_t   *bhv_res_t = NULL;
//    static uint32_t cnt = 0;
    //caculate odba, it will return a result every BHV_SAMPLE_INTERAL_DEF(default is one hour)
    
    bhv_res_t = Behavior_Update((DataPoint_t *)p_acc_xyz_t, xyz_nums, time);
    if(bhv_res_t != NULL)
    {
        app_estrus_generate_record(bhv_res_t->m_odba,time);
    }
    
}

//for test
void user_estrus_generate_record_test(void)
{
//    int32_t i=0;
    Record_Delete(RECORD_TYPE_ESTRUS,(ESTRUS_NUM_SECTORS-1)*4096/sizeof(estrus_record_t));
    //for(i=0;i<60;i++)
    {
        //app_estrus_generate_record(i);
    }
    estrus_record_t  test_record;
    test_record.activity    = 32;
    test_record.est_thres   = 10;
    test_record.odba        = 20;
    test_record.timestamp   = hal_rtc2_get_unix_time();
    
    app_save_estrus_records(&test_record, 1);
}
/**
* @function@name:    user_estrus_upload_data
* @description  :    function for upload estrus data through ble or nb-iot channle, @ref  trans_interface_type
* @input        :    chn_type: @ref trans_interface_type  
* @output       :    none
* @return       :    true if upload successed
*/
//bool user_estrus_upload_data(trans_interface_type chn_type)
bool user_estrus_upload_data(uint8_t * encode_buf, uint32_t encode_buf_size, 
                  uint8_t * send_buf, uint32_t send_buf_size,  
                  trans_interface_type chn_type
                 )
{
    if(encode_buf == NULL || send_buf == NULL)
    {
        return false;
    }
    if(encode_buf_size == 0 || send_buf_size == 0)
    {
        return false;
    }
    if(chn_type >= COMMUNICATION_INTERFACE_TYPE_MAX || chn_type <= COMMUNICATION_INTERFACE_TYPE_MIN)
    {
        return false ;
    }
    if(chn_type == COMMUNICATION_INTERFACE_TYPE_BLE)
    {
        if(!ble_get_connect_status())
        {
            return false;
        }
    }
    if(encode_buf == NULL || send_buf == NULL)
    {
        DBG_LOG("\r\n pointer encode_buf or send_buf is null\r\n");
        return false;
    }
    int32_t        estrus_record_size = ESTRUS_ONCE_UPLOAD_RECORD_NUM;      //send 125 point of xyz each send time
    estrus_record_t   *estrus_record_buf = (estrus_record_t *)send_buf;//[ESTRUS_ONCE_UPLOAD_RECORD_NUM];
    user_identity_t iden;
    //int32_t        ret = 0;
    //bool        wait_ret = 0;
    int32_t     frame_seq = 0;    
    //uint8_t     send_buf[256] = {0};
    //uint8_t     p_encode_buf[256] = {0};
    //uint8_t     *send_buf = (uint8_t *)malloc(256*sizeof(uint8_t));
    //uint8_t     *p_encode_buf = (uint8_t *)malloc(256*sizeof(uint8_t));
    DBG_LOG("\r\nsend_buf : 0x%x, p_encode_buf: 0x%x\r\n",send_buf,encode_buf);
    //char        p_dev_mac[DEVICE_ID_MAX_LEN];
    uint8_t     func_code = USER_COMMON_CODE;  //0X01    
    int32_t     byte_len = 0;
    bool        res = false;
    memset(&iden,0,sizeof(iden));
    binary_semaphore_wait_est_response = xSemaphoreCreateBinary();
    if(binary_semaphore_wait_est_response == NULL)
    {
        DBG_LOG("\r\n>>>>>>>> upload estrus: binary semaphore create failed <<<<<<<<<\\r\n\0");
        return false;
    }
    xSemaphoreTake( binary_semaphore_wait_est_response, 0);
    //uint32_t    record_items = Record_Count(RECORD_TYPE_ESTRUS);
    do
    {
        
        estrus_record_size = user_estrus_read_records(estrus_record_buf,ESTRUS_ONCE_UPLOAD_RECORD_NUM);
        DBG_LOG("\r\nread estrus records len = %d\r\n",estrus_record_size);
        if(estrus_record_size > 0)
        {
            frame_seq++;
            if(!frame_seq)
            {
                frame_seq++;
            }
            app_get_indetity_msg(&iden.head);
            app_iden_get_device_id_char(iden.dev_id);
            //encode data
            DBG_LOG("\r\n upload estrus pachage sequence  = %d\r\n\0",frame_seq);
            byte_len = user_estrus_encode_data(encode_buf,
                                                encode_buf_size,
                                                estrus_record_buf,
                                                estrus_record_size,
                                                iden);
            if(byte_len>0)  
            {
                DBG_LOG("estrus encode len = %d\r\n\0",byte_len);
                //package encoded data
                byte_len = user_data_packge(send_buf, send_buf_size, encode_buf, byte_len, 
                                            PROTOCOL_HEADER_TYPE_TYPE_ESTRUS_REQ,
                                            frame_seq,func_code,chn_type); 
                if(byte_len > 0)
                {                    
                    //int retry = 0;
                    DBG_LOG("estrus data package len = %d\r\n\0",byte_len);
                    //do
                    //{                        
                        //transmit data
                        res = user_send_data(send_buf,byte_len,chn_type);
                        if(res == true)
                        {
                            res = user_estrus_wait_rsp_nb(chn_type);
                        }
                    //}while(!res && retry++ < 2);
                    if(res)
                    {
                        //if(chn_type == COMMUNICATION_INTERFACE_TYPE_BLE)
                        {
                            DBG_LOG("\r\ndelete estrus record numbers = %d \r\n",estrus_record_size);
                            if(Record_Delete(RECORD_TYPE_ESTRUS,estrus_record_size))
                            {
                                DBG_LOG("successful\r\n\r\n",estrus_record_size);
                                
                            }
                            else
                            {
                                DBG_LOG("failed\r\n\r\n",estrus_record_size);
                            }
                        }
                    }
                }
            }                         
        }
    }while(estrus_record_size && byte_len && res);
    DBG_LOG("estrus data upload over\r\n\0");
    vSemaphoreDelete( binary_semaphore_wait_est_response );
    //free(send_buf);
    //free(p_encode_buf);
    return res;
}
/*
static void app_estrus_detct_task(void *arg)
{
    vTaskDelay( 5000 );
    int32_t odba_temp;
//    int32_t odba_record;
    m_estrus_record_buf_cnt = 0;
    while(xQueueReceive(queue_estrus_detect, &odba_temp , 0) != errQUEUE_EMPTY);
    BaseType_t semaphore = pdFALSE;
    while(1)
    {
        if(xQueueReceive(queue_estrus_detect, &odba_temp , portMAX_DELAY) == pdPASS )
        {        
            app_estrus_generate_record(odba_temp);
        } 
    }
}
void user_create_estrus_task(void)
{
    BaseType_t ret;  
    queue_estrus_detect = xQueueCreate( 2, sizeof(int32_t) );
    
    ret = xTaskCreate( app_estrus_detct_task, "estr", 256, NULL, 1, &estrus_detect_handle );
    if(pdPASS != ret)
    {
        APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
    }  
}
void user_estrus_give_trans_semaphore_to_trans(void)
{
    BaseType_t semaphore = pdFALSE;
    xSemaphoreGiveFromISR(binary_semaphore_estrus_transmit,&semaphore);   
}
void user_estrus_give_continue_trans_semaphore_to_trans(void)
{
    BaseType_t semaphore = pdFALSE;
    xSemaphoreGiveFromISR(binary_semaphore_estrus_continue_transmit,&semaphore);   
}
void user_send_queue_odba_to_estrus_task(int32_t odba)
{
     while(xQueueSendToBack(queue_estrus_detect, &odba, 0) != pdPASS);
}
*/
void user_suspend_estrus_detct_task(void)
{
    vTaskSuspend(estrus_detect_handle);
}
void user_resume_estrus_detct_task(void)
{
    vTaskResume(estrus_detect_handle);
}
//end functions

