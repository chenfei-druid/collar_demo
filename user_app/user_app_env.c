/** 
 * user_app_env.c
 *
 * @group:       collar project
 * @author:      Chenfei
 * @create time: 2018/5/7
 * @version:     V0.0
 * 
 *
 */
#include <math.h>
#include "freertos_platform.h"
#include "string.h"
#include "time.h"
#include "Record.h"
//#include "hal_rtc.h"
#include "alarm_clock.h"
#include "user_pb_callback.h"
#include "Define.pb.h"
#include "pb_encode.h"
#include "pb_decode.h"
#include "SimpleRsp.pb.h"
#include "user_data_pkg.h"
#include "user_send_data.h"
#include "user_app_env.h"
#include "user_battery_mgt.h"
#include "user_illumination.h"
#include "user_adc_common.h"
#include "user_app_temp.h"
#include "user_app_log.h"
#include "user_app_env.h"
#include "drv_nb_modul.h"
#include "Env.pb.h"
#include "user_app_identity.h"
#include "user_analyse_commu_cmd.h"
#include "ble_application.h"
#include "user_app_set.h"

/*****************************************user defines*******************************************/
#define     test_decode     0
#define     USER_ENV_ONCE_UPLOAD_RECORD_NUM     5//(((DRV_NB_UNIT_SEND_DATA_LEN) / (sizeof(env_record_t))))    //
#define     S   1000
typedef struct
{
    int32_t             count;
    protocol_env_t     *record;
}env_encode_t;

#if test_decode
typedef struct
{
    int32_t             count;
    protocol_env_t     *record;
}env_decode_t;
#endif

static      TaskHandle_t            env_detect_handle;
static      xSemaphoreHandle        binary_semaphore_env_upload = NULL;
static      xSemaphoreHandle        binary_semaphore_wait_response = NULL;

//static      bool                    env_rsp_flag = 0;
//static      uint32_t                user_env_sample_interval = 100 * S;
static      env_setting_t           m_env_sample_param = {.interval = ENV_SAMPLE_INTERVAL_DEFAULT, .mode = 0};
/*****************************************user functions*******************************************/

/**
* @functionname : user_env_set_sample_interval 
* @description  : function for set environment sample interval
* @input        : interval: sample interval, unit is second  
* @output       : none 
* @return       : true if valid
*/
bool user_env_set_sample_interval(int32_t interval)
{
    if(interval < 60)
    {
        DBG_LOG("\r\nenvironmen set failed of invalid interval = %d\r\n\0",interval);
        return false;
    }
    //if(interval < 60)
    {
    //    interval = 60;       //60S
    }        
    m_env_sample_param.interval = interval;
    DBG_LOG("\r\nenv set interval = %d\r\n\0", interval);
    return true;
}
/**
* @functionname : user_env_set_sample_mode 
* @description  : function for set environment sample mode
* @input        : mode: 0, colse, 1: time to sample 
* @output       : none 
* @return       : true if valid
*/
bool user_env_set_sample_mode(int32_t mode)
{
    if(mode > 1 || mode < 0)
    {
        DBG_LOG("\r\nenvironmen set failed of invalid mode = %d\r\n\0",mode);
        return false;
    }
    m_env_sample_param.mode = mode;
    DBG_LOG("\r\nenv set mode = %d\r\n\0", mode);
    return true;
}
/**
* @functionname : user_env_setting 
* @description  : function for set sample parameter, the mode ,and interval time
* @input        : setting: @ref env_setting_t
* @output       : none 
* @return       : true if valid
*/
bool user_env_setting(env_setting_t setting)
{
    bool ret = true;
    ret &= user_env_set_sample_interval(setting.interval);
    ret &= user_env_set_sample_mode(setting.mode);
    return ret;
}
/**
* @function@name:    user_env_get_timestamp
* @description  :    Function for getting a timestamp from rtc2
* @input        :    none
* @output       :    none
* @return       :    timestatmp
**/ 
uint32_t user_env_get_timestamp(void)
{
    uint32_t timestamp = hal_rtc2_get_unix_time();
    DBG_LOG("env:timestamp = %d\r\n\0",timestamp);
    return timestamp;
}
/**
* @function@name:    user_env_get_battery_percent
* @description  :    Function for getting battery percent
* @input        :    none
* @output       :    none
* @return       :    battery percent
* @createtime   :2018-5-10
**/ 
int32_t user_env_get_battery_power_percent(void)
{
    int32_t pwr_percent = drv_get_battary_power_percentage();
    DBG_LOG("env:pwr_percent = %d\r\n\0",pwr_percent);
    return pwr_percent;
}
/**
* @function@name:    user_env_get_battery_voltage
* @description  :    Function for getting battery voltage
* @input        :    none
* @output       :    none
* @return       :    battery voltage
* @createtime   :2018-5-10
**/
int32_t user_env_get_battery_voltage(void)
{
    int32_t battery_vol = drv_get_battery_voltage();
    DBG_LOG("env:battery_vol = %d\r\n\0",battery_vol);
    return battery_vol;
}
/**
* @function@name:    user_env_get_illumination
* @description  :    Function for getting illumination
* @input        :    none
* @output       :    none
* @return       :    battery voltage
* @createtime   :2018-5-10
**/
int32_t user_env_get_illumination(void)
{
    int32_t lux = drv_get_illumination_intensity();
    DBG_LOG("env:lux = %d\r\n\0",lux);
    return lux;
}
/**
* @function@name:    user_env_get_temperature
* @description  :    Function for getting temperature
* @input        :    none
* @output       :    none
* @return       :    local temperature
* @createtime   :2018-5-10
**/
int32_t user_env_get_temperature(void)
{
    int32_t temp = user_get_temperature();
    DBG_LOG("env:temp = %d\r\n\0",temp);
    return temp;
}
/**
* @functionname : user_env_encode_repeated_var_struct 
* @description  : function for encode the environment records, the record number is filled at head position
* @input        : stream, encode stream,  
*               : fields: the env fields
*               : src_struct: the env struct will be encoded
* @output       : none 
* @return       : true if successed
*/
static bool user_env_encode_repeated_var_struct(pb_ostream_t *stream, const pb_field_t fields[],  void * const *src_struct)
{
    
    bool res;
    env_encode_t *m_head_t = (env_encode_t *)*src_struct;
    protocol_env_t    *record_t        = m_head_t->record;
    for(uint8_t i=0; i<m_head_t->count; i++)
    {
        if (!pb_encode_tag_for_field(stream, fields))
        {
            DBG_LOG("encode env tag failed\r\n\0");
            return false;
        }
        res = pb_encode_submessage(stream, protocol_env_fields, &record_t[i]);
        //DBG_LOG("encode behavior record %d is %s", i,(res? "successed":"failed"));
        if(res == false)
        {
            DBG_LOG("encode env struct failed\r\n\0");
            break;
        }     
        //DBG_LOG("encode env struct number %d of %d successed\r\n\0",i,m_head_t->count);
    }
    DBG_LOG("encode env struct is %s\r\n", res? "successed":"failed");
    return res;
}
/**
* @functionname : user_env_decode_repeated_var_struct 
* @description  : function for decode a register struct data
* @input        : stream, encode stream,  
*               : fields: the register fields
*               : src_struct: the struct data will be decoded
* @output       : none 
* @return       : true if successed
*/
/*
static bool user_env_decode_repeated_var_struct(pb_istream_t *stream, const pb_field_t *field, void **src_struct)
{
    //static int decode_struct_cnt = 0;
    env_encode_t    *m_env_struct_t = (env_encode_t *)*src_struct;
    
   // protocol_env_t *env_t = (protocol_env_t *)(*src_struct);
    bool res = false;
    //res = pb_decode(stream,protocol_env_fields,&env_t[decode_struct_cnt]);
    res = pb_decode(stream,protocol_env_fields,&m_env_struct_t->record[m_env_struct_t->count++]);
    DBG_LOG("decode env struct %s count %d\r\n", res? "successed":"failed",m_env_struct_t->count); 
    return res;
}
*/
/**
* @createtime   :    2018-5-12
* @function@name:    user_env_save_one_record
* @description  :    function for save one env record into memory
* @input        :    p_rec_t:  one env record data @ref env_record_t
* @output       :    none
* @return       :    true if success
*/
static bool user_env_save_one_record(env_record_t const * const p_rec_t)
{
    if(p_rec_t == NULL)
    {
        DBG_LOG("\r\nsave env record pointer is NULL.\r\n");
        return 0;
    }
    //DBG_LOG("\r\nsave one env record:::::::::\r\n");
    //DBG_LOG("\r\ntimestamp = %d\r\nbattery_voltage = %d\r\nlux = %d\r\ntemperature = %d\r\nbattery_percent = %d\r\n",\
            p_rec_t->timestamp,p_rec_t->battery_voltage,p_rec_t->illumination,\
            p_rec_t->temperature,p_rec_t->battery_percent);
    return Record_Write(RECORD_TYPE_ENV,(uint8_t *)p_rec_t);
}
/**
* @createtime   :2018-5-12
* @function@name:    user_env_save_records
* @description  :    function for save some env records into memory
* @input        :    p_rec_t:  pointer to env record start addr  @ref env_record_t
* @output       :    none
* @return       :    true if success
*/
int32_t user_env_save_records(env_record_t * const p_rec_t, int32_t const record_size)
{
    if(p_rec_t == NULL)
    {
        DBG_LOG("env pointor is null");
        return false;
    }
    for(uint8_t i=0;i<record_size;i++)
    {
        if(user_env_save_one_record(&p_rec_t[i]) == false)
        {
            DBG_LOG("env datas save failed\r\n");
            return false;
        }
    }
    DBG_LOG("env datas save successed\r\n");

    return true;
}
/**
* @function@name:    user_env_read_records
* @description  :    Function for getting environment record from memory
* @input        :    none
* @output       :    none
* @return       :    record size
* @createtime   :2018-5-12
**/
#if 0
int32_t user_env_read_records(env_record_t * p_rec_t, int32_t record_size)
{
    if(p_rec_t == NULL)
    {
        return 0;
    }
    int32_t ret = record_size;
    //ret = Record_Read(RECORD_TYPE_ENV, (uint8_t *)p_rec_t, record_size * sizeof(env_record_t));  
    for(uint32_t i=0;i<record_size;i++)
    {
        p_rec_t[i].battery_percent = 78;
        p_rec_t[i].battery_voltage = 3965;
        p_rec_t[i].illumination    = 123;
        p_rec_t[i].temperature     = 256;
        p_rec_t[i].timestamp       = hal_rtc2_get_unix_time();
    }
    DBG_LOG("env read record number = %d\r\n", ret);
    
    for(uint32_t i=0;i<ret;i++)
    {
        DBG_LOG("timestamp = %d\r\n", p_rec_t[i].timestamp);
        DBG_LOG("\r\nnumber %d record:\r\n",i);
        DBG_LOG("battary voltage = %d\r\n", p_rec_t[i].battery_voltage);
        DBG_LOG("battary power percentage = %d\r\n", p_rec_t[i].battery_percent);
        DBG_LOG("lux = %d\r\n", p_rec_t[i].illumination);
        DBG_LOG("temperature = %d\r\n", p_rec_t[i].temperature);        
    }
    return ret;
}
#else
int32_t user_env_read_records(env_record_t * const p_rec_t, int32_t record_size,trans_interface_type chn_type)
{
    if(p_rec_t == NULL)
    {
        return 0;
    }
    int32_t ret;
    
    if(chn_type == COMMUNICATION_INTERFACE_TYPE_BLE)
    {
        ret = Record_Read(RECORD_TYPE_ENV, (uint8_t *)p_rec_t, record_size * sizeof(env_record_t));  
    }
    else
    {
        //read last record
        ret = Record_Peek(RECORD_TYPE_ENV, (uint8_t *)p_rec_t, sizeof(env_record_t));
    }
    
    //ret = Record_Read(RECORD_TYPE_ENV, (uint8_t *)p_rec_t, record_size * sizeof(env_record_t)); 
    DBG_LOG("\r\nenv read record number = %d\r\n", ret);
    if(ret > record_size)
    {
        ret = record_size;
    }
    for(uint32_t i=0;i<ret;i++)
    {
        DBG_LOG("number %d record:\r\n",i);
        DBG_LOG("timestamp = %d\r\n", p_rec_t[i].timestamp);        
        DBG_LOG("battary voltage = %d\r\n", p_rec_t[i].battery_voltage);
        DBG_LOG("battary power percentage = %d\r\n", p_rec_t[i].battery_percent);
        DBG_LOG("lux = %d\r\n", p_rec_t[i].illumination);
        DBG_LOG("temperature = %d\r\n\r\n", p_rec_t[i].temperature);        
    }
    return ret;
}
#endif
/*
static int32_t user_env_read_peek_record(env_record_t * const p_rec_t, int32_t record_size)
{
    if(p_rec_t == NULL)
    {
        return 0;
    }
    int32_t ret = 0;
    ret = Record_Peek(RECORD_TYPE_ENV, (uint8_t *)p_rec_t, record_size * sizeof(env_record_t));
    DBG_LOG("\r\n env read peek record, numbers = %d\r\n", ret);
    for(uint32_t i=0;i<ret;i++)
    {
        DBG_LOG("number %d record:\r\n",i);
        DBG_LOG("timestamp = %d\r\n", p_rec_t[i].timestamp);        
        DBG_LOG("battary voltage = %d\r\n", p_rec_t[i].battery_voltage);
        DBG_LOG("battary power percentage = %d\r\n", p_rec_t[i].battery_percent);
        DBG_LOG("lux = %d\r\n", p_rec_t[i].illumination);
        DBG_LOG("temperature = %d\r\n", p_rec_t[i].temperature);        
    }
    return ret;
}
*/
#if test_decode
static int32_t user_data_pb_decode_test(uint8_t * decode_buf, int32_t len)
{
    protocol_env_req_t    env_whl_t;
    protocol_env_t        env_info_t;
    protocol_env_t        p_record_buf[USER_ENV_ONCE_UPLOAD_RECORD_NUM] = {0};
    env_decode_t          p_decode_t;
    uint8_t id[20] = {0};
    
    p_decode_t.count = 0;
    p_decode_t.record = p_record_buf;
    
    env_whl_t.Iden.DeviceID.funcs.decode    = &user_app_decode_repeated_var_string;
    env_whl_t.Iden.DeviceID.arg             = id;
    
    env_whl_t.EnvInfo.funcs.decode     = &user_env_decode_repeated_var_struct;
    env_whl_t.EnvInfo.arg              = &p_decode_t;
    
    pb_istream_t i_stream ;
    i_stream = pb_istream_from_buffer(decode_buf,len);
    bool res = pb_decode(&i_stream,protocol_env_req_fields,&env_whl_t);   
    DBG_LOG("\r\nenv decode %s\r\n",res? "successed":"failed");
    return res;
}
#endif
/**
* @functionname : user_env_encode_data 
* @description  : function for encode data
* @input        : stream, encode stream,  
*               : fields: the behavior fields
*               : str: the string data will be encoded
* @output       : none 
* @return       : true if successed
*/
static int32_t user_env_encode_data(    uint8_t         * const encode_out_buffer,
                                        uint32_t        const buffer_size,
                                        env_record_t    * const p_record_buf, 
                                        uint32_t        const record_size,  
                                        user_identity_t const iden
                                    )
{
    if(encode_out_buffer == NULL || p_record_buf == NULL)
    {
        DBG_LOG("\r\nenv encode pointer is null\r\n");
        return 0;
        
    }
    
    protocol_env_req_t    pro_env_req_t;
    protocol_env_t        encode_record_t[record_size];
    env_encode_t          v_env_encode_t;
    
    //char p_dev_mac[13] ;
    memset(encode_record_t, 0, sizeof(encode_record_t));
    pro_env_req_t.Iden = iden.head;
    
    v_env_encode_t.count    = record_size;
    v_env_encode_t.record   = encode_record_t;
    
    //fill record data
    for(int32_t i=0; i<record_size; i++)
    {
        
        encode_record_t[i].has_BatteryPower = 1;
        encode_record_t[i].has_BatteryVoltage = 1;        
        encode_record_t[i].has_ChargeCurrent = false;        
        encode_record_t[i].has_ChargeVoltage = false;
        encode_record_t[i].has_InnerHumidity = false;
        encode_record_t[i].has_InnerPressure = false;
        encode_record_t[i].has_InnerTemperature = 1;        
        encode_record_t[i].has_Timestamp = 1;
        encode_record_t[i].has_AmbientLight = false;        
        //encode_record_t[i].AmbientLight     = p_record_buf[i].illumination;
        encode_record_t[i].BatteryPower     = p_record_buf[i].battery_percent;
        encode_record_t[i].BatteryVoltage   = p_record_buf[i].battery_voltage;
        encode_record_t[i].InnerTemperature = p_record_buf[i].temperature;
        encode_record_t[i].Timestamp        = p_record_buf[i].timestamp;     
    }
 
    pro_env_req_t.Iden.DeviceID.funcs.encode    = &user_app_encode_repeated_var_string;
    pro_env_req_t.Iden.DeviceID.arg             = (char *)iden.dev_id;
    
    pro_env_req_t.EnvInfo.funcs.encode          = &user_env_encode_repeated_var_struct;
    pro_env_req_t.EnvInfo.arg                   = &v_env_encode_t;
    
    pb_ostream_t            m_stream ;    
    m_stream = pb_ostream_from_buffer(encode_out_buffer,buffer_size);
    bool res = pb_encode(&m_stream,protocol_env_req_fields,&pro_env_req_t);
    if(res)
    {
        DBG_LOG("encode env is successed\r\n\0");
        #if test_decode
        user_data_pb_decode_test(encode_out_buffer,m_stream.bytes_written);
        #endif
        return m_stream.bytes_written;
    }
    DBG_LOG("encode env is failed\r\n\0");
    return 0;
}

/**
* @createtime   :2018-5-12
* @function@name:    app_env_instruction_parse
* @description  :    function for parse response, judge the device id, and msgtoken
* @input        :    none
* @output       :    none
* @return       :    true if success
*/
static uint32_t app_env_instruction_parse(protocol_simple_rsp_t const *const p_msg_t, char const * const p_id)
{
    if(p_msg_t == NULL)
    {
        DBG_LOG("pointor is NULL");
        return 0;
    }
    uint32_t msgtoken = 0;
    char device_id[13] = {0};
    app_iden_get_device_id_char(device_id);
    uint16_t give_random = p_msg_t->Iden.MsgToken & 0x0ffff;
    if(p_id[0] != '\0')
    {
        DBG_LOG("env parse : give dev id is: %s\r\n",p_id);
        //if(strcmp(p_id,device_id) != 0)
        if(memcmp(p_id,device_id,12) != 0)
        {
            DBG_LOG("\r\nenv parse: error, device id is: %s\r\n",device_id);            
            return 0;
        }        
        msgtoken = app_get_msg_token(p_id,give_random);
    }
    else
    {
        msgtoken = app_get_msg_token(device_id,give_random);
    }
    if(msgtoken != p_msg_t->Iden.MsgToken)
    {
        DBG_LOG("\r\nenv parse: check msgtoken is error\r\n");
        return 0;
    }
    if(p_msg_t->Iden.has_RspCode)
    {
        if(p_msg_t->Iden.RspCode != 0)
        {
            return 0;
        }
    }
    DBG_LOG("Iden.DeviceID = %s\r\n",p_id);    
    DBG_LOG("register MsgIndex = %u\r\n",p_msg_t->Iden.MsgIndex);
    DBG_LOG("register MsgToken = %x\r\n",p_msg_t->Iden.MsgToken);   
    return true;
}
/**
* @createtime   :2018-5-12
* @function@name:    app_env_rsp_parse_handle
* @description  :    function for parse message, first decode msssage, then call app_env_instruction_parse to parse the instruction
* @input        :    none
* @output       :    none
* @return       :    true if success
*/
bool app_env_rsp_parse_handle(uint8_t const *  const p_rsp, int32_t len)
{
    protocol_simple_rsp_t     p_msg_t;
    char dev_id[15] = {0};
    if(p_rsp == NULL || len <= 0)
    {
        DBG_LOG("env rsp buffer pointor NULL, or len <= 0\r\n\0");
        return false;
    }
    if(user_app_decode_simple_rsp(&p_msg_t,dev_id,p_rsp,len) == false)
    {
        DBG_LOG("\r\nenv response decode failed\r\n",dev_id);
        return false;
    }
    if(app_env_instruction_parse(&p_msg_t,dev_id) != true)
    {
        DBG_LOG("\r\nenv response parse failed\r\n",dev_id);
        return false;
    }      
    xSemaphoreGive(binary_semaphore_wait_response);
    return true;
}
/**
* @createtime   :2018-5-12
* @function@name:    user_env_wait_rsp_nb
* @description  :    function for wait receive response successfull
* @input        :    none
* @output       :    none
* @return       :    true if success
*/
static bool user_env_wait_rsp_nb(trans_interface_type chn_type)
{
    uint32_t wait_response_timeout;
    if(chn_type == COMMUNICATION_INTERFACE_TYPE_NB)
    {
        wait_response_timeout = WAIT_SERVER_RESPONSE;
    }
    else
    {
        wait_response_timeout = 5;
    }
    DBG_LOG("waiting parse env response\r\n\0");
    if(xSemaphoreTake( binary_semaphore_wait_response, wait_response_timeout*1000 ) == pdPASS)    //wait 10s
    {
        DBG_LOG("\r\n-----------------------env response parse successful----------------------\r\n\0");
        return true;
    }
    DBG_LOG("receive env response failed\r\n\0");
    return false;
}

/**
* @createtime   :2018-5-19
* @function@name:    user_env_get_sample
* @description  :    function for get a environment sample data
* @input        :    none
* @output       :    env_infor: 
* @return       :    true if success
*/
bool user_env_get_sample(env_record_t   * const env_infor)
{
    if(env_infor == NULL)
    {
        DBG_LOG("\r\nget env sample pointer is null\r\n\0");
        return false;
    }
    DBG_LOG("\r\n*********************env sample*****************\r\n\0");
    DBG_LOG("env sample: get timestamp\r\n\0");
    env_infor->timestamp = user_env_get_timestamp();
    DBG_LOG("env sample: get battery voltage\r\n\0");
    env_infor->battery_voltage = user_env_get_battery_voltage();
    DBG_LOG("env sample: get lux\r\n\0");
    env_infor->illumination    = user_env_get_illumination();
    DBG_LOG("env sample: get temperature\r\n\0");
    env_infor->temperature     = user_env_get_temperature();
    DBG_LOG("env sample: get power percent\r\n\0");
    env_infor->battery_percent = user_env_get_battery_power_percent();
    DBG_LOG("*********************env sample complete*****************\r\n\0");
    //DBG_LOG("\r\ntimestamp = %d\r\nbattery_voltage = %d\r\nlux = %d\r\ntemperature = %d\r\nbattery_percent = %d\r\n",\
    //        env_infor->timestamp,env_infor->battery_voltage,env_infor->illumination,\
    //        env_infor->temperature,env_infor->battery_percent);
    return true;
}
/**
* @createtime   :2018-5-12
* @function@name:    user_env_data_sample
* @description  :    function for sample environment data, and save into memory
* @input        :    none
* @output       :    none
* @return       :    true if success
*/
bool user_env_data_sample_record(void)
{
    env_record_t   env_infor;
#if 1    
    user_env_get_sample(&env_infor);      
     
    if(user_env_save_one_record(&env_infor)!=true)          //save a record into memory
    {
        DBG_LOG("\r\nsave env record failed\r\n\0");
        return false;
    } 
    DBG_LOG("\r\nget and save a env sample record successed\r\n\0");
#else    //for test
for(uint8_t i=0;i<1;i++)
{
    user_env_get_sample(&env_infor);      
     
    if(user_env_save_one_record(&env_infor)!=true)          //save a record into memory
    {
        DBG_LOG("\r\nsave env record failed\r\n\0");
        return false;
    } 
    DBG_LOG("\r\nget and save a env sample record successed\r\n\0");
    memset(&env_infor,0, sizeof(env_infor));
    
    if(Record_Peek(RECORD_TYPE_ENV, (uint8_t *)&env_infor, sizeof(env_infor))!=true) 
    {
        DBG_LOG("\r\nread env record failed\r\n\0");
        return false;
    }    
    DBG_LOG("\r\nafter save, read env data:::\r\ntimestamp = %d\r\nbattery_voltage = %d\r\nlux = %d\r\ntemperature = %d\r\nbattery_percent = %d\r\n",\
            env_infor.timestamp,env_infor.battery_voltage,env_infor.illumination,\
            env_infor.temperature,env_infor.battery_percent); 
    /*
    env_record_t  record_buf[10];
    user_env_read_records(record_buf,10,COMMUNICATION_INTERFACE_TYPE_BLE);
    */    
}    
#endif    
    return true;
}
void user_env_init(void)
{
    user_com_adc_config();
    user_battery_mgt_init();
    user_illumination_intensity_init();
}

void user_env_detect_task(void *arg)
{
    //vTaskDelay( 5000 );
    user_env_init();
    portTickType xLastWakeTime;
    xLastWakeTime = xTaskGetTickCount();
    uint32_t interval_time;
    //uint32_t time_cnt = 0;
    //user_env_data_sample_record();
    
    while(1)
    {       
        interval_time = (m_env_sample_param.interval < 60) ? 60*S :  m_env_sample_param.interval*S;
        vTaskDelayUntil( &xLastWakeTime, pdMS_TO_TICKS(interval_time));    //wake up every 60 seconds
        if(m_env_sample_param.mode == 1)
        {
            //sample environment information, battary power, temperateure......
            DBG_LOG("\r\nit's time to env sample\r\n\0");
            user_env_data_sample_record();
            DBG_LOG("\r\nsample one estrust information complete\r\n");   
            //user_set_rewrite_timestamp();
        }
    }
}
/**
* @createtime   :2018-5-12
* @function@name:    user_env_upload_data
* @description  :    function for upload behavior data, until no data
* @input        :    none
* @output       :    none
* @return       :    true if success
*/

//bool user_env_upload_data(trans_interface_type chn_type)
bool user_env_upload_data(uint8_t * encode_buf, uint32_t encode_buf_size, 
                  uint8_t * send_buf, uint32_t send_buf_size,  
                  trans_interface_type chn_type
                 )
{
    if(chn_type >= COMMUNICATION_INTERFACE_TYPE_MAX || chn_type <= COMMUNICATION_INTERFACE_TYPE_MIN)
    {
        return false;
    }
    if(chn_type == COMMUNICATION_INTERFACE_TYPE_BLE)
    {
        if(!ble_get_connect_status())
        {
            return false;
        }
    }
    if(encode_buf == NULL || send_buf == NULL)
    {
        DBG_LOG("\r\n pointer encode_buf or send_buf is null\r\n");
    }
    int32_t        env_record_size = USER_ENV_ONCE_UPLOAD_RECORD_NUM;      //send 125 point of xyz each send time
    env_record_t   *env_record_buf = (env_record_t *)send_buf;//[USER_ENV_ONCE_UPLOAD_RECORD_NUM];
    
    user_identity_t iden;
//    int32_t        ret = 0;
    //bool           wait_ret = 0;
    int32_t     frame_seq = 0;
    //int32_t     len_temp = 0;
    //uint8_t     send_buf[256] = {0};
    //uint8_t     p_encode_buf[256] = {0};
    
    //uint8_t     *send_buf = (uint8_t *)malloc(256*sizeof(uint8_t));
    //uint8_t     *p_encode_buf = (uint8_t *)malloc(256*sizeof(uint8_t));
    DBG_LOG("\r\nsend_buf : 0x%x, p_encode_buf: 0x%x\r\n",send_buf,encode_buf);
    //char        p_dev_mac[DEVICE_ID_MAX_LEN];
    uint8_t     func_code = USER_COMMON_CODE;  //0X01
    int32_t     byte_len = 0;
    bool        res = false;
    memset(&iden,0,sizeof(iden));
    binary_semaphore_wait_response = xSemaphoreCreateBinary();
    if(binary_semaphore_wait_response == NULL)
    {
        DBG_LOG("\r\n>>>>>>>> upload env: binary semaphore create failed <<<<<<<<<\\r\n\0");
        return false;
    }
    xSemaphoreTake( binary_semaphore_wait_response, 0);
    /////////////////////////////////////for test///////////////////////////////////////////////////////
    //Record_Delete(RECORD_TYPE_ENV,(ENV_NUM_SECTORS-1)*W25Q_SECTOR_SIZE/sizeof(env_record_t));
    //user_env_data_sample_record();
    do
    {
        frame_seq++;
        if(!frame_seq)
        {
            frame_seq++;
        }
        DBG_LOG("\r\n upload env pachage sequence  = %d\r\n\0",frame_seq);
        env_record_size = user_env_read_records(env_record_buf,env_record_size,chn_type);
        DBG_LOG("\r\nread env records len = %d\r\n",env_record_size);        
        if(env_record_size > 0)
        {
            app_get_indetity_msg(&iden.head);
            app_iden_get_device_id_char(iden.dev_id);
            
            byte_len = user_env_encode_data(encode_buf,
                                            encode_buf_size,
                                            env_record_buf,
                                            env_record_size,
                                            iden
                                            );
            DBG_LOG("env encode len = %d\r\n\0",byte_len);                                
            if(byte_len>0)  
            {                
                //package encoded data
                byte_len = user_data_packge(send_buf, send_buf_size, encode_buf, byte_len, 
                                            PROTOCOL_HEADER_TYPE_TYPE_ENV_REQ,
                                            frame_seq,func_code,chn_type); 
                DBG_LOG("\r\nenv data package len = %d\r\n\0",byte_len);
                if(byte_len > 0)
                {                    
                    //int retry = 0;                    
                    //do
                    //{                        
                        //transmit data
                        res = user_send_data(send_buf,byte_len,chn_type);
                        if(res == true)
                        {
                            res = user_env_wait_rsp_nb(chn_type);
                        }
                    //}while(!res && retry++ < 2);
                    if(res)
                    {
                        if(chn_type == COMMUNICATION_INTERFACE_TYPE_BLE)
                        {
                            DBG_LOG("\r\ndelete env record numbers = %d ",env_record_size);
                            if(Record_Delete(RECORD_TYPE_ENV,env_record_size))
                            {
                                DBG_LOG("delete env record successful\r\n\r\n",env_record_size);                                
                            }
                            else
                            {
                                DBG_LOG("failed\r\n\r\n",env_record_size);
                            }
                        }
                    }
                }
            }             
        }
    }while(env_record_size && res && byte_len && (chn_type == COMMUNICATION_INTERFACE_TYPE_BLE));
    DBG_LOG("env data upload over\r\n\0");
    vSemaphoreDelete( binary_semaphore_wait_response );
    //free(send_buf);
    //free(p_encode_buf);
    return res;
}
void user_creat_env_detect_task(void)
{
    BaseType_t ret;  
    ret = xTaskCreate( user_env_detect_task, "env", 256, NULL, 1, &env_detect_handle );
    if(pdPASS != ret)
    {
        APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
    }  
    //DBG_LOG("\r\nenv_detect_handle = 0x%x\r\n\0",env_detect_handle);
}

void user_enb_give_upload_semaphore(void)
{
    BaseType_t semaphore = pdFALSE;
    xSemaphoreGiveFromISR(binary_semaphore_env_upload,&semaphore); 
}
void user_suspend_env_detect_task(void)
{
    vTaskSuspend(env_detect_handle);
}
void user_resume_env_detect_task(void)
{
    vTaskResume(env_detect_handle);
}
void user_env_memory_test(void)
{
    env_record_t   env_record_buf;
    
    user_env_data_sample_record();
    user_env_read_records(&env_record_buf,1,COMMUNICATION_INTERFACE_TYPE_NB);
}
//end
