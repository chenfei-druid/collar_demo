/**
 * user_task.c
 * Date:    2018/4/11
 * Author:  Chenfei
 * 
 * @brief smart-meter project.
 * by  nrf_52832 freertos platform
 *
 */

#include "nrf_drv_rtc.h"
#include "nrf_rtc.h"
#include "FreeRTOS.h"
#include "nrf_sdh_freertos.h"
#include "semphr.h"
#include "user_app.h"
#include "nrf_drv_gpiote.h"
#include "hal_rtc.h"
#include "user_task.h"

#define RTC_TIME_CONFIG_FREQUENCY 8
#define BLINK_RTC_TICKS   (RTC_US_TO_TICKS(1*1000000ULL, RTC_TIME_CONFIG_FREQUENCY))


volatile    uint32_t            user_tiks_count = 0;
volatile    uint32_t            user_unix_time_count = 0; 

//static uint32_t rtc_cnt = 0;
//static uint32_t tick_counter = 0;
bool integral_clock_flag = 0;
bool zero_clock_flag = 0;
const nrf_drv_rtc_t   rtc2 = NRF_DRV_RTC_INSTANCE(2);


static nrf_drv_rtc_config_t  m_rtc_config = 
{                                                                                                
    .prescaler          = RTC_FREQ_TO_PRESCALER(RTC_TIME_CONFIG_FREQUENCY),                   
    .interrupt_priority = RTC_DEFAULT_CONFIG_IRQ_PRIORITY,                                       
    .reliable           = RTC_DEFAULT_CONFIG_RELIABLE,                                           
    .tick_latency       = RTC_US_TO_TICKS(NRF_MAXIMUM_LATENCY_US, RTC_TIME_CONFIG_FREQUENCY),
};
static void hal_rtc2_handler(nrf_drv_rtc_int_type_t inter_type)
{
    if(inter_type == NRF_DRV_RTC_INT_COMPARE0)
    {
        uint32_t err_code;
        err_code = nrf_drv_rtc_cc_set(&rtc2, 0,BLINK_RTC_TICKS, true);
        APP_ERROR_CHECK(err_code);     
        user_tiks_count ++;
        user_unix_time_count ++;     
    }
    nrf_drv_rtc_counter_clear(&rtc2);
}
bool hal_rtc2_init(uint32_t time)
{
    
    uint32_t err_code;
    err_code = nrf_drv_rtc_init(&rtc2, &m_rtc_config, hal_rtc2_handler);
    APP_ERROR_CHECK(err_code);
    err_code = nrf_drv_rtc_cc_set(&rtc2, 0 ,BLINK_RTC_TICKS, true);
    APP_ERROR_CHECK(err_code);
    nrf_drv_rtc_enable(&rtc2);
    //nrf_gpio_cfg_output(12);
    //user_unix_time_count = nrf_rtc_counter_get(NRF_RTC2);
    user_unix_time_count = time;
    /*
    if(user_unix_time_count > 2177424000 || user_unix_time_count < 1514736000)
    {
        user_unix_time_count = 1514736000;   //2018/1/1 00:00:00       
    }
    */
    return true;
}

uint32_t hal_rtc2_get_unix_time(void)
{
    return user_unix_time_count;
}

bool read_date(hal_rtc_date_t *p_date)
{
    //char date_string[80];
    time_t unix_time = user_unix_time_count;
    struct tm *p_tm;
    unix_time = hal_rtc2_get_unix_time();
    //unix_time = 1523435000;     //2018/4/11 16:23:20
    unix_time += 28800;
    p_tm = localtime(&unix_time);
    p_date->sec  =   p_tm->tm_sec;
    p_date->min  =   p_tm->tm_min;
    p_date->hour =   p_tm->tm_hour;
    p_date->day  =   p_tm->tm_mday;
    p_date->mon  =   p_tm->tm_mon + 1;
    p_date->year =   p_tm->tm_year+1900;
    return true;
}
uint8_t nrf_cal_get_time_string(uint8_t *date)
{
    hal_rtc_date_t date_t;
    uint8_t *p_date = date;
    read_date(&date_t);
    //year hex to ascii
    *p_date++ = date_t.year / 1000 + 0x30;
    *p_date++ = (date_t.year / 100)%10 + 0x30;
    *p_date++ = (date_t.year / 10)%10 + 0x30;
    *p_date++ = date_t.year%10 + 0x30;
    //second hex to ascii
    *p_date++ = '-';    
    *p_date++ = date_t.mon / 10 + 0x30;
    *p_date++ = date_t.mon % 10  + 0x30;
    //year hex to ascii
    *p_date++ = '-';
    *p_date++ = date_t.day / 10 + 0x30;
    *p_date++ = date_t.day % 10  + 0x30;
    //year hex to ascii
    *p_date++ = '-';
    *p_date++ = date_t.hour / 10 + 0x30;
    *p_date++ = date_t.hour % 10  + 0x30;
    //year hex to ascii
    *p_date++ = ':';
    *p_date++ = date_t.min / 10 + 0x30;
    *p_date++ = date_t.min % 10  + 0x30;

    *p_date++ = ':';
    //second hex to ascii
   *p_date++ = date_t.sec / 10 + 0x30;
   *p_date++ = date_t.sec % 10  + 0x30;
   
   return (p_date - date);
}
uint8_t read_date_bcd(uint8_t *date_bcd)
{
    hal_rtc_date_t date_t;
    uint8_t *p_date = date_bcd;
    read_date(&date_t);
    //year hex to bcd
    //*p_date++ = (((date_t.year / 1000) << 4) & 0x0f) | (((date_t.year / 100)%10)&0x0f);
    *p_date++ = (((date_t.year / 10)%10 << 4) & 0xf0) | ((date_t.year % 10)&0x0f);
    //month hex to bcd
    *p_date++ = ((date_t.mon / 10 << 4) & 0xf0) | ((date_t.mon % 10)& 0x0f);
    //day hex to bcd
    *p_date++ = ((date_t.day / 10 << 4) & 0xf0) | ((date_t.day % 10)& 0x0f);
    //hour hex to bcd
    *p_date++ = ((date_t.hour / 10 << 4) & 0xf0) | ((date_t.hour % 10)& 0x0f);
    //minute hex to bcd
    *p_date++ = ((date_t.min / 10 << 4) & 0xf0) | ((date_t.min % 10)& 0x0f);
    //second hex to bcd
    *p_date++ = ((date_t.sec / 10 << 4) & 0xf0) | ((date_t.sec % 10)& 0x0f);
    return (p_date - date_bcd);
}
bool hal_rtc2_set_time_by_date(hal_rtc_date_t const date)
{
    struct tm *p_tm;
    memset(p_tm, 0, sizeof(struct tm));
    p_tm->tm_sec = date.sec;
    p_tm->tm_min = date.min;
    p_tm->tm_hour= date.hour;
    p_tm->tm_mday= date.day;
    p_tm->tm_mon = date.mon;
    p_tm->tm_year= date.year - 1900;  
    return true;
}
bool hal_rtc2_set_time_by_timestamp(time_t const new_time)
{  
    user_unix_time_count = new_time;
    return true;
}
bool hal_judge_integral_time(void)
{
    if(user_unix_time_count % 3600 <= TASK_WORK_INTERVAL)
    {
        integral_clock_flag = 1;
        return true;
    }
    return false;
}
bool hal_judge_zero_clock(void)
{
    if((user_unix_time_count + 28800) % 86400 <= TASK_WORK_INTERVAL)
    //if((user_unix_time_count) % 86400 <= TASK_WORK_INTERVAL)
    {
        zero_clock_flag = 1;
        return true;
    }
    return false;
}
bool is_integral_time(void)
{
    if(integral_clock_flag == 1)
    {
        integral_clock_flag = 0;
        return true;
    }
    return false;
}
bool is_zero_clock(void)
{
    if(zero_clock_flag ==1 )
    {
        zero_clock_flag = 0;
        return true;
    }
    return false;
}
void clear_clock_flag(void)
{
    zero_clock_flag = 0;
    integral_clock_flag = 0;
}
uint32_t hal_rtc_get_user_tiks(void)
{
    return user_tiks_count;
}

