/* Copyright (c) 2014 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 */
#include "FreeRTOS.h"
#include "user_app_calendar.h"
#include "nrf.h"
#include "timers.h"
#include "semphr.h"
#include "user_task.h"
static TimerHandle_t    m_base_clock_timer;

struct tm t;
uint8_t TimeUpdataFlag;

static bool on_time_flag = 0;
bool is_at_zero_clock_flag = 0;
 

void nrf_cal_set_time(uint32_t year, uint32_t month, uint32_t day, uint32_t hour, uint32_t minute, uint32_t second)
{

    t.tm_year = year - 1900;
    t.tm_mon = month - 1;
    t.tm_mday = day;
    t.tm_hour = hour;
    t.tm_min = minute;
    t.tm_sec = second;           
}


void nrf_cal_get_time_string(char * const cal_string)
{ 
	  strftime(cal_string, 80, "%Y-%m-%d-%H:%M:%S", &t);
}
void read_date(char * const p_date)
{
    static char date_string[80];
    nrf_cal_get_time_string(date_string);
    
    p_date[0] = ((date_string[2] - '0') << 4) | (date_string[3] - '0');         //year
    p_date[1] = ((date_string[5] - '0') << 4) | (date_string[6] - '0');         //month
    p_date[2] = ((date_string[8] - '0') << 4) | (date_string[9] - '0');        //date
    p_date[3] = ((date_string[11] - '0') << 4) | (date_string[12] - '0');        //hour
    p_date[4] = ((date_string[14] - '0') << 4) | (date_string[15] - '0');        //minute
    p_date[5] = ((date_string[17] - '0') << 4) | (date_string[18] - '0');        //second
    
}
static char not_leap(void)      //check for leap year
{
	if (!(t.tm_year%100))
	{
		return (char)(t.tm_year%400);
	}
	else
	{
		return (char)(t.tm_year%4);
	}
}
bool is_integral_time(void)
{
    if(on_time_flag == 1)
    {        
        on_time_flag = 0;
        return 1;
    }
    return 0;
}
bool is_at_zero_clock(void)
{
    if(is_at_zero_clock_flag == 1)
    {
        is_at_zero_clock_flag = 0;
        return 1;
    }
    else
        return 0;
}

void CAL_updata(void)
{
	TimeUpdataFlag = UPDATA_SEC;
	if (++t.tm_sec==60)        
	{
		TimeUpdataFlag = UPDATA_HM;
		t.tm_sec=0;
		if (++t.tm_min==60)
		{
            on_time_flag = 1;       //on time flag, for collecting data
			t.tm_min=0;
			if (++t.tm_hour==24)
			{
                is_at_zero_clock_flag = 1; ////zero clock, for collecting data
				TimeUpdataFlag = UPDATA_DATE;
				t.tm_hour=0;
				if (++t.tm_mday==32)
				{
					t.tm_mday++;
					t.tm_mday=1;
				}
				else if (t.tm_mday==31)
				{
					if ((t.tm_mon==4) || (t.tm_mon==6) || (t.tm_mon==9) || (t.tm_mon==11))
					{
						t.tm_mon++;
						t.tm_mday=1;
					}
				}
				else if (t.tm_mday==30)
				{
					if(t.tm_mon==2)
					{
						t.tm_mon++;
						t.tm_mday=1;
					}
				}
				else if (t.tm_mday==29)
				{
					if((t.tm_mon==2) && (not_leap()))
					{
						t.tm_mon++;
						t.tm_mday=1;
					}
				}
				if (t.tm_mon==13)
				{
					t.tm_mon=1;
					t.tm_year++;
				}
			}
		}
	}	
}
extern xSemaphoreHandle xBinarySemaphore;
void base_1s_clock_timeout_handler(TimerHandle_t xTimer)
{
    
    user_tick_timer_handler();
    //user_tiks_count++;
    //CAL_updata();
}
void user_calendar_clock_init(void)
{
    m_base_clock_timer = xTimerCreate("clock",
                                   1000,        //ms
                                   pdTRUE,
                                   NULL,
                                   base_1s_clock_timeout_handler);
}
void user_calendar_clock_start(void)
{
    if (pdPASS != xTimerStart(m_base_clock_timer, 1))   //2 ticks
    {
        APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
    }
}

//end
