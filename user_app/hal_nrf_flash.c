/**--------------------------------------------------------------------------------------------------------
** Commpany     :       Druid
** Created by   :		chenfei
** Created date :		2017-12-1
** Version      :	    0.0
** Descriptions :		user_nrf_flash.c
*/

#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
//#include "softdevice_handler.h"
#include "ble_flash.h"
#include "hal_nrf_flash.h"
#include "nrf_fstorage_sd.h"
#include "nrf_fstorage.h"
#include "app_error.h"

//nrf_flash_rw    p_saved  = {.addr = (uint32_t *)USER_DATA_MEMORY_START, .word_count = 0};
//nrf_flash_rw    p_upload = {.addr = (uint32_t *)USER_DATA_MEMORY_START, .word_count = 0};
//fs_config_t     m_fs_config;
static volatile bool     fs_callback_flag;
static void fstorage_evt_handler(nrf_fstorage_evt_t * p_evt);
static nrf_fstorage_info_t  m_flash_info = {.erase_unit = USER_PAGE_SIZE,  //in bytes
                                            .program_unit = sizeof(int32_t),
                                            .rmap = true,
                                            .wmap = false,            
                                            };
NRF_FSTORAGE_DEF(nrf_fstorage_t fstorage) =
{
    .p_flash_info = &m_flash_info,
    /* Set a handler for fstorage events. */
    .evt_handler = fstorage_evt_handler,
    //.evt_handler = NULL,

    /* These below are the boundaries of the flash space assigned to this instance of fstorage.
     * You must set these manually, even at runtime, before nrf_fstorage_init() is called.
     * The function nrf5_flash_end_addr_get() can be used to retrieve the last address on the
     * last page of flash available to write data. */
    .start_addr = USER_MEMORY_START,  
    .end_addr   = USER_MEMORY_END,
};

//static void user_save_current_position(uint32_t upload_addr, uint32_t save_addr);
typedef struct
{
    uint32_t    p_data_save;
    uint32_t    p_data_upload;
    uint32_t    data_save_length;
}user_data_head;
/**
* @function@name:    user_nrf_flash_event_handler
* @description  :    the call back of flash event
* @input        :    none
* @output       :    none
* @return       :    none
*/
static void fstorage_evt_handler(nrf_fstorage_evt_t * p_evt)
{
    if (p_evt->result == NRF_SUCCESS)
    {
        fs_callback_flag = 1;
    }
}

/**
* @function@name:    user_nrf_flash_init
* @description  :    Function for initializing flash on system
* @input        :    none
* @output       :    none
* @return       :    none
*/
void user_nrf_flash_init(void)
{
    ret_code_t rc;
    //fs_init();
    nrf_fstorage_api_t * p_fs_api = &nrf_fstorage_sd;
    rc = nrf_fstorage_init(&fstorage, p_fs_api, NULL);
    APP_ERROR_CHECK(rc);
}
/**
* @function@name:    user_nrf_flash_page_erase
* @description  :    Function for erase page on flash
* @input        :    page_start: pointor to page start address, page_num: page numbers will be erased
* @output       :    none
* @return       :    none
*/
bool user_nrf_flash_page_erase(uint32_t *page_start,int8_t page_num)
{
    if(page_num <= 0 || page_num > USER_PAGE_MAX)
    {
        return false;
    }
    fs_callback_flag = 0;

    ret_code_t rc = nrf_fstorage_erase(&fstorage, (uint32_t)page_start, page_num, NULL);

	uint32_t retry = 5000000;
    while(fs_callback_flag == 0 && retry-- > 0);
    if(fs_callback_flag == 0)
    {
        return false;
    }
    return true;
}
/**
* @function@name:    user_nrf_flash_word_write
* @description  :    Function for write one word into flash memory
* @input        :    p_address: pointor to word data address
* @output       :    value: pointor to word data will be write
* @return       :    none
*/
bool user_nrf_flash_word_write(uint32_t * p_address, uint32_t value)
{    
    
    fs_callback_flag = 0;
    if((uint32_t)p_address >fstorage.end_addr || (uint32_t)p_address < fstorage.start_addr)
    {
        //p_address = (uint32_t *)USER_DATA_MEMORY_START;
        return false;
    }

    ret_code_t rc = nrf_fstorage_write(&fstorage, (uint32_t)p_address, &value, sizeof(value), NULL);
    uint32_t retry = 5000000;
    while(fs_callback_flag == 0 && retry-- > 0);
    if(fs_callback_flag == 0)
    {
        return false;
    }
    return true;
}
/**
* @function@name:    user_nrf_flash_words_write
* @description  :    write some data as words into a searial continue place
* @input        :    p_address: the start address,
*                    word_buff: pointor to data will be writed, len: data length, one word = 4 byte
* @output       :    none
* @return       :    none
*/
bool user_nrf_flash_words_write(uint32_t * p_address, const uint32_t *const word_buff, const uint32_t len)
{
    for(uint32_t i=0;i<len;i++)
    {
        if(user_nrf_flash_word_write(p_address, word_buff[i]) == false)
        {
            return false;
        }
        p_address ++;
    }
    return true;
}
/**
* @function@name:    user_nrf_flash_word_read
* @description  :    Function for read one word from flash memory
* @input        :    p_address: pointor to word data address
* @output       :    value: pointor to cache of acquired word of word
* @return       :    none
*/
void user_nrf_flash_word_read(uint32_t * p_address, uint32_t * value)
{    
    if((uint32_t)p_address == 0x00000000)
    {
        p_address = (uint32_t *)USER_DATA_MEMORY_START;
    }
    //*value = *p_address;
    nrf_fstorage_read(&fstorage, (uint32_t)p_address, (uint32_t *)value, 4);
    
    //memcpy(value, (uint32_t *)0x00074000, 1);
}
/**
* @function@name:    user_nrf_flash_read_page
* @description  :    Function for getting page from flash memory
* @input        :    page_start: pointor to page start address
* @output       :    p_out_array: pointor to cache of acquired datas of word
* @return       :    none
*/
void user_nrf_flash_read_page(uint32_t *page_start, uint32_t * p_out_array)
{
    //ble_flash_page_read(page_num, p_out_array, p_word_count);
    uint32_t *p_page = page_start;
    for(uint16_t i=0;i<USER_PAGE_SIZE/4;i++)
    {
        user_nrf_flash_word_read((p_page+i), &p_out_array[i]);
    }
}
/**
* @function@name:    user_nrf_flash_read_words
* @description  :    Function for getting words from flash memory
* @input        :    p_address: pointor to data address, byte_len: word data length
* @output       :    data_buff: pointor to cache of acquired words of word
* @return       :    none
*/
void user_nrf_flash_read_words(uint32_t * p_address, uint32_t * data_buff, uint32_t len)  
{
    for(uint32_t i=0;i<len;i++)
    {
        user_nrf_flash_word_read(p_address,&data_buff[i]);
        p_address ++;
    }
}
/**
* @function@name:    user_nrf_flash_get_bytes
* @description  :    Function for getting bytes from flash memory
* @input        :    p_address: pointor to data address, byte_len: data length
* @output       :    data_buff: pointor to cache of acquired byte datas
* @return       :    none
*/
uint32_t user_nrf_flash_get_bytes(uint32_t * p_address, uint8_t * data_buff, uint32_t byte_len)
{
    return nrf_fstorage_read(&fstorage, (uint32_t)p_address, data_buff, byte_len);
}

/**
* @function@name:    user_nrf_flash_save_bytes
* @description  :    save some data as byte into a searial continue place
* @input        :    p_address: the start address,
*                    data_buff: pointor to data will be writed, len: data length as byte
* @output       :    none
* @return       :    none
*/
bool user_nrf_flash_save_bytes(uint32_t * p_address, uint8_t * data_buff, uint32_t byte_len)
{
    uint32_t word_len = byte_len / 4;
    word_len = (byte_len % 4 == 0)? (byte_len / 4):(byte_len / 4 + 1);
    
    fs_callback_flag = 0;
    user_nrf_flash_words_write(p_address, (uint32_t *)data_buff, word_len);
    //nrf_fstorage_write(&fstorage, (uint32_t)p_address, data_buff, byte_len, NULL);
    uint32_t retry = 5000000;
    //while(fs_callback_flag == 0);
    
    while(fs_callback_flag == 0 && retry-- > 0);    
    if(fs_callback_flag == 0)
    {
        return false;
    }
    
    return true;
}
bool user_flash_next_page_start(uint32_t *p_addr)
{
    *p_addr = (*p_addr + USER_PAGE_SIZE) & 0xfffff000;
    return true;
}
/**
* @function@name:    user_save_datas_as_page
* @description  :    save some datas into a page
* @input        :    save_addr: save data start addr, data_buff: datas writed cache, byte_len: data length as byte                    
* @output       :    none
* @return       :    byte_len
*/
uint32_t user_save_datas_as_page(uint32_t save_addr, uint8_t * data_buff, uint32_t byte_len)
{
    if(data_buff == NULL)
    {
        return false;
    }
    if(byte_len > USER_PAGE_SIZE)
    {
        byte_len = USER_PAGE_SIZE;
    }
    if(save_addr >=fstorage.end_addr || save_addr < fstorage.start_addr)
    {
        save_addr = USER_DATA_MEMORY_START;
    }
    user_nrf_flash_page_erase((uint32_t *)save_addr,1);
    user_nrf_flash_save_bytes((uint32_t *)save_addr, data_buff, byte_len);
    
    return byte_len;
}
/**
* @function@name:    user_save_datas_into_flash
* @description  :    save some datas as byte, and the address is changing after saved datas, the first address 
*                    @ref USER_BUFF_START, end address @ref USER_BUFF_END, if the address is reached end address, 
*                    the data will be saved from first address, and the old data will be covered
* @input        :    data_buff: pointor to data will be writed, byte_len: data length as byte                    
* @output       :    none
* @return       :    none
*/
bool user_save_datas_into_flash(uint32_t *save_addr, uint8_t * data_buff, uint32_t byte_len)
{
    
    //uint32_t save_addr_to_end_len, start_addr_to_current_len;
    if(*save_addr < fstorage.start_addr || *save_addr > fstorage.end_addr)
    {
        return false;
    }
    //uint32_t next_page_addr = ((((uint32_t)*save_addr) + m_flash_info.erase_unit) >> 12) << 12;
    uint32_t next_page_addr = (*save_addr & 0xfffff000) + m_flash_info.erase_unit  ;    //next page address = current page address + unit page words length
    //current page remaining spaces is more than data lenght in byte, directly save data is safely
    if((next_page_addr - *save_addr) > byte_len )                  
    {
        user_nrf_flash_save_bytes((uint32_t *)*save_addr, data_buff, byte_len);
        *save_addr += byte_len;
    }
    //there is no enough spaces in current page to save datas, so must extend next pages,
    else            
    {
        //get how many pages to save datas 
        uint8_t page_cnt = byte_len / m_flash_info.erase_unit;
        if(byte_len % m_flash_info.erase_unit != 0)             
        {
            page_cnt += 1;
        }    
        //if there is enough memory on flash end part, erase pages and save datas
        if((fstorage.end_addr - (uint32_t)*save_addr) > byte_len )   
        {
            user_nrf_flash_page_erase((uint32_t *)next_page_addr,page_cnt);
            user_nrf_flash_save_bytes((uint32_t *)*save_addr, data_buff, byte_len);
            *save_addr += byte_len;
        }
        //if there is no enough memory on flash end part, save first data into memory end, then save remaining data into memory start
        else
        {
            //save first data on memory end
            user_nrf_flash_page_erase((uint32_t *)next_page_addr, (fstorage.end_addr - (uint32_t)*save_addr) / m_flash_info.erase_unit);
            user_nrf_flash_save_bytes((uint32_t *)*save_addr, data_buff, (fstorage.end_addr - (uint32_t)*save_addr));
            //save remaining data on memory start
            user_nrf_flash_page_erase((uint32_t *)USER_DATA_MEMORY_START,page_cnt - (fstorage.end_addr - (uint32_t)*save_addr) / m_flash_info.erase_unit);
            user_nrf_flash_save_bytes((uint32_t *)USER_DATA_MEMORY_START, data_buff, byte_len - (fstorage.end_addr - (uint32_t)*save_addr));
            *save_addr = (uint32_t) USER_DATA_MEMORY_START + (byte_len - (fstorage.end_addr - (uint32_t)*save_addr));
        }
    }
    return true;
}
/**
* @function@name:    user_get_datas_from_flash
* @description  :    Function for getting data will be sent from flash memory
* @input        :    get_addr: pointor to upload data address, byte_len: data length
* @output       :    data_buff: pointor to cache of acquired datas
* @return       :    none
*/
bool user_get_datas_from_flash(uint32_t *get_addr, uint8_t * data_buff, uint32_t byte_len)
{
    //uint32_t word_len = byte_len / 4;
    if(*get_addr < fstorage.start_addr || *get_addr > fstorage.end_addr)
    {
        return false;
    }
    uint32_t get_addr_to_end_len, start_addr_to_current_len;
    if((fstorage.end_addr - (uint32_t)*get_addr) < byte_len )
    {
        //the total length of bytes is more than last memory space, so must read end memory data firstly, then read the rest of data from start
        
        //get length of data in the end memory 
        get_addr_to_end_len = fstorage.end_addr - (uint32_t)*get_addr;
        //get length of data in the start memory
        start_addr_to_current_len = byte_len - get_addr_to_end_len;
        
        //read end memory data, 
        user_nrf_flash_get_bytes((uint32_t *)*get_addr, data_buff, get_addr_to_end_len);                 //read by byte, so use byte len
        //read rest of data from start memory
        user_nrf_flash_get_bytes((uint32_t *)USER_DATA_MEMORY_START, (data_buff+get_addr_to_end_len), start_addr_to_current_len);    //read by byte, so use byte len
        *get_addr = USER_DATA_MEMORY_START + start_addr_to_current_len;
    }
    else//the end memory space is more than total data length will be got
    {
         user_nrf_flash_get_bytes((uint32_t *)*get_addr, data_buff, byte_len);
        *get_addr += byte_len;
    }
    return true;
}

/**
* @function@name:    user_get_diff_len_read_save
* @description  :    Function for caculate the difference value of uploaded and saved address on flash
* @input        :    upload_addr: uploaded address, save_addr: saved address
* @output       :    byte_len: byte length of difference value
* @return       :    none
*/
bool user_get_diff_len_read_save(uint32_t *upload_addr, uint32_t *save_addr, int32_t * byte_len)
{
    if(*upload_addr < fstorage.start_addr || *upload_addr > fstorage.end_addr)
    {
        * byte_len = 0;
        *upload_addr = USER_DATA_MEMORY_START;
        return false;
    }
    if(*save_addr < fstorage.start_addr || *save_addr > fstorage.end_addr)
    {
        * byte_len = 0;
        *save_addr = USER_DATA_MEMORY_START;
        return false;
    }
    if(*save_addr < *upload_addr)
    {
        *byte_len = ((fstorage.end_addr - *upload_addr) + (*save_addr - USER_DATA_MEMORY_START));
    }
    else
    {
        *byte_len = (*save_addr - *upload_addr);  //aligned to word, word length * 4 = bytes
    }
    return true;
}
/*
static void user_save_current_position(uint32_t upload_addr, uint32_t save_addr)
{
    
     uint32_t flash_data_temp[6];        
     user_nrf_flash_read_words((uint32_t *)USER_INDEX_MEMORY_START,flash_data_temp,6);           //先将信息读出来
     flash_data_temp[2] = (uint32_t)p_saved.addr;                                           //将新设置的信息填入相应的位置
     flash_data_temp[3] = (uint32_t)p_upload.addr;
    
     user_nrf_flash_page_erase((uint32_t *)USER_INDEX_MEMORY_START,1);                           //erase flash page of message
     user_nrf_flash_words_write((uint32_t *)USER_INDEX_MEMORY_START, flash_data_temp, 6);    //再将所有数据重新写入flash
    
}
*/
/*
void user_get_last_position(void)
{
    user_nrf_flash_word_read((uint32_t *)USER_DATA_UPLD_ADDR,(uint32_t *)&p_upload.addr);
    user_nrf_flash_word_read((uint32_t *)USER_DATA_SAVE_ADDR,(uint32_t *)&p_saved.addr);
    if((uint32_t)p_upload.addr == 0xffffffff || (uint32_t)p_saved.addr == 0xffffffff)       //任何一个都不可能为FFFFFFFF，因为最多才0x00080000
    {
        //uint32_t temp[2];
        //user_nrf_flash_read_words((uint32_t *)(USER_BUFF_END-2),temp,2);
        //if(temp[0] == 0xffffffff && temp[1] == 0xffffffff)
        //{
            p_saved.addr  = (uint32_t *)USER_DATA_MEMORY_START;
            p_upload.addr = (uint32_t *)USER_DATA_MEMORY_START;
            
            uint32_t flash_data_temp[6];        
            user_nrf_flash_read_words((uint32_t *)USER_INDEX_MEMORY_START,flash_data_temp,6);           //先将信息读出来
            flash_data_temp[2] = (uint32_t)p_saved.addr;                                           //将新设置的信息填入相应的位置
            flash_data_temp[3] = (uint32_t)p_upload.addr;
    
            user_nrf_flash_page_erase((uint32_t *)USER_INDEX_MEMORY_START,1);                           //erase flash page of message
            user_nrf_flash_words_write((uint32_t *)USER_INDEX_MEMORY_START, flash_data_temp, 6);    //再将所有数据重新写入flash
        //}
    }
}
*/
/*
void user_erase_data_memory(void)
{    
    uint32_t flash_data_temp[6];        
    user_nrf_flash_read_words((uint32_t *)USER_INDEX_MEMORY_START,flash_data_temp,6);           //先将信息读出来
    p_saved.addr  = (uint32_t *)USER_DATA_MEMORY_START;
    p_upload.addr = (uint32_t *)USER_DATA_MEMORY_START;
    flash_data_temp[2] = (uint32_t)p_saved.addr;
    flash_data_temp[3] = (uint32_t)p_upload.addr;
    user_nrf_flash_page_erase((uint32_t *)USER_DATA_MEMORY_START,11);                          //erase flash page of message
    user_nrf_flash_words_write((uint32_t *)USER_INDEX_MEMORY_START, flash_data_temp, 6);    //再将所有数据重新写入flash
}
*/
void user_nrf_flash_read_data_accord_point(uint8_t *p_buff, uint32_t byte_nums)
{
    
}
/*
void user_nrf_flash_test(void)
{
    uint32_t temp[3]={0x12345678,0x22222222};
    uint8_t save[5];
    uint8_t count;
    uint32_t *addr;
    
    user_nrf_flash_init();
    user_save_datas_into_flash((uint8_t *)"abcd1234",8);
    user_get_datas_from_flash(save, 8);
    
}
*/
