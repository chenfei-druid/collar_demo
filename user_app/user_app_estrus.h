
#ifndef     USER_APP_ESTRUS_H
#define     USER_APP_ESTRUS_H

#include <stdint.h>
#include <stdbool.h>
#include "Estrus.pb.h"
#include "user_data_pkg.h"
#include "user_acc.h"

#define     ESTRUS_ODBA_WINDOW_DEFAULT          51
#define     ESTRUS_BASE_ODBA_RESULT_TIME        60
#define     ESTRUS_RECORD_BYTES                 (sizeof(estrus_record_t))
#define     ESTRUS_ONCE_UPLOAD_RECORD_NUM       5//(DRV_NB_UNIT_SEND_DATA_LEN / ESTRUS_RECORD_BYTES)       //
#define     ESTRUS_WAIT_RESPONSE_TIMEOUT        (WAIT_SERVER_RESPONSE*1000)    //5S

typedef struct
{
    uint32_t    timestamp;
    int32_t     activity;
    int32_t     odba;
    int32_t     est_thres;
}estrus_record_t;

typedef struct
{
    int32_t                 count;
    protocol_estrus_t       *record;
}estrus_encode_t;

typedef struct
{
    int32_t     mode;
    int32_t     interval;
    int32_t     estrus_threshold;
}estrus_setting_t;

void user_estrus_init(void);
void user_create_estrus_task(void);
void user_send_queue_odba_to_estrus_task(int32_t odba);
//bool user_estrus_upload_data(trans_interface_type chn_type);
bool user_estrus_upload_data(uint8_t * encode_buf, uint32_t encode_buf_size, 
                  uint8_t * send_buf, uint32_t send_buf_size,  
                  trans_interface_type chn_type
                 );
bool app_estrus_rsp_parse_handle(uint8_t *p_rsp, int32_t len);
bool user_estrus_set_sample_interval(int32_t interval);
bool user_estrus_set_sample_mode(int32_t mode);
void user_estrus_give_trans_semaphore_to_trans(void);
void user_estrus_give_continue_trans_semaphore_to_trans(void);
int32_t user_estrus_get_activity(void);
void user_suspend_estrus_detct_task(void);
void user_resume_estrus_detct_task(void);
int32_t user_estrus_get_sample_mode(void);
void user_estrus_sample_handle(acc_origin_point_t * const p_acc_xyz_t, uint32_t xyz_nums,uint32_t time);
bool user_estrus_setting(estrus_setting_t setting);

#endif      //USER_APP_ESTRUS_H

