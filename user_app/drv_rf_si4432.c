/**
 * drv_rf_si4432.c
 * Date:    2018/3/28
 * Author:  Chenfei
 * 
 * @brief neck strap project.
 * by  nrf_52832 freertos platform
 *
 */
#include <stdint.h>
#include "si4432.h"
#include "nrf_drv_spi.h"
#include "drv_rf_si4432.h"
#include "app_error.h"
#include "FreeRTOS.h"
#include "nrf_sdh_freertos.h"
#include "task.h"
#include "nrf_delay.h"
#define     SPI0_RF          0
static      volatile bool   spi_xfer_done; 
static const nrf_drv_spi_t spi0_rf = NRF_DRV_SPI_INSTANCE(SPI0_RF);  //< SPI instance.   

void hal_rf_spi0_event_handler(nrf_drv_spi_evt_t const * p_event, void * p_context)
{
    spi_xfer_done = true;
}
static void hal_rf_spi0_init(void)
{
    uint32_t err_code; 
    nrf_drv_spi_config_t spi0_rf_config ;
    
    spi0_rf_config.miso_pin     = PIN_RF_SPI0_MISO;
    spi0_rf_config.mosi_pin     = PIN_RF_SPI0_MOSI;
    spi0_rf_config.sck_pin      = PIN_RF_SPI0_SCK;
    spi0_rf_config.ss_pin       = PIN_RF_SPI0_CS;
    spi0_rf_config.mode         = NRF_DRV_SPI_MODE_0;
    spi0_rf_config.orc          = 0xFF;
    spi0_rf_config.irq_priority = SPI_DEFAULT_CONFIG_IRQ_PRIORITY;
    spi0_rf_config.frequency    = NRF_DRV_SPI_FREQ_4M;
    spi0_rf_config.bit_order    = NRF_DRV_SPI_BIT_ORDER_MSB_FIRST;
    
     
    err_code=nrf_drv_spi_init(&spi0_rf, &spi0_rf_config, hal_rf_spi0_event_handler,NULL);
    APP_ERROR_CHECK(err_code);
}
static void hal_rf_spi0_write_registor(uint8_t write_addr, uint8_t *write_data, uint8_t write_len)
{
    
    uint8_t rx_buf[3];
    nrf_drv_spi_transfer(&spi0_rf, write_data, write_len, rx_buf, 0);
}
static void hal_rf_spi0_read_reg(uint8_t read_addr, uint8_t *cmd, uint8_t cmd_len,uint8_t *data_buf, uint8_t read_len)
{
    //uint8_t rx_buf[3];
    nrf_drv_spi_transfer(&spi0_rf, cmd, cmd_len, data_buf, read_len);
    
}
/**
* @function@name:    drv_rf_si4432_write_registor
* @description  :    Function for write some command or data into si4432 registor
* @input        :    *p_u8StartAddr: pointor to registor start address, u8Bytes: write data length of bytes              
* @output       :    none
* @return       :    none
*/
void drv_rf_si4432_write_registor(uint8_t *p_u8StartAddr, uint8_t u8Bytes)
{
  uint8_t i,bytes,u8Command;

  i=0;

  while(i<u8Bytes)
  {
    bytes = *p_u8StartAddr;

    i++;
    p_u8StartAddr++;

    u8Command = *p_u8StartAddr;

    i++;
    p_u8StartAddr++;

    //BSP_RW_SPI2(u8Command,p_u8StartAddr,bytes,WRITE);

    i += bytes;
    p_u8StartAddr += bytes;
  }
}
/**
* @function@name:    drv_si4432_init
* @description  :    Function for si4432 initialization
* @input        :    none              
* @output       :    none
* @return       :    none
*/
uint16_t drv_si4432_init(void)
{
  uint16_t u16Temp;

  uint8_t  a_u8InitPara[116] = {1,WR_REG|GPIOConfig0_0B,
                                  GPIO_DRV_L0|TXSTAT_OUT,
                                1,WR_REG|GPIOConfig1_0C,
                                  GPIO_DRV_L0|RXSTAT_OUT,
                                1,WR_REG|GPIOConfig2_0D,
                                  GPIO_DRV_L0|PULLUP_200K|RXDATA_OUT,
                                1,WR_REG|IFFilterBandwidth_1C,
                                  0x1E,
                                1,WR_REG|AFCLoopGearshiftOverride_1D,
                                  0x40,
                                1,WR_REG|AFCTimingControl_1E,
                                  0x0A,
                                1,WR_REG|ClkRcvGearshiftOverride_1F,
                                  0x03,
                                1,WR_REG|ClkRcvOversamplingRate_20,
                                  0xE8,
                                1,WR_REG|ClkRcvOffset2_21,
                                  0x60,
                                1,WR_REG|ClkRcvOffset1_22,
                                  0x20,
                                1,WR_REG|ClkRcvOffset0_23,
                                  0xC5,
                                1,WR_REG|ClkRcvTimingLoopGain1_24,
                                  0x00,
                                1,WR_REG|ClkRcvTimingLoopGain0_25,
                                  0x03,
                                1,WR_REG|AFCLimiter_2A,
                                  0x50,
                                1,WR_REG|DataAccessControl_30,
                                  0xAD,
                                1,WR_REG|HeaderControl1_32,
                                  0x8C,
                                1,WR_REG|HeaderControl2_33,
                                  0x02,
                                1,WR_REG|PreambleLength_34,
                                  0x08,
                                1,WR_REG|PreambleDetectionControl1_35,
                                  0x22,
                                1,WR_REG|SyncWord3_36,
                                  0x2D,
                                1,WR_REG|SyncWord2_37,
                                  0xD4,
                                1,WR_REG|SyncWord1_38,
                                  0x00,
                                1,WR_REG|SyncWord0_39,
                                  0x00,
                                9,WR_REG|TransHeader3_3A,
                                  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
                                4,WR_REG|HeaderEnable3_43,
                                  0xFF,0xFF,0xFF,0xFF,
                                1,WR_REG|AGC_Override1_69,
                                  0x60,
                                1,WR_REG|TX_Power_6D,
                                  TX_PWR_RESET|TX_PWR,
                                  //TX_PWR_RESET|TX_PWR_MAX,
                                1,WR_REG|TX_Data_Rate1_6E,
                                  0x10,
                                  //DATA_RATE_HIGH,
                                1,WR_REG|TX_Data_Rate0_6F,
                                 0x62,
                                  //DATA_RATE_LOW,
                                1,WR_REG|Modulation_Mode_Control1_70,
                                  0x24,
                                  //0x0C|DATA_RATE_CTR,
                                  //0x2c,
                                1,WR_REG|Modulation_Mode_Control2_71,
                                  0x2b,
                                1,WR_REG|Frequency_Deviation_72,
                                  0x50,
                                1,WR_REG|FrequencyBandSelect_75,
                                  0x53,
                                1,WR_REG|NominalCarrierFreqHigh_76,
                                 0x57,
                                1,WR_REG|NominalCarrierFreqLow_77,
                                  0x80
                              };

  //u16Temp = clear_Si4432State();                                                //读状态并清除

  drv_rf_si4432_write_registor(&a_u8InitPara[0],116);                                    // 初始化基础参数

  //return(u16Temp);
  //return true;
}
void drv_si4432_power_down(void)
{
    uint8_t dt;
    hal_rf_spi0_init();
    dt = 0x80;
    hal_rf_spi0_write_registor(0x07,&dt,1);
    //vTaskDelay(20);
    nrf_delay_ms(20);
    dt = 0x00;
    hal_rf_spi0_write_registor(0x07,&dt,1);
}
