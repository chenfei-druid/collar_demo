/** 
 * user_acc.c
 *
 * @group:       collar project
 * @author:      Chenfei
 * @create time: 2018/4/20
 * @version:     V0.0
 *
 */
#include <math.h>
#include <string.h>
#include <stdint.h>
#include "nrf.h"
#include "FreeRTOS.h"
#include "nrf_sdh_freertos.h"
#include "semphr.h"
#include "task.h"
#include "drv_acc_KX022.h"
//#include "Record.h"
#include "user_app_log.h"
#include "user_acc.h"
#include "user_task.h"
#include "behavior.h"
#include "user_app_bhv.h"
#include "user_app_origin.h"
#include "user_app_set.h"
#include "alarm_clock.h"
#include "Record.h"
#include "user_app_estrus.h"

static  TaskHandle_t        acc_collect_handle;
//static xSemaphoreHandle    binary_semaphore_acc_collect = NULL;
static xSemaphoreHandle     binary_semaphore_acc_setting = NULL;
static  QueueHandle_t       acc_queue = NULL;
//extern  QueueHandle_t       queue_interrupt;
//static xSemaphoreHandle    semaphore_test = NULL;
//static  TaskHandle_t       test_handle;
/*
static  acc_sample_time_flag      user_time_type[] = {
                                                        ACC_SAMPLE_START_TYPE,
                                                        ACC_SAMPLE_END_TYPE 
                                                     }; 
*/                                                     
static bool acc_sample_status  = 0;  
/***********************************************user functions**********************************************/

/**
* @function@name:    hal_acc_get_status
* @description  :    function for getting the g-sensor status
* @input        :    none
* @output       :    none
* @return       :    status of g-sensor
*/                                                     
bool hal_acc_get_status(void)
{
    DBG_LOG("\r\nacc_sample_status = %d\r\n",acc_sample_status);
    return acc_sample_status;
}

/**
* @function@name:    hal_acc_read_fifo_data
* @description  :    function for read acc sensor fifo datas, each point include x-axis, y-axis, z-axis
* @input        :    p_buf: pointor to data cache buffer
* @output       :    point_numbers: all points number the acc sensor generated according to OUT-DATA-RATE
* @return       :    point numbers
*/
/*
static uint32_t hal_acc_read_fifo_data(acc_origin_point_t *p_buf, uint8_t max_size)
{
    if(p_buf == NULL)
    {
        return 0;
    }
    uint8_t temp;
    uint8_t *acc_buf = (uint8_t *)p_buf;
    uint8_t give_len=0, len_cnt=0;
    //DBG_LOG("\r\nhal acc give length = ");
    xQueueReceive(acc_queue, &give_len , 0);
    //DBG_LOG(" %d\r\nhal acc len cnt =",give_len);
    while(xQueueReceive(acc_queue, &temp , 0) != errQUEUE_EMPTY)
    {
        acc_buf[len_cnt++] = temp;
        if(len_cnt >= max_size)
        {
            xQueueReset(acc_queue);
            break;
        }
    }
    //DBG_LOG(" %d\r\n",len_cnt);
    if(give_len == len_cnt)
    {
        return give_len / DRV_kx022_ONE_XYZ_DATA_LEN;
    }
    else
    {
        return 0;
    }
}
*/
/*
static uint32_t hal_acc_read_fifo_data(acc_origin_point_t *p_buf)
{
    bool ret = false;
    //uint8_t i;
    uint8_t int_flag;
    uint8_t point_numbers;
    int_flag = drv_kx022_read_int_flag();
    if(int_flag & 0x20)
    {
        point_numbers = drv_kx022_get_fifo_points();   
        
        if(point_numbers > HAL_ACC_POINT_SAMPLES)
        {
            point_numbers = HAL_ACC_POINT_SAMPLES;            
        }
        if(point_numbers != 0)
        {
             //ret = drv_kx022_get_sample_points(p_buf,point_numbers); 
        }                              
    }
    else
    {
        point_numbers = 0;
    }
    drv_kx022_clear_interrupt_source();  
    DBG_LOG("read fifo point numbers = %d\r\n",point_numbers);
    if(ret == true)
    {
        return point_numbers;
    }
    return point_numbers;
}
*/
/**
* @function@name:    hal_acc_start_collect
* @description  :    Function for g-sensor interrupt event handler
* @input        :    none
* @output       :    none
* @return       :    none
*/
/*
static void hal_acc_interrupt_event_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    //DBG_LOG("\r\nacc generated interrupt\r\n");
    //user_acc_send_collect_semaphore();
    DBG_LOG("\r\n+++++++++++++++++++++++acc interrupt++++++++++++++++++++++\r\n");
    //user_acc_send_queue_to_task(ACC_TASK_MSG_TYPE_COLLECT);
}*/
#if 0       //for test 
static uint32_t hal_acc_read_data_test(acc_origin_point_t *acc_buffer)
{
    for(int8_t i=0; i<25; i++)
    {   
        acc_buffer[i].x = rand() % 10;
        acc_buffer[i].y = rand() % 10;
        acc_buffer[i].z = rand() % 10;
    }
    return 25;
}
#endif
/**
* @function@name:    hal_acc_start_collect
* @description  :    Function for starting accelerater collectiong
* @input        :    none
* @output       :    none
* @return       :    none
*/
/*
bool hal_acc_start_collect(void)
{
    int32_t acc_mode = 0;
    acc_mode = user_origin_get_sample_mode() + user_bhv_get_sample_mode() + user_estrus_get_sample_mode();
    if(acc_mode > 0)
    {
        //if g-sensor start or not, if start, not do start operation
        if(!hal_acc_get_status())
        {
            DBG_LOG("\r\nstarting g-sensor sample\r\n");
            //if(drv_kx022_start_fifo_sample(hal_acc_interrupt_event_handler) != true)
            {
                return false;
            }
            acc_sample_status = 1;
            DBG_LOG("\r\nacc startup sample\r\n");
        }
        else
        {
            DBG_LOG("\r\ng-sensor is started, not neet to start again\r\n");
        }
    }

    return true;
}
*/
/**
* @function@name:    hal_acc_stop_collect
* @description  :    Function for stop accelerater collectiong
* @input        :    none
* @output       :    none
* @return       :    none
*/
/*
bool hal_acc_stop_collect(void)
{
    int32_t acc_mode = 0;
    acc_mode = user_origin_get_sample_mode() + user_bhv_get_sample_mode() + user_estrus_get_sample_mode();
    if(acc_mode == 0)
    {
        if(drv_kx022_stop_fifo_sample() != true)
        {
            DBG_LOG("\r\n stop g-sensor failed\r\n");
            return false;
        }
        acc_sample_status = 0;
        DBG_LOG("\r\nacc stop sample\r\n");
    }
    return true;
}
*/
/**
* @function@name:    user_acc_save_sample
* @description  :    Function for getting g-snsor sample of (x-axis, y-axis, z-axis) samples, 
*                    and save to origin, behavior and estrus data
* @input        :    none
* @output       :    none
* @return       :    none
*/
void user_acc_save_sample(acc_origin_point_t  *xyz_buf, uint32_t    pt_num)
{

    if(pt_num == HAL_ACC_POINT_SAMPLES)
    {
        #if 0
        for(uint8_t i=0;i<pt_num;i++)
        {
            DBG_LOG("\r\nX = %d, Y = %d, Z = %d\r\n",xyz_buf[i].x,xyz_buf[i].y,xyz_buf[i].z);
        }
        #endif
        uint32_t time = hal_rtc2_get_unix_time();
        if(user_origin_get_sample_mode())
        {
            //DBG_LOG("\r\nacc save origin sample\r\n");
            app_origin_save_handle(xyz_buf,pt_num,time);
        }
        if(user_bhv_get_sample_mode())
        {
            //DBG_LOG("\r\nacc save behavior sample\r\n");
            user_bhv_sample_handle(xyz_buf,pt_num,time);
        }
        if(user_estrus_get_sample_mode())
        {
            //DBG_LOG("\r\nacc save estrus sample\r\n");
            user_estrus_sample_handle(xyz_buf,pt_num,time);
        }
    }
}
/**
* @function@name:    user_acc_set_sensor_performance
* @description  :    Function for set the acc sensor running mode, if neet to sample, start the sensor, if not, stop sensor 
* @input        :    none
* @output       :    none
* @return       :    none
*/
bool user_acc_set_sensor_performance(void)
{
    int32_t acc_mode = 0;
    acc_mode = user_origin_get_sample_mode() + user_bhv_get_sample_mode() + user_estrus_get_sample_mode();
    DBG_LOG("\r\nuser_acc_set_sensor_performance: acc_mode = %d\r\n",acc_mode);
    if(acc_mode > 0)
    {
        drv_kx022_start_fifo_sample();
    }
    else
    {
        drv_kx022_into_sleep_mode();
    }
    return true;
}

/**
* function for data collect task, it performs the acc data process
*/
void user_acc_data_collect_task(void *arg)
{
    //vTaskDelay( 1000);
    //static uint8_t test_cnt = 0;
    static uint32_t     pt_num;
    static acc_origin_point_t      xyz_buf[HAL_ACC_POINT_SAMPLES];  
    user_bhv_init();
    user_estrus_init();
    
    if(binary_semaphore_acc_setting == NULL)
    {
        binary_semaphore_acc_setting = xSemaphoreCreateBinary(); 
        xSemaphoreTake( binary_semaphore_acc_setting, 10 );
    }
    
    while(1)
    {
        if(xSemaphoreTake( binary_semaphore_acc_setting, 10) == pdPASS )
        {
            user_acc_set_sensor_performance();
        }
        pt_num = drv_kx022_read_cached_sample((uint8_t *)xyz_buf,sizeof(xyz_buf),5*1000);
        if(pt_num > 0)
        {
            user_acc_save_sample((acc_origin_point_t *)xyz_buf,pt_num);
        }   
    }
}
/**
* @function@name:    user_creat_acc_data_collect_task
* @description  :    Function for creat the acc sample task 
* @input        :    none
* @output       :    none
* @return       :    none
*/
void user_creat_acc_data_collect_task(void)
{
    BaseType_t ret;  
    //acc_queue = xQueueCreate( 256, sizeof(uint8_t) );
    binary_semaphore_acc_setting = xSemaphoreCreateBinary();       
      
    ret = xTaskCreate( user_acc_data_collect_task, "acc", 512, NULL, 1, &acc_collect_handle );
    if(pdPASS != ret)
    {
        APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
    }      
    //xQueueReset(acc_queue);
    //DBG_LOG("\r\nacc_collect_handle = 0x%x\r\n\0",acc_collect_handle);
}
/**
* @function@name:    user_acc_send_queue_to_task
* @description  :    Function for send a queue to task, the type @ref acc_task_msg_type 
* @input        :    none
* @output       :    none
* @return       :    none
*/
/*
void user_acc_send_queue_to_task(uint8_t *p_buf,uint8_t len)
{
    for(uint8_t i=0;i<len;i++)
    {
        while(xQueueSendToBack(acc_queue, p_buf, 10) != pdPASS); 
    }
}
*/
void user_acc_set(void)
{   
    static portBASE_TYPE xHigherPriorityTaskWoken;
    xHigherPriorityTaskWoken = pdFALSE;
    //uint8_t type = ACC_TASK_MSG_TYPE_SET;
    
    if(binary_semaphore_acc_setting == NULL)
    {
        binary_semaphore_acc_setting = xSemaphoreCreateBinary(); 
    }
    DBG_LOG("\r\ngive acc semaphore...");
    //xSemaphoreGive(binary_semaphore_acc_setting);
    xSemaphoreGiveFromISR(binary_semaphore_acc_setting,&xHigherPriorityTaskWoken);
    DBG_LOG("ok\r\n");
    
}
void user_acc_trans_data(uint8_t *p_buf, uint8_t len)
{
    uint8_t type = ACC_TASK_MSG_TYPE_COLLECT;
    //user_acc_send_queue_to_task(&type,1);
    while(xQueueSendToBack(acc_queue, &type, 10) != pdPASS); 
    while(xQueueSendToBack(acc_queue, &len, 10) != pdPASS); 
    for(uint8_t i=0;i<len;i++)
    {
        while(xQueueSendToBack(acc_queue, p_buf, 10) != pdPASS); 
    }
}
//end
