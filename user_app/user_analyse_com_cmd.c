/** 
 * user_analyse_commu.c
 *
 * @group:       collar project
 * @author:      Chenfei
 * @create time: 2018/4/23
 * @version:     V0.0
 *
 */

#include "freertos_platform.h"
#include "user_ble_inter_mgt.h"
#include "user_app_origin.h"
#include "user_app_bhv.h"
#include "user_app_log.h"
#include "user_app_register.h"
#include "crc16.h"
#include "user_analyse_commu_cmd.h"
#include "user_data_pkg.h"
#include "Define.pb.h"
#include "user_app_register.h"
#include "user_app_origin.h"
#include    "user_app_set.h"
#include "user_app_env.h"
#include "user_app_estrus.h"
#include "user_app_warning.h"

static      QueueHandle_t       queue_inst_rsp;
static      TaskHandle_t        instruct_handle; 
/*
static const   p_app_rsp_parse  app_rsp_parse_handle[] = {
    NULL,
    NULL,
    NULL,
    app_register_rsp_parse,     //PROTOCOL_HEADER_TYPE_TYPE_REGISTER_RSP = 3,
    NULL,
    //PROTOCOL_HEADER_TYPE_TYPE_PARAMETER_RSP = 5,
    NULL,
    app_setting_rsp_parse_handle,      //PROTOCOL_HEADER_TYPE_TYPE_SETTING_RSP = 7,
    NULL,
    //PROTOCOL_HEADER_TYPE_TYPE_ENV_RSP = 9,
    NULL,
    //PROTOCOL_HEADER_TYPE_TYPE_BEHAVIOR_RSP = 11,
    NULL,
    //PROTOCOL_HEADER_TYPE_TYPE_G_P_S_RSP = 13,
    NULL,
    //PROTOCOL_HEADER_TYPE_TYPE_CELLULAR_RSP = 15,
    NULL,
    //PROTOCOL_HEADER_TYPE_TYPEWARNING_RSP = 17,
    NULL,
    //PROTOCOL_HEADER_TYPE_TYPE_DOWNLOAD_RSP = 19,
    NULL,
    //PROTOCOL_HEADER_TYPE_TYPE_UPLOAD_RSP = 21,
    NULL,
    //PROTOCOL_HEADER_TYPE_TYPE_SETTING_AND_PARAM_RSP = 23,
    //PROTOCOL_HEADER_TYPE_TYPE_S_M_S_MSG = 24,
    //PROTOCOL_HEADER_TYPE_TYPE_S_M_S_RPLY = 25,
    NULL,
    //PROTOCOL_HEADER_TYPE_TYPE_BEHAVIOR2_RSP = 27,
    NULL,
    //PROTOCOL_HEADER_TYPE_TYPE_ESTRUS_RSP = 29,
    NULL,
    //PROTOCOL_HEADER_TYPE_TYPE_ORIGIN_RSP = 31,
    NULL,
    //PROTOCOL_HEADER_TYPE_TYPE_STATUS_RSP = 33,
    NULL,
    //PROTOCOL_HEADER_TYPE_TYPE_DEBUG_RSP = 1003,
    //PROTOCOL_HEADER_TYPE_TYPE_SENSOR_RSP = 1005,
    //PROTOCOL_HEADER_TYPE_TYPE_PING_RSP = 1007,
    //PROTOCOL_HEADER_TYPE_TYPE_APP_RSP = 1009
    NULL,//app_param_rsp_parse,    
};
*/
/*
static uint16_t general_frame_construct_frame(uint8_t *p_dest,const pkg_prot_head_t *p_head, const uint8_t *p_data)
{
    if(!p_dest || !p_head || !p_data)
    {
        DBG_LOG("NULL Pointer");
    }
    pkg_prot_head_t *p_dest_head = (pkg_prot_head_t*)p_dest;
    *p_dest_head = *p_head;
    uint16_t crc16 = crc16_compute(p_dest, sizeof(pkg_prot_head_t) - sizeof(p_dest_head->crc16), NULL);
    crc16 = crc16_compute(p_data,p_head->frame_len,&crc16);
    p_dest_head->crc16 = crc16;
    memcpy(p_dest + sizeof(pkg_prot_head_t), p_data, p_head->frame_len);
    return sizeof(pkg_prot_head_t) + p_head->frame_len;
}


static uint16_t general_frame_get_frame_info(uint8_t *p_data,pkg_prot_head_t *p_head, const uint8_t * p_frame)
{
    pkg_prot_head_t *p_src_head = (pkg_prot_head_t *)(p_frame);

    uint16_t crc16 = 0;
    crc16 = crc16_compute(p_frame,sizeof(pkg_prot_head_t) - sizeof(p_src_head->crc16),NULL);
    
    crc16 = crc16_compute(p_frame + sizeof(pkg_prot_head_t),p_src_head->frame_len,&crc16);

    if(p_src_head->crc16 == crc16)
    {
        *p_head = *p_src_head;
        memcpy(p_data,p_frame + sizeof(pkg_prot_head_t),p_src_head->frame_len);
        return p_src_head->frame_len;
    }
    return 0;
}
*/
static uint32_t user_ble_update_combine_new_frame(uint8_t **p_dst_final, 
                                           uint8_t const *p_src_new_frame, 
                                           uint8_t frame_len)
{
    if(!*p_dst_final || !p_src_new_frame)
    {
        DBG_LOG("NULL Pointer\r\n");
        return 0;
    }
    memcpy(*p_dst_final,p_src_new_frame,frame_len);
    *p_dst_final += frame_len;
    return  frame_len;
}
static uint32_t user_ble_get_valid_msg(uint8_t *p_dst, uint8_t const *p_src, uint32_t length)
{
    if(!p_dst|| !p_src)
    {
        DBG_LOG("\r\np_dest or p_data is NULL\r\n");
        return 0;
    }
    
    pkg_final_frame_head_t *ble_pkg_head = (pkg_final_frame_head_t *) p_src;
    if(length < (sizeof(pkg_final_frame_head_t)+ble_pkg_head->data_len))
    {
        DBG_LOG("\r\ncommand length is not enough\r\n");
        return 0;
    }
    for(uint32_t i=0;i<length;i++)
    {
        DBG_LOG("%x ",p_src[i]);
    }
    //ble_data_trans_head_t *p_head = (ble_data_trans_head_t *)p_frame;
    uint16_t *p_crc_trans = (uint16_t *)((void *)(p_src + sizeof(pkg_final_frame_head_t) + ble_pkg_head->data_len));
    uint16_t crc_cal = crc16_compute(p_src, sizeof(pkg_final_frame_head_t) + ble_pkg_head->data_len,NULL);
    if( crc_cal != *p_crc_trans )
    {
        DBG_LOG("\r\nCrc is wrong, given crc = 0x%x , calcrc = 0x%x \r\n",*p_crc_trans,crc_cal);
        DBG_LOG("\r\nCrc caculate len = %d \r\n",sizeof(pkg_final_frame_head_t) + ble_pkg_head->data_len);
        return 0;
    }
    memcpy(p_dst, p_src + sizeof(pkg_final_frame_head_t),ble_pkg_head->data_len);
    return ble_pkg_head->data_len;
}
static uint16_t user_ble_combine_split_frame(uint8_t *p_dest_frame, const uint8_t *p_split_frame, int16_t split_len)
{
    if(split_len == 0)
    {
        DBG_LOG("ble length == 0\r\n");
        return 0;
    }
    if(!p_split_frame || !p_dest_frame)
    {
        DBG_LOG("NULL Pointer\r\n");
        return 0;
    }
    int16_t frame_len = 0;
    split_head_t *p_split_head = (split_head_t *)(p_split_frame);
    static construct_mode_t current_construct_mode = CONSTRUCT_MODE_INVALID;
    //static uint8_t  data_buf[255];
    static uint8_t  *p_current_pos = NULL;

    
    DBG_LOG("type is: %d\r\n", p_split_head->type);

    switch(p_split_head->type)
    {
        case SPLIT_FRAME_TYPE_MIDDLE:
        {
            DBG_LOG("this is middle frame type\r\n");
            if(current_construct_mode == CONSTRUCT_MODE_CONSTRUCTING)
            {
                // calucate current positon to story
                // copy data to dest
                user_ble_update_combine_new_frame(&p_current_pos,
                                                  p_split_frame + sizeof(split_head_t),
                                                  split_len - sizeof(split_head_t)
                                                 );
                DBG_LOG("middle frame len is:%d\r\n",p_current_pos - p_dest_frame );
            }
        }
            break;
        case SPLIT_FRAME_TYPE_START:
        {
            DBG_LOG("this is first frame type\r\n");
            current_construct_mode = CONSTRUCT_MODE_CONSTRUCTING;
            p_current_pos = p_dest_frame;
            user_ble_update_combine_new_frame(&p_current_pos,
                                                  p_split_frame + sizeof(split_head_t),
                                                  split_len - sizeof(split_head_t)
                                                 );            
            DBG_LOG("first frame len is:%d\r\n", p_current_pos - p_dest_frame);
            // update destination
            break;
        }
        
        case SPLIT_FRAME_TYPE_LAST:
        {
            DBG_LOG("this is last frame type\r\n");
            // calucate current positon to story
            // copy data to dest
            user_ble_update_combine_new_frame(&p_current_pos,
                                                  p_split_frame + sizeof(split_head_t),
                                                  split_len - sizeof(split_head_t)
                                                 );
            current_construct_mode = CONSTRUCT_MODE_CONSTRUCTED;                            
            frame_len = p_current_pos - p_dest_frame;
            DBG_LOG("lase frame len is:%d\r\n", frame_len);
        }
            break;
        case SPLIT_FRAME_TYPE_TOTAL:
        {
            DBG_LOG("this is total frame type\r\n");
            p_current_pos = p_dest_frame;
            memcpy(p_current_pos, p_split_frame + sizeof(split_head_t), split_len - sizeof(split_head_t));
            current_construct_mode = CONSTRUCT_MODE_CONSTRUCTED;
            frame_len = split_len - sizeof(split_head_t);
            DBG_LOG("total frame len is:%d\r\n", frame_len);
            break;
        }
        default:
            break;
    }

    //if(current_construct_mode == CONSTRUCT_MODE_CONSTRUCTED)
    {
        //return user_ble_get_valid_msg(p_dest_frame,data_buf,frame_len);
    }
    return frame_len;
}

static bool user_parse_instruction(void * const p_msg, int32_t const inst_len)
{
    if(p_msg == NULL || inst_len <= 0)
    {
        return 0;
    }
    pkg_prot_head_t     *p_prot_head_t = p_msg;
    uint8_t             *p_prot_frame_t;
    bool ret ;
    //point to proto buffer data
    p_prot_frame_t = (uint8_t *)p_msg + sizeof(pkg_prot_head_t);
    DBG_LOG("\r\ncommand type = %d\r\n",p_prot_head_t->cmd_type);
    switch(p_prot_head_t->cmd_type)
    {
        case PROTOCOL_HEADER_TYPE_TYPE_REGISTER_RSP:
            ret = app_register_rsp_parse(p_prot_frame_t,p_prot_head_t->frame_len);
            break;
        case PROTOCOL_HEADER_TYPE_TYPE_PARAMETER_RSP:
            
            break;
        case PROTOCOL_HEADER_TYPE_TYPE_SETTING_RSP:
            ret = app_setting_rsp_parse_handle(p_prot_frame_t,p_prot_head_t->frame_len);
            break;
        case PROTOCOL_HEADER_TYPE_TYPE_ENV_RSP:
            ret = app_env_rsp_parse_handle(p_prot_frame_t,p_prot_head_t->frame_len);
            break;
        case PROTOCOL_HEADER_TYPE_TYPE_BEHAVIOR_RSP:
            
            break;
        case PROTOCOL_HEADER_TYPE_TYPE_G_P_S_RSP:
            break;
        case PROTOCOL_HEADER_TYPE_TYPE_CELLULAR_RSP:
            break;
        case PROTOCOL_HEADER_TYPE_TYPE_WARNING_RSP:
            ret = app_warning_rsp_parse_handle(p_prot_frame_t,p_prot_head_t->frame_len);
            break;
        case PROTOCOL_HEADER_TYPE_TYPE_DOWNLOAD_RSP:
            break;
        case PROTOCOL_HEADER_TYPE_TYPE_UPLOAD_RSP:
            break;
        case PROTOCOL_HEADER_TYPE_TYPE_SETTING_AND_PARAM_RSP:
            break;
        case PROTOCOL_HEADER_TYPE_TYPE_S_M_S_RPLY:
            break;
        case PROTOCOL_HEADER_TYPE_TYPE_BEHAVIOR2_RSP:
            ret = app_bhv_rsp_parse_handle(p_prot_frame_t,p_prot_head_t->frame_len);
            break;
        case PROTOCOL_HEADER_TYPE_TYPE_ESTRUS_RSP:
            ret = app_estrus_rsp_parse_handle(p_prot_frame_t,p_prot_head_t->frame_len);
            break;
        case PROTOCOL_HEADER_TYPE_TYPE_ORIGIN_RSP:
            ret = app_origin_rsp_parse_handle(p_prot_frame_t,p_prot_head_t->frame_len);
            break;
        case PROTOCOL_HEADER_TYPE_TYPE_STATUS_RSP:
            break;
        default: 
            break;
    }
    DBG_LOG("instruction parse complete\r\n");
    return ret;
}
/**
* @function@name:    user_parse_instruction_rsp_task
* @description  :    function for parse received command or response
*               :    message queue struct:
*               :    ************************************************************************************************
*               :    queue: | channle-type filed(1 byte) | msg-length filed(1byte) | datas filed (length byte) |
*               :    ************************************************************************************************
* @input        :    
*               :    record_len: want to read record length, if not enough on memory , return all remained on memory
* @output       :    p_record_t: record buffer
* @return       :    actual records length have read
*/
static void user_parse_instruction_rsp_task(void *arg)
{
      //valid is 247-3
    vTaskDelay( 5000 );
    int32_t     temp_len = 0;
    uint8_t     rx_len_cnt = 0;
    uint8_t     rx_give_len = 0;
    uint8_t     rec_temp;
    static uint8_t     rx_buf[256];
    //static uint8_t     inst_msg[256];
    static uint8_t     rsp_data[256];
    //static queue_msg_t        msg_buf;
    uint8_t     trans_type;
    //xQueueReceive(queue_inst_rsp, &rec_temp , 200);
    xQueueReset(queue_inst_rsp);
    while(1)
    {                
        rx_len_cnt = 0;
        rx_give_len = 0;
        temp_len = 0;
        trans_type = USER_MSG_CHANNLE_MAX;
        //memset(&msg_buf,0,sizeof(msg_buf));
        //memset(inst_msg,0,sizeof(inst_msg));
        //memset(rsp_data,0,sizeof(rsp_data));
        //xQueueReset(queue_inst_rsp);
        //DBG_LOG("\r\ninstruct_handle = 0x%x\r\n\0",instruct_handle);
        //do
        //{
            rx_len_cnt = 0;
            //receive channle type
            if(xQueueReceive(queue_inst_rsp, &trans_type , portMAX_DELAY) == pdPASS)
            {
                //rsp_buf[len++] = rec_temp;
                DBG_LOG("\r\ninst parse: channel type = %d: %s\r\n",trans_type,trans_type == USER_MSG_CHANNLE_NB? "USER_MSG_CHANNLE_NB":"USER_MSG_CHANNLE_BLE");
            }   
            //vTaskDelay(200);
            //receive give length of message
            xQueueReceive(queue_inst_rsp, &rx_give_len , 0);
            DBG_LOG("\r\ninst parse: given len = %d\r\n",rx_give_len);
            if(rx_give_len > 0)
            {
                while(xQueueReceive(queue_inst_rsp, &rec_temp , 0) != errQUEUE_EMPTY)
                {
                    rx_buf[rx_len_cnt++] = rec_temp;
                    if(rx_len_cnt >= rx_give_len)
                    {
                        //
                        break;
                    }
                }     
                // add validity judge  later/////////
                //where the data is valid or not, accroding to the give length and received length
                DBG_LOG("\r\ncount len = %d\r\n",rx_len_cnt);
                if(rx_give_len == rx_len_cnt)
                {           
                    #if 1
                    for(uint8_t i=0;i<rx_len_cnt;i++)
                    {
                        DBG_LOG("%x ",rx_buf[i]);
                    }
                    #endif
                    //temp_len = rx_len_cnt;
                    switch(trans_type)
                    {
                        case USER_MSG_CHANNLE_BLE:   
                            DBG_LOG("\r\nparse ble instruction\r\n");
                            temp_len = user_ble_combine_split_frame(rsp_data, rx_buf, rx_len_cnt);
                            if(temp_len > 0)
                            {
                                memset(rx_buf,0,sizeof(rx_buf)); //re-use the ram space 
                                temp_len = user_ble_get_valid_msg(rx_buf,rsp_data,temp_len);
                                if(temp_len > 0)
                                {
                                    user_parse_instruction(rx_buf,temp_len);
                                    //memset(inst_msg,0,sizeof(inst_msg));                                    
                                }                                
                                memset(rsp_data,0,sizeof(rsp_data));
                                rx_len_cnt = 0;
                            }
                            break;
                        case USER_MSG_CHANNLE_NB:   
                                DBG_LOG("\r\nparse NB instruction,send to distribute parse\r\n");                      
                                user_parse_instruction(rx_buf,rx_len_cnt);
                                //memset(inst_msg,0,sizeof(inst_msg));
                                memset(rsp_data,0,sizeof(rsp_data));
                        break;
                            
                        default:break;
                    }
                }
                else
                {
                    xQueueReset(queue_inst_rsp);
                    DBG_LOG("\r\ncount len != given len,count len = %d, give len = %d\r\n", rx_len_cnt,rx_give_len);
                }
            }
            else
            {
                xQueueReset(queue_inst_rsp);
                DBG_LOG("\r\nrx_give_len = 0\r\n");
            }
            //user_parse_instruction(rsp_buf,len);
        //}while(trans_type == USER_MSG_CHANNLE_BLE && rx_len_cnt && rx_give_len);
    }
}
/*
static void user_parse_instruction_rsp_task(void *arg)
{
      //valid is 247-3
    vTaskDelay( 5000 );
    int32_t     temp_len = 0;
    uint32_t    rx_len_cnt = 0;
    uint8_t    rx_give_len = 0;
    uint8_t     rec_temp;
    static uint8_t     rx_buf[256];
    static uint8_t     inst_msg[256];
    static uint8_t     rsp_data[256];
    uint8_t     trans_type;
    xQueueReceive(queue_inst_rsp, &rec_temp , 200);
    while(1)
    {                
        rx_len_cnt = 0;
        rx_give_len = 0;
        temp_len = 0;
        trans_type = USER_MSG_CHANNLE_MAX;
        memset(rx_buf,0,sizeof(rx_buf));
        memset(inst_msg,0,sizeof(inst_msg));
        memset(rsp_data,0,sizeof(rsp_data));
        //receive channle type
        if(xQueueReceive(queue_inst_rsp, &trans_type , portMAX_DELAY) == pdPASS)
        {
            //rsp_buf[len++] = rec_temp;
            DBG_LOG("\r\ninst parse: channel type = %s\r\n",trans_type == USER_MSG_CHANNLE_NB? "USER_MSG_CHANNLE_NB":"USER_MSG_CHANNLE_BLE");
        }
        vTaskDelay(200);
        //receive give length of message
        xQueueReceive(queue_inst_rsp, &rx_give_len , portMAX_DELAY);
        DBG_LOG("\r\ninst parse: given len = %d\r\n",rx_give_len);
        while(xQueueReceive(queue_inst_rsp, &rec_temp , 0) != errQUEUE_EMPTY)
        {
                rx_buf[rx_len_cnt++] = rec_temp;
        }     
        // add validity judge  later/////////
        //where the data is valid or not, accroding to the give length and received length
        DBG_LOG("\r\ncount len = %d\r\n",rx_len_cnt);
        if(rx_give_len == rx_len_cnt)
        {           
            //temp_len = rx_len_cnt;
            switch(trans_type)
            {
                case USER_MSG_CHANNLE_BLE:   
                    DBG_LOG("\r\nparse ble instruction\r\n");
                    temp_len = user_ble_combine_split_frame(rsp_data, rx_buf, rx_len_cnt);
                    if(temp_len > 0)
                    {
                        temp_len = user_ble_get_valid_msg(inst_msg,rsp_data,temp_len);
                        if(temp_len > 0)
                        {
                            user_parse_instruction(inst_msg,temp_len);
                        }
                    }
                    break;
                case USER_MSG_CHANNLE_NB:   
                        DBG_LOG("\r\nparse NB instruction,send to distribute parse\r\n");                      
                        user_parse_instruction(rx_buf,rx_len_cnt);
                break;
                default:break;
            }
        }
        else
        {
            DBG_LOG("\r\ncount len != given len\r\n");
        }
        //user_parse_instruction(rsp_buf,len);
    }
}
*/
/**
* @function@name:    user_creat_inst_parse_task
* @description  :    Function for creating a task for instruction parse
* @input        :    none:                     
* @output       :    none
* @return       :    none
*/
void user_creat_inst_parse_task(void)
{
    //queue_inst_rsp = xQueueCreate( 260, sizeof(uint8_t) );
    queue_inst_rsp = xQueueCreate( 260, sizeof(uint8_t) );
    BaseType_t ret;    
    ret = xTaskCreate( user_parse_instruction_rsp_task, "pars", 512, NULL, 1, &instruct_handle );
    if(pdPASS != ret)
    {
        APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
    }  
    //DBG_LOG("\r\ninstruct_handle = 0x%x\r\n\0",instruct_handle);
}
void user_parse_send_queue(uint8_t * const p_buf, uint8_t const len, uint8_t const msg_chn)
{
    if(p_buf == NULL || len <= 0)
    {
        DBG_LOG("\r\ninstruction response queue Pointer is NULL\r\n");
    }
    //DBG_LOG("\r\nsend instruction channle 0x%x : %d, queue addr = 0x%x \r\n",&msg_chn, msg_chn, &queue_inst_rsp);
    while(xQueueSendToBack(queue_inst_rsp, &msg_chn, 10) != pdPASS);
    //DBG_LOG("\r\nsend instruction length = %d\r\n", len);
    while(xQueueSendToBack(queue_inst_rsp, &len, 10) != pdPASS);    //1bytes
    //DBG_LOG("\r\nsend instruction content\r\n");
    for(uint8_t i=0;i<len;i++)
    {
        while(xQueueSendToBack(queue_inst_rsp, &p_buf[i], 10) != pdPASS);
    }
}
//end

