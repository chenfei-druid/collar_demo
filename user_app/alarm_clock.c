/*
 * @Author: jiaqi.chen 
 * @Date: 2018-04-12 18:00:25 
 * @Last Modified by: jiaqi.chen
 * @Last Modified time: 2018-04-17 14:33:02
 */

#include "alarm_clock.h"
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "nrf_drv_rtc.h"
#include "nrf_rtc.h"
//#include "user_rtc.h"
#include "nrf_log.h"
#include "time.h"
#include "user_app_log.h"

#define ALARM_CLOCK_RTC 2
/**
 * @brief RTC compare channel used
 *
 */
#define ALARM_CLOCK_RTC_CC 0

#define ALARM_CLOCK_MAX_COUNT 50

#define RAM_TIME_STAMP_ADDR 0x2000fff0
#define RAM_CHECK_SUM_ADDR 0x2000fff4

/**
 * @brief Number of RTC ticks between interrupts
 */
#define RTC_TIME_CONFIG_FREQUENCY 8

#define ALARM_CLOCK_RTC_TICKS   (RTC_US_TO_TICKS(1000000ULL, RTC_TIME_CONFIG_FREQUENCY))
#define ALARM_CLOCK_RTC_OVERFLOW_VALUE (((0xffffffff & RTC_COUNTER_COUNTER_Msk) + 1) / RTC_TIME_CONFIG_FREQUENCY)

#define ACTIVATE_ALARM(p)              \
    do                                 \
    {                                  \
        p->status = STATUS_ACTIVATE;   \
        control_info.activate_count++; \
    } while (0);

#define IDLE_ALARM(p)                      \
    do                                     \
    {                                      \
            p->status = STATUS_IDLE;       \
            control_info.activate_count--; \
            control_info.idle_count++;     \
    } while (0);

#define DISTROY_ALARM(p)                   \
    do                                     \
    {                                      \
        if (p->status == STATUS_ACTIVATE)  \
        {                                  \
            control_info.activate_count--; \
        }                                  \
        else if (p->status == STATUS_IDLE) \
        {                                  \
            control_info.idle_count--;     \
        }                                  \
        else                               \
        {                                  \
        }                                  \
        control_info.registered_count --;  \
        p->status = STATUS_DISTROYED;      \
    } while (0);

/*-----------Defines-------------------------------------------------------------*/
/*-----------Privates-------------------------------------------------------------*/
typedef enum
{
    STATUS_DISTROYED = 0,
    STATUS_ACTIVATE  ,  // ready
    STATUS_RUNNING,     // running
    STATUS_IDLE         // not enabled
}alarm_clock_status_t;




typedef struct
{
    alarm_clock_config_t config; // config of alrm
    alarm_clock_status_t status; // status of alarm
}control_block_t;

typedef struct 
{
    uint8_t activate_count;  // current activate num of alarms
    uint8_t idle_count;      // current idle num of alarms
    uint8_t registered_count;     //current  registerd num if alarms
    uint8_t malloc_count;     // malloced num of registerd alarms 
    control_block_t *handles[ALARM_CLOCK_MAX_COUNT]; //this part can be changed to linklist, if free() is supported
}registered_alarms_info_t;  

static registered_alarms_info_t control_info; //  store all the registered alarms

/**--------------------Hareware related part--------------------------------------*/
/**
 * @brief Relationship of compared register and  bock
 * @note  other planform , rtc_channel can be changed to compared register address
 */
typedef struct
{
    const uint8_t rtc_channel; // the channel that usered to compare, compared register
    control_block_t * control_block; //the related control block, userted to get information,when the event come
}scheduler_item_t;

#if 0
typedef struct
{
    uint32_t next_run_tics;
    control_block_t *control_block;
}selector_item_t;
#endif

/**
 * @brief usered to sehedule clock events
 * @ compare register 0 is usered for timestamp
 * @ Note this part is related to plantform
 */
#if 0
static scheduler_item_t schedule_list[] = 
{
    // compare register 0 is usered for timestamp
    {1,NULL} // compare register 1,
}; // store the relationship of compared register and control block 
#endif

static control_block_t *running_list[ALARM_CLOCK_MAX_COUNT] = {0,};

#if 0
static selector_item_t selector[] = 
{
    {0,NULL},
    {0,NULL},
    {0,NULL}
};
#endif



/*-----------Local val-------------------------------------------------------------*/
static nrf_drv_rtc_t const m_rtc = NRF_DRV_RTC_INSTANCE(ALARM_CLOCK_RTC);

static uint32_t time_stamp = 0;

static nrf_drv_rtc_config_t  m_rtc_config = 
{                                                                                                
    .prescaler          = RTC_FREQ_TO_PRESCALER(RTC_TIME_CONFIG_FREQUENCY),                   
    .interrupt_priority = RTC_DEFAULT_CONFIG_IRQ_PRIORITY,                                       
    .reliable           = RTC_DEFAULT_CONFIG_RELIABLE,                                           
    .tick_latency       = RTC_US_TO_TICKS(NRF_MAXIMUM_LATENCY_US, RTC_TIME_CONFIG_FREQUENCY),
};

#define NEXT_PERID(current, cycle) ((cycle) - ((current) % (cycle)) + (current))
#define NEXT_MINUTE(current) NEXT_PERID((current), 60)
#define NEXT_HOUR(current) NEXT_PERID((current), 3600)
#define NEXT_DAY(current) NEXT_PERID((current), 86400)
#define NEXT_WEEK(current) (NEXT_PERID((current), 604800) - 3 * 86400)
#define NEXT_MONTH(current) (next_month((current)))
#define NEXT_YEAR(current) (next_year((current)))

/*
#define DIFF_PERID(current, cycle) ((cycle) - ((current) % (cycle)))
#define DIFF_MINUTE(current) DIFF_PERID((current), 60)
#define DIFF_HOUR(current) DIFF_PERID((current), 3600)
#define DIFF_DAY(current) DIFF_PERID((current), 86400)
#define DIFF_WEEK(current) (DIFF_PERID((current), 604800) - 3 * 86400)
#define DIFF_MONTH(current) ((next_month((current))) - current)
#define DIFF_YEAR(current) ((next_year((current))) - current)
*/

#define DIFF_PERID(current,pos,circle) ( ( (pos) - ( (current) % (circle) ) + (circle) ) % (circle) )
#define DIFF_MINUTE(current,pos) DIFF_PERID((current),pos, 60)
#define DIFF_HOUR(current,pos) DIFF_PERID((current),pos,3600)
#define DIFF_DAY(current, pos) DIFF_PERID((current),pos, 86400)
#define DIFF_DAY(current, pos) DIFF_PERID((current),pos, 86400)
#define DIFF_WEEK(current,pos) (DIFF_PERID((current) +  3 * 86400,pos, 604800))
#define DIFF_MONTH(current) ((next_month((current))) - current)
#define DIFF_YEAR(current) ((next_year((current))) - current)

 
uint32_t next_month(uint32_t time_stamp)
{
    struct tm *p;
    time_t t = time_stamp;
    p = gmtime(&t);
    //NRF_LOG_INFO("%d-%d-%d %d:%d:%d", p->tm_year ,p->tm_mon + 1,p->tm_mday,p->tm_hour,p->tm_min,p->tm_sec); 
    //NRF_LOG_INFO("%d-%d", p->tm_wday, p->tm_yday); 

    struct tm next_month = *p;

    next_month.tm_mon  = next_month.tm_mon + 1;
    next_month.tm_mday = 1;
    next_month.tm_hour = 0;
    next_month.tm_min = 0;
    next_month.tm_sec = 0;

    t = mktime(&next_month);
    //NRF_LOG_INFO("Next month %d", t);

    return t;
}

static void load_time_stamp_from_ram()
{
    uint32_t *p_time_stamp= (void *)RAM_TIME_STAMP_ADDR;
    uint32_t *p_checksum= (void *)RAM_CHECK_SUM_ADDR;

    if(!((*p_checksum) & (*p_time_stamp)))
    {
        time_stamp = *p_time_stamp;
        //alarm_clock_set_time_stamp(*p_time_stamp);
        //upload_config();
    }
}


uint32_t next_year(uint32_t time_stamp)
{
    struct tm *p;
    time_t t = time_stamp;
    p = gmtime(&t);

    struct tm next_year = *p;

    next_year.tm_year ++;
    next_year.tm_mon = 0;
    next_year.tm_mday = 1;
    next_year.tm_hour = 0;
    next_year.tm_min = 0;
    next_year.tm_sec = 0;

    t = mktime(&next_year);
    //NRF_LOG_INFO("Next year %d", t);
    return t;
}

/*---------------Private Functions-------------------------------------------*/
uint32_t calculate_diff_ticks(const control_block_t *p_ctrl)
{
    //uint32_t future_time = 0;
    uint32_t diff = 0;
    switch(p_ctrl->config.mode)
    {
        case ALARM_TYPE_RUN_EVERY_MINUTE:
            //future_time = NEXT_MINUTE(time_stamp) + p_ctrl->config.time_stamp;
            diff = DIFF_MINUTE(time_stamp ,p_ctrl->config.time_stamp);
            if(diff == 0)
            {
                diff = 60;
            }
            //DBG_LOG("%d-%d",diff, future_time - time_stamp);
            
            break;
        case ALARM_TYPE_RUN_EVERY_HOUR:
            //future_time = NEXT_HOUR(time_stamp) + p_ctrl->config.time_stamp;
            diff = DIFF_HOUR(time_stamp ,p_ctrl->config.time_stamp);
            if(diff == 0)
            {
                diff = 3600;
            }
            //DBG_LOG("%d-%d",diff, future_time - time_stamp);

            break;
        case ALARM_TYPE_RUN_EVERY_DAY:
            //future_time = NEXT_DAY(time_stamp)+ p_ctrl->config.time_stamp;
            diff = DIFF_DAY(time_stamp, p_ctrl->config.time_stamp);
            //DBG_LOG("%d-%d",diff, future_time - time_stamp);
            break;

        case ALARM_TYPE_RUN_EVERY_WEAK:
            //future_time = NEXT_WEEK(time_stamp)+ p_ctrl->config.time_stamp;
            diff = DIFF_WEEK(time_stamp ,p_ctrl->config.time_stamp);
            //DBG_LOG("%d-%d",diff, future_time - time_stamp);

            break;
        case ALARM_TYPE_RUN_EVERY_MONTH:
            //future_time = NEXT_MONTH(time_stamp)+ p_ctrl->config.time_stamp;
            diff = DIFF_MONTH(time_stamp) + p_ctrl->config.time_stamp;
            //DBG_LOG("%d-%d",diff, future_time - time_stamp);

            break;
        case ALARM_TYPE_RUN_EVERY_YEAR:
            //future_time = NEXT_YEAR(time_stamp)+ p_ctrl->config.time_stamp;
            diff = DIFF_YEAR(time_stamp) + p_ctrl->config.time_stamp;
            //DBG_LOG("%d-%d",diff, future_time - time_stamp);

            break;
        case ALARM_TYPE_RUN_SIGLE:
            diff = (time_stamp >= p_ctrl->config.time_stamp)?0:(p_ctrl->config.time_stamp - time_stamp);
            //future_time = p_ctrl->config.time_stamp;

            break;
        default:
            //future_time = 0;

            break;
    }
    return diff * RTC_TIME_CONFIG_FREQUENCY  ;
}



static void upload_config()
{
    //DBG_LOG(__FUNCTION__);
    control_block_t *p_ctrl;

    control_block_t *p_select_ctrol = NULL;

    uint32_t min_ticks = 0xffffffff;
    //DBG_LOG("Start select ***");

    uint8_t running_count = 0;

    for(int i = 0; i < control_info.malloc_count; i++)
    {

        p_ctrl = control_info.handles[i];

        if(p_ctrl->status == STATUS_DISTROYED )
        {
            continue;
        }

        uint32_t ticks = calculate_diff_ticks(p_ctrl);
        //DBG_LOG("ALarm %s (%d) addr(%08x)",p_ctrl->config.iden,ticks,p_ctrl);

        if (ticks == 0 || (ticks + m_rtc.p_reg->COUNTER) > ALARM_CLOCK_RTC_OVERFLOW_VALUE)
        {
            continue;
        }

        if(p_select_ctrol == NULL)
        {
            p_select_ctrol = p_ctrl;
            min_ticks = ticks;
            //DBG_LOG("Select %s(%d)",p_ctrl->config.iden,ticks);
            running_list[running_count++] = p_select_ctrol;
            running_list[running_count] = NULL;
            continue;
        }

        if(ticks < min_ticks )
        {
            p_select_ctrol = p_ctrl;
            min_ticks = ticks;
            //DBG_LOG("Select %s(%d)",p_ctrl->config.iden,ticks);
            running_count = 0;
            running_list[running_count++] = p_select_ctrol;
            running_list[running_count] = NULL;
            continue;
        }

        if(ticks == min_ticks)
        {
            running_list[running_count++] = p_ctrl;
            running_list[running_count] = NULL;
        }
    }
    //DBG_LOG("End select *** %s", p_select_ctrol->config.iden);

    if(p_select_ctrol)
    {
        //schedule_list[0].control_block = p_select_ctrol;
        //p_select_ctrol->status = STATUS_RUNNING;
        nrf_drv_rtc_cc_set(&m_rtc,1,min_ticks + m_rtc.p_reg->COUNTER, true);
        for(int i = 0; running_list[i]; i++)
        {
            running_list[i]->status = STATUS_RUNNING;
        }
    }
}


/**
 * @brief 
 * 
 * @param int_type 
 */
static void user_rtc_handler(nrf_drv_rtc_int_type_t int_type)
{
    ret_code_t err_code;
    if(int_type == NRF_DRV_RTC_INT_COMPARE0)
    {
        //alarm_clock_print_date();
        //alarm_clock_set_time_stamp(time_stamp + 1);
        time_stamp++;
        uint32_t *p_time_stamp= (void *)RAM_TIME_STAMP_ADDR;
        uint32_t *p_checksum= (void *)RAM_CHECK_SUM_ADDR;
        *p_time_stamp = time_stamp;
        *p_checksum = ~(*p_time_stamp);


        err_code = nrf_drv_rtc_cc_set(
            &m_rtc,
            ALARM_CLOCK_RTC_CC,
            (nrf_rtc_cc_get(m_rtc.p_reg, ALARM_CLOCK_RTC_CC) + ALARM_CLOCK_RTC_TICKS) & RTC_COUNTER_COUNTER_Msk,
            true);
        //DBG_LOG("0:%d",time_stamp);
        APP_ERROR_CHECK(err_code);
    }
    else if(int_type == NRF_DRV_RTC_INT_COMPARE1)
    {
        //DBG_LOG("alarm %s come, %u",schedule_list[0].control_block->config.iden, time_stamp);
        /*
        if(schedule_list[0].control_block->config.callback)
        {
            schedule_list[0].control_block->config.callback(time_stamp,schedule_list[0].control_block->config.param);
        }
        */
        //schedule_list[0].control_block->status = STATUS_ACTIVATE;

        for(int i = 0; running_list[i]; i++)
        {
            control_block_t *p = running_list[i];
            if(p->config.callback)
            {
                p->config.callback(time_stamp, p->config.param);
            }
            p->status = STATUS_ACTIVATE;
        }

        upload_config();
    }
    else if (int_type == NRF_DRV_RTC_INT_COMPARE2)
    {
        /**This compare register is not usered*/
    }
    else if (int_type == NRF_DRV_RTC_INT_COMPARE3)
    {
        /**This compare register is not usered*/
    }
    else
    {
        DBG_LOG("\r\nOVERFLOW EVENT %d\r\n",int_type);
        upload_config();
    }
}

/**
 * @brief 
 * 
 * @return true 
 * @return false 
 */
bool alarm_clock_init()
{
    uint32_t err_code;
    err_code = nrf_drv_rtc_init(&m_rtc, &m_rtc_config, user_rtc_handler);
    APP_ERROR_CHECK(err_code);
    //err_code = nrf_drv_rtc_cc_set(&m_rtc, BLINK_RTC_CC ,BLINK_RTC_TICKS, true);

    err_code = nrf_drv_rtc_cc_set(&m_rtc, 0, ALARM_CLOCK_RTC, true);
/*
    err_code = nrf_drv_rtc_cc_set(&m_rtc, 1, ALARM_CLOCK_RTC, true);
    err_code = nrf_drv_rtc_cc_set(&m_rtc, 2, ALARM_CLOCK_RTC, true);
    err_code = nrf_drv_rtc_cc_set(&m_rtc, 3, ALARM_CLOCK_RTC, true);
*/
    APP_ERROR_CHECK(err_code);
    nrf_drv_rtc_enable(&m_rtc);
    load_time_stamp_from_ram();
    memset(&control_info, 0, sizeof(control_info));
    //time_stamp = 0;
    return true;
}

void alarm_clock_set_time_stamp(uint32_t t)
{
    if((t > 1533139200) && (t < 2147483647))           //2018/8/2 00:00:00 ~ 2038/1/19 11:14:7
    {
    time_stamp = t;
    uint32_t *p_time_stamp= (void *)RAM_TIME_STAMP_ADDR;
    uint32_t *p_checksum= (void *)RAM_CHECK_SUM_ADDR;
    *p_time_stamp = time_stamp;
    *p_checksum = ~(*p_time_stamp);
    //upload_config();
    }
}


/**
 * @brief 
 * 
 * @return uint32_t 
 */
uint32_t hal_rtc2_get_unix_time()
{
    return time_stamp;
}

/**
 * @brief 
 * 
 */
void alarm_clock_print_date()
{
    struct tm *p;
    time_t t = time_stamp;
    p = gmtime(&t);
    NRF_LOG_INFO("%d-%d-%d %d:%d:%d", p->tm_year + 1900,p->tm_mon + 1,p->tm_mday,p->tm_hour,p->tm_min,p->tm_sec); 
}

int alarm_clock_create_alarm(alarm_clock_config_t * p_config)
{
    for(int i = 0; i < ALARM_CLOCK_MAX_COUNT; i++)
    {
        if(control_info.handles[i] == NULL)
        {
            control_info.handles[i] = (control_block_t *)malloc(sizeof(control_block_t));
            if(control_info.handles[i])
            {
                //NRF_LOG_INFO("%08x,%d,%d", control_info.handles[i],sizeof(control_block_t), sizeof(alarm_clock_config_t));
                
                memcpy(&(control_info.handles[i]->config),p_config,sizeof(alarm_clock_config_t));
                control_info.handles[i]->status = STATUS_IDLE;
                control_info.registered_count++;
                control_info.idle_count++;
                control_info.malloc_count++;
                //DBG_LOG("Create:%s",control_info.handles[i]->config.iden);
                return i;
            }
            return -1;
        }

        if(control_info.handles[i]->status == STATUS_DISTROYED)
        {
            //DBG_LOG("Create:%s",control_info.handles[i]->config.iden);
            memcpy(&(control_info.handles[i]->config),p_config,sizeof(alarm_clock_config_t));
            control_info.handles[i]->status = STATUS_IDLE;
            control_info.registered_count++;
            control_info.idle_count++;
            return i;
        }
    }
    return -1;
}
/**/
bool alarm_clock_create_alarm_new(alarm_app_type_s type, alarm_clock_config_t * p_config)
{
//    uint8_t alarm_type = type;
    if(type >= ALARM_TYPE_MAX || p_config == NULL)
    {
        return false;
    }
    uint32_t len = sizeof(alarm_clock_config_t);
    control_info.handles[type] = (control_block_t *)malloc(sizeof(control_block_t));
    memcpy(&(control_info.handles[type]->config),p_config,len);
    control_info.handles[type]->status = STATUS_RUNNING;
    //control_info.registered_count++;
    //control_info.idle_count++;
    control_info.malloc_count++;
    //DBG_LOG("Create:%s",control_info.handles[i]->config.iden);
    upload_config();
    return true;
}

int alarm_clock_enable(alarm_clock_handle_t handler)
{
    if(handler >= ALARM_CLOCK_MAX_COUNT)
    {
        return -1;
    }

    control_block_t *p_ctrl = control_info.handles[handler];

    //DBG_LOG("Enable %s\r\n", p_ctrl->config.iden);
    
    if(p_ctrl->status == STATUS_ACTIVATE) // already activate 
    {
        return 0;
    }

    if(p_ctrl->status == STATUS_DISTROYED) // this block is already free
    {
        return -1;
    }
    ACTIVATE_ALARM(p_ctrl);

    upload_config();
    return 0;
}

int alarm_clock_disable(alarm_clock_handle_t handler)
{
    if(handler >= ALARM_CLOCK_MAX_COUNT)
    {
        return -1;
    }
    control_block_t *p_ctrl = control_info.handles[handler];

    if(p_ctrl->status == STATUS_IDLE)
    {
        return 0;
    }

    if(p_ctrl->status == STATUS_DISTROYED)
    {
        return -1;
    }
    IDLE_ALARM(p_ctrl);
    upload_config();
    return 0;
}

int alarm_clock_distroy(alarm_clock_handle_t handler)
{
    if (handler >= ALARM_CLOCK_MAX_COUNT)
    {
        return -1;
    }

    control_block_t *p_ctrl = control_info.handles[handler];

    if(p_ctrl->status == STATUS_DISTROYED)
    {
        return -1;
    }
    DISTROY_ALARM(p_ctrl);
    upload_config();
    return 0;
}

int alarm_clock_update_alarm(alarm_clock_handle_t handler,const alarm_clock_config_t * config)
{
    //DBG_LOG(__FUNCTION__);
    if (handler >= ALARM_CLOCK_MAX_COUNT)
    {
        return -1;
    }
    control_block_t *p_ctrl = NULL;
    p_ctrl = control_info.handles[handler];
    if(p_ctrl == NULL || p_ctrl->status == STATUS_DISTROYED)
    {
        return -1;
    }

    #if 1
    
    //DBG_LOG("[%d]p_ctrl %d---->config %d",handler , p_ctrl->config.time_stamp, config->time_stamp);
    p_ctrl->config.time_stamp = config->time_stamp;
    p_ctrl->config.mode = config->mode;
    p_ctrl->config.callback = config->callback;
    p_ctrl->config.param = config->param;
    #endif

    //memcpy(&(p_ctrl->config),config,sizeof(alarm_clock_config_t));
    p_ctrl->status = STATUS_ACTIVATE;
    upload_config();

    return 0;
}
