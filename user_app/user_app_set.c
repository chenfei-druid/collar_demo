
/** 
 * user_app_set.c
 *
 * @group:       collar project
 * @author:      Chenfei
 * @create time: 2018/5/4
 * @version:     V0.0
 *
 */
 /***********************************************includes**********************************************/
#include    <stdint.h>
#include "freertos_platform.h"
#include    "user_app_set.h"
#include    "user_app_estrus.h"
#include    "user_app_bhv.h"
#include    "user_app_env.h"
#include    "user_app_origin.h"
#include    "user_app_warning.h"
#include    "user_app_identity.h"
#include    "user_app_init.h"
//#include    "user_app_env.h"
#include    "Setting.pb.h"
#include    "SimpleRsp.pb.h"
#include    "SEGGER_RTT.h"
#include    "Define.pb.h"
#include    "user_pb_callback.h"
#include    "user_app_log.h"
#include    "hal_nrf_flash.h"
#include    "user_acc.h"
#include    "user_data_pkg.h"
#include    "user_send_data.h"
#include    "ble_application.h"
#include    "user_common_alg.h"

/***********************************************user defines**********************************************/
//static      TaskHandle_t        setting_handle;
//static      xSemaphoreHandle    semaphore_setting = NULL;

typedef struct
{
    app_identity_t      iden;
    env_setting_t       env_setting;
    bhv_setting_t       bhv_setting;   
    //gps_setting_t       gps_setting;
    nb_comm_setting_t   nb_comm_setting;    
    pwr_setting_t       pwr_setting;
    ota_setting_t       ota_setting;
    sms_setting_t       sms_setting;
    //alarm_setting_t     alarm_setting;  
    accorigin_setting_t accorigin_setting;
    estrus_setting_t    estrus_setting;
    int32_t             warning_mode;
    int32_t             reset_device;
    uint32_t            timestamp;
    warning_setting_t   warning_setting;
}user_app_setting_t;

//static  bool    setting_rsp_flag = 0;
static  xSemaphoreHandle            binary_semaphore_wait_response;

/***********************************************defined functions**********************************************/

bool user_save_settings(uint8_t * const p_buf, int32_t len)
{
    if(p_buf == NULL || len == 0)
    {
        DBG_LOG("user_save_settings pointer is null\r\n");
        return false;
    }
    uint8_t *p_dt = (uint8_t *)p_buf;
    if(user_nrf_flash_page_erase((uint32_t *)USER_INDEX_MEMORY_START,1) == false)
    {
        DBG_LOG("user_save_settings erase flash is failed\r\n");
        return false;
    }       
    //uint32_t save_setting_addr = USER_INDEX_MEMORY_START;
    if(user_save_datas_as_page(USER_INDEX_MEMORY_START, p_dt, sizeof(user_app_setting_t)) == false)
    {
        DBG_LOG("user_save_settings write data into flash failed\r\n");
        return false;
    }
    return true;
}
bool user_get_settings_from_flash(user_app_setting_t * const p_buf)
{
    if(p_buf == NULL)
    {
        DBG_LOG("user_get_settings pointer is null\r\n");
        return false;
    }
    if(user_nrf_flash_get_bytes((uint32_t *)USER_INDEX_MEMORY_START,(uint8_t *)p_buf,sizeof(user_app_setting_t)) != NRF_SUCCESS)
    {
        DBG_LOG("user_get_settings read flash  failed\r\n");
        return false;
    }
    return true;
}
bool user_setting_update_timestamp(uint32_t timestamp)
{
    user_app_setting_t     dev_param_t;

    user_get_settings_from_flash(&dev_param_t);
    
    dev_param_t.timestamp = timestamp;
    
    if(user_save_settings((uint8_t *)&dev_param_t, sizeof(user_app_setting_t)) != true)
    {
        DBG_LOG("delete origin setting failed\r\n");
        return false;
    }
    return true;
}
/**
* @functionname : user_delete_setting_origin 
* @description  : function for delete origin setting
* @input        : none
* @output       : none 
* @return       : true if successed
*/
bool user_delete_setting_origin(void)
{
    user_app_setting_t     dev_param_t;

    user_get_settings_from_flash(&dev_param_t);
    
    dev_param_t.accorigin_setting.mode = 0;
    dev_param_t.accorigin_setting.pair_num = 0;
    memset(dev_param_t.accorigin_setting.range, 0, sizeof(dev_param_t.accorigin_setting.range));
    if(user_save_settings((uint8_t *)&dev_param_t, sizeof(user_app_setting_t)) != true)
    {
        DBG_LOG("delete origin setting failed\r\n");
        return false;
    }
    return true;
}
bool user_delete_setting_env(void)
{
    user_app_setting_t     dev_param_t;

    user_get_settings_from_flash(&dev_param_t);
    
    dev_param_t.env_setting.mode = 0;
    dev_param_t.env_setting.interval = 0;
    if(user_save_settings((uint8_t *)&dev_param_t, sizeof(user_app_setting_t)) != true)
    {
        DBG_LOG("delete env setting failed\r\n");
        return false;
    }
    return true;
}
bool user_delete_setting_bhv(void)
{
    user_app_setting_t     dev_param_t;

    user_get_settings_from_flash(&dev_param_t);
    
    dev_param_t.bhv_setting.mode = 0;
    dev_param_t.bhv_setting.interval = 0;
    
    if(user_save_settings((uint8_t *)&dev_param_t, sizeof(user_app_setting_t)) != true)
    {
        DBG_LOG("delete bhv setting failed\r\n");
        return false;
    }
    return true;
}
bool user_delete_setting_estrus(void)
{
    user_app_setting_t     dev_param_t;

    user_get_settings_from_flash(&dev_param_t);
    
    dev_param_t.estrus_setting.mode = 0;
    dev_param_t.estrus_setting.interval = 0;
    
    if(user_save_settings((uint8_t *)&dev_param_t, sizeof(user_app_setting_t)) != true)
    {
        DBG_LOG("delete estrus setting failed\r\n");
        return false;
    }
    return true;
}
bool user_delete_setting_nb_communication(void)
{
    user_app_setting_t     dev_param_t;

    if(user_get_settings_from_flash(&dev_param_t) != true)
    {
        DBG_LOG("delete setting: get setting from flash is failed\r\n");
        return false;
    }
    
    dev_param_t.nb_comm_setting.mode = 0;
    dev_param_t.nb_comm_setting.interval = 0;
    memset(dev_param_t.nb_comm_setting.time_list, 0, sizeof(dev_param_t.nb_comm_setting.time_list));
    if(user_save_settings((uint8_t *)&dev_param_t, sizeof(user_app_setting_t)) != true)
    {
        DBG_LOG("delete setting: save failed\r\n");
        return false;
    }
    return true;
}
bool user_delete_setting_all(void)
{
    if(user_nrf_flash_page_erase((uint32_t *)USER_INDEX_MEMORY_START,1) == false)
    {
        DBG_LOG("user_save_settings erase flash is failed\r\n");
        return false;
    }    
    return true;    
}
bool user_set_rewrite_timestamp(void)
{
    user_app_setting_t  set;
    user_get_settings_from_flash(&set);
    set.timestamp = hal_rtc2_get_unix_time();
    user_save_settings((uint8_t *)&set,sizeof(user_app_setting_t));
    //memset(&set,0,sizeof(set));
    //user_get_settings_from_flash(&set);
    return true;
}
/**
* @functionname : user_app_origin_decode_repeated_var_struct 
* @description  : function for decode a setting struct data
* @input        : stream, encode stream,  
*               : fields: the setting fields
*               : src_struct: the struct data will be decoded
* @output       : none 
* @return       : true if successed
*/
/*
static bool user_app_setting_decode_repeated_var_struct(pb_istream_t *stream, const pb_field_t *field, void **src_struct)
{

    protocol_setting_t *set_t = (protocol_setting_t *)(*src_struct);
    bool res = false;

    res = pb_decode(stream,protocol_setting_fields,set_t);
    DBG_LOG("decode settings struct is %s\r\n", (res? "successed":"failed"));
    return res;
}
*/
/**
* @functionname : app_encode_origin_data 
* @description  : function for encode data
* @input        : stream, encode stream,  
*               : fields: the origin fields
*               : str: the string data will be encoded
* @output       : none 
* @return       : true if successed
*/

static uint32_t user_app_encode_setting_req(uint8_t *const out_buffer, int16_t buf_len)
{
    if(out_buffer == NULL || buf_len <= 0)
    {
        DBG_LOG("user_app_encode_setting_req pointer is null\r\n");
        return 0;
    }
    char p_dev_id[13] = {0};
    pb_ostream_t   m_stream ;
    protocol_setting_req_t     set_req_encode_t;
    
    //get identity message
    set_req_encode_t.Iden.MsgIndex = 0;
    app_get_indetity_msg(&set_req_encode_t.Iden);
    //get device id(mac)
    app_iden_get_device_id_char(p_dev_id);
    
    set_req_encode_t.Iden.has_RspCode = false;
    set_req_encode_t.Iden.RspCode = 0;
    
    set_req_encode_t.Iden.DeviceID.funcs.encode  = &user_app_encode_repeated_var_string;
    set_req_encode_t.Iden.DeviceID.arg           = p_dev_id;
       
    m_stream = pb_ostream_from_buffer(out_buffer,buf_len);
    bool res = pb_encode(&m_stream,protocol_setting_req_fields,&set_req_encode_t);
    //memcpy(p_out,enc_buf,m_stream.bytes_written);
    DBG_LOG("\r\nencode setting is %s, out size = %d\r\n",  (res? "successed":"failed"),m_stream.bytes_written);
    
#if test_decode  
    user_data_pb_decode_test(out_buffer,m_stream.bytes_written);
#endif  //test_decode
    //DBG_LOG("register out_buffer addr = 0x%x \r\n", out_buffer);
    return m_stream.bytes_written;
}
/**
* @functionname : user_app_decode_setting_time_range 
* @description  : function for decode a setting of time range field , @ref protocol_time_range_t
* @input        : stream, encode stream,  
*               : fields: the setting fields
*               : src_struct: the time range data will be decoded
* @output       : none 
* @return       : true if successed
*/
static bool user_app_decode_setting_time_range(pb_istream_t *stream, const pb_field_t *fields,  void **arg)
{    
    protocol_time_range_t   time_range;
    bool res = false;

    res = pb_decode(stream,protocol_time_range_fields,&time_range);
    DBG_LOG("decode settings time range is %s\r\n", (res? "successed":"failed"));
    if( res == false)
    {
        return res;
    }
    usre_decode_time_range_t   *p_decode_time_range = *arg;
    
    if(*p_decode_time_range->count >= ACC_TIME_RANGE_PAIR_NUM_MAX)
    {
        DBG_LOG("settings time range pair > max, clear to 0\r\n");
        *p_decode_time_range->count = 0;
        
    }
    p_decode_time_range->time[*p_decode_time_range->count].Begin = time_range.Begin;
    p_decode_time_range->time[*p_decode_time_range->count].End   = time_range.End;
    
    DBG_LOG("decode settings time range pair = %d\r\n", *p_decode_time_range->count);
    DBG_LOG("start time = %d\r\n", p_decode_time_range->time[*p_decode_time_range->count].Begin);
    DBG_LOG("end time = %d\r\n", p_decode_time_range->time[*p_decode_time_range->count].End);
    
    (*p_decode_time_range->count)++;
    return true;
}
/**
* @functionname : user_app_setting_decode 
* @description  : function for decoding the setting message,the decoded message match to @ref protocol_setting_rsp_t
* @input        : p_set_rsp_s, 
*               :  const p_set_info_t,
*               :  const p_buf, 
*               :  const len
* @output       : none 
* @return       : true if successed
*/
static bool user_app_setting_decode(protocol_setting_rsp_t * const p_set_rsp_s, 
                                    user_app_setting_t    * const p_set_info_t,
                                    uint8_t         const  * const p_buf, 
                                    int32_t                  const len
                                   )
{
    pb_istream_t    i_stream ;  
    bool    ret;    
    usre_decode_time_range_t    p_dec_origin_time_range;
    usre_decode_time_range_t    p_dec_pwroff_time_range;
    
    p_dec_origin_time_range.time  = p_set_info_t->accorigin_setting.range;
    p_dec_origin_time_range.count = &p_set_info_t->accorigin_setting.pair_num;
    
    p_dec_pwroff_time_range.time  = &p_set_info_t->pwr_setting.range;
    p_dec_pwroff_time_range.count = &p_set_info_t->pwr_setting.pair_num;
    
    p_set_rsp_s->Iden.DeviceID.funcs.decode             = user_app_decode_repeated_var_string;
    p_set_rsp_s->Iden.DeviceID.arg                      = p_set_info_t->iden.dev_id;
    
    p_set_rsp_s->SettingInfo.PowerOffTime.funcs.decode  = user_app_decode_setting_time_range;
    p_set_rsp_s->SettingInfo.PowerOffTime.arg           = &p_dec_pwroff_time_range;
    
    p_set_rsp_s->SettingInfo.OriginTime.funcs.decode    = user_app_decode_setting_time_range;
    p_set_rsp_s->SettingInfo.OriginTime.arg             = &p_dec_origin_time_range;
    
    p_set_rsp_s->SettingInfo.CommunicationTimeTable.funcs.decode = user_app_decode_repeated_var_string;
    p_set_rsp_s->SettingInfo.CommunicationTimeTable.arg          = p_set_info_t->nb_comm_setting.time_list;
    
    p_set_rsp_s->SettingInfo.OTAFirmwareID.funcs.decode          = user_app_decode_repeated_var_string;
    p_set_rsp_s->SettingInfo.OTAFirmwareID.arg                   = p_set_info_t->ota_setting.ota_firmware_id;
       
    i_stream = pb_istream_from_buffer(p_buf,len);
    ret = pb_decode(&i_stream,protocol_setting_rsp_fields,p_set_rsp_s); 
    DBG_LOG("decode settings is %s\r\n", (ret? "successed":"failed"));
    return ret;
}

bool user_app_update_param_setting(user_app_setting_t * const p_set_info_t)
{  
    return user_save_settings((uint8_t*) p_set_info_t,  sizeof(user_app_setting_t));
}

/**
* @functionname : user_app_setting_parse 
* @description  : function for update the application perform parameters, @ref user_setting_info_t
* @input        : p_set_rsp_s: decoded message,  
*               : p_set_info_t: all perform parameter data
* @output       : none 
* @return       : true if successed
*/
bool user_app_setting_parse(protocol_setting_rsp_t * const p_set_rsp_s, user_app_setting_t * const p_set_info_t)
{
    if(!p_set_rsp_s->has_SettingInfo)
    {
        DBG_LOG("user_app_setting_parse pointer is null\r\n");
        return 0;
    }
    //p_set_rsp_s->SettingInfo.has_AlarmMode ? p_set_info_t->alarm_mode = p_set_rsp_s->SettingInfo.AlarmMode : 0;
    if(p_set_rsp_s->SettingInfo.has_AlarmMode)
    {
        p_set_info_t->warning_mode = p_set_rsp_s->SettingInfo.AlarmMode;
        DBG_LOG("set alarm mode : %d\r\n",  p_set_info_t->warning_mode); 
    }
    //set behavior sample interval time , unit is second
    /*if(p_set_rsp_s->SettingInfo.has_BehaviorSampleInterval)
    {
        p_set_info_t->bhv_setting.interval = p_set_rsp_s->SettingInfo.BehaviorSampleInterval;
        user_bhv_set_sample_interval(p_set_info_t->bhv_setting.interval);
        DBG_LOG("set behavior sample interval : %dS\r\n",  p_set_info_t->bhv_setting.interval); 
    }
    */
    //set behavior sample mode , ,0: closed, 1: time to sample
    if(p_set_rsp_s->SettingInfo.has_BehaviorSampleMode && p_set_rsp_s->SettingInfo.has_BehaviorSampleInterval)
    {
        p_set_info_t->bhv_setting.mode = p_set_rsp_s->SettingInfo.BehaviorSampleMode;
        p_set_info_t->bhv_setting.interval = p_set_rsp_s->SettingInfo.BehaviorSampleInterval;
        //user_bhv_set_sample_mode(p_set_info_t->bhv_setting.mode);
        user_bhv_setting(p_set_info_t->bhv_setting);
        DBG_LOG("set behavior sample mode : %d\r\n",  p_set_info_t->bhv_setting.mode); 
        DBG_LOG("set behavior sample interval : %dS\r\n",  p_set_info_t->bhv_setting.interval);
    }
    //if(p_set_rsp_s->SettingInfo.has_CommunicationInterval)
    //{
    //    p_set_info_t->nb_comm_setting.interval = p_set_rsp_s->SettingInfo.CommunicationInterval;
        
    //    DBG_LOG("set communication interval : %dS\r\n",  p_set_info_t->nb_comm_setting.interval);
    //}
    if(p_set_rsp_s->SettingInfo.has_CommunicationMode && p_set_rsp_s->SettingInfo.has_CommunicationInterval)
    {
        p_set_info_t->nb_comm_setting.mode     = p_set_rsp_s->SettingInfo.CommunicationMode;
        p_set_info_t->nb_comm_setting.interval = p_set_rsp_s->SettingInfo.CommunicationInterval;
        user_nb_communication_setting(p_set_info_t->nb_comm_setting);
        DBG_LOG("set communication mode : %d\r\n",  p_set_info_t->nb_comm_setting.mode);
        DBG_LOG("set communication interval : %dS\r\n",  p_set_info_t->nb_comm_setting.interval);
    }
    //if(p_set_rsp_s->SettingInfo.has_EnvSampleInterval)
    //{
    //    p_set_info_t->env_setting.interval = p_set_rsp_s->SettingInfo.EnvSampleInterval;
    //     DBG_LOG("set env sample interval : %dS\r\n",  p_set_info_t->env_setting.interval);
    //}
    if(p_set_rsp_s->SettingInfo.has_EnvSampleMode && p_set_rsp_s->SettingInfo.has_EnvSampleInterval)
    {
        p_set_info_t->env_setting.mode = p_set_rsp_s->SettingInfo.EnvSampleMode;
        p_set_info_t->env_setting.interval = p_set_rsp_s->SettingInfo.EnvSampleInterval;
        user_env_setting(p_set_info_t->env_setting);
        DBG_LOG("set env sample interval : %dS\r\n",  p_set_info_t->env_setting.interval);
        DBG_LOG("set env sample mode : %d\r\n",  p_set_info_t->env_setting.mode);
    }
    //if(p_set_rsp_s->SettingInfo.has_EstrusSampleInterval)
    //{
    //    p_set_info_t->estrus_setting.interval = p_set_rsp_s->SettingInfo.EnvSampleInterval;
    //    user_estrus_set_sample_interval(p_set_info_t->estrus_setting.interval);        
    //    DBG_LOG("set estrus sample interval : %d\r\n",  p_set_info_t->estrus_setting.interval);
    //}
    if(p_set_rsp_s->SettingInfo.has_EstrusSampleMode && p_set_rsp_s->SettingInfo.has_EstrusSampleInterval)
    {
        p_set_info_t->estrus_setting.mode = p_set_rsp_s->SettingInfo.EstrusSampleMode;
        p_set_info_t->estrus_setting.interval = p_set_rsp_s->SettingInfo.EnvSampleInterval;
        user_estrus_setting(p_set_info_t->estrus_setting);
        DBG_LOG("set estrus sample interval : %d\r\n",  p_set_info_t->estrus_setting.interval);
        DBG_LOG("set estrus sample mode : %d\r\n",  p_set_info_t->estrus_setting.mode);
    }
    if(p_set_rsp_s->SettingInfo.has_OriginMode)
    {
        p_set_info_t->accorigin_setting.mode = p_set_rsp_s->SettingInfo.OriginMode;   
        user_origin_setting(p_set_info_t->accorigin_setting);
        DBG_LOG("set acc origin mode : %d\r\n",  p_set_info_t->accorigin_setting.mode);
    }
    if(p_set_rsp_s->SettingInfo.has_AlarmMode)
    {
        p_set_info_t->warning_setting.mode = p_set_rsp_s->SettingInfo.AlarmMode;
        user_warning_mode_set(&p_set_info_t->warning_setting);
    }
    if(p_set_rsp_s->SettingInfo.has_OTAFirmwareVersion)
    {
        p_set_info_t->ota_setting.ota_firmware_version        = p_set_rsp_s->SettingInfo.OTAFirmwareVersion;
    }
    if(p_set_rsp_s->SettingInfo.has_OTAForceUpgrade)
    {
        p_set_info_t->ota_setting.ota_force_up_grade        = p_set_rsp_s->SettingInfo.OTAForceUpgrade;
    }

    if(p_set_rsp_s->SettingInfo.has_OTAServerPort)
    {
        p_set_info_t->ota_setting.ota_server_port        = p_set_rsp_s->SettingInfo.OTAServerPort;
    }
    if(p_set_rsp_s->SettingInfo.has_PowerOffMode)
    {
        p_set_info_t->pwr_setting.mode        = p_set_rsp_s->SettingInfo.PowerOffMode;
    }
    if(p_set_rsp_s->SettingInfo.has_ResetDevice)
    {
        p_set_info_t->reset_device        = p_set_rsp_s->SettingInfo.ResetDevice;
    }
    if(p_set_rsp_s->SettingInfo.has_SMSInterval)
    {
        p_set_info_t->sms_setting.sms_interval        = p_set_rsp_s->SettingInfo.SMSInterval;
    }
    if(p_set_rsp_s->SettingInfo.has_SMSMode)
    {
        p_set_info_t->sms_setting.sms_mode        = p_set_rsp_s->SettingInfo.SMSMode;
    }
    p_set_info_t->timestamp = hal_rtc2_get_unix_time();
    return user_app_update_param_setting(p_set_info_t);
}

/**
* @functionname : app_setting_rsp_parse_handle 
* @description  : function for parsing the received message, include decode, and enforce the settings for parameters
* @input        : p_buf: received protocal-buffer message,  
*               : len: message length
* @output       : none 
* @return       : true if successed
*/
bool app_setting_rsp_parse_handle(void const * const p_buf, int32_t const len)
{
    user_app_setting_t         asrp_set_info_t;
    protocol_setting_rsp_t      asrp_pbd_set_rsp_t;
    
    memset(&asrp_set_info_t,0, sizeof(asrp_set_info_t));
    memset(&asrp_pbd_set_rsp_t,0, sizeof(asrp_pbd_set_rsp_t));
    
    //bool ret = true;
    
    DBG_LOG("decoding setting message\r\n");
    if(user_app_setting_decode(&asrp_pbd_set_rsp_t, &asrp_set_info_t, p_buf, len) != true)
    {
        DBG_LOG("decode setting is failed\r\n");
        return false;
    }
    char device_id[13] = {0};
    uint32_t msgtoken = 0;
    app_iden_get_device_id_char(device_id);
    uint16_t give_random = asrp_pbd_set_rsp_t.Iden.MsgToken & 0x0ffff;
    
    if(asrp_set_info_t.iden.dev_id[0] != '\0')
    {
        //if(!user_string_compare(asrp_set_info_t.iden.dev_id,device_id))
        //if(strcmp(asrp_set_info_t.iden.dev_id,device_id) != 0)
        if(memcmp(asrp_set_info_t.iden.dev_id,device_id,12) != 0)    
        {
            DBG_LOG("\r\norigin parse: device id is error\r\n");
            return 0;
        }    
        msgtoken = app_get_msg_token(asrp_set_info_t.iden.dev_id,give_random);
        }
    else
    {
        msgtoken = app_get_msg_token(device_id,give_random);
    }
    if(msgtoken != asrp_pbd_set_rsp_t.Iden.MsgToken)
    {
        DBG_LOG("\r\norigin parse: check msgtoken is error\r\n");
        return 0;
    }
    DBG_LOG("parse setting parameters\r\n");
    if(user_app_setting_parse(&asrp_pbd_set_rsp_t, &asrp_set_info_t) != true)
    {
        DBG_LOG("parse setting failed\r\n");
        return false;
    }
    xSemaphoreGive(binary_semaphore_wait_response);
    //setting_rsp_flag = 1;
    DBG_LOG("parse setting successed\r\n");
    return true;
}
/**
* @functionname : user_app_setting_wait_rsp 
* @description  : function for waiting the response after send setting require, timeout 10S
* @input        : p_buf: received protocal-buffer message,  
*               : len: message length
* @output       : none 
* @return       : true if successed
*/
bool user_app_setting_wait_rsp(void)
{
    if(xSemaphoreTake( binary_semaphore_wait_response, 20 * 1000) == pdPASS)
    {
        DBG_LOG("receive setting response successed\r\n\0");
        return true;
    }
    return false;
}
/**
* @function@name:    app_send_register_msg
* @description  :    function for send register message
* @input        :    none
* @output       :    none
* @return       :    send length
*/
//bool user_app_setting_require(trans_interface_type chn_type)
bool user_app_setting_require(uint8_t * encode_buf, uint32_t encode_buf_size, 
                  uint8_t * send_buf, uint32_t send_buf_size,  
                  trans_interface_type chn_type
                 )
{
    if(chn_type >= COMMUNICATION_INTERFACE_TYPE_MAX || chn_type <= COMMUNICATION_INTERFACE_TYPE_MIN)
    {
        return false;
    }
    if(chn_type == COMMUNICATION_INTERFACE_TYPE_BLE)
    {
        if(!ble_get_connect_status())
        {
            return false;
        }
    }
    if(encode_buf == NULL || send_buf == NULL)
    {
        DBG_LOG("\r\n pointer encode_buf or send_buf is null\r\n");
        return false;
    }
    int32_t length;
    bool ret;
    //uint8_t encode_buf[34] = {0};
    //uint8_t send_buf[46] = {0};
    DBG_LOG("\r\nsend_buf : 0x%x, p_encode_buf: 0x%x\r\n",send_buf,encode_buf);
    binary_semaphore_wait_response = xSemaphoreCreateBinary();
    if(binary_semaphore_wait_response == NULL)
    {
        DBG_LOG("\r\n>>>>>>>> setting require: binary semaphore create failed <<<<<<<<<\\r\n\0");
        return false;
    }
    xSemaphoreTake( binary_semaphore_wait_response, 0 );
    
    length = user_app_encode_setting_req(encode_buf,encode_buf_size);
    DBG_LOG("setting encode length = %d\r\n",length);
    if(length <= 0)
    {
        return false;
    }
    length = user_data_packge(send_buf, send_buf_size, encode_buf, length,
                                PROTOCOL_HEADER_TYPE_TYPE_SETTING_REQ, 
                                USER_COMMON_SEQ, USER_COMMON_CODE,chn_type);
    DBG_LOG("setting package length = %d\r\n",length);
    if(length <= 0)
    {
        return false;
    }
    if(user_send_data(send_buf,length,chn_type) == false)
    {
        DBG_LOG("setting send by ble is failed = %d\r\n",length);
        return false;
    }
    ret &= user_app_setting_wait_rsp();
    vSemaphoreDelete( binary_semaphore_wait_response );
    return ret;
}
void user_app_print_all_setting(user_app_setting_t setting)
{
    DBG_LOG("setting.accorigin_setting.mode = %d\r\n",setting.accorigin_setting.mode);
    DBG_LOG("setting.accorigin_setting.pair_num = %d\r\n",setting.accorigin_setting.pair_num);
    //DBG_LOG("setting.accorigin_setting.mode = %d\r\n",setting.accorigin_setting.range);
    DBG_LOG("setting.bhv_setting.mode = %d\r\n",setting.bhv_setting.mode);
    DBG_LOG("setting.bhv_setting.interval = %d\r\n",setting.bhv_setting.interval);
    DBG_LOG("setting.env_setting.mode = %d\r\n",setting.env_setting.mode);
    DBG_LOG("setting.env_setting.interval = %d\r\n",setting.env_setting.interval);
    DBG_LOG("setting.estrus_setting.mode = %d\r\n",setting.estrus_setting.mode);
    DBG_LOG("setting.estrus_setting.interval = %d\r\n",setting.env_setting.interval);
    DBG_LOG("setting.nb_comm_setting.mode = %d\r\n",setting.nb_comm_setting.mode);
    DBG_LOG("setting.nb_comm_setting.interval = %d\r\n",setting.nb_comm_setting.interval);
    DBG_LOG("setting.nb_comm_setting.time_list = %s\r\n",setting.nb_comm_setting.time_list);
    DBG_LOG("setting.warning_mode = %d\r\n",setting.warning_mode);
    DBG_LOG("setting.timestamp = %d\r\n",setting.timestamp);
    
}
/**
* @functionname : user_app_param_init 
* @description  : function for initializing the device all parameters, which get from flash memory
* @input        : none
* @output       : none 
* @return       : timestatmp: time saved at last running, if not true time, initialize to 2018/1/1 00:00:00
*/
uint32_t user_app_setting_init(void)
{
    user_app_setting_t     dev_param_t;
    //user_delete_setting_all();
    //user_delete_setting_origin();
    DBG_LOG("get setting from flash\r\n");
    user_get_settings_from_flash(&dev_param_t);

    //DBG_LOG("+++++++++++++++++++++++++++++++set timestamp++++++++++++++++++++\r\n");
    alarm_clock_set_time_stamp(dev_param_t.timestamp);
    
    //DBG_LOG("+++++++++++++++++++++++++++++++set behavior parameter++++++++++++++++++++\r\n");
    #if 0
    dev_param_t.bhv_setting.interval = 70;
    dev_param_t.bhv_setting.mode = 1;
    #endif
    user_bhv_setting(dev_param_t.bhv_setting);
    //DBG_LOG("+++++++++++++++++++++++++++++++set environment parameter++++++++++++++++++++\r\n");
     #if 0
    dev_param_t.env_setting.interval = 80;
    dev_param_t.env_setting.mode = 1;
    #endif
    user_env_setting(dev_param_t.env_setting);
    //DBG_LOG("+++++++++++++++++++++++++++++++set estrus parameter++++++++++++++++++++\r\n");
    #if 0
    dev_param_t.estrus_setting.interval = 65;
    dev_param_t.estrus_setting.mode = 1;
    dev_param_t.estrus_setting.estrus_threshold = 60;
    #endif
    user_estrus_setting(dev_param_t.estrus_setting);
    //DBG_LOG("+++++++++++++++++++++++++++++++set acc origin parameter++++++++++++++++++++\r\n");
    #if 0
    //dev_param_t.accorigin_setting.mode = ACC_SAMPLE_MODE_DISABLE;//ACC_SAMPLE_MODE_BASE_ON_UNIXTIME;
    dev_param_t.accorigin_setting.mode = ACC_SAMPLE_MODE_BASE_ON_MINUTE;
    dev_param_t.accorigin_setting.pair_num = 1;
    dev_param_t.accorigin_setting.range[0].Begin = 30;
    dev_param_t.accorigin_setting.range[0].End  = 40;
    #endif
    user_origin_setting(dev_param_t.accorigin_setting);
    //DBG_LOG("+++++++++++++++++++++++++++++++set nb communication parameter++++++++++++++++++++\r\n");
    user_nb_communication_setting(dev_param_t.nb_comm_setting);
    user_warning_mode_set(&dev_param_t.warning_setting);
    return dev_param_t.timestamp;
}
/*
void user_app_setting_task(void *arg)
{
    while(xSemaphoreTake( semaphore_setting, 0 ) == pdPASS);
    while(1)
    {
        
    }
}
void user_creat_setting_task(void)
{
    BaseType_t ret;  
    semaphore_setting = xSemaphoreCreateBinary();
    ret = xTaskCreate( user_app_setting_task, "set", 256, NULL, 1, &setting_handle );
    if(pdPASS != ret)
    {
        APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
    }  
    //DBG_LOG("\r\nacc_collect_handle = 0x%x\r\n\0",acc_collect_handle);
}
void user_app_send_setting_semaphore(void)
{
    DBG_LOG("\r\ngive acc collect semaphore\r\n");
    xSemaphoreGive(semaphore_setting);
    DBG_LOG("\r\ngive acc collect semaphore over\r\n");
}
*/
//end

