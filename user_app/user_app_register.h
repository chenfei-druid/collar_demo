
#ifndef USER_APP_REGISTER
#define USER_APP_REGISTER
#include <stdint.h>
#include <stdbool.h>
#include "user_data_pkg.h"

//because the register is only one frame, use the fixed seq and function code
#define         USER_REGT_SEQ           1   //0x01-0xff
#define         USER_FUNC_CODE          1  //0X01

#define         USER_IMEI_CODE          "123456789\0" 
#define         USER_IMSI_CODE          "ABCD98765\0"
#define         USER_REGISTER_FW_VS     113
#define         USER_REGISTER_HW_VS     102
#define         USER_REGISTER_DEV_TYPE  10040
typedef struct
{
    int32_t     devicetype;
    int32_t     hd_ver;
    int32_t     fm_ver;
    int32_t     status;
    int32_t     bat_vol;
    int32_t     bat_pwr;
    int32_t     sg_strth;
    int32_t     bit_err_rt;
    int32_t     radio_acs_type;
    int32_t     net_optr;
}app_register_req_t;

typedef struct
{
    uint32_t    timestamp;
    int32_t     sim_type;
}app_register_rsp_t;

//bool app_register_test(void);
//bool app_register(trans_interface_type chn_type);
bool app_register(uint8_t * encode_buf, uint32_t encode_buf_size,
                  uint8_t * send_buf, uint32_t send_buf_size, 
                  trans_interface_type chn_type
                 );
bool app_register_rsp_parse(void const * const p_buf, int32_t len);
//bool app_register_wait_rsp(void);
void test_register(void);

#endif

