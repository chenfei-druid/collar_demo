
#if NRF_LOG_ENABLED
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"


#define  DBG_LOG(...)  NRF_LOG_INFO(__VA_ARGS__)

#endif //NRF_LOG_ENABLED

