/**
 * user_task.c
 * Date:    2018/2/2
 * Author:  Chenfei
 * 
 * @brief smart-meter project.
 * by  nrf_52832 freertos platform
 *
 */
#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "nrf.h"
#include "FreeRTOS.h"
#include "nrf_sdh_freertos.h"
#include "semphr.h"
#include "task.h"
#include "app_error.h"
#include "app_timer.h"
#include "peer_manager.h"
#include "bsp_btn_ble.h"
#include "timers.h"
#include "fds.h"
#include "nrf_drv_clock.h"
#include "ble_sys_cfg.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "user_task.h"
#include "user_app.h"
#include "hal_nrf_uart.h"
#include "nrf_uarte.h"
#include "drv_nb_modul.h"
#include "hal_rf.h"
#include "user_app_calendar.h"
//#include "hal_acc.h"
#include "user_data_struct.h"
#include "user_acc.h"
#include "user_app_env.h"
#include "user_app_bhv.h"
#include "user_app_estrus.h"
#include "user_app_origin.h"
#include "user_ble_inter_mgt.h"
#include "user_analyse_commu_cmd.h"
#include "user_app_warning.h"
#include "hal_nrf_wdt.h"
#include "user_send_data.h"

static      xSemaphoreHandle    binary_semaphore_user_tick;

static      TaskHandle_t        app_handle;
static      QueueHandle_t       led_queue;
void user_app_send_led_queue(uint32_t freq)
{
    if(led_queue == NULL)
    {
        led_queue = xQueueCreate( 1, sizeof(uint32_t) );
    }
    xQueueSendToBack(led_queue, &freq, 10);
}
static void user_app_handler_task(void *arg)
{
    static uint32_t queue;
    static uint32_t interver;
    user_leds_pin_config();
    user_leds_off();
    user_app_init();
    int32_t acc_mode = 0; 
    while(1)
    { 
        acc_mode = 0;
        user_led_on(LED_INDEX_1);
        vTaskDelay( 5 );
        user_led_off(LED_INDEX_1);
        acc_mode = user_origin_get_sample_mode() + user_bhv_get_sample_mode() + user_estrus_get_sample_mode();
        if(acc_mode > 0)
        {
            interver = 4;
        }
        else
        {
            interver = 8;
        }
        if(xQueueReceive(led_queue, &queue , interver*1000) == pdPASS)
        {
            if((queue > 0) && (queue < 10000))
            {
                uint32_t temp = queue;
                while(1)       //wait end
                {
                    user_led_on(LED_INDEX_1);
                    vTaskDelay( 5 );
                    user_led_off(LED_INDEX_1);
                    //vTaskDelay( 1000/temp );
                    if(xQueueReceive(led_queue, &queue , temp) == pdPASS)
                    {
                        if(queue == 0)
                        {
                            break;
                        }
                    }
                }
            }
            
        }
    }
}

/**
* @function@name:    user_tick_timer_handler
* @description  :    when a user tick arrived, sending a semaphore to handler task
* @input        :    *arg:                     
* @output       :    none
* @return       :    none
*/
void user_tick_timer_handler(void)
{
    static BaseType_t semaphore = pdFALSE;
    xSemaphoreGiveFromISR(binary_semaphore_user_tick,&semaphore);
}

/**
* @function@name:    user_creat_app_handler_task
* @description  :    Functiono for transmit a semaphore for external peripheral interrupt
* @input        :    *arg:                     
* @output       :    none
* @return       :    none
*/

void user_creat_app_handler_task(void)
{
    BaseType_t ret;  
    //vSemaphoreCreateBinary( binary_semaphore_user_tick );
    led_queue = xQueueCreate( 1, sizeof(uint32_t) );
    ret = xTaskCreate( user_app_handler_task, "app", 256, NULL, 1, &app_handle );
    if(pdPASS != ret)
    {
        APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
    }  
    xQueueReset(led_queue);
}
/*
static void user_create_app_task(void)
{
    user_creat_ble_transmit_task();

    user_creat_acc_data_collect_task();

    user_creat_env_detect_task();
    user_creat_inst_parse_task();
    user_creat_warning_detect_task();
}
*/
/**
* @function@name:    user_task_init
* @description  :    Functiono for creating user application task
* @input        :    *arg:                     
* @output       :    none
* @return       :    none
*/

void user_task_init(void)
{    
    drv_kx022_creat_task();
    hal_creat_uart_handler_task(); 
    user_creat_app_handler_task();
    user_creat_nb_timer_transmit_task();
    user_creat_nb_receive_handler_task();
    user_creat_ble_transmit_task();    
    user_creat_acc_data_collect_task();
    user_creat_env_detect_task();
    user_creat_inst_parse_task();
    user_creat_warning_detect_task();
    hal_create_wdt_task();
    //user_create_ble_tx_semaphore();
    
}
