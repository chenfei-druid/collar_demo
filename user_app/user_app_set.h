
#ifndef     USER_APP_SET_H
#define     USER_APP_SET_H

#include <stdint.h>
#include <stdbool.h>
#include "user_app_identity.h"
#include    "user_data_pkg.h"

typedef struct
{
    uint32_t    Begin;
    uint32_t    End;
}user_time_range_t;

typedef struct
{
    uint32_t            *count;
    user_time_range_t   *time;
}usre_decode_time_range_t;

typedef struct
{
    user_time_range_t   range;
    int32_t             mode;
    uint32_t             pair_num;
}pwr_setting_t;

typedef struct
{
    int32_t             sms_mode ;
    int32_t             sms_interval;
}sms_setting_t;

typedef struct
{
    int32_t             ota_firmware_version;
    uint8_t             ota_firmware_id[25];        //OTA firmware id, ascii code
    int32_t             ota_force_up_grade;
    uint8_t             ota_server_host[20];        //server ip or field name
    int32_t             ota_server_port;
}ota_setting_t;
/*
typedef struct
{
    app_identity_t      iden;
    int32_t             env_sample_mode;            //environment sample mode ,0: closed, 1: time to sample
    int32_t             env_sample_interval;        //environment sample time interval, unit is Second
    int32_t             bhv_sample_mode;            //behavior sample mode, 0: closed, 1: time to sample
    int32_t             bhv_sample_interval;        //
    //bhv_setting_t       bhv_setting;
    int32_t             gps_sample_mode;
    int32_t             gps_sample_interval;
    int32_t             comm_mode;
    int32_t             comm_interval;
    uint8_t             comm_time_list[25];         //transmit message on a'clock list ,length is 24, means 0~23 o'clock,
                                                    //every one is ascii code of '0' or '1', 
                                                    //if '1', it will transmit at corresponding clock  
    int32_t             reset_device;
    user_time_range_t   power_off_time;
    uint32_t            pwroff_time_count;
    int32_t             power_off_mode;
    int32_t             ota_firmware_version;
    uint8_t             ota_firmware_id[25];        //OTA firmware id, ascii code
    int32_t             ota_force_up_grade;
    uint8_t             ota_server_host[20];        //server ip or field name
    int32_t             ota_server_port;
    int32_t             sms_mode ;
    int32_t             sms_interval;
    int32_t             alarm_mode;
    user_time_range_t   origin_time[20];            //acc origin collect time range, may has some ranges
    uint32_t            origin_time_count;
    int32_t             origin_mode;
    int32_t             estrus_sample_mode;
    int32_t             estrus_sample_interval;
}user_setting_info_t;
*/


bool app_setting_rsp_parse_handle(void const * const p_buf, int32_t const len);
//bool user_app_setting_require(trans_interface_type chn_type);
bool user_app_setting_require(uint8_t * encode_buf, uint32_t encode_buf_size, 
                  uint8_t * send_buf, uint32_t send_buf_size,  
                  trans_interface_type chn_type
                 );
bool user_save_settings(uint8_t * const p_buf, int32_t len);
uint32_t user_app_setting_init(void);
void user_creat_setting_task(void);
void user_app_send_setting_semaphore(void);
bool user_set_rewrite_timestamp(void);
bool user_setting_update_timestamp(uint32_t timestamp);

#endif

