
/**
 * hal_env.c
 * @group:       nack_trap project
 * @author:      Chenfei
 * @createdate: 2018/4/9
 * @modifydate: 2018/4/9
 * @version:     V0.0
 *
 * 
 */
 
#include "hal_env.h"
#include "user_data_struct.h"
#include "user_battery_mgt.h"
#include "user_illumination.h"

bool hal_env_get_battary_voltage(uint32_t *p_vol)
{
    drv_battery_level_measurment(p_vol);
    return true;
}

bool hal_env_get_temperature(uint32_t *p_temp)
{
    
    return true;
}

bool hal_env_get_illumination(uint32_t *p_lux)
{
    
    return true;
}

