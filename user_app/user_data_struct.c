
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>

#include "user_data_struct.h"
#include "alltypes.pb.h"
#include "test_helpers.h"
#include "unittests.h"
#include "SEGGER_RTT.h"
#include "SEGGER_RTT_Conf.h"
#include "Define.pb.h"
#include "hal_per_flash.h"


protocol_identity_msg_t     m_protocol_identity_msg_t;

protocol_behavior2_req_t    m_protocol_behavior2_req_t = PROTOCOL_BEHAVIOR2_REQ_INIT_ZERO;
protocol_behavior2_t        m_protocol_behavior2_t  =  PROTOCOL_BEHAVIOR2_INIT_ZERO;
/*
protocol_env_req_t          m_protocol_env_req_t;
protocol_env_t              m_protocol_env_t;

protocol_estrus_req_t       m_protocol_estrus_req_t;
protocol_estrus_t           m_protocol_estrus_t;

protocol_warning_req_t      m_protocol_warning_req_t;
protocol_warning_t          m_protocol_warning_t;

protocol_setting_rsp_t      m_protocol_setting_rsp_t;
protocol_setting_req_t      m_protocol_setting_req_t;
protocol_setting_t          m_protocol_setting_t;
*/
/**
*   @ref ds_encoded_record_store_index_t
*   record[]      = {rec0, rec1, rec2,..., recx}
*   recordindex[] = {len0, len1, len2,..., lenx}, the elements in recorindex are corresponding to record data one by one,
*                   for instance: len0 = rec0's length, lenx = recx's length
*/
//static  ds_encoded_record_store_index_t     ds_store_index_t = {0};
/*
static bool user_save_encode_record_index_flash(ds_encoded_record_store_index_t * p_index_t)
{
    
}
*/
static bool write_repeated_var_struct(pb_ostream_t *stream, const pb_field_t fields[],  void * const *src_struct)
{
    
    bool res;
    bhv_head_t *m_bhv_head_t = (bhv_head_t *)*src_struct;
    res = pb_encode_tag_for_field(stream, fields);
    SEGGER_RTT_printf(0,"pb encode tag for struct is  %s\r\n", (res? "successed":"failed"));
    for(uint8_t i=0; i<m_bhv_head_t->record_count; i++)
    {
        res = pb_encode_submessage(stream, protocol_behavior2_fields, &m_bhv_head_t->bhv_t);
        SEGGER_RTT_printf(0,"encode behavior record %d is %s\r\n", i,(res? "successed":"failed"));
    }
    return res;
}
static bool write_repeated_var_string(pb_ostream_t *stream, const pb_field_t fields[],  void * const *str)
{
    bool res;
//    uint32_t *p = *str;
    res = pb_encode_tag_for_field(stream, fields);
    SEGGER_RTT_printf(0,"pb encode tag for string is  %s\r\n", (res? "successed":"failed"));
    res = pb_encode_string(stream,*str,strlen(*str));
    SEGGER_RTT_printf(0,"pb encode string \"%s\" is %s\r\n", *str, (res? "successed":"failed"));
    return res;
}
static bool read_repeated_var_struct(pb_istream_t *stream, const pb_field_t *field, void **src_struct)
{
    //uint8_t *p = *src_struct;
    //uint32_t tag;
    //pb_wire_type_t wire_type;
    //bool eof;
    bool res = false;
    SEGGER_RTT_printf(0,"decoding struct..., the bytes left is %d\r\n", stream->bytes_left);
    //res = pb_decode_tag(stream, &wire_type, &tag, &eof);
    //SEGGER_RTT_printf(0,"pb decode tag is %s\r\n", (res? "successed":"failed"));
    res = pb_decode(stream,protocol_behavior2_fields,*src_struct);
    SEGGER_RTT_printf(0,"pb decode struct is %s\r\n", (res? "successed":"failed"));
    return res;
}
static bool read_repeated_var_string(pb_istream_t *stream, const pb_field_t *field, void **str)
{
//    uint32_t tag;
//    uint32_t *p = *str;
//    pb_wire_type_t wire_type;
    pb_byte_t   cache_buf[stream->bytes_left];
    uint8_t byte_len = stream->bytes_left;
//    bool eof;
    bool res = false;
    res = pb_read(stream,cache_buf, stream->bytes_left);

    SEGGER_RTT_printf(0,"pb decode string \"%s\" is %s\r\n" , cache_buf, (res? "successed":"failed"));
    if(res == true)
    {
        memcpy(*str, cache_buf, byte_len);
    }
    else
    {
        *str = NULL;
        return false;
    }
    return true;
}
/**
* @functionname : user_data_pb_bhv_encode 
* @description  : function for encode behavior data, and saving them into buffer including to times 
* @input        : p_bhv_req_t: pointor to behavior struct, @ref protocol_behavior2_req_t
*               : p_bhv_t: pointor to behavior struct, @ref protocol_behavior2_t
*               : device_id, device id or uuid
*               :mannufactory_id, it's a mannufactory id
* @output       : none 
* @return       : true if successed
*/
/*
static bool user_data_pb_bhv_encode(//protocol_behavior2_req_t * p_bhv_req_t, 
                             protocol_behavior2_t * p_bhv_t[],
                             uint32_t record_num,
                             identity_msg p_identity_msg,
                             //void * device_id,
                             //void * manufactory_id)
                             uint8_t *bhv_encode_buffer,
                             uint16_t *encode_length
                            )
{
    protocol_behavior2_req_t * p_bhv_req_t;
    //uint8_t bhv_encode_buffer[sizeof(p_bhv_req_t)+ record_num*sizeof(p_bhv_t[0])];     //save data encoded from struct @ref protocol_behavior2_req_t
    
    bhv_head_t  bhv_head = {.record_count = record_num,
                            .bhv_t = p_bhv_t};
    
    //p_bhv_req_t->Iden.UUID.funcs.encode          = &write_repeated_var_string;
    //p_bhv_req_t->Iden.UUID.arg                   = &p_identity_msg.uuid;
    
    //p_bhv_req_t->Iden.ManufactureID.funcs.encode  = &write_repeated_var_string;
    //p_bhv_req_t->Iden.ManufactureID.arg           = &p_identity_msg.mfid;
        
    p_bhv_req_t->BehaviorInfo.funcs.encode        = &write_repeated_var_struct;
    p_bhv_req_t->BehaviorInfo.arg                 = &bhv_head;
                            
    p_bhv_req_t->Iden.RspCode  =   p_identity_msg.code;
    p_bhv_req_t->Iden.MsgToken =   p_identity_msg.token;
    p_bhv_req_t->Iden.MsgIndex =   p_identity_msg.index;
                            
    pb_ostream_t m_stream ;
    m_stream = pb_ostream_from_buffer(bhv_encode_buffer,sizeof((uint8_t *)bhv_encode_buffer));
    bool res = pb_encode(&m_stream,protocol_behavior2_req_fields,p_bhv_req_t);
    //store a record into memeory
    //hal_per_part_write_record(USER_MEMORY_PARTITION_RECORD_BEHAVIOR, bhv_encode_buffer, m_stream.bytes_written);
    
    if(res != true)
    {
        
#if defined(DEG_RTT) && (DEG_RTT == 1)
        SEGGER_RTT_printf(0,"encode is %s\r\n", "failed");
#endif  //DEG_RTT 
        //store the record length of number cnt                 
        return false;
    }
    *encode_length = m_stream.bytes_written;
    
#if defined(DEG_RTT) && (DEG_RTT == 1)
    SEGGER_RTT_printf(0,"encode is %s\r\n", "successed");
#endif  //DEG_RTT
    
    ds_store_index_t.bhv_store_index.index[ds_store_index_t.bhv_store_index.cnt++] = m_stream.bytes_written;
    user_save_encode_record_index_flash(&ds_store_index_t);
    return true;
}
*/
#define TEST_PB    1
#if defined(TEST_PB) && (TEST_PB == 1)

static  uint8_t app_buffer[1024] = {0};
uint8_t filed_index[3]  ={0};
char *p_uuid = "12345678";
char *p_id      = "111";
char buff_uuid[10];
char id_buf[10];
//static int cnt = 0;
static void user_data_pb_bhv_encode_test(void)
{  
    bool res;
    m_protocol_behavior2_t.has_ODBAX = true;
    m_protocol_behavior2_t.ODBAX     = 0x92;
    m_protocol_behavior2_t.has_ODBAY = true;
    m_protocol_behavior2_t.ODBAY     = 0x98;
    m_protocol_behavior2_t.has_ODBAZ = true;
    m_protocol_behavior2_t.ODBAZ     = 0x33;
    
    m_protocol_behavior2_t.has_MeandlX = true;
    m_protocol_behavior2_t.has_MeandlY = true;
    m_protocol_behavior2_t.has_MeandlZ = true;
    m_protocol_behavior2_t.MeandlX     = 0x22;
    m_protocol_behavior2_t.MeandlY     = 0x34;
    m_protocol_behavior2_t.MeandlZ     = 0x56;
    
    m_protocol_behavior2_req_t.Iden.has_RspCode= true;
    m_protocol_behavior2_req_t.Iden.RspCode    = 0x123;
    m_protocol_behavior2_req_t.Iden.MsgIndex    = 0x77889966;
    
    m_protocol_behavior2_req_t.Iden.DeviceID.funcs.encode    = &write_repeated_var_string;
    m_protocol_behavior2_req_t.Iden.DeviceID.arg             = p_uuid;
    
    //m_protocol_behavior2_req_t.Iden.ManufactureID.funcs.encode  = &write_repeated_var_string;
    //m_protocol_behavior2_req_t.Iden.ManufactureID.arg           = p_id;
    
    
    m_protocol_behavior2_req_t.BehaviorInfo.funcs.encode = &write_repeated_var_struct;
    m_protocol_behavior2_req_t.BehaviorInfo.arg          = &m_protocol_behavior2_t;
    
    pb_ostream_t m_stream ;
    m_stream = pb_ostream_from_buffer(app_buffer,sizeof(app_buffer));
    res = pb_encode(&m_stream,protocol_behavior2_req_fields,&m_protocol_behavior2_req_t);
    SEGGER_RTT_printf(0,"encode is %s\r\n", (res? "successed":"failed"));
}

static void user_data_pb_bhv_decode_test(void)
{
    pb_istream_t i_stream ;
    bool res;
    protocol_behavior2_req_t    bhv_req_t = {0};
    protocol_behavior2_t        bhv_t = {0};

    bhv_req_t.Iden.DeviceID.funcs.decode    = &read_repeated_var_string;
    bhv_req_t.Iden.DeviceID.arg             = buff_uuid;
    
    //bhv_req_t.Iden.ManufactureID.funcs.decode  = &read_repeated_var_string;
    //bhv_req_t.Iden.ManufactureID.arg           = id_buf;
    
    bhv_req_t.BehaviorInfo.funcs.decode = &read_repeated_var_struct;
    bhv_req_t.BehaviorInfo.arg          = &bhv_t;
    
    i_stream = pb_istream_from_buffer(app_buffer,sizeof(app_buffer));
    res = pb_decode(&i_stream,protocol_behavior2_req_fields,&bhv_req_t);
    
    SEGGER_RTT_printf(0,"decode is %s\r\n", (res? "successed":"failed"));
    
}
void user_encode_decode_test(void)
{
    user_data_pb_bhv_encode_test();
    user_data_pb_bhv_decode_test();
}
#endif  //TEST_PB



