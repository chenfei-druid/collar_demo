#ifndef     USER_APP_HAL_H
#define     USER_APP_HAL_H
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include "user_app_log.h"

typedef struct
{
    char        *mfid;
    char        *uuid;
    uint32_t    token;
    uint32_t    index;
    uint32_t    code;
}identity_msg;

bool app_send_behavior_record_handler(identity_msg p_identity_msg);

#endif

