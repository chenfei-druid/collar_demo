/**
 * user_task.h
 * Date:    2018/2/2
 * Author:  Chenfei
 * 
 * @brief smart-meter project.
 * by  nrf_52832 freertos platform
 *
 */
#ifndef __USER_TASK_H__
#define __USER_TASK_H__
#include <stdint.h>



void user_create_meter_application_task(void);
void user_creat_handler_task(void);

void user_tick_timer_handler(void);
void user_ext_interrupt_handler(void);
void user_creat_task_app(void);
uint32_t user_uart_tx_queue_send(uint8_t *p_buf, uint8_t len);
void user_give_uart_rx_semaphore(void);
void user_command_decode_handler(void);
void user_task_init(void);
void user_app_send_led_queue(uint32_t freq);

#endif          //__USER_TASK_H__

