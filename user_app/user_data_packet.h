
#ifndef _APP_DATA_PACAKE_
#define _APP_DATA_PACAKE_

#include <stdint.h>
#include <stdbool.h>
//#include "lora_hal.h"
//#include "user_ble_transmition.h"

typedef enum
{
    LORA_DATA_TYPE   = 0xF0,                //the packets number is LORA_PACKET_SIZE
    BLE_DATA_TYPE    = 0xf1                 //the packets number is BLE_PACKET_SIZE
} data_send_type;

typedef struct
{
    uint8_t     send_packet_buff[5000];     //the complete data will be send buff
    uint16_t    send_packet_data_length;
    uint8_t     attribute;                  //to distinuish the data is  LORA_DATA_TYPE or BLE_DATA_TYPE
}data_send_packet;

typedef struct 
{
    uint8_t     seq;                        //the sequence filed in the data struct, it will be change continous with adding 1 of next complet data
    uint8_t     cmd;                        //the command filed in the data struct, it is fixed.
    uint16_t    datafiled_len;              //the datafiled length, not including seq, cmd and crc
    uint8_t     datafiled[1500];            //the valid data of accelermeter and the channel for reconglizing completeness of packet
    uint16_t    crc;                        //verify all data
}transm_data_module;

typedef struct                              //the send data directly
{
    uint8_t     cmd;                        //
    uint16_t    uinit_len;                  // the numbers of one packet, it is for lora or ble
    uint16_t    total_len;                  // the total numbers of send data    
    uint8_t     send_buff[5000];            // the buff of send data directly
}unit_data_send_unpacket;



uint16_t add_check_package_channle(uint8_t * datafiled_p, uint8_t * orignal_data_p, uint16_t data_len);
void copy_module_data_to_send_buff(data_send_packet *data_send_packet_t, transm_data_module *transf_data_module_p);
void add_field_data_module(uint8_t *buff, transm_data_module *transf_data_module_p, uint16_t len);
void app_data_send_unpacket(unit_data_send_unpacket *unit_data_send_unpacket_t, data_send_packet *data_send_packet_p);


#endif  //_APP_DATA_PACAKE_
