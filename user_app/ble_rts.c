/**
 * ble_rts.c
 * Date:    2018/01/30
 * Author:  Chenfei
 * 
 * @brief nrf_52832 freertos template.
 *
 * This file contains rx and tx service application, and send and receive data on ble.
 *
 */
#include "sdk_common.h"
#if BLE_RTS
#include "ble.h"
#include "ble_rts.h"
#include "ble_srv_common.h"
#include "user_ble_inter_mgt.h"

#define BLE_UUID_RTS_TX_CHARACTERISTIC 0x0003                      /**< The UUID of the TX Characteristic. */
#define BLE_UUID_RTS_RX_CHARACTERISTIC 0x0002                      /**< The UUID of the RX Characteristic. */

#define BLE_RTS_MAX_RX_CHAR_LEN        BLE_RTS_MAX_DATA_LEN        /**< Maximum length of the RX Characteristic (in bytes). */
#define BLE_RTS_MAX_TX_CHAR_LEN        BLE_RTS_MAX_DATA_LEN        /**< Maximum length of the TX Characteristic (in bytes). */

#define RTS_BASE_UUID                  {{0x9E, 0xCA, 0xDC, 0x24, 0x0E, 0xE5, 0xA9, 0xE0, 0x93, 0xF3, 0xA3, 0xB5, 0x00, 0x00, 0x04, 0x01}} /**< Used vendor specific UUID. */

static  bool ble_notity_enable_flag = 0;

bool ble_rts_get_notity(void)
{
    return ble_notity_enable_flag;
}
/**@brief Function for handling the @ref BLE_GAP_EVT_CONNECTED event from the SoftDevice.
 *
 * @param[in] p_rts     Nordic UART Service structure.
 * @param[in] p_ble_evt Pointer to the event received from BLE stack.
 */
static void on_connect(ble_rts_t * p_rts, ble_evt_t const * p_ble_evt)
{
    p_rts->conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
}


/**@brief Function for handling the @ref BLE_GAP_EVT_DISCONNECTED event from the SoftDevice.
 *
 * @param[in] p_rts     Nordic UART Service structure.
 * @param[in] p_ble_evt Pointer to the event received from BLE stack.
 */
static void on_disconnect(ble_rts_t * p_rts, ble_evt_t const * p_ble_evt)
{
    UNUSED_PARAMETER(p_ble_evt);
    p_rts->conn_handle = BLE_CONN_HANDLE_INVALID;
}


/**@brief Function for handling the @ref BLE_GATTS_EVT_WRITE event from the SoftDevice.
 *
 * @param[in] p_rts     Nordic UART Service structure.
 * @param[in] p_ble_evt Pointer to the event received from BLE stack.
 */
static void on_write(ble_rts_t * p_rts, ble_evt_t const * p_ble_evt)
{
    ble_gatts_evt_write_t const * p_evt_write = &p_ble_evt->evt.gatts_evt.params.write;
    ble_rts_evt_t evt;
    evt.p_rts = p_rts;
    if (   (p_evt_write->handle == p_rts->tx_handles.cccd_handle)
        && (p_evt_write->len == 2))
    {
        if (ble_srv_is_notification_enabled(p_evt_write->data))
        {
            p_rts->is_notification_enabled = true;
            evt.type = BLE_RTS_EVT_COMM_STARTED;
            if(p_rts->conn_handle != BLE_CONN_HANDLE_INVALID)
            {
                ble_notity_enable_flag = 1;
            }
            user_ble_notify_enable();
        }
        else
        {
            p_rts->is_notification_enabled = false;
            evt.type = BLE_RTS_EVT_COMM_STOPPED;
            ble_notity_enable_flag = 0;
            //user_ble_notify_disable();
        }
        p_rts->data_handler(&evt);
    }
    else if (   (p_evt_write->handle == p_rts->rx_handles.value_handle)
             && (p_rts->data_handler != NULL))
    {
        evt.params.rx_data.p_data = p_evt_write->data;
        evt.params.rx_data.length = p_evt_write->len;
        evt.type = BLE_RTS_EVT_RX_DATA;
        p_rts->data_handler(&evt);
    }
    else
    {
        // Do Nothing. This event is not relevant for this service.
    }
}


/**@brief Function for adding TX characteristic.
 *
 * @param[in] p_rts       Nordic UART Service structure.
 * @param[in] p_rts_init  Information needed to initialize the service.
 *
 * @return NRF_SUCCESS on success, otherwise an error code.
 */
static uint32_t tx_char_add(ble_rts_t * p_rts, ble_rts_init_t const * p_rts_init)
{
    /**@snippet [Adding proprietary characteristic to the SoftDevice] */
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_md_t cccd_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;

    memset(&cccd_md, 0, sizeof(cccd_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);

    cccd_md.vloc = BLE_GATTS_VLOC_STACK;

    memset(&char_md, 0, sizeof(char_md));

    char_md.char_props.notify = 1;
    char_md.p_char_user_desc  = NULL;
    char_md.p_char_pf         = NULL;
    char_md.p_user_desc_md    = NULL;
    char_md.p_cccd_md         = &cccd_md;
    char_md.p_sccd_md         = NULL;

    ble_uuid.type = p_rts->uuid_type;
    ble_uuid.uuid = BLE_UUID_RTS_TX_CHARACTERISTIC;

    memset(&attr_md, 0, sizeof(attr_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);

    attr_md.vloc    = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth = 0;
    attr_md.wr_auth = 0;
    attr_md.vlen    = 1;

    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid    = &ble_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.init_len  = sizeof(uint8_t);
    attr_char_value.init_offs = 0;
    attr_char_value.max_len   = BLE_RTS_MAX_TX_CHAR_LEN;

    return sd_ble_gatts_characteristic_add(p_rts->service_handle,
                                           &char_md,
                                           &attr_char_value,
                                           &p_rts->tx_handles);
    /**@snippet [Adding proprietary characteristic to the SoftDevice] */
}


/**@brief Function for adding RX characteristic.
 *
 * @param[in] p_rts       Nordic UART Service structure.
 * @param[in] p_rts_init  Information needed to initialize the service.
 *
 * @return NRF_SUCCESS on success, otherwise an error code.
 */
static uint32_t rx_char_add(ble_rts_t * p_rts, const ble_rts_init_t * p_rts_init)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;

    memset(&char_md, 0, sizeof(char_md));

    char_md.char_props.write         = 1;
    char_md.char_props.write_wo_resp = 1;
    char_md.p_char_user_desc         = NULL;
    char_md.p_char_pf                = NULL;
    char_md.p_user_desc_md           = NULL;
    char_md.p_cccd_md                = NULL;
    char_md.p_sccd_md                = NULL;

    ble_uuid.type = p_rts->uuid_type;
    ble_uuid.uuid = BLE_UUID_RTS_RX_CHARACTERISTIC;

    memset(&attr_md, 0, sizeof(attr_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);

    attr_md.vloc    = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth = 0;
    attr_md.wr_auth = 0;
    attr_md.vlen    = 1;

    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid    = &ble_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.init_len  = 1;
    attr_char_value.init_offs = 0;
    attr_char_value.max_len   = BLE_RTS_MAX_RX_CHAR_LEN;

    return sd_ble_gatts_characteristic_add(p_rts->service_handle,
                                           &char_md,
                                           &attr_char_value,
                                           &p_rts->rx_handles);
}


void ble_rts_on_ble_evt(ble_evt_t const * p_ble_evt, void * p_context)
{
    if ((p_context == NULL) || (p_ble_evt == NULL))
    {
        return;
    }

    ble_rts_t * p_rts = (ble_rts_t *)p_context;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            on_connect(p_rts, p_ble_evt);
            break;

        case BLE_GAP_EVT_DISCONNECTED:
            on_disconnect(p_rts, p_ble_evt);
            break;

        case BLE_GATTS_EVT_WRITE:
            on_write(p_rts, p_ble_evt);
            break;

        case BLE_GATTS_EVT_HVN_TX_COMPLETE:
        {
            //notify with empty data that some tx was completed.
            ble_rts_evt_t evt = {
                    .type = BLE_RTS_EVT_TX_RDY,
                    .p_rts = p_rts
            };
            p_rts->data_handler(&evt);
            break;
        }
        default:
            // No implementation needed.
            break;
    }
}


uint32_t ble_rts_init(ble_rts_t * p_rts, ble_rts_init_t const * p_rts_init)
{
    uint32_t      err_code;
    ble_uuid_t    ble_uuid;
    ble_uuid128_t rts_base_uuid = RTS_BASE_UUID;

    VERIFY_PARAM_NOT_NULL(p_rts);
    VERIFY_PARAM_NOT_NULL(p_rts_init);

    // Initialize the service structure.
    p_rts->conn_handle             = BLE_CONN_HANDLE_INVALID;
    p_rts->data_handler            = p_rts_init->data_handler;
    p_rts->is_notification_enabled = false;

    /**@snippet [Adding proprietary Service to the SoftDevice] */
    // Add a custom base UUID.
    err_code = sd_ble_uuid_vs_add(&rts_base_uuid, &p_rts->uuid_type);
    VERIFY_SUCCESS(err_code);

    ble_uuid.type = p_rts->uuid_type;
    ble_uuid.uuid = BLE_UUID_RTS_SERVICE;

    // Add the service.
    err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY,
                                        &ble_uuid,
                                        &p_rts->service_handle);
    /**@snippet [Adding proprietary Service to the SoftDevice] */
    VERIFY_SUCCESS(err_code);

    // Add the RX Characteristic.
    err_code = rx_char_add(p_rts, p_rts_init);
    VERIFY_SUCCESS(err_code);

    // Add the TX Characteristic.
    err_code = tx_char_add(p_rts, p_rts_init);
    VERIFY_SUCCESS(err_code);

    return NRF_SUCCESS;
}


uint32_t ble_rts_string_send(ble_rts_t * p_rts, uint8_t * p_string, uint16_t * p_length)
{
    ble_gatts_hvx_params_t hvx_params;

    VERIFY_PARAM_NOT_NULL(p_rts);

    if ((p_rts->conn_handle == BLE_CONN_HANDLE_INVALID) || (!p_rts->is_notification_enabled))
    {
        return NRF_ERROR_INVALID_STATE;
    }

    if (*p_length > BLE_RTS_MAX_DATA_LEN)
    {
        return NRF_ERROR_INVALID_PARAM;
    }

    memset(&hvx_params, 0, sizeof(hvx_params));

    hvx_params.handle = p_rts->tx_handles.value_handle;
    hvx_params.p_data = p_string;
    hvx_params.p_len  = p_length;
    hvx_params.type   = BLE_GATT_HVX_NOTIFICATION;

    return sd_ble_gatts_hvx(p_rts->conn_handle, &hvx_params);
}

#endif // BLE_RTS
