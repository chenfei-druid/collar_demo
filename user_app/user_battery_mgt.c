
/** 
 * user_battery_mgt.c
 *
 * @group:       neck_strap project
 * @author:      Chenfei
 * @createdate: 2017/10/11
 * @modifydate: 2018/4/2
 * @version:     V0.0
 *
 */
 
 /*********************************includes**************************************/
#include "sdk_common.h"
#if NRF_MODULE_ENABLED(SAADC)
#include "nrf_drv_saadc.h"
#include "nrf_drv_gpiote.h"
#include "nrf_gpio.h"
#include "user_app.h"
#include "user_battery_mgt.h"
#include "freertos_platform.h"
#include "user_common_alg.h"
#include "user_app_log.h"

/********************************user defined  variable*************************************/
const uint16_t bat_voltage_table[] = {4200,4060,3980,3920,3870,3820,3790,3770,3740,3680,3450,3000};
const uint8_t  bat_power_table[] = {100 ,90  ,80  ,70  ,60  ,  50  ,40  ,30  ,20  ,10  ,5   ,0};
static uint32_t vol[10] = {0};
static uint8_t  per[10] = {0};
static uint32_t bat_last = 0;
/****************************defined functions begin******************************/
/**
* @function@name:    battery_level_update
* @description  :    Function for performing battery measurement and updating the Battery Level characteristic
* @input        :    value: result of adc
* @output       :    none
* @return       :    none
**/
static uint32_t drv_battery_voltage_caculate(uint32_t adc_value)
{	
    uint32_t vol_value;
	vol_value       =   adc_value*600*5/1024;   //mV, 增益为1/5,所以这里要乘以5, AD 12bit, ref_v=0.6V, 这里扩大了1000倍，结果为毫伏
    //DBG_LOG("Battery value: V/3 = %dmV\r\n", vol_value);
	vol_value       =   vol_value * 3 ;	 //分压3倍，四舍五入（-140mV补偿）
    return vol_value;
}

/**
* @function@name:    saadc_uninit
* @description  :    Function for uninitializing the SAADC. Stop all ongoing conversions and disables all channels.
* @input        :    value: result of adc
* @output       :    none
* @return       :    none
**/
/*
void saadc_uninit(void)
{
	nrf_drv_saadc_uninit();             //释放所有通道
}
*/
static void drv_battary_saadc_channle_uninit(void)
{
    nrf_drv_saadc_channel_uninit(HAL_BAT_CHN);
}


/**
* @function@name:    saadc_init
* @description  :    Function for initializing the SAADC.
*                    NRF_SAADC_INPUT_AIN4 --  BAT_SMP, battery measure
* @input        :    value: result of adc
* @output       :    none
* @return       :    none
**/
static void drv_battery_saadc_channel_init(void)
{
    ret_code_t err_code;
    
    //battery measure adc channel
    nrf_saadc_channel_config_t channel_config_bat = NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(HAL_BAT_ADC_IN_CHN);  
    channel_config_bat.gain = NRF_SAADC_GAIN1_5;        //增益为1/5
    err_code = nrf_drv_saadc_channel_init(HAL_BAT_CHN, &channel_config_bat);
    APP_ERROR_CHECK(err_code);    
}

/**
* @function@name:    battery_level_measurment
* @description  :    Function for performing battery measurement and updating the Battery Level characteristic.
* @input        :    none
* @output       :    none
* @return       :    none
*/
#define BAT_SAMPLES     20
static int32_t drv_battery_level_measurment(void)
{
    uint32_t battery_value;
    int16_t adc_result[BAT_SAMPLES];
    uint32_t sum = 0;
    uint32_t avg;
    uint8_t i;
    nrf_saadc_enable(); 
    vTaskDelay(10);
    //nrf_drv_saadc_sample_convert(HAL_BAT_CHN, (int16_t *)&avg);
    
    for(i=0;i<BAT_SAMPLES;i++)
    {
        nrf_drv_saadc_sample_convert(HAL_BAT_CHN, &adc_result[i]);  
        vTaskDelay(1);
    }    
    nrf_saadc_disable();
    user_app_rank_min_to_max_16(adc_result,BAT_SAMPLES);
    sum = 0;
    for(i=7;i<15; i++)       //
    {
        sum += adc_result[i];
        //DBG_LOG("\r\nnumber %d adc value = %d\r\n",i,adc_result[i]);
    }
    avg = sum / 8;
    
    battery_value = drv_battery_voltage_caculate(avg);  
    //DBG_LOG("\r\nbattary vol = %dmV\r\n",battery_value);
    return battery_value;
}
/**
* @function@name:    user_battery_mgt_init
* @description  :    Function for initializing the battery management module, initialize adc module.
* @input        :    none
* @output       :    none
* @return       :    none
*/
void user_battery_mgt_init(void)
{
    drv_battery_saadc_channel_init();
}
/**
* @function@name:    user_battery_mgt_uninit
* @description  :    Function for close battery voltage sample module.
* @input        :    none
* @output       :    none
* @return       :    none
*/
void user_battery_mgt_uninit(void)
{
    drv_battary_saadc_channle_uninit();
}
/**
* @function@name:    drv_get_battery_voltage
* @description  :    Function for getting battary voltage.
* @input        :    none
* @output       :    none
* @return       :    battary voltage, *1000, mV;
*/
int32_t drv_get_battery_voltage(void)
{
    int32_t bat_vol = 0;
    int32_t sum = 0;
    uint8_t i,k;
    uint8_t len = sizeof(vol)/sizeof(&vol[0]);
    //DBG_LOG("battary voltage converting\r\n");
    bat_vol = drv_battery_level_measurment();
    for(i=0;i<len;i++)
    {
        if(vol[i] == 0)
        {
            break;
        }
        sum += vol[i];
    }
    bat_vol = (bat_vol + sum) / (i+1);
    if(i == len)
    {        
        for(k = 0; k < i - 1; k++)
        {
            vol[k] = vol[k+1];
        }
        vol[k] = bat_vol;
    }
    else
    {
        vol[i] = bat_vol;
    }
    //DBG_LOG("get battery voltage = %dmV",bat_vol);
    bat_last = bat_vol;
    return bat_vol;
}
/**
* @function@name:    drv_get_battery_voltage
* @description  :    Function for getting battary voltage.
* @input        :    none
* @output       :    none
* @return       :    battary voltage, *1000, mV;
*/
int32_t drv_get_battary_power_percentage(void)
{
    int32_t bat_vol;
    int32_t pwr_per;
    int32_t vol_unit_range;
    
    bat_vol = bat_last;
    //bat_vol = drv_battery_level_measurment();
    int i;
    for(i = 0; i< sizeof(bat_voltage_table) - 1; i++)
    {
        if(bat_vol <= bat_voltage_table[i] && bat_vol >= bat_voltage_table[i+1])
        {
            vol_unit_range = bat_voltage_table[i] - bat_voltage_table[i+1];
            pwr_per = 10*(bat_vol - bat_voltage_table[i+1]) / vol_unit_range + bat_power_table[i+1];
            //DBG_LOG("power value section is %d:%d ~ %d:%d",i,bat_voltage_table[i],i+1,bat_voltage_table[i+1]);
            break;
        }
    }
    int32_t sum = 0;
    uint8_t k;
    uint8_t len = sizeof(per)/sizeof(&per[0]);
    for(i=0;i<len;i++)
    {
        if(per[i] == 0)
        {
            break;
        }
        sum += per[i];
    }
    pwr_per = (pwr_per + sum) / (i+1);
    if(i == len)
    {        
        for(k = 0; k < i - 1; k++)
        {
            per[k] = per[k+1];
        }
        per[k] = pwr_per;
    }
    else
    {
        per[i] = pwr_per;
    }
    //DBG_LOG("get power percent = %d",pwr_per);
    return pwr_per;
}
#endif      //NRF_MODULE_ENABLED(SAADC)
 


