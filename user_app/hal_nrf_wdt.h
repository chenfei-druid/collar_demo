#ifndef HAL_NRF_WDT_H
#define HAL_NRF_WDT_H

#define FEED_BUTTON_ID          0                           /**< Button for feeding the dog. */
#define RELOAD_VALUE            65000
#define RESET_FLAG_RAM_SIZE     USER_RAM_PARAM_SIZE

void hal_create_wdt_task(void);
void hal_wdt_disable(void);
void hal_wdt_feed(void);
void wdt_event_handler(void);
void user_system_reset(void);
#endif  //HAL_NRF_WDT_H

