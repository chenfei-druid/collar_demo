/** 
 * user_temperature.c
 *
 * @group    :      graziery_management
 * @author   :      Chen.fei
 * @create   :      2017/10/11
 * @version  :      V0.0
 * @copyright:      Copyright (c) 2017 - 2017, Druid Tech      
 *
 */

/*********************************includes**************************************/
#include "nrf_temp.h"
#include "user_app_log.h"
#include "freertos_platform.h"
#include "user_common_alg.h"

/********************************user defined*************************************/
//extern bool     temperature_timer_flag;

static int32_t temper[5] = {0};

/****************************defined functions begin******************************/
/**
* @function@name:    user_temp_init
* @description  :    Function for initianizing temperature measurement.
* @input        :    none
* @output       :    none
* @return       :    none
*/
void user_temp_init(void)
{
    nrf_temp_init();            
}

/**
* @function@name:    temperature_read_on_soc
* @description  :    Function for reading the SOC temperature measurement result value.
* @input        :    none
* @output       :    temp_value: pointor to the temperature variable
* @return       :    true: read ok false: read failed
*/
#define SAMPLE  50
static int32_t temperature_read_on_soc(void)
{
    //int32_t temp = 0; 
    int32_t temp_value[SAMPLE] = {0};
    int32_t sum=0, temp_result;
    int i;
    for(i=0;i<SAMPLE;i++)
    {
        sd_temp_get(&temp_value[i]);
        vTaskDelay(2);
    }
    uint32_t len = user_app_rank_min_to_max_32(temp_value,SAMPLE);
    sum=0;
    for(i=10;i<40;i++)
    {
        sum += temp_value[i];
        //DBG_LOG("\r\nnumber %d temperature = %d\r\n",i,temp_value[i]);
    }
    temp_result = sum/30;
    temp_result = (100 * temp_result / 4 +5)/10;
    //DBG_LOG("\r\nget soc temperature = %d\r\n",temp_result);
    return temp_result;
}

/**
* @function@name:    user_temperature_read_process
* @description  :    user process for temperature measurement.
* @input        :    none
* @output       :    none
* @return       :    none
*/
int32_t user_get_temperature(void)
{
    int32_t temp = temperature_read_on_soc() - 50;
    /*
    int32_t sum = 0;
    uint8_t i,k;
    
    uint8_t len = sizeof(temper)/sizeof(&temper[0]);

    for(i=0;i<len;i++)
    {
        if(temper[i] == 0)
        {
            break;
        }
        sum += temper[i];
    }
    temp = (temp + sum) / (i+1);
    if(i == len)
    {        
        for(k = 0; k < i - 1; k++)
        {
            temper[k] = temper[k+1];
        }
        temper[k] = temp;
    }
    else
    {
        temper[i] = temp;
    }
    */
    //DBG_LOG("\r\nlocal temperature = %d\r\n",temp);
    return temp;
}
