
#ifndef USER_BATTERY_MGT_H
#define USER_BATTERY_MGT_H

#include <stdint.h>
#include <stdbool.h>

#define     HAL_BAT_ADC_IN_CHN      NRF_SAADC_INPUT_AIN2
#define     HAL_BAT_CHN             2

#define     HAL_BAT_MAX_VOLTAGE     4150
#define     HAL_BAT_MIN_VOLTAGE     3600

void user_battery_mgt_init(void);
void user_battery_mgt_uninit(void);
int32_t drv_get_battery_voltage(void);
int32_t drv_get_battary_power_percentage(void);
#endif  //USER_BATTERY_MGT_H
