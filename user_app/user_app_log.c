#include <stdint.h>
#include <string.h>
//#include <stdlib.h>
#include <stdio.h>
//#include <math.h>
#include "hal_nrf_uart.h"
#include "user_app_log.h"
#include "HexStr.h"


/**
* @function@name:    user_app_uart_config
* @description  :    Function for initializing application
* @input        :    none
* @output       :    none
* @return       :    none
*/
 /*
void user_app_log_init(void)
{
   
    static uart_attr    app_uart_attr ;

    app_uart_attr.uart_params_t.baud_rate     = UART_BAUDRATE_BAUDRATE_Baud115200;
    app_uart_attr.uart_params_t.flow_control  = APP_UART_FLOW_CONTROL_DISABLED;
    app_uart_attr.uart_params_t.rx_pin_no     = UART_DEBUG_PIN_RXD;
    app_uart_attr.uart_params_t.tx_pin_no     = UART_DEBUG_PIN_TXD;
    hal_uart_chnl_config(&app_uart_attr,UART_CHANNEL_DEBUG);
    
}

void user_app_log_chn_switch(void)
{
    hal_uart_change_channel(UART_CHANNEL_DEBUG);
}
void user_app_print_log(uint8_t * const p_log, uint16_t len)
{
    //hal_uart_change_channel(UART_CHANNEL_DEBUG);
    hal_uart_send_data(p_log,len);
}

void user_app_print_string(char * str)
{
    hal_uart_send_string(str);
}
void user_app_print_int(uint32_t dt)
{
    char int_var[12] = {0};
    sprintf(int_var,"%d",dt);
    user_app_print_string(int_var);
    user_app_print_string("\r\n\0");
}
*/
/*
void printch(const char ch)   //????  
{    
    putchar(ch);    
}    
  
  
void printint(const int dec)     //?????  
{    
    if(dec == 0)    
    {    
        return;    
    }    
    printint(dec / 10);    
    putchar((char)(dec % 10 + '0'));    
}    
  
  
void printstr(const char *ptr)        //?????  
{    
    while(*ptr)    
    {    
        putchar(*ptr);    
        ptr++;    
    }    
}    
  
  
void printfloat(const float flt)     //?????,????5?????  
{    
    int tmpint = (int)flt;    
    int tmpflt = (int)(100000 * (flt - tmpint));    
    if(tmpflt % 10 >= 5)    
    {    
        tmpflt = tmpflt / 10 + 1;    
    }    
    else    
    {    
        tmpflt = tmpflt / 10;    
    }    
    printint(tmpint);    
    putchar('.');    
    printint(tmpflt);    
  
}    
  
  
void my_printf(const char *format,...)    
{    
    va_list ap;    
    va_start(ap,format);     //?ap????????????  
    while(*format)    
    {    
        if(*format != '%')    
        {    
            putchar(*format);    
            format++;    
        }    
        else    
        {    
            format++;    
            switch(*format)    
            {    
                case 'c':    
                {    
                    char valch = va_arg(ap,int);  //????????????  
                    printch(valch);    
                    format++;    
                    break;    
                }    
                case 'd':    
                {    
                    int valint = va_arg(ap,int);    
                    printint(valint);    
                    format++;    
                    break;    
                }    
                case 's':    
                {    
                    char *valstr = va_arg(ap,char *);    
                    printstr(valstr);    
                    format++;    
                    break;    
                }    
                case 'f':    
                {    
                    float valflt = va_arg(ap,double);    
                    printfloat(valflt);    
                    format++;    
                    break;    
                }    
                default:    
                {    
                    printch(*format);    
                    format++;    
                }    
            }      
        }    
    }  
    va_end(ap);           
}
*/

