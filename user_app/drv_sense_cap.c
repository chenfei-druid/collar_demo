/** 
 * drv_sense_capacitor.c
 *
 * @group:       neck_strap project
 * @author:      Chenfei
 * @create:      2018/4/3
 * @version:     V0.0
 *
 */
 /************************************includes******************************/
#include "sdk_common.h"
#include "nrf_drv_saadc.h"
#include "nrf_drv_gpiote.h"
#include "nrf_gpio.h"
#include "SEGGER_RTT.h"
#include "SEGGER_RTT_Conf.h"
#include "drv_sense_cap.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"
#include "user_app_log.h"

/************************************defines**************************************/
/**
* @function@name:    drv_sns_cap_init
* @description  :    Function for initializing the sense captor adc detect interface, and driver pin
* @input        :    none
* @output       :    none
* @return       :    none
**/ 
void drv_sns_cap_init(void)
{
    ret_code_t err_code;

    /*illumination intensity measure adc channel*/
    nrf_saadc_channel_config_t chn_cfg_sns_cap = NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_AIN1);
    chn_cfg_sns_cap.gain = NRF_SAADC_GAIN1_5;        //增益为1/5
    chn_cfg_sns_cap.acq_time = NRF_SAADC_ACQTIME_20US;
    err_code = nrf_drv_saadc_channel_init(DRV_SNS_CAP_ADC_CHN, &chn_cfg_sns_cap);
    APP_ERROR_CHECK(err_code);
    
    nrf_gpio_cfg_output(DRV_SNS_CAP_LEVEL_PIN);
    nrf_gpio_pin_clear(DRV_SNS_CAP_LEVEL_PIN);    
}
void drv_sns_cap_uninit(void)
{
    nrf_gpio_pin_clear(DRV_SNS_CAP_LEVEL_PIN);
    nrf_drv_saadc_channel_uninit(DRV_SNS_CAP_ADC_CHN);
}
/**
* @function@name:    illumination_intensity_update
* @description  :    Function for performing battery measurement and updating the Battery Level characteristic
* @input        :    value: result of adc
* @output       :    none
* @return       :    none
**/
static uint32_t drv_sns_cap_vol_caculate(int16_t const adc_value)
{	
    uint16_t vol_value;
	vol_value       =   adc_value*600*5/1024;                   //mV， 增益为1/5,所以这里要乘以5, AD 12bit, ref_v=0.6V, 这里扩大了1000倍，结果为毫伏   
    return vol_value;
}

/**
* @function@name:    drv_sns_cap_vol_msr
* @description  :    Function for performing ADC for sensor capacitor sampled. 
* @input        :    none
* @output       :    none
* @return       :    none
*/
bool drv_sns_cap_vol_msr(uint16_t * const level)
{
    int16_t adc_result[50];
    uint32_t sum = 0;
    uint16_t adc_avg = 0;
    //int16_t adc_result1, adc_result2, adc_result3;   
    
    nrf_gpio_pin_set(DRV_SNS_CAP_LEVEL_PIN);            //starting to charge the capacitor
    vTaskDelay(1);
    nrf_saadc_enable();                     //enable AD convertor
    for(uint8_t i=0;i<sizeof(adc_result);i++)
    {
        nrf_drv_saadc_sample_convert(DRV_SNS_CAP_ADC_CHN, &adc_result[i]);  
        sum += adc_result[i];
        vTaskDelay(1);
    }
    nrf_saadc_disable();
    adc_avg = sum / sizeof(adc_result);
    *level = drv_sns_cap_vol_caculate(adc_avg);
    DBG_LOG("cap voltage = %d\r\n", *level);

    return true;
}

bool drv_sns_cap_fall_detect(void)
{
    uint16_t cap_level = 0;
    
    drv_sns_cap_vol_msr(&cap_level);
    
    if(cap_level < DRV_DEV_FALL_THRESHOLD)
    {
        return false;
    }
    return true;
}

