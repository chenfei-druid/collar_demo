#ifndef DRV_NB_MODUL_H
#define DRV_NB_MODUL_H

#include "FreeRTOS.h"
#include "semphr.h"
/*&****************************************************************************************************************
    optional int32 CommunicationMode = 10 [default=1]; // ????: 0 - ???, 1 - ????, 2 - ????(??eDRX), 3 - ????
    optional int32 CommunicationInterval = 11 [default=0];	// ????: ?
    optional string CommunicationTimeTable = 12 [default=""];	// ???????
***************************************************************************************************************** */   
//#define     S       CONCAT_2(*,1000)
#define     DRV_NB_TXD_UART_MCU_RXD_PIN     15
#define     PIN_NB_RXD_UART_MCU_TXD_PIN     14

#define     DRV_NB_RST_PIN                  13
#define     DRV_NB_RI_PIN                   19
#define     DRV_NB_PWR_PIN                  18

#define     DRV_NB_UNIT_SEND_DATA_LEN       (256-32)

#define     DRV_NB_DEFAULT_UP_INTERVAL      28800//(SECOND(60))//3600    //S
#define     WAIT_SERVER_RESPONSE            25

typedef enum
{
    DRV_NB_COMMU_MODE_UNDEFINE = 0,
    DRV_NB_COMMU_MODE_INTERVAL,
    DRV_NB_COMMU_MODE_ONLINE,
    DRV_NB_COMMU_MODE_TIMER,
    DRV_NB_COMMU_MODE_MAX,
}nb_commu_mode;

typedef enum
{
    NB_TRANS_TASK_RUN_TYPE_SETTING = 1,
    NB_TRANS_TASK_RUN_TYPE_WARNING,
    NB_TRANS_TASK_RUN_TYPE_NORMAL,
    NB_TRANS_TASK_RUN_TYPE_MAX,
}nb_trans_task_run_type;
//transmit message on a'clock list ,length is 24, means 0~23 o'clock,
                                                    //every one is ascii code of '0' or '1', 
                                                    //if '1', it will transmit at corresponding clock  
typedef struct
{
    int32_t     mode;
    int32_t     interval;
    char        time_list[24];
}nb_comm_setting_t;

extern QueueHandle_t    queue_nb_tx;

void user_creat_nb_timer_transmit_task(void);
void user_creat_nb_receive_handler_task(void);
//uint32_t drv_nb_send_data(uint8_t *dt, uint16_t len);
bool drv_get_nb_tx_complete_flag(void);
uint32_t drv_get_nb_max_send_len(void);
int32_t drv_nb_receive_response(void);
bool drv_nb_send_trans_queue(uint8_t *p_data, int16_t len);
bool drv_nb_trans_data(uint8_t *dt, uint8_t len);
bool drv_nb_restart(void);
//suspend the timer to transmit task
void user_suspend_nb_timer_transmit_task(void);
//resume the timer to transmit task
void user_resume_nb_timer_transmit_task(void);
void user_suspend_nb_receive_handler_task(void);
//resume the timer to transmit task
void user_resume_nb_receive_handler_task(void);
void user_nb_send_semphore_to_rx_task(void);
void user_nb_send_semphore_to_tx_task(void);
void user_get_nb_imei(char * const imei);
void user_get_nb_imsi(char * const imsi);
bool user_nb_communication_setting(nb_comm_setting_t setting);
void user_nb_send_queue_to_task(uint8_t type);
#endif  //DRV_NB_MODUL_H
