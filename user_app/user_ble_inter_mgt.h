#ifndef USER_BLE_INTER_MGT
#define USER_BLE_INTER_MGT
#include <stdint.h>
#include <stdbool.h>

typedef enum
{
    BLE_TRANSMIT_ENABLE,
    BLE_TRANSMIT_DISABLE,
    
}semaphore_ble_type;

enum split_frame_type
{
    SPLIT_FRAME_TYPE_MIDDLE = 0x00,
    SPLIT_FRAME_TYPE_START= 0x01,
    SPLIT_FRAME_TYPE_LAST = 0x02,
    SPLIT_FRAME_TYPE_TOTAL = 0x03
};

typedef enum 
{
    CONSTRUCT_MODE_INVALID =0,
    CONSTRUCT_MODE_CONSTRUCTING ,
    CONSTRUCT_MODE_CONSTRUCTED
}construct_mode_t;

typedef struct 
{
    uint8_t type:4; // low 4 bit is type
    uint8_t version:4;// high 4 bit is version
}split_head_t;

typedef struct
{
    uint16_t len;
    uint8_t  *dt;
}ble_rsp_t;
void user_give_ble_semaphore(semaphore_ble_type);
void user_ble_send_queue_to_task(uint8_t *p_queue, uint8_t len);
void user_ble_notify_enable(void);
void user_ble_notify_disable(void);
void user_creat_ble_transmit_task(void);
void user_creat_ble_receive_task(void);
uint32_t user_ble_get_rx_data(uint8_t *p_buf);
void user_suspend_ble_trans_task(void);
void user_resume_ble_trans_task(void);

#endif ///USER_BLE_INTER_MGT
