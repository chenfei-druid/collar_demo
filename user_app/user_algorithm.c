
/** 
 * user_algorithm.c
 *
 * @group:       nack_trap project
 * @author:      Chenfei
 * @createdate: 2018/4/2
 * @modifydate: 2018/4/2
 * @version:     V0.0
 *
 */
 
#include <math.h>
#include "string.h"

#include "user_ble_application.h"
//#include "nrf_delay.h"
#include "nrf_drv_gpiote.h"
#include "hal_acc.h"
#include "SEGGER_RTT.h"
#include "SEGGER_RTT_Conf.h"

#define     ALG_DATA_WINDOW_LEN_MAX     51

#if defined(ALG_DATA_VALID_BIT_NUMS) && (ALG_DATA_VALID_BIT_NUMS == BIT_16)
static    acc_data_point_16_t       acc_last_orig_data_point_t[ALG_DATA_WINDOW_POINTS_NUMS/2];
static    acc_data_point_16_t       acc_new_orig_data_point_t[ALG_DATA_WINDOW_POINTS_NUMS];
static    acc_data_point_16_t       acc_buf[ALG_DATA_WINDOW_LEN_MAX];
static    acc_data_point_16_t       sba_buf[ALG_DATA_WINDOW_LEN_MAX];
#elif   defined(ALG_DATA_VALID_BIT_NUMS) && (ALG_DATA_VALID_BIT_NUMS == BIT_8)
static    acc_data_point_8_t        acc_last_orig_data_point_t[ALG_DATA_WINDOW_POINTS_NUMS/2];
static    acc_data_point_8_t       acc_new_orig_data_point_t[ALG_DATA_WINDOW_POINTS_NUMS];
#endif

/*
* function name: user_moving_mean
* description  : 求滑动平均值, 同时原始数据丢掉前后 (depth/2)个数据， 深度最好是奇数
* ----------------------------------------------------------------
*                mean(0) = [x0+x1+x2+...+x(0+depth-1)] / depth
*                mean(1) = x1+x2+x3+...+x(1+depth-1)
*                mean(2) = x2+x3+x4+...+x(2+depth-1)
*                 ...
*                mean(n) = x(n)+x(n+1)+x(n+2)+...+x(n+depth-1)
* ----------------------------------------------------------------
* input        : buffer 原始数据串，len为数据总长度,depth为求平均的深度
* output       : user_moving_mean_t，该结构体存储计算得到的平均值，及平均值的个数
*/
#if (HAL_ACC_ONE_POINT_DATA_LEN == 3)             //8位数据
static void user_moving_mean(int8_t * buffer_s, int8_t *  buffer_d, uint8_t *const p_length, uint8_t const depth)  //0.53ms
#elif (HAL_ACC_ONE_POINT_DATA_LEN == 6)             //如果是12位数据
static void user_moving_mean(int16_t * buffer_s, int16_t * const buffer_d, uint8_t *const p_length, uint8_t const depth)  //0.53ms
#endif
{
    uint32_t i,j;
    uint32_t sum=0;
    uint32_t    len = 0;
    //*p_length = 0;
    for(i=0; i<*p_length; i++)       //滑动平均，深度为depth，所以
    {
        if(i <= *p_length-depth)
        {
            for(j=0;j<depth;j++)
            {
                sum += buffer_s[i+j];
            }
            buffer_d[i] = (sum*10/depth+5)/10;        //round-off
            //buffer_d->data_len++;
            len++;
            buffer_s[i] = buffer_s[i+depth/2];  //从第depth/2个数据开始， 到len-depth/2个数据结束， 前后各丢弃depth/2个数据
            sum = 0;
        }
        else
        {
            buffer_s[i] = 0;        //后面的depth 个空间赋值0
        }       
    }    
    *p_length = len;
}
/*
* function name: user_average_compute
* description  : 求一组数据的算术平均值
* input        : buffer 指向原始数据的指针，len为数据总长度
* output       : average得出的平均值
*/
static void user_average_compute(const int32_t * const buffer, uint32_t const len, uint32_t *const average)
{
    uint32_t i;
    uint32_t sum=0;
    
    for(i=0;i<len;i++)       //
    {
        sum += buffer[i];
    }
    *average = sum / len;///////////////////////
}
/*
* function name: user_variance_compute
* description  : 求一组数据的方差
* input        : buffer 指向原始数据的指针，len为数据总长度
* output       : variance得出的方差值
*/
static void user_variance_compute(const int32_t * const buffer, uint32_t len, uint32_t *const variance)
{
    uint32_t i;
    uint32_t average=0;
    uint32_t sum=0;
    
    user_average_compute(buffer, len,&average);
    for(i=0;i<len;i++)       //
    {
        sum += (buffer[i] - average) * (buffer[i] - average);
    }
    *variance = sqrt(sum);      //0.11 mS
}
/*
* function name: user_absolute_mean_compute
* description  : 计算两组数据各个点的相对值，并计算这些相对值的平均值
* input        : buffer 指向原始数据的指针，len为数据总长度
* output       : absolute_mean 计算结果
*/
#if (HAL_ACC_ONE_POINT_DATA_LEN == 3)             //8位数据
static void user_deviation_mean_compute(int8_t const * const buffer1, int8_t const * const buffer2, uint8_t const len,uint8_t *const abs_avg)
#elif (HAL_ACC_ONE_POINT_DATA_LEN == 6)             //如果是12位数据
static void user_deviation_mean_compute(int16_t const * const buffer1, int16_t const * const buffer2, uint16_t const len,uint16_t *const abs_avg)
#endif
{
    int32_t temp = 0;
    uint32_t sum = 0;
    for(uint16_t i=0; i < len; i++)
    {
        temp = buffer1[i] - buffer2[i];
        sum += temp >= 0 ? temp : (-temp);
    }
    *abs_avg = sum / len;
    
}
/*
* function name: user_sensor_data_compute
* description  : 分步计算各轴的滑动平均，并用原始数据与滑动平均求差，然后把各轴的差值求平均，然后在把各轴的平均值相加
*               X,Y,Z: average(|原始数据-滑动平均|)
*               X_Y+Z
* input        : g_sensor_xyz_t 指向原始数据的指针，accumulate: 计算结果
* output       : none
*/
void user_sensor_data_compute(g_sensor_xyz_s g_sensor_xyz_t, uint32_t * const accumulate)
{
    uint8_t absolute_mean_x,absolute_mean_y,absolute_mean_z;    //各轴与滑动平均值差的平均值
    g_sensor_xyz_s snr_moving_mean_data_xyz;        //存放滑动平均值结果
    
    //滑动平均值     (3.8mS)
    snr_moving_mean_data_xyz.samples_x_len = g_sensor_xyz_t.samples_number;
    user_moving_mean(g_sensor_xyz_t.samples_x_axis, snr_moving_mean_data_xyz.samples_x_axis,&snr_moving_mean_data_xyz.samples_x_len,USER_MOVING_MEAN_DEPTH);
    snr_moving_mean_data_xyz.samples_y_len = g_sensor_xyz_t.samples_number;
    user_moving_mean(g_sensor_xyz_t.samples_y_axis, snr_moving_mean_data_xyz.samples_y_axis,&snr_moving_mean_data_xyz.samples_y_len,USER_MOVING_MEAN_DEPTH);
    snr_moving_mean_data_xyz.samples_z_len = g_sensor_xyz_t.samples_number;
    user_moving_mean(g_sensor_xyz_t.samples_z_axis, snr_moving_mean_data_xyz.samples_z_axis,&snr_moving_mean_data_xyz.samples_z_len,USER_MOVING_MEAN_DEPTH);
   
    //求每个轴的原始数据与滑动平均值各个点的差的平均值, 第一个为原始数据，第二个参数为滑动平均值， 第三个参数为计算后的个轴平均值
    user_deviation_mean_compute(g_sensor_xyz_t.samples_x_axis, snr_moving_mean_data_xyz.samples_x_axis, snr_moving_mean_data_xyz.samples_x_len, &absolute_mean_x);
    user_deviation_mean_compute(g_sensor_xyz_t.samples_y_axis, snr_moving_mean_data_xyz.samples_y_axis, snr_moving_mean_data_xyz.samples_y_len, &absolute_mean_y);
    user_deviation_mean_compute(g_sensor_xyz_t.samples_z_axis, snr_moving_mean_data_xyz.samples_z_axis, snr_moving_mean_data_xyz.samples_z_len, &absolute_mean_z);
    
    //将三个轴的绝对平均值相加
    *accumulate = absolute_mean_x + absolute_mean_y + absolute_mean_z;
}
/*
* function name: user_org_data_to_xyz_data
* description  : 将加速度原始数据 提取出来，分别转换成3个轴对应的数据，并分别存储在g_sensor_xyz_t结构体中
* input        : g_sensor_data_s_p 指向原始数据结构体的指针，存放原始加速度数据
* output       : g_sensor_xyz_t 分别以X,Y,Z存放加速度数据
*/
void user_org_data_to_xyz_data(hal_snr_data const g_sensor_data_s_p, g_sensor_xyz_s * const p_g_sensor_xyz_t)
{
    uint32_t i;
    p_g_sensor_xyz_t->samples_number = g_sensor_data_s_p.data_len/HAL_ACC_ONE_POINT_DATA_LEN;
    
    if(HAL_ACC_ONE_POINT_DATA_LEN == 6)           //12位数据，占两个自己，三轴就是6个字节
    {
        for(i=0;i<p_g_sensor_xyz_t->samples_number;i++)       //
        {

            p_g_sensor_xyz_t->samples_x_axis[i] = g_sensor_data_s_p.data_s[i*HAL_ACC_ONE_POINT_DATA_LEN]   + ((uint16_t)(g_sensor_data_s_p.data_s[i*HAL_ACC_ONE_POINT_DATA_LEN+1]& 0x0f)<<8);
            p_g_sensor_xyz_t->samples_y_axis[i] = g_sensor_data_s_p.data_s[i*HAL_ACC_ONE_POINT_DATA_LEN+2] + ((uint16_t)(g_sensor_data_s_p.data_s[i*HAL_ACC_ONE_POINT_DATA_LEN+3]& 0x0f)<<8);
            p_g_sensor_xyz_t->samples_z_axis[i] = g_sensor_data_s_p.data_s[i*HAL_ACC_ONE_POINT_DATA_LEN+4] + ((uint16_t)(g_sensor_data_s_p.data_s[i*HAL_ACC_ONE_POINT_DATA_LEN+5]& 0x0f)<<8);
            //如果是负数的话，是以补码方式存放，所以这里还需要将补码转换为整型负数
            //判断X轴正负
            if(p_g_sensor_xyz_t->samples_x_axis[i] & 0x0800 )    //negative
            {
                p_g_sensor_xyz_t->samples_x_axis[i] =  ((~((p_g_sensor_xyz_t->samples_x_axis[i]&0x7ff) - 1))&0x7ff) | 0x8000;
                //g_sensor_xyz_t.samples_x_axis[i] = (0x0fff - (g_sensor_xyz_t.samples_x_axis[i]&0x07ff) + 1) | 0x8000;
            }
            //判断Y轴正负
            if(p_g_sensor_xyz_t->samples_y_axis[i] & 0x0800 )    //negative
            {
                p_g_sensor_xyz_t->samples_y_axis[i] = ((~((p_g_sensor_xyz_t->samples_y_axis[i]&0x7ff) - 1))&0x7ff) | 0x8000;
            }
            //判断Z轴正负        
            if(p_g_sensor_xyz_t->samples_z_axis[i] & 0x0800 )    //negative
            {
                p_g_sensor_xyz_t->samples_z_axis[i] = ((~((p_g_sensor_xyz_t->samples_z_axis[i]&0x7ff) - 1))&0x7ff) | 0x8000;
            }
        }
    }   
    else if(HAL_ACC_ONE_POINT_DATA_LEN == 3)      //8位数据，也就是一个轴是一个字节数据
    {
        for(i=0;i<p_g_sensor_xyz_t->samples_number;i++)       //
        {
            p_g_sensor_xyz_t->samples_x_axis[i] = g_sensor_data_s_p.data_s[i*HAL_ACC_ONE_POINT_DATA_LEN];
            p_g_sensor_xyz_t->samples_y_axis[i] = g_sensor_data_s_p.data_s[i*HAL_ACC_ONE_POINT_DATA_LEN+1];
            p_g_sensor_xyz_t->samples_z_axis[i] = g_sensor_data_s_p.data_s[i*HAL_ACC_ONE_POINT_DATA_LEN+2];
            //如果是负数的话，是以补码方式存放，所以这里还需要将补码转换为整型负数
            //判断X轴正负
            if(p_g_sensor_xyz_t->samples_x_axis[i] & 0x80 )    //negative
            {
                p_g_sensor_xyz_t->samples_x_axis[i] =  ((~((p_g_sensor_xyz_t->samples_x_axis[i]&0x7f) - 1))&0x7f) | 0x80;
                //g_sensor_xyz_t.samples_x_axis[i] = (0x0fff - (g_sensor_xyz_t.samples_x_axis[i]&0x07ff) + 1) | 0x80;
            }
            //判断Y轴正负
            if(p_g_sensor_xyz_t->samples_y_axis[i] & 0x80 )    //negative
            {
                p_g_sensor_xyz_t->samples_y_axis[i] = ((~((p_g_sensor_xyz_t->samples_y_axis[i]&0x7f) - 1))&0x7f) | 0x80;
            }
            //判断Z轴正负        
            if(p_g_sensor_xyz_t->samples_z_axis[i] & 0x80 )    //negative
            {
                p_g_sensor_xyz_t->samples_z_axis[i] = ((~((p_g_sensor_xyz_t->samples_z_axis[i]&0x7f) - 1))&0x7f) | 0x80;
            }
        }
    }
}
