#ifndef     HAL_RTC_H
#define     HAL_RTC_H

#include <stdint.h>
#include <stdbool.h>
#include "time.h"

#define TIME_INTERVAL           1   //s
#define TASK_WORK_INTERVAL      10   //unit is S, 1~60S, it'a a interval for task performance period
typedef struct
{
    int16_t year;
    char mon;
    char day;
    char hour;
    char min;
    char sec;
    
}hal_rtc_date_t;

bool hal_rtc2_init(uint32_t time);
bool read_date(hal_rtc_date_t *p_date);
bool hal_rtc2_set_time_by_date(hal_rtc_date_t const date);
bool hal_rtc2_set_time_by_timestamp(time_t const new_time);
uint8_t nrf_cal_get_time_string(uint8_t *date);
uint8_t read_date_bcd(uint8_t *date_bcd);
bool hal_judge_integral_time(void);
bool hal_judge_zero_clock(void);
uint32_t hal_rtc2_get_unix_time(void);
bool is_integral_time(void);
bool is_zero_clock(void);
void clear_clock_flag(void);
uint32_t hal_rtc_get_user_tiks(void);

#endif      //HAL_RTC_H

