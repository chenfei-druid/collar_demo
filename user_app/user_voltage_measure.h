#include "sdk_common.h"
#include "nrf_drv_saadc.h"



typedef struct
{
    uint8_t cnt;
	uint8_t buf[20]; 
}adc_value_t;


void saadc_callback(nrf_drv_saadc_evt_t const * p_event);
void saadc_init(void);
