#ifndef     USER_APP_LOG_H
#define     USER_APP_LOG_H

#include    "nrf_log.h"
#include    "SEGGER_RTT.h"


#define     UART_DEBUG_PIN_TXD  23
#define     UART_DEBUG_PIN_RXD  22

#define     SEGGER_LOG_ENABLED  0

#define     PRINT_S(x)    0//user_app_print_string(x);
#define     PRINT_T(x)    0//user_app_print_int(x);

#if NRF_LOG_ENABLED
#define  DBG_LOG(...)  \
         user_app_log_chn_switch(); \
         NRF_LOG_INFO(__VA_ARGS__)

#elif   SEGGER_LOG_ENABLED
#define  DBG_LOG(...)    SEGGER_RTT_printf(0,__VA_ARGS__); \
                         //vTaskDelay(1);
            

#else
#define  DBG_LOG(...)    ;

#endif
//#define DBG_LOG(...)

void user_app_log_init(void);
void user_app_log_chn_switch(void);
void user_app_print_string(char * str);
void user_app_print_int(uint32_t dt);

#endif  //USER_APP_LOG_H

