
#ifndef __USER_TEMPERATURE_H__
#define __USER_TEMPERATURE_H__

#include <stdint.h>
#include <stdbool.h>

void user_temp_init(void);
int user_get_temperature(void);


#endif  //__USER_TEMPERATURE_H__

