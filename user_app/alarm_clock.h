/*
 * @Author: jiaqi.chen 
 * @Date: 2018-04-12 18:00:14 
 * @Last Modified by: jiaqi.chen
 * @Last Modified time: 2018-04-17 11:47:22
 */
#ifndef __ALARM_CLOCK__H__
#define __ALARM_CLOCK__H__

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#define     ALARM_NUMBERS_MAX       50

typedef enum 
{
    ALARM_TYPE_RUN_EVERY_MIN = 0,
    ALARM_TYPE_RUN_EVERY_MINUTE, //repeteated
    ALARM_TYPE_RUN_EVERY_HOUR ,
    ALARM_TYPE_RUN_EVERY_DAY,
    ALARM_TYPE_RUN_EVERY_WEAK,
    ALARM_TYPE_RUN_EVERY_MONTH,
    ALARM_TYPE_RUN_EVERY_YEAR,
    ALARM_TYPE_RUN_SIGLE,          // sigle
    ALARM_TYPE_RUN_EVERY_MAX
}alarm_clock_run_type_t;

typedef enum 
{
    ALARM_TYPE_ACC_SAMPLE_START = 0,
    ALARM_TYPE_ACC_SAMPLE_END,
    ALARM_TYPE_PWR_OFF_START ,
    ALARM_TYPE_PWR_OFF_END,
    
    ALARM_TYPE_MAX
}alarm_app_type_s;

typedef struct 
{
    uint32_t start;
    uint32_t stop;
}alarm_clock_period_t;

typedef void (* alarm_clock_func_cb_t)(uint32_t time_stamp, void * arg);

typedef int alarm_clock_handle_t;

typedef struct
{
   alarm_clock_run_type_t mode;  //
   alarm_clock_func_cb_t callback;
   uint32_t time_stamp; //
   uint8_t iden[20];
   void *param;
}alarm_clock_config_t;


bool alarm_clock_init(void);
int alarm_clock_create_alarm(alarm_clock_config_t * p_config);
void alarm_clock_print_date(void);
int alarm_clock_enable(alarm_clock_handle_t handler);
int alarm_clock_disable(alarm_clock_handle_t handler);
int alarm_clock_distroy(alarm_clock_handle_t handler);
void alarm_clock_set_time_stamp(uint32_t t);
void alarm_clock_print_date(void);
uint32_t hal_rtc2_get_unix_time(void);


int alarm_clock_update_alarm(alarm_clock_handle_t handler,const alarm_clock_config_t * config);

bool alarm_clock_create_alarm_new(alarm_app_type_s type, alarm_clock_config_t * p_config);


#endif
