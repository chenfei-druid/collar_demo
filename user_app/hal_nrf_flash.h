/**--------------------------------------------------------------------------------------------------------
** Commpany     :       Druid
** Created by   :		chenfei
** Created date :		2017-12-1
** Version      :	    0.0
** Descriptions :		user_nrf_flash_fds.h
*/

#ifndef _USER_NRF_FLASH_H_
#define _USER_NRF_FLASH_H_


#include <stdint.h>
#include <stdbool.h>

/**************************************************************user defined*******************************************************************/

#define     USER_PAGE_SIZE                   4096                //1K word of one page size
#define     USER_MEMORY_START           0x00060000 
#define     USER_INDEX_MEMORY_START     0x00060000 
#define     USER_DATA_MEMORY_START      0x00062000
#define     USER_MEMORY_END             0x0006A000
#define     USER_PAGE_MAX               ((USER_MEMORY_END - USER_INDEX_MEMORY_START) / USER_PAGE_SIZE)


typedef struct
{
    uint32_t * addr;
    uint32_t word_count;
}nrf_flash_rw;


void user_nrf_flash_init(void);
bool user_nrf_flash_word_write(uint32_t * p_address, uint32_t value);
void user_nrf_flash_read_page(uint32_t *page_start, uint32_t * p_out_array);
bool user_nrf_flash_page_erase(uint32_t *page_start,int8_t page_num);
void user_nrf_flash_test(void);
bool user_nrf_flash_words_write(uint32_t * p_address, const uint32_t *const word_buff, const uint32_t len);
void user_nrf_flash_word_read(uint32_t * p_address, uint32_t * value);
void user_nrf_flash_read_words(uint32_t * p_address, uint32_t * data_buff, uint32_t len);
uint32_t user_nrf_flash_get_bytes(uint32_t * p_address, uint8_t * data_buff, uint32_t byte_len);
bool user_nrf_flash_save_bytes(uint32_t * p_address, uint8_t * data_buff, uint32_t byte_len);
bool user_save_datas_into_flash(uint32_t *save_addr,uint8_t * data_buff, uint32_t byte_len);
bool user_get_datas_from_flash(uint32_t *get_addr,uint8_t * data_buff, uint32_t byte_len);
bool user_get_diff_len_read_save(uint32_t *upload_addr, uint32_t *save_addr, int32_t * byte_len);
void user_get_last_position(void);
void user_erase_data_memory(void);
void user_increase_upload_addr(uint32_t inc);
void user_decrease_upload_addr(uint32_t dec);
uint32_t user_save_datas_as_page(uint32_t save_addr, uint8_t * data_buff, uint32_t byte_len);
bool user_flash_next_page_start(uint32_t *p_addr);

#endif //_USER_NRF_FLASH_H_

