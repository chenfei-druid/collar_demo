
#include <stdbool.h>
#include <stdint.h>

#include "nrf_drv_spi.h"
#include "app_util_platform.h"
#include "user_spi_hal.h"

static volatile bool spi0_rf_done;
static volatile bool spi1_flash_done;
static volatile bool spi2_acc_done;

static volatile bool spi_xfer_done;  /**< Flag used to indicate that SPI instance completed the transfer. */
static volatile uint8_t m_spi_instance;
static const nrf_drv_spi_t spi0_rf = NRF_DRV_SPI_INSTANCE(0);  /**< SPI instance. */
static const nrf_drv_spi_t spi1_flash = NRF_DRV_SPI_INSTANCE(1);  /**< SPI instance. */
static const nrf_drv_spi_t spi2_acc = NRF_DRV_SPI_INSTANCE(2);  /**< SPI instance. */
/**
 * @brief SPI user event handler.
 * @param event
 */
void spi_event_handler(nrf_drv_spi_evt_t const * p_event, void * p_context)
{
    switch(m_spi_instance)
    {
        case 0:
            spi0_rf_done = true;
            break;
        case 1:
            spi1_flash_done = true;
            break;
        case 2:
            spi2_acc_done = true;
            break;
        default:
            break;
    }
}
/*
void spi_init(uint8_t const instance, nrf_drv_spi_config_t spi_config)
{
    uint32_t err_code;   

    static const nrf_drv_spi_t spi_n ;
    switch(instance)
    {
        case 0:
            //spi_n = NRF_DRV_SPI_INSTANCE(0);
            break;
        case 1:
            break;
        case 2:
            break;
        default:
            break;
    }
    
    //static const nrf_drv_spi_t spi_n = NRF_DRV_SPI_INSTANCE(instance);  //< SPI instanc								//nRF51822只能使用GPIO作为片选，所以这个单独定义了SPI CS管脚   
    err_code=nrf_drv_spi_init(&spi_n, &spi_config, spi_event_handler,NULL);
    APP_ERROR_CHECK(err_code);
}
*/
bool hal_spi_read_write(uint8_t cmd, uint8_t reg, uint8_t *data_p, uint8_t data_len, spi_atribute atr_p)
{
    /*
    uint32_t err_code;
    spi_transm_s spi_transm_t;
    uint16_t retry=1000;
    uint8_t i=0;
    uint8_t tx_len=0, rx_len=0;
    spi_xfer_done = false;  
    if(cmd != NULL)
    {
        spi_transm_t.transm_buff[i++] = cmd;
        tx_len++;
    }
    if(reg != NULL)
    {
        spi_transm_t.transm_buff[i++] = reg;
        tx_len++;
    }
    if(atr_p == write)
    {
        rx_len = 0;
        if(data_len != 0)
        {
            while(data_len--)
            {
                spi_transm_t.transm_buff[i++] = *data_p++;
                tx_len++;
            }
        }
    }
    else
    {
        rx_len = data_len;
    }
    err_code=nrf_drv_spi_transfer(&spi, spi_transm_t.transm_buff, tx_len, spi_transm_t.transm_buff, rx_len);
    APP_ERROR_CHECK(err_code);
	while(spi_xfer_done == false && (retry--))
    {;}
    if(retry == 0)
    {
        return false;
    }
    if(atr_p == read)
    {
        for(i=0;i<data_len;i++)
        {
            data_p[i]   =   spi_transm_t.transm_buff[tx_len+i];
        }
    } 
    return true;
    */
}
/*
uint8_t spi_write(uint8_t spi_index, uint8_t write_addr, uint8_t *write_data, uint8_t write_len)
{
    uint16_t retry=1000;

    uint8_t rx_buf[5];
    nrf_drv_spi_t   spi_instance;
    switch(spi_index)
    {
        case 0:
            spi0_rf_done = false;
            spi_instance = spi0_rf;
            break;
        case 1:
            spi1_flash_done = false;
            spi_instance = spi1_flash;
            break;
        case 2:
            spi2_acc_done = false;
            spi_instance = spi2_acc;
            break;
        default:
            break;
    }
        
    nrf_drv_spi_transfer(&spi_instance, write_data, write_len, rx_buf, 0);
    //APP_ERROR_CHECK();
	while(spi0_xfer_done == false && (retry--)){;}
    if(spi0_xfer_done == false && retry == 0)
    {
        return false;
    }
    return true;
}
*/