#ifndef USER_ILLUMINATION_H
#define USER_ILLUMINATION_H

#include <stdint.h>
#include <stdbool.h>

#define     DRV_ILLUM_ADC_IN_CHN            NRF_SAADC_INPUT_AIN1
#define     DRV_ILLUM_CHN                   1
#define     DRV_ILLUM_CTL_PIN               2

#define     DRV_ILLUM_DIVIDER_RESISTOR      2       //2K


void user_illumination_intensity_init(void);
void user_illumination_intensity_uinit(void);
int32_t drv_get_illumination_intensity(void);

#endif  //USER_ILLUMINATION_H



