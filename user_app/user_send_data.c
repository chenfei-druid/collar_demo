/** 
 * user_send_data_handler.c
 *
 * @group:       collar project
 * @author:      Chenfei
 * @create time: 2018/4/16
 * @version:     V0.0
 *
 */

#include "user_app_hal.h"
#include "user_data_struct.h"
#include "alltypes.pb.h"
#include "test_helpers.h"
#include "unittests.h"
#include "SEGGER_RTT.h"
#include "SEGGER_RTT_Conf.h"
#include "Define.pb.h"
#include "hal_per_flash.h"
#include "user_data_struct.h"
#include "Record.h"
#include "behavior.h"
#include "user_app_get_record.h"
#include "user_app_hal.h"
#include "user_data_pkg.h"
#include "drv_nb_modul.h"
#include "user_app_log.h"
#include "user_send_data.h"
#include "ble_application.h"
#include "user_app.h"
#include "HexStr.h"
//#include "SEGGER_RTT.h"
//#include "SEGGER_RTT_Conf.h"

bool get_ble_tx_complete_flag(void);
extern uint8_t get_ble_max_data_len(void);
extern bool ble_send_long_datas(uint8_t *p_dt, uint32_t len);
uint32_t ble_send_data(uint8_t *p_dt, uint16_t len);

static      xSemaphoreHandle            sema_ble_tx_complete = NULL;

xSemaphoreHandle user_create_ble_tx_semaphore(void)
{
    sema_ble_tx_complete = xSemaphoreCreateBinary();
    if(sema_ble_tx_complete == NULL)
    {
        return NULL;
    }
    return sema_ble_tx_complete;
}
void user_give_ble_tx_complete_semaphore(void)
{
    DBG_LOG("BLE_GATTS_EVT_HVN_TX_COMPLETE\r\n");
    if(sema_ble_tx_complete == NULL)
    {
        DBG_LOG("sema_ble_tx_complete is invalid\r\n");
        return;
    }
    xSemaphoreGive(sema_ble_tx_complete);
}
/**
 * @brief 
 * 
 * @param p_buf 
 * @param length 
 * @return true 
 * @return false 
 */
static bool ble_s_send_one_split(void * const p_buf, uint16_t length)
{
    uint8_t *buf = (uint8_t *)p_buf;
    if(buf == NULL)
    {
        return false;
    }
    if(length <= 0 || length > get_ble_max_data_len())
    {
        return false;
    } 
    uint32_t err_code;
    do
    {
        err_code = ble_send_data((uint8_t*)buf,length);   
        if(err_code != NRF_SUCCESS)
        {
            if(err_code == NRF_ERROR_NOT_FOUND)
            {
                DBG_LOG("NRF_ERROR_NOT_FOUND\r\n");
                return false;
            }
            if(err_code == NRF_ERROR_INVALID_STATE)
            {
                DBG_LOG("ble transmit failed: NRF_ERROR_INVALID_STATE\r\n");
                return false;
            }
            if(err_code == NRF_ERROR_INVALID_PARAM)
            {
                DBG_LOG("ble transmit failed with NRF_ERROR_INVALID_PARAM\r\n");
                return false;
            }        
            DBG_LOG("ble waiting tx complete\r\n");
            if(xSemaphoreTake( sema_ble_tx_complete, BLE_TX_COMPLETE_WAIT ) != pdPASS)
            {
                DBG_LOG("-----  ble transmit timeout  %dS -----\r\n",BLE_TX_COMPLETE_WAIT);
                return false;
            }
            
        }
    }while((err_code != NRF_SUCCESS));
#if 1
    #include "HexStr.h"
    char str[256] = {0};
    HexToStr(str,buf,length);
    DBG_LOG("%s\r\n",str);
#endif
#if 0
    DBG_LOG("send one split");
    for(uint8_t i=0;i<length;i++)
    {
        DBG_D("%x ",buf[i]);
    }
    DBG_D("\r\n");
#endif
    return true;
}
/**
 * @brief 
 * 
 * @param p_buf 
 * @param length 
 * @return uint32_t 
 */
uint32_t ble_s_send(void * const buffer, uint16_t const length)
{
    if(length <= 0 || buffer == NULL)
    {
        return false;
    }
    uint8_t     max_mtu     = get_ble_max_data_len();//get_ble_max_data_len();
    uint16_t    len_cnt     = length;   
    uint32_t    err_code    = NRF_SUCCESS;      
    uint8_t     * send_buff = NULL;     
    uint8_t     sub_pkg_len = 0;
    uint8_t    *p_data      = (uint8_t *)buffer;
    bool ret = false;
    uint8_t count = 0;
    
    vPortFree(send_buff);
    send_buff = pvPortMalloc(max_mtu); 
    
    if(sema_ble_tx_complete == NULL)
    {
        return 0;
    }
    xSemaphoreTake( sema_ble_tx_complete, 0 );
    DBG_LOG("BLE max len = %d\r\n, total send len = %d\r\n", max_mtu,length);  
    do
    {            
        //split the datas to sub package  to send
        sub_pkg_len = user_get_one_pkg_from_cache(&p_data[length-len_cnt],send_buff,len_cnt,length,max_mtu-1);
        ret = ble_s_send_one_split(send_buff,(sub_pkg_len+1));
        if(ret)
        {
            len_cnt -= sub_pkg_len;
        }
        else
        {
            //re-send here
            break;
        }
        //DBG_LOG("ble_s number %d pkg length = %d\r\n",count++,(sub_pkg_len+1));
    }while(len_cnt && ret);  
    if(!ret)
    {
        DBG_LOG("------ ble transmit failed  ----- %d\r\n");
    }
    DBG_LOG("ble send data complete\r\n");
    vPortFree(send_buff);
    return ret;
}
/**
* @functionname : user_data_pb_bhv_encode 
* @description  : function for send some datas in given buffer through ble channel
* @input        : buffer: pointor to data buf, length: data length will be sent
* @output       : none 
* @return       : true if successed
*/
bool user_send_data_by_ble(uint8_t * const buffer, int32_t length)
{
    if(!ble_get_connect_status())
    {
        return false;
    }
    if(length <= 0 || buffer == NULL)
    {
        return false;
    }
    
    uint8_t     ble_max_data_len        = get_ble_max_data_len();
    uint16_t    len_cnt                 = length;   
    uint32_t    err_code                = NRF_SUCCESS;      
    uint8_t     send_buff[NRF_SDH_BLE_GATT_MAX_MTU_SIZE];   
    uint8_t     valid_pkg_len = 0;
    char str_buf[NRF_SDH_BLE_GATT_MAX_MTU_SIZE*2];
    if(sema_ble_tx_complete == NULL)
    {
        sema_ble_tx_complete = xSemaphoreCreateBinary();
    }
    xSemaphoreTake( sema_ble_tx_complete, 0 );
    DBG_LOG("ble send beginning...\r\nthe max send len = %d\r\n, total len = %d\r\n", ble_max_data_len,length);  
    do
    {            
        //split the datas to sub package  to send
        valid_pkg_len = user_get_one_pkg_from_cache(&buffer[length-len_cnt],send_buff,len_cnt,length,ble_max_data_len-1);        
        HexToStr(str_buf,send_buff,valid_pkg_len+1);
        DBG_LOG("%s",str_buf);
        if(len_cnt < ble_max_data_len)
        {            
            valid_pkg_len = len_cnt;
            err_code = ble_send_data(send_buff,valid_pkg_len+1);
            len_cnt = 0;
        }
        else    //len_cnt > m_ble_rts_max_data_len
        {
            valid_pkg_len = ble_max_data_len - 1;
            err_code = ble_send_data(send_buff,ble_max_data_len);
            len_cnt -= (ble_max_data_len - 1);
        }
        if(err_code != NRF_SUCCESS)
        {
            if(err_code == NRF_ERROR_INVALID_STATE)
            {
               //DBG_LOG("ble transmit failed: NRF_ERROR_INVALID_STATE\r\n");
                return false;
            }
            if(err_code == NRF_ERROR_INVALID_PARAM)
            {
                //DBG_LOG("ble transmit failed with NRF_ERROR_INVALID_PARAM\r\n");
                return false;
            }
            //ble send buffer is full, waiting for tx complete  
            DBG_LOG("waiting for tx complete\r\n");            
            if(xSemaphoreTake( sema_ble_tx_complete, BLE_TX_COMPLETE_WAIT ) != pdPASS)
            {
                DBG_LOG(">>>>>>>>>>>>>>  ble transmit timeout  <<<<<<<<<<<<<< %d\r\n");
                //vSemaphoreDelete( sema_ble_tx_complete );
                return false;
            }
            len_cnt += valid_pkg_len;
            DBG_LOG("BLE RESEND AGAIN\r\n");
        }               
        DBG_LOG("\r\nble current split pkg len = %d, remain len = %d\r\n",valid_pkg_len,len_cnt);
        //vTaskDelay( 2 );//for print all
    }while(len_cnt);  
    while(xSemaphoreTake( sema_ble_tx_complete, 50 ) == pdPASS);

    DBG_LOG("ble send data complete\r\n");
    //vSemaphoreDelete( sema_ble_tx_complete );
    return true;
}

/**
* @functionname : user_send_data_by_nb 
* @description  : function for send some datas in given buffer through nb-modul channel
* @input        : buffer: pointor to data buf, length: data length will be sent
* @output       : none 
* @return       : true if successed
*/
bool user_send_data_by_nb(uint8_t * const buffer, int32_t length)
{
    if(length <= 0 || buffer == NULL)
    {
        return false;
    }
    int16_t     nb_max_data_len        = drv_get_nb_max_send_len();
    bool        err_code                = true;
    DBG_LOG("\r\ntarnsmit data via nb-modul begin, total length = %d\r\n", length);
    if(length > nb_max_data_len)
    {
        return false;
    }
    //user_led_on(LED_INDEX_1);
    err_code = drv_nb_trans_data(buffer,length);
    //user_led_off(LED_INDEX_1);
    DBG_LOG("\r\n send data via nb-modul complete\r\n");
    
    return err_code;
}
/**
* @functionname : user_send_data 
* @description  : function for send datas through different channel @ref trans_interface_type
* @input        : buffer: pointor to data buf, length: data length will be sent
* @output       : none 
* @return       : true if successed
*/
bool user_send_data(uint8_t * const buffer, int32_t length,trans_interface_type chn_type)
{
    if(buffer == NULL)
    {
        DBG_LOG("\r\n send data pointer is null\r\n");
        return 0;
    }
    bool ret;
    switch(chn_type)
    {
        case COMMUNICATION_INTERFACE_TYPE_NB:
            ret = user_send_data_by_nb(buffer,length);      
            break;
        
        case COMMUNICATION_INTERFACE_TYPE_BLE:
            //ret = user_send_data_by_ble(buffer,length);  
            ret = ble_s_send(buffer,length);     
            break;
        
        default:
            DBG_LOG("\r\n send data invalid interface type\r\n");
            ret = false;
            break;
    }
    return ret;
}

