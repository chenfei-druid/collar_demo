
#ifndef _USER_APP_
#define _USER_APP_
#include "hal_nrf_uart.h"
#include "nrf_drv_gpiote.h"
#define     LED_INDEX_1         17
//#define     LED_INDEX_2         
//#define     LED_INDEX_3         

#define     user_leds_pin_config()  nrf_gpio_cfg_output(LED_INDEX_1);//nrf_gpio_cfg_output(LED_INDEX_2);
#define     user_leds_off()         user_led_off(LED_INDEX_1); ;//nrf_gpio_pin_clear(LED_INDEX_2);
#define     user_led_off(indx)       nrf_gpio_pin_set(indx)
#define     user_led_on(indx)      nrf_gpio_pin_clear(indx)
#define     user_led_invert(indx)   nrf_gpio_pin_toggle(indx)

#define     UART_DEBUG_PIN_TXD  23
#define     UART_DEBUG_PIN_RXD  22

#define     USER_NB_UPLOAD_INTERVAL     60      //S

void user_app_init(void);
void user_app_handle(void);
//void user_leds_off(void);

//void user_led_on(unsigned int indx);

//void user_led_off(unsigned int indx);
//void user_led_invert(unsigned int indx);

#endif  //_USER_APP_

