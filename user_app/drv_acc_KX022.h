#ifndef DRV_ACC_KX022_H
#define DRV_ACC_KX022_H

#include <stdint.h>
#include <stdbool.h>
#include "nrf_gpio.h"
#include "nrf_drv_gpiote.h"

#define     DRV_KX022_SPI_INSTANCE           0

#define     ACT_THRESH_VALUE        2000
#define     ACT_THRESH_VALUE_L      ACT_THRESH_VALUE&0xff
#define     ACT_THRESH_VALUE_H      (ACT_THRESH_VALUE>>8)&0x07
#define     ACT_DETECT_TIME         10

#define     INACT_THRESH_VALUE      200
#define     INACT_THRESH_VALUE_L    INACT_THRESH_VALUE&0xff
#define     INACT_THRESH_VALUE_H    (INACT_THRESH_VALUE>>8)&0x07
#define     INACT_DETECT_TIME       10

#define     DRV_KX022_SPI_CS_PIN     31         //
#define     DRV_KX022_SPI_SCK_PIN    30         //
#define     DRV_KX022_SPI_MOSI_PIN   28
#define     DRV_KX022_SPI_MISO_PIN   29

//#define     DRV_KX022_SPI_CS_L       nrf_gpio_cfg_output(DRV_KX022_SPI_CS_PIN); \
                                     nrf_gpio_pin_clear(DRV_KX022_SPI_CS_PIN);
                                     
//#define     DRV_KX022_SPI_CS_H       nrf_gpio_cfg_output(DRV_KX022_SPI_CS_PIN); \
                                     nrf_gpio_pin_set(DRV_KX022_SPI_CS_PIN);
                                     
#define     DRV_KX022_INIT1_PIN       27

#define     DRV_KX022_OUT_DATA_RATE_DEFALT                  ACC_KX022_OUT_DATA_RATE_25H

#define     DRV_KX022_OUT_POINT_SIZE_SECOND                 25  //25HZ

#define     DRV_KX022_RESULT_DIV       16

typedef enum 
{
    DRV_KX022_SAMPLES_8_BIT = 0,
    DRV_KX022_SAMPLES_16_BIT,
}drv_kx022_sample_bits_t;


#define     DRV_KX022_SAMPLE_BITS                           DRV_KX022_SAMPLES_16_BIT
#define     DRV_KX022_SAMPLES_IS_8_BIT                      0


#if DRV_KX022_SAMPLES_IS_8_BIT
#define     DRV_KX022_FIFO_BUFFER_MAX       84
#define     DRV_KX022_FIFO_SAMPLES          75                  //When BUF_RES=0,sample is 8-bit, themaximum number of samples is 84
#define     DRV_kx022_ONE_XYZ_DATA_LEN      3

#else
#define     DRV_KX022_FIFO_BUFFER_MAX       41
#define     DRV_KX022_FIFO_SAMPLES          25                  //When BUF_RES=1,sample is 16-bit, themaximum number of samples is 41
#define     DRV_kx022_ONE_XYZ_DATA_LEN      6
#endif

typedef void (*drv_kx022_int_callback) (void *arg);   
typedef struct drv_kx022_point_xyz_s    drv_kx022_point_xyz_t;

typedef enum
{
    DRV_KX022_TASK_TYPE_READ_SAMPLE = 1,
    DRV_KX022_TASK_TYPE_FIFO_SAMPLE_START ,
    DRV_KX022_TASK_TYPE_FIFO_SAMPLE_STOP,
    DRV_KX022_TASK_TYPE_SINGLE_SAMPLE_START,
    DRV_KX022_TASK_TYPE_SINGLE_SAMPLE_STOP,
    
}drv_kx022_task_run_type_t;

typedef enum
{
    DRV_KX022_OUT_DATA_RATE_12_5H = 0,
    DRV_KX022_OUT_DATA_RATE_25H,
    DRV_KX022_OUT_DATA_RATE_50H,
    DRV_KX022_OUT_DATA_RATE_100H,
    DRV_KX022_OUT_DATA_RATE_200H,
    DRV_KX022_OUT_DATA_RATE_400H,
    DRV_KX022_OUT_DATA_RATE_800H,
    DRV_KX022_OUT_DATA_RATE_1600H,
    DRV_KX022_OUT_DATA_RATE_0_781H,
    DRV_KX022_OUT_DATA_RATE_1_563H,
    DRV_KX022_OUT_DATA_RATE_3_125H,
    DRV_KX022_OUT_DATA_RATE_6_25H,
}drv_kx022_out_data_rate_t;

typedef enum 
{
    DRV_KX022_INTERRUPT_TILT_POSITION = 0,
    DRV_KX022_INTERRUPT_WAKE_UP = 1,
    DRV_KX022_INTERRUPT_TAP_OR_DOUBLETAP = 2,
    DRV_KX022_INTERRUPT_DATA_READY = 4,
    DRV_KX022_INTERRUPT_WATERMARK = 5,
    DRV_KX022_INTERRUPT_BUFFER_FULL = 6,
    DRV_KX022_INTERRUPT_NONE = 7,
}drv_kx022_int_report_t;

typedef enum 
{
    DRV_KX022_2G = 0,
    DRV_KX022_4G,
    DRV_KX022_8G,
}drv_kx022_range_t;

typedef enum
{
    DRV_KX022_FIFO_MODE = 0,
    DRV_KX022_STREAM_MODE,
    DRV_KX022_TRIGGER_MODE,
    DRV_KX022_FILO_MODE,
}drv_kx022_fifo_mode_t;

typedef enum
{
    DRV_KX022_FILTER_APPLIED = 0,
    DRV_KX022_FILTER_BYPASSED,
}drv_kx022_filter_apply_t;
typedef enum
{
    DRV_KX022_FILT_FREQ_ODR_9 = 0,      //filter corner frequency set to ODR/9
    DRV_KX022_FILT_FREQ_ODR_2,          //filter corner frequency set to ODR/2
    DRV_KX022_FILT_FREQ_ODR_0           //low current
}drv_kx022_filter_freq_t;

typedef enum
{
    DRV_KX022_INT_ACTIVE_LOW = 0,
    DRV_KX022_INT_ACTIVE_HIGH,
}drv_kx022_int_active_t;

typedef enum
{
    DRV_KX022_INT_LATCH_UNTIL_READ = 0,
    DRV_KX022_INT_50US_PULSE,
}drv_kx022_int_resp_type_t;

typedef enum
{
    DRV_KX022_LOW_CURRENT = 0,
    DRV_KX022_HIGH_CURRENT,         //high current. Bandwidth (Hz) = ODR/2
}drv_kx022_perform_mode_t;

#if DRV_KX022_SAMPLES_IS_8_BIT
struct drv_kx022_point_xyz_s
{
    int8_t     x;
    int8_t     y;
    int8_t     z;
};
#else
struct drv_kx022_point_xyz_s
{
    int16_t     x;
    int16_t     y;
    int16_t     z;
};
#endif

typedef struct
{
    drv_kx022_int_callback  cb_handler;
    
}drv_kx022_evt_cb_t;

typedef struct
{
    drv_kx022_out_data_rate_t   out_rate;
    uint8_t                     fifo_points;
    drv_kx022_sample_bits_t     sample_bits;    
    drv_kx022_int_report_t      int_report;
    drv_kx022_range_t           range;
    drv_kx022_fifo_mode_t       fifo_mode;
    drv_kx022_filter_freq_t     filt_mode;
    drv_kx022_int_active_t      int_act;
    drv_kx022_int_resp_type_t   int_type;
    drv_kx022_perform_mode_t    pwr_mode;
    drv_kx022_int_callback      int_cb;
    nrf_drv_gpiote_evt_handler_t gpio_int_handler;
}drv_kx022_cfg_t;

/**
* @brief Macro for config default kx022 parameter
* out data rate = 25HZ, range is +-8G, fifo watermark-interrupt, fifo stream mode, 
* interrupt-pin active high and 50us pulse, low current power

*/
#define DRV_KX022_CONFIG_DEFAULT                    \
{                                                   \
    .out_rate   = DRV_KX022_OUT_DATA_RATE_25H,      \
    .sample_bits= DRV_KX022_SAMPLE_BITS,            \
    .fifo_points= DRV_KX022_FIFO_SAMPLES,           \
    .int_report = DRV_KX022_INTERRUPT_WATERMARK,    \
    .range      = DRV_KX022_4G,                     \
    .fifo_mode  = DRV_KX022_STREAM_MODE,            \
    .filt_mode  = DRV_KX022_FILT_FREQ_ODR_2,    \
    .int_act    = DRV_KX022_INT_ACTIVE_HIGH,        \
    .int_type   = DRV_KX022_INT_LATCH_UNTIL_READ ,        \
    .pwr_mode   = DRV_KX022_LOW_CURRENT,            \
    .int_cb     = NULL                              \
}

bool drv_kx022_init(void);
bool drv_kx022_config(drv_kx022_cfg_t * const p_cfg_t, uint8_t const spi_instance);
bool drv_kx022_read_one_xyz_data(drv_kx022_point_xyz_t * xyz_buff);
//bool drv_kx022_start_fifo_sample(drv_kx022_int_callback const cb);
bool drv_kx022_start_fifo_sample(void);
bool drv_kx022_stop_fifo_sample(void);
bool drv_kx022_into_sleep_mode(void);
uint32_t drv_kx022_get_sample_points(drv_kx022_point_xyz_t * fifo_buff,uint8_t points);
//static uint32_t drv_kx022_get_sample_points(void);
bool drv_kx022_clear_interrupt_source(void);
bool drv_kx022_clear_buffer(void);
bool drv_kx022_read_id(void);
uint8_t drv_kx022_read_int_flag(void);
uint8_t drv_kx022_get_fifo_points(void);
void drv_kx022_creat_task(void);
void drv_kx022_fifo_sample_start_type(void);
void drv_kx022_fifo_sample_stop_type(void);
uint32_t drv_kx022_read_cached_sample(uint8_t *p_xyz, uint16_t buf_size, uint32_t tiemout_ms);

#endif  //DRV_ACC_KX022_H

