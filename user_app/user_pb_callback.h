/** 
 * user_pb_decode_callback.h
 *
 * @group:       collar project
 * @author:      Chenfei
 * @create_time: 2018/5/4
 * @version:     V0.0
 *
 */

#ifndef     USER_PB_CALLBACK_H
#define     USER_PB_CALLBACK_H

#include    <stdint.h>
#include    "pb_encode.h"
#include    "pb_decode.h"
#include    "SimpleRsp.pb.h"

bool user_app_encode_repeated_var_string(pb_ostream_t *stream, const pb_field_t *fields,  void * const *str);
bool user_app_decode_repeated_var_string(pb_istream_t *stream, const pb_field_t *field, void **str);
bool user_app_decode_simple_rsp(protocol_simple_rsp_t * const p_rsp_t, 
                                        char * const dev_id, 
                                        uint8_t const * const p_buf, 
                                        uint32_t const len);
                                        
#endif      //USER_PB_CALLBACK_H

