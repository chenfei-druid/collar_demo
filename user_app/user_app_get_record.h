
#ifndef     USER_APP_GET_RECORD_H
#define     USER_APP_GET_RECORD_H
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include "behavior.h"
#include "Behavior2.pb.h"

uint32_t user_app_get_behavior_record(
                                      protocol_behavior2_t  *pt_bhv_t[], 
                                      uint8_t record_num
                                      );
                                      
#endif  //USER_APP_GET_RECORD_H

