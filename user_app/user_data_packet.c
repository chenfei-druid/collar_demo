/**--------------------------------------------------------------------------------------------------------
** Commpany     :       Druid
** Created by   :		chenfei
** Created date :		2017-8-11
** Version      :	    1.0
** Descriptions :		application of process data will be send
**--------------------------------------------------------------------------------------------------------*/
#include <stdint.h>
#include <string.h>
#include "nrf_delay.h"
#include "user_data_packet.h"
#include "user_ble_transmition.h"

#define     MODULE_CMD                0x1f

//static  data_send_packet            data_send_packet_t;
//static  unit_data_send_unpacket     unit_data_send_unpacket_t;


typedef enum 
{
    HEAD_CMD_FIRST_PACKET    = 0x11,        //the first packet of transmission data
    HEAD_CMD_MIDDLE_PACKET   = 0x10,        //the middle packet of transmission data
    HEAD_CMD_LAST_PACKET     = 0x12         //the last packet of transmission data
}packet_head_cmd;
typedef enum
{
    LORA_PACKET_SIZE = LORA_SEND_UINT_LEN,         //the unit-packet bytes of LORA transmission
    BLE_PACKET_SIZE  = BLE_TRANSF_MAX_LEN                   //the unit-packet bytes of BLE transmission
}packet_size;

extern  int16_t battery_level_last,battery_level,adc_value;

/*
* function name: add_check_package_channle 
* description  : 添加一个通道，存放连续变化的计数器，两个字节，用于判断是否丢包
* input   : datafiled_p: 添加后的数据域指针， orignal_data_p： 原始数据域指针， len：数据长度
* output  : 添加后的数据域长度
*/
uint8_t left_numbers=0; 
volatile uint16_t pkg_cnt=0;
uint16_t add_check_package_channle(uint8_t * datafiled_p, uint8_t * orignal_data_p, uint16_t data_len)
{
    uint16_t i,k=0;
    uint16_t j=0;
    
    if(left_numbers != 0)   //如果上次在加上计数通道后剩下的数据字节数为0，也就是刚好是N个三轴数据  如果上次数据不是刚好N个三轴数据，而是还余下一部分，那么这里就要加上这部分，数据才能衔接上
    {
        j=6-left_numbers;
    }
  
    for(i=0;i<data_len;i++)
    {
        if((i-j)%6==0 && i!=0)
        {
            datafiled_p[k++]    =   pkg_cnt&0x0ff;
            datafiled_p[k++]    =   ((pkg_cnt>>8)&0x0f)|0xf0; 
            pkg_cnt+=100;
            if(pkg_cnt>1000)
            {
                pkg_cnt=0;
            }
        }
        datafiled_p[k++]     = orignal_data_p[i];
        
    }
    //i=data_len
    left_numbers = (i-j)%6;
    if(left_numbers==0)             //到最后，如果刚好的话，就加上计数通道数据，如果不是刚好N个三轴数据，则将剩下的数据字节数保存下来
    {
            datafiled_p[k++]    =   pkg_cnt&0x0ff;
            datafiled_p[k++]    =   ((pkg_cnt>>8)&0x0f)|0xf0; 
            pkg_cnt+=100;
            if(pkg_cnt>1000)
            {
                pkg_cnt=0;
            }
    }
    return k;
}


/*
* function name: copy_module_data_to_send_buff 
* description  : 将各个字段分开存放的原始数据打包存放于发送缓存区，以方便分包发送
* input   : transf_data_module_p：数据包结构体，包含seq字段,cmd字段，len字段，data字段，crc校验或结束字段， data_send_packet_t：发送缓存区结构体
* output  : none
*/
void copy_module_data_to_send_buff(data_send_packet   * data_send_packet_t, 
                                          transm_data_module * transf_data_module_p)
{
    uint16_t i;
    memset(data_send_packet_t->send_packet_buff, 0 , sizeof(data_send_packet_t->send_packet_buff));
 
    data_send_packet_t->send_packet_buff[0] = transf_data_module_p->seq;
    data_send_packet_t->send_packet_buff[1] =   transf_data_module_p->cmd;
    //有效数据字段装填
    for(i=0;i<transf_data_module_p->datafiled_len;i++)
    {
        data_send_packet_t->send_packet_buff[4+i] = transf_data_module_p->datafiled[i];
    }
#if defined (BATTERY_MEASUREMENT)  //若需要采集电池电压，则此处需要添加电池电压字段
    if(battery_level_last!=battery_level)
	{
		data_send_packet_t->send_packet_buff[4+transf_data_module_p->datafiled_len]    =   battery_level & 0x00ff;
		data_send_packet_t->send_packet_buff[4+transf_data_module_p->datafiled_len+1]  =   (battery_level >> 8) & 0x00ff;
		transf_data_module_p->datafiled_len					+= 	2;				//if need to send battery value, the len must add 2
		battery_level_last										=	battery_level;
	}
#endif 
    //长度域，表示有效数据长度，除开cmd,seq,len,crc字段
    data_send_packet_t->send_packet_buff[2]=(transf_data_module_p->datafiled_len&0x00ff);            
    data_send_packet_t->send_packet_buff[3]=((transf_data_module_p->datafiled_len >>8) &0x00ff);
	
	data_send_packet_t->send_packet_buff[4+transf_data_module_p->datafiled_len]    =   transf_data_module_p->crc & 0x00ff;		//end of package or crc
	data_send_packet_t->send_packet_buff[4+transf_data_module_p->datafiled_len+1]  =   (transf_data_module_p->crc >> 8) & 0x00ff;	//end of package or crc
    data_send_packet_t->send_packet_data_length     =   transf_data_module_p->datafiled_len	+ 6;
}

/*
* function name: add_field_data_module 
* description  : 数据组包，将各个字段数据按照协议存放于原始数据结构体中
* input   : buffer：刚被读取出来的传感器数据，transf_data_module_p：按照传输协议存放的数据包，包含seq字段,cmd字段，len字段，data字段，crc校验或结束字段， len：传感器数据长度
* output  : none
*/
void add_field_data_module(uint8_t              * buffer, 
                           transm_data_module   * transf_data_module_p, 
                           uint16_t              len)
{
    uint16_t k=0;
    transf_data_module_p->seq++;

    
    if(transf_data_module_p->seq==0)
    {
        transf_data_module_p->seq++;
    }
    memset(&transf_data_module_p->datafiled, 0 , sizeof(transf_data_module_p->datafiled));
    transf_data_module_p->cmd           = MODULE_CMD;                                          //the command filed of data transform protocal over ble
    
    k=add_check_package_channle(transf_data_module_p->datafiled, buffer,len);   //add package check channel data
    
    transf_data_module_p->datafiled_len = k;                                //the length of valid data, not include filed ofseq & cmd & crc & len
	//transf_data_module_p->crc           = crc;
    transf_data_module_p->crc           = 0xffff;    
    //copy_module_data_to_send_buff(&ble_transf_protocal_t);                            //add package to whole package pointer
    //protocal_p        = proto_trasnf;                                                         //pointer of whole package
	//protocal_data_len = ble_transf_protocal_t.datafiled_len+APP_PROTOCAL_HEAD_END_LEN;  //the length of whole package , not include battery value
	
}
//拆分数据包，每一包数据大小由属性制定,属性为1，表示BLE, 属性为2,表示LORA
/*
* function name: add_field_data_module 
* description  : 拆分数据包，每一包数据大小由属性制定,属性为0xf1，表示BLE, 属性为0xf0,表示LORA，
* input   : data_send_packet_p: 组包后的数据包， unit_data_send_unpacket_t：拆分成若干个小包的组成的大包
* output  : none
*/
void app_data_send_unpacket(unit_data_send_unpacket * unit_data_send_unpacket_t, 
                            data_send_packet        * data_send_packet_p)
{
     uint16_t len;
    uint16_t i,k=0;
    //uint8_t unpk_len;
    len = data_send_packet_p->send_packet_data_length;
    unit_data_send_unpacket_t->total_len = data_send_packet_p->send_packet_data_length;
    //uint8_t **unpk_p ;
    //unpk_p = unit_data_send_unpacket_t.send_buff;
    if(data_send_packet_p->attribute == LORA_DATA_TYPE)        //数据包为通过lora发送的数据
    {
        //unpk_len = 200;   
        unit_data_send_unpacket_t->uinit_len = LORA_PACKET_SIZE;
    }
    else if(data_send_packet_p->attribute == BLE_DATA_TYPE)   //数据包为通过BLE发送的数据
    {
        //unpk_len = 20;
        unit_data_send_unpacket_t->uinit_len = BLE_PACKET_SIZE;
    }      
    for(i=0;i<data_send_packet_p->send_packet_data_length;i++)  //
    {
        if(i%(unit_data_send_unpacket_t->uinit_len-1) == 0)
        {
                if(len==data_send_packet_p->send_packet_data_length && len > (unit_data_send_unpacket_t->uinit_len-1))
                {
                    unit_data_send_unpacket_t->send_buff[k]   =   HEAD_CMD_FIRST_PACKET;
                    k++;
                    unit_data_send_unpacket_t->total_len += 1;
                }
                else if(len <= (unit_data_send_unpacket_t->uinit_len-1))
                {
                    unit_data_send_unpacket_t->send_buff[k]   =   HEAD_CMD_LAST_PACKET;
                    k++;
                    unit_data_send_unpacket_t->total_len += 1;
                }
                else
                {
                    unit_data_send_unpacket_t->send_buff[k]   =   HEAD_CMD_MIDDLE_PACKET;
                    k++;
                    unit_data_send_unpacket_t->total_len += 1;
                }
        }
        unit_data_send_unpacket_t->send_buff[k]   =   data_send_packet_p->send_packet_buff[i];
        k++;
        len--;
    }
}


