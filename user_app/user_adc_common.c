
 /*********************************includes**************************************/
#include "sdk_common.h"
#if NRF_MODULE_ENABLED(SAADC)
#include "nrf_drv_saadc.h"
#include "nrf_drv_gpiote.h"
#include "nrf_gpio.h"
#include "SEGGER_RTT.h"
#include "SEGGER_RTT_Conf.h"
//#include "nrf_delay.h"
#include "user_app.h"
#include "user_illumination.h"
#include "user_battery_mgt.h"

/**
* @function@name:    saadc_callback
* @description  :    Function for the callback of SAADC.
* @input        :    value: result of adc
* @output       :    none
* @return       :    none
**/
static void saadc_callback(nrf_drv_saadc_evt_t const * p_event)
{
}

/**
* @function@name:    saadc_init
* @description  :    Function for initializing the SAADC.
*                    NRF_SAADC_INPUT_AIN4 --  BAT_SMP, battery measure
* @input        :    value: result of adc
* @output       :    none
* @return       :    none
**/
static void user_com_saadc_init(void)
{
    ret_code_t err_code;
    
    nrf_drv_saadc_config_t p_config = NRF_DRV_SAADC_DEFAULT_CONFIG;
    p_config.resolution = NRF_SAADC_RESOLUTION_10BIT;           //10-bit adc
    err_code = nrf_drv_saadc_init(&p_config, saadc_callback);
    APP_ERROR_CHECK(err_code);   
}
/**
* @function@name:    saadc_uninit
* @description  :    Function for uninitializing the SAADC. Stop all ongoing conversions and disables all channels.
* @input        :    value: result of adc
* @output       :    none
* @return       :    none
**/
void user_com_saadc_uninit(void)
{
	nrf_drv_saadc_uninit();             //释放所有通道
}

void user_com_adc_config(void)
{
    user_com_saadc_init();
    user_battery_mgt_init();
    user_illumination_intensity_init();    
}

#endif      //NRF_MODULE_ENABLED(SAADC)

