/** 
 * user_nrf_adc.c
 *
 * @group:       neck_strap project
 * @author:      Chenfei
 * @create:      2017/10/11
 * @modify:      2018/4/2
 * @version:     V0.0
 *
 */
 /*********************************includes**************************************/
#include "sdk_common.h"
#if NRF_MODULE_ENABLED(SAADC)
#include "nrf_drv_saadc.h"
#include "nrf_drv_gpiote.h"
#include "nrf_gpio.h"
#include "SEGGER_RTT.h"
#include "SEGGER_RTT_Conf.h"
//#include "nrf_delay.h"
#include "user_app.h"
#include "user_illumination.h"
#include "freertos_platform.h"
#include "user_common_alg.h"
#include "user_app_log.h"

/********************************user defined  variable*************************************/
/****************************defined functions begin******************************/
/**
* @function@name:    saadc_uninit
* @description  :    Function for uninitializing the SAADC. Stop all ongoing conversions and disables all channels.
* @input        :    value: result of adc
* @output       :    none
* @return       :    none
**/
void drv_illumination_saadc_channle_uninit(void)
{
    nrf_drv_saadc_channel_uninit(DRV_ILLUM_CHN);
}

/**
* @function@name:    saadc_init
* @description  :    Function for initializing the SAADC.
*                    NRF_SAADC_INPUT_AIN4 --  BAT_SMP, battery measure
*                    NRF_SAADC_INPUT_AIN1 --  ALS_SMP, illumination intensity
* @input        :    value: result of adc
* @output       :    none
* @return       :    none
**/
void drv_illumination_saadc_channel_init(void)
{
    ret_code_t err_code;

    /*illumination intensity measure adc channel*/
    nrf_saadc_channel_config_t channel_config_als = NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_AIN1);
    channel_config_als.gain = NRF_SAADC_GAIN1_5;        //增益为1/5
    channel_config_als.acq_time = NRF_SAADC_ACQTIME_20US;
    err_code = nrf_drv_saadc_channel_init(DRV_ILLUM_CHN, &channel_config_als);
    APP_ERROR_CHECK(err_code);

}
/**
* @function@name:    user_ullumination_current_to_lux_compute
* @description  :    Function for converting current to lux.
* @input        :    current_ua: current(unit is uA) value of sensor, lux: illumination intensity
* @output       :    none
* @return       :    none
*/
static uint32_t drv_ullumination_current_to_lux_compute(uint32_t const current_ua)
{
    if(current_ua <= 15)
    {
        return (676 * current_ua + 50) / 100;
    }
    else
    {
        return (49 * current_ua) /10 + 26;
    }
}
/**
* @function@name:    illumination_intensity_update
* @description  :    Function for performing battery measurement and updating the Battery Level characteristic
* @input        :    value: result of adc
* @output       :    none
* @return       :    none
**/
static uint32_t drv_illumination_intensity_update(int16_t const adc_value)
{	
    uint16_t vol_value;
    uint32_t c_ua;
	vol_value       =   adc_value*600*5/1024;                   //mV， 增益为1/5,所以这里要乘以5, AD 12bit, ref_v=0.6V, 这里扩大了1000倍，结果为毫伏
    c_ua = vol_value / DRV_ILLUM_DIVIDER_RESISTOR;             //mA    
    return drv_ullumination_current_to_lux_compute(c_ua);
}


/**
* @function@name:    illumination_intensity_measurement
* @description  :    Function for performing illumination intensity measurement and updating the lux value. 
* @input        :    none
* @output       :    none
* @return       :    none
*/
#define LUX_SAMPLES     20
static uint32_t drv_illumination_intensity_measurement(void)
{
    uint32_t lux_value = 0;
    uint8_t i;
    uint32_t sum=0, avg = 0;
    int16_t adc_result[LUX_SAMPLES]; 
    nrf_gpio_cfg_output(DRV_ILLUM_CTL_PIN);        //
    nrf_gpio_pin_set(DRV_ILLUM_CTL_PIN);           //打开光照传感器
    //saadc_init();
    nrf_saadc_enable();  
    for(i=0;i<LUX_SAMPLES;i++)
    {
        nrf_drv_saadc_sample_convert(DRV_ILLUM_CHN, &adc_result[i]);  
        vTaskDelay(1);
    }    
   
    nrf_saadc_disable();
    nrf_gpio_pin_clear(DRV_ILLUM_CTL_PIN);           //关闭光照传感器
    nrf_gpio_cfg_input(DRV_ILLUM_CTL_PIN,NRF_GPIO_PIN_PULLDOWN);
    
    user_app_rank_min_to_max_16(adc_result,LUX_SAMPLES);
    sum = 0;
    for(i=5;i<LUX_SAMPLES-5; i++)       //
    {
        sum += adc_result[i];
        //DBG_LOG("\r\n lux number %d adc value = %d\r\n",i,adc_result[i]);
    }
    avg = sum / (LUX_SAMPLES - 10);
    lux_value = drv_illumination_intensity_update(avg);
    
    //DBG_LOG("get illumination intensity adc = %d,  lux = %d",avg,lux_value);
    //DBG_LOG("illumination_intensity: Lux = %d lux\r\n\r\n", *lux_value);

    return lux_value;
}
/**
* @function@name:    drv_get_illumination_intensity
* @description  :    Function for performing illumination intensity measurement and updating the lux value. 
* @input        :    none
* @output       :    none
* @return       :    none
*/
int32_t drv_get_illumination_intensity(void)
{
    int32_t lux;
    lux = drv_illumination_intensity_measurement();
    return lux;
}

void user_illumination_intensity_init(void)
{
    drv_illumination_saadc_channel_init();
}
void user_illumination_intensity_uinit(void)
{
    drv_illumination_saadc_channle_uninit();
}
#endif      //NRF_MODULE_ENABLED(SAADC)


