/** 
 * user_algorithm.h
 *
 * @group:       nack_trap project
 * @author:      Chenfei
 * @createdate: 2018/4/2
 * @modifydate: 2018/4/4
 * @version:     V0.0
 *
 */
#ifndef     USER_ALGORITHM_H
#define     USER_ALGORITHM_H

#include <stdint.h>

#define     ALG_DATA_VALID_BIT_NUMS         BIT_16
#define     ALG_DATA_WINDOW_POINTS_NUMS     50
typedef enum
{
    BIT_8   =   1,
    BIT_16,
}bit_nums;

typedef struct 
{
    int8_t x;
    int8_t y;
    int8_t z;
} acc_data_point_8_t;

typedef struct 
{
    int16_t x;
    int16_t y;
    int16_t z;
} acc_data_point_16_t;

typedef struct 
{
    int32_t x;
    int32_t y;
    int32_t z;
} acc_data_point_32_t;

typedef struct 
{
    uint32_t time;
    acc_data_point_16_t odba;
    acc_data_point_16_t meandl;
    int16_t dummy[12];
}alg_behavior_record_t;



#endif  //

