/**--------------------------------------------------------------------------------------------------------
** Commpany     :       Druid
** Created by   :		chenfei
** Created date :		2017-10-24
** Version      :	    0.0
** Descriptions :		user_app.c
**--------------------------------------------------------------------------------------------------------*/


#include "nrf_drv_gpiote.h"

#include "hal_nrf_uart.h"
#include "user_app.h"
#include "SEGGER_RTT.h"
#include "SEGGER_RTT_Conf.h"
#include "user_task.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"
//#include "hal_acc.h"
#include "user_app_get_record.h"
//#include "hal_debug_log.h"
#include "user_app_log.h"
//#include "hal_rtc.h"
#include "alarm_clock.h"
#include "user_app_init.h"
#include "user_app_set.h"
#include "user_adc_common.h"
#include "W25Q.h"
#include "user_app_env.h"

/**
* @function@name:    user_app_init
* @description  :    Function for initializing application
* @input        :    none
* @output       :    none
* @return       :    none
*/
void user_app_init(void)
{    
 #if 0
    W25Q_Lock();  
    W25Q_Erase(0,DRV_GD25Q127_TOTAL_SIZE);
    W25Q_Unlock();
 #endif
    user_app_setting_init();
    //hal_rtc2_init(user_app_param_init()); 
    DBG_LOG("\r\nCollar app star\r\n");  
    //user_identity_t iden;
    //app_iden_get_device_id_char(iden.dev_id);
    //app_add_colon_to_mac_addr(iden.dev_id);
    //uint8_t temp[10];
    
}
/**
* @function@name:    user_app_process
* @description  :    Function for process application
* @input        :    none
* @output       :    none
* @return       :    none
*/
void user_app_handle(void)
{
    
}
//#include ".h"
void vApplicationStackOverflowHook(TaskHandle_t xTask, char *pcTaskName)
{
    DBG_LOG("\r\n&&&&&&&&&&&application stack over flow taskhandle = %x &&&&&&&&&&\r\n",xTask);
    DBG_LOG("\r\ntask name is: %s\r\n",pcTaskName);
}
