/** 
 * user_app_init.c
 *
 * @group:       collar project
 * @author:      Chenfei
 * @create time: 2018/5/3
 * @version:     V0.0
 *
 */

#include "user_app_init.h"
#include "user_app_set.h"
#include "hal_nrf_flash.h"
#include "nrf_error.h"
#include "user_app_origin.h"
//#include "user_app_bhv.h"
#include "user_app_estrus.h"
#include "user_app_env.h"
/*
static bool user_app_get_param_from_memory(user_app_setting_t *p_dev_param_t)
{
    if(user_nrf_flash_get_bytes((uint32_t *)user_nrf_flash_get_bytes,(uint8_t *)p_dev_param_t,sizeof(p_dev_param_t)) != NRF_SUCCESS)
    {
        return false;
    }
    return true;
}
static bool user_app_save_dev_param_into_memory(user_app_setting_t *p_dev_param_t)
{
    user_save_settings(p_dev_param_t);
    //user_save_datas_as_page(USER_DATA_MEMORY_START,(uint8_t *)p_dev_param_t,sizeof(p_dev_param_t));
    return true;
}
static bool user_app_initial_device_param(user_app_setting_t const p_dev_param_t)
{
    user_app_setting_t p_dev_param_init;
    
    user_app_get_param_from_memory(&p_dev_param_init);
    //user_acc_set_sameple_time(p_dev_param_init.origin_time,p_dev_param_init.origin_mode,p_dev_param_init.origin_time_count);
    return true;
}
*/
/**
* @functionname : user_app_param_init 
* @description  : function for initializing the device all parameters, which get from flash memory
* @input        : none
* @output       : none 
* @return       : timestatmp: time saved at last running, if not true time, initialize to 2018/1/1 00:00:00
*/
/*
uint32_t user_app_param_init(void)
{
    uint32_t    timestatmp = 1514736000;    //initialize at 2018/1/1 00:00:00
    user_app_setting_t     a_dev_param_t;
    user_app_get_param_from_memory(&a_dev_param_t);
    
    return timestatmp;
}
*/
//end
