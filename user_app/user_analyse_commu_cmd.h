#ifndef     USER_ANALYSE_COMMU_CMD_H
#define     USER_ANALYSE_COMMU_CMD_H

#include <stdint.h>
#include <user_data_pkg.h>

typedef bool (*p_app_rsp_parse)(void const* const p_buf, int32_t const len);

typedef enum
{
    USER_MSG_CHANNLE_BLE = 0,
    USER_MSG_CHANNLE_NB,
    USER_MSG_CHANNLE_MAX,
}user_msg_channle;

typedef struct
{
    user_msg_channle    interface_type;
    uint8_t             length;
    uint8_t             msg[255];
}queue_msg_t;

uint16_t general_frame_construct_frame(uint8_t *p_dest,const pkg_prot_head_t *p_head, const uint8_t *p_data);
uint16_t general_frame_get_frame_info(uint8_t *p_data,pkg_prot_head_t *p_head, const uint8_t * p_frame);
void user_parse_send_queue(uint8_t * const p_buf, uint8_t const len, uint8_t const msg_chn);
void user_creat_inst_parse_task(void);


#endif      //USER_ANALYSE_COMMU_CMD_H

