/** 
 * user_app_get_record.c
 *
 * @group:       collar project
 * @author:      Chenfei
 * @create time: 2018/4/16
 * @version:     V0.0
 *
 */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>

#include "user_data_struct.h"
#include "alltypes.pb.h"
#include "test_helpers.h"
#include "unittests.h"
#include "SEGGER_RTT.h"
#include "SEGGER_RTT_Conf.h"
#include "Define.pb.h"
#include "hal_per_flash.h"
#include "user_data_struct.h"
#include "Record.h"
#include "behavior.h"

/**
* @functionname : user_app_get_behavior_record 
* @description  : function for get records from memory
* @input        : bhv_records_buf: pointor behavior origin data buffer, 
*               : record_num: record numbers will be read
* @output       : pt_bhv_t: pointor to behavior data of protocol buffer 
* @return       : true if successed
*/
uint32_t user_app_get_behavior_record( 
                                      protocol_behavior2_t  *pt_bhv_t[], 
                                      uint8_t record_num
                                      )
{
    Behavior_Record_t     *bhv_records_buf[record_num];
    
    if(pt_bhv_t == NULL)
    {
        return 0;
    }
    uint32_t bytes_len = record_num*sizeof(Behavior_Record_t);
    Record_Read(RECORD_TYPE_BEHAVIOR, (uint8_t *)bhv_records_buf, bytes_len);
    for(uint8_t i=0; i<record_num;i++)
    {
        pt_bhv_t[i]->has_MeandlX = true;
        pt_bhv_t[i]->has_MeandlY = true;
        pt_bhv_t[i]->has_MeandlZ = true;
        
        pt_bhv_t[i]->has_ODBA    = true;
        
        pt_bhv_t[i]->has_ODBAX   = true;
        pt_bhv_t[i]->has_ODBAY   = true;
        pt_bhv_t[i]->has_ODBAZ   = true;
        
        pt_bhv_t[i]->has_Timestamp=true;
        
        pt_bhv_t[i]->MeandlX     = bhv_records_buf[i]->meandl.x;
        pt_bhv_t[i]->MeandlY     = bhv_records_buf[i]->meandl.y;
        pt_bhv_t[i]->MeandlZ     = bhv_records_buf[i]->meandl.z;
        
        pt_bhv_t[i]->ODBA        = bhv_records_buf[i]->m_odba;
        
        pt_bhv_t[i]->ODBAX       = bhv_records_buf[i]->odba.x;
        pt_bhv_t[i]->ODBAY       = bhv_records_buf[i]->odba.y;
        pt_bhv_t[i]->ODBAZ       = bhv_records_buf[i]->odba.z;
        
        pt_bhv_t[i]->Timestamp   = bhv_records_buf[i]->time;
    }
    return bytes_len;
}
