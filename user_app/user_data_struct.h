#ifndef     USER_DATA_STRUCT_H
#define     USER_DATA_STRUCT_H

#include <stdint.h>
#include <stdbool.h>
#include "pb.h"
#include "pb_encode.h"
#include "pb_decode.h"
#include "Behavior2.pb.h"
#include "IdentityMsg.pb.h"
#include "Env.pb.h"
#include "Estrus.pb.h"
#include "Warning.pb.h"
#include "Setting.pb.h"
#include "user_app_hal.h"

typedef struct
{
    uint32_t    start_address;
    uint32_t    cnt;
    uint8_t     index[50];
}ds_encoded_record_index_t;

typedef struct
{
    uint32_t    start_address;
    uint32_t    cnt;
    uint8_t     index[50];
}ds_encoded_record_bhv_saved_index_t;

typedef struct
{
    uint32_t    start_address;
    uint32_t    cnt;
    uint8_t     index[50];
}ds_encoded_record_env_saved_index_t;

typedef struct
{
    uint32_t    start_address;
    uint32_t    cnt;
    uint8_t     index[50];
}ds_encoded_record_warn_saved_index_t;

typedef struct
{
    uint32_t    start_address;
    uint32_t    cnt;
    uint8_t     index[50];
}ds_encoded_record_est_saved_index_t;

typedef struct
{
    uint32_t    start_address;
    ds_encoded_record_bhv_saved_index_t     bhv_store_index;
    ds_encoded_record_env_saved_index_t     env_store_index;
    ds_encoded_record_warn_saved_index_t    war_store_index;
    ds_encoded_record_est_saved_index_t     est_store_index;
}ds_encoded_record_store_index_t;

typedef struct
{
    uint32_t record_count;
    protocol_behavior2_t bhv_t;
}bhv_head_t;

void user_encode_decode_test(void);

void user_data_pb_bhv_encode_test(void);
void user_data_pb_bhv_decode_test(void);

bool user_data_pb_bhv_encode(//protocol_behavior2_req_t * p_bhv_req_t, 
                             protocol_behavior2_t * p_bhv_t[],
                             uint32_t record_num,
                             identity_msg p_identity_msg,
                             //void * device_id,
                             //void * manufactory_id);
                             uint8_t *bhv_encode_buffer,
                             uint16_t *encode_length
                            );

#endif      //USER_DATA_STRUCT_H
