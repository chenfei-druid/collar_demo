
#ifndef HAL_NRF_UART_H
#define HAL_NRF_UART_H

#include <stdint.h>
#include <stdbool.h>
#include "app_uart.h"
#include "FreeRTOS.h"
#include "nrf_sdh_freertos.h"
#include "semphr.h"

//uart buffer size for easyDMA
#define UART_TX_BUF_SIZE                256                                         /**< UART TX buffer size. */
#define UART_RX_BUF_SIZE                512                                      /**< UART RX buffer size. */

#define UART_BUF_SIZE_MAX               550//(512+21+10)  
//#define UART_TRANSMISION_MAX_LEN        60

#define UART_CHANNEL_MAX_NUMS           3   //UART channel max numbers

#define UART_IDEN_MAX_LEN               5    //uart rx start or end indentifier lenght
#define UART_CHN_NOT_USE                0XFF

typedef enum
{       
    UART_CHANNEL_MIN = 0,   
    UART_CHANNEL_NB = 0,
    UART_CHANNEL_DEBUG ,
    UART_CHANNEL_MAX,
}uart_channel;

//uart base communication parameter
typedef struct
{
    uint8_t buffer[UART_BUF_SIZE_MAX];
    uint16_t data_cnt;
}uart_rx_t;
//uart base communication parameter
typedef struct
{
    uint32_t    baud_rate;      //uart baud rate
    bool        parity;         // Even parity if TRUE, no parity if FALSE
}uart_parameter;

//uart of rx message
typedef struct
{
    char    buff[50];           //uart received data buffer
    uint8_t length;             //uart received data size
    bool    flag;               //uart received data successed
}uart_rx;

//uart of tx message
typedef struct
{
    char    buff[50];           //uart trasmit buffer
    uint8_t length;             //uart transmit size
    bool    flag;               //uart transmit complete flag
}uart_tx;

//uart interface pin
typedef struct 
{
    uint32_t        rx_pin;     //uart RXD pin
    uint32_t        tx_pin;     //uart TXD pin
    uint32_t        cts_pin;    //uart cts pin
    uint32_t        rts_pin;    //uart rts pin
}uart_pin;

//uart message
typedef struct
{
    uart_tx         tx;
    uart_rx         rx;    
}uart_msg;
//uart start and end charactor
typedef struct
{
    uint8_t     flag_start[UART_IDEN_MAX_LEN];      //start charactor buffer, the maximum length @ref UART_IDEN_MAX_LEN
    uint8_t     flag_start_length;                  //start charactor length
    uint8_t     flag_end[UART_IDEN_MAX_LEN];        //end charactor buffer,the maximum length @ref UART_IDEN_MAX_LEN
    uint8_t     flag_end_length;                    //end charactor length
}uart_start_end_flag;

//uart interface attributes
typedef struct
{
    uart_start_end_flag     uart_flag;
    app_uart_comm_params_t  uart_params_t;
    uint8_t                 csc_start;
    bool                    csc_mode;
    //bool                    if_en_event;
}uart_attr;

//uart config parameter whole
typedef struct
{
    uart_attr   uart_attr_t[UART_CHANNEL_MAX_NUMS];
    uint8_t     channel_nums;
}uart_conf;

extern  QueueHandle_t       queue_uart_tx;
//extern bool        tx_complete_flag;

//void log_init(void);
//void uart_init(void);
void hal_uart_close(void);
bool hal_uart_send_ch(uint8_t ch);
//void hal_uart_send_string(uint8_t *str, uint32_t len, bool crlf_mode);
bool hal_uart_send_string(const char *str);
bool hal_uart_send_data(const uint8_t *dt, int32_t len);
bool hal_uart_chnl_config(uart_attr * p_uart_attr, uint8_t chn);
void hal_uart_change_channel(uint8_t chn);
bool hal_uart_channel_config(uart_start_end_flag * p_m_uart_flag,app_uart_comm_params_t * p_uart_attr);
bool hal_uart_get_data(char *temp_c);

void hal_uart_rx_judge(void);
bool hal_uart_check_rx(uint8_t *p_buf, uint8_t *len);
extern uint32_t app_uart_get_rx_fifo_addr(uint8_t *buffer);
extern uint32_t app_uart_get_rx_fifo_datas(uint8_t *p_buf, uint8_t *len);
extern uint32_t app_uart_clear_rx_buf(void);
extern uint32_t app_uart_get_rx_amount(void);
uint8_t hal_uart_channel_check(void);
bool hal_clear_uart_rx_buf(void);
void hal_uart_set_parity(bool parity, uint8_t chn);
void hal_uart_set_buad_rate(uint32_t baud_rate, uint8_t chn);
void hal_uart_lock_channel(void);
void hal_uart_unlock_channel(void);
void hal_uart_chn_release(uint8_t chn);
uint32_t hal_uart_get_rx_fifo_datas(uint8_t *p_buf, uint16_t max_len, uart_channel chn_type);
uint32_t uart_receive(void *buf, uint32_t max_size,uint32_t timeout);

#ifdef FREERTOS
void hal_creat_uart_handler_task(void);
#endif     
#endif //hal_NRF_UART_H



