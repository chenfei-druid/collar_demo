#ifndef USER_SEND_DATA_H
#define USER_SEND_DATA_H

#include <stdint.h>
#include <stdbool.h>
#include "user_data_pkg.h"
#include "freertos_platform.h"

#define BLE_TX_COMPLETE_WAIT    4000     //MS
xSemaphoreHandle user_create_ble_tx_semaphore(void);
bool user_send_data_by_ble(uint8_t *const buffer, int32_t length);
bool user_send_data_by_nb(uint8_t *const buffer, int32_t length);
bool user_send_data(uint8_t * const buffer, int32_t length,trans_interface_type chn_type);
void user_give_ble_tx_complete_semaphore(void);

#endif      //USER_SEND_DATA_H

