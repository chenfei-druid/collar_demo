/**
 * drv_nv_modul.c
 * Date:    2018/3/29
 * Author:  Chenfei
 * 
 * @brief driver for NB-IOT modul .
 * by  nrf_52832 freertos platform
 *
 */

#include <stdint.h>
#include <string.h>
#include "hal_nrf_uart.h"
#include "drv_nb_modul.h"
#include "nordic_common.h"
#include "nrf.h"
#include "app_error.h"
//#include "FreeRTOS.h"
#include "nrf_sdh_freertos.h"
//#include "semphr.h"
#include "task.h"
//#include "nrf_delay.h"
#include "user_app.h"
#include "SEGGER_RTT.h"
#include "SEGGER_RTT_Conf.h"
#include "nrf_drv_gpiote.h"
#include "user_app_env.h"
#include "user_app_log.h"
#include "HexStr.h"
#include "drv_nb_modul.h"
#include "user_app_estrus.h"
#include "user_common_alg.h"
#include "user_analyse_commu_cmd.h"
#include "user_app_warning.h"
#include "user_app_register.h"
#include "user_ble_inter_mgt.h"
#include "user_task.h"

/*********************************************define variable***********************************************/
#define     S                   1000
#define     AT_SEND_DATA        ("AT+NMGS=")
//handle define
//static      bool                nv_tx_complete_flag = 0;
static      TaskHandle_t        nb_rx_handle;
static      TaskHandle_t        nb_tx_handle;
//queue define
static      xSemaphoreHandle    binary_semaphore_nb_tx;
static      xSemaphoreHandle    binary_semaphore_nb_rx;
//static      QueueHandle_t       queue_nb_rx;
            QueueHandle_t       queue_nb_mode;
            
static      nb_comm_setting_t   nb_commu_setting = {.mode = DRV_NB_COMMU_MODE_UNDEFINE,
                                                    .interval   = DRV_NB_DEFAULT_UP_INTERVAL,
                                                    .time_list  = {1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0}
                                                    };
static      const char socket[] = "AT+NSOCR=DGRAM,17,5001,1\r\0";       //establish socket befor send data
static      const char nb_udp_ip[] = "0,47.52.138.210,9410,\0";          //send data to server of ip:
static      const char cmd_udp_tx[] = "AT+NSOST=0,47.52.138.210,9410,\0";          //send data to server command:                                                    

            char nb_imei[16] = {0};
            char nb_imsi[16] = {0};
static      char trans_rev_buf[600] = {0};

/**/
static  const char          nb_at_command_t[][50] = {
    
    //"AT+NBAND=5\r\0",       //ask frequency band
    
    "AT+CFUN=1\r\0",        //open RF all function
    "AT+CGATT=1\r\0"
    "AT+CMEE=1\r\0",        // open wrong indication
    "AT+NNMI=1\r\0",        // open coap down data notification,not need under UDP
    "AT+CSCON=1\r\0",       //  set base station connect inform
    "AT+NPSMR=1\r\0",       // set mode PSM inform       
    "AT+NCDP?\r\0",         //ask ncdp ip
    //"AT+NMSTATUS?\r\0",     //      
};
/*********************************************functions start***********************************************/
bool user_nb_communication_setting(nb_comm_setting_t setting)
{
    if(setting.mode >= DRV_NB_COMMU_MODE_MAX)
    {
        DBG_LOG("\r\nnb communication set, invalid mode is %d\r\n",setting.mode);
        return 0;
    }
    if(setting.interval == 0xffffffff)
    {
        DBG_LOG("\r\nnb communication set, invalid interval = %d\r\n",setting.interval);
        return 0;
    }
    for(uint8_t i=0;i<24;i++)
    {
        if(setting.time_list[i] != '0' && setting.time_list[i] != '1')
        {
            DBG_LOG("\r\nnb communication set, invalid time list %s \r\n",setting.time_list);
            return 0;
        }
    }
    bool ret = true;
    switch(setting.mode)
    {
        case DRV_NB_COMMU_MODE_UNDEFINE:
            DBG_LOG("\r\nnb communication set, mode = DRV_NB_COMMU_MODE_UNDEFINE\r\n");
            nb_commu_setting.mode = DRV_NB_COMMU_MODE_UNDEFINE;
            nb_commu_setting.interval = DRV_NB_DEFAULT_UP_INTERVAL;
            memset(nb_commu_setting.time_list,0,sizeof(nb_commu_setting.time_list));
        
            break;
        case DRV_NB_COMMU_MODE_INTERVAL:
            DBG_LOG("\r\nnb communication set, mode = DRV_NB_COMMU_MODE_INTERVAL\r\n");
            nb_commu_setting.mode = DRV_NB_COMMU_MODE_INTERVAL;
            if(setting.interval < 60)       //<60S
            {
                nb_commu_setting.interval = DRV_NB_DEFAULT_UP_INTERVAL;
            }
            else
            {
                nb_commu_setting.interval = setting.interval;
            }
            DBG_LOG("\r\ninterval = %d\r\n",nb_commu_setting.interval);
            //memset(nb_commu_setting.time_list,0,sizeof(nb_commu_setting.time_list));
            break;
        //case DRV_NB_COMMU_MODE_ONLINE: 
        //    nb_commu_setting.mode = DRV_NB_COMMU_MODE_ONLINE;
        //    break;
        case DRV_NB_COMMU_MODE_TIMER:
            DBG_LOG("\r\nnb communication set, mode = DRV_NB_COMMU_MODE_TIMER\r\n");
            nb_commu_setting.mode = DRV_NB_COMMU_MODE_TIMER;
            nb_commu_setting.interval = 0;
            for(uint8_t i=0;i<24;i++)
            {
                nb_commu_setting.time_list[i] = setting.time_list[i];
            }
            DBG_LOG("\r\n%s\r\n",nb_commu_setting.time_list);
            break;
        default:
            DBG_LOG("\r\nnb communication set, mode = invalid\r\n");
            ret = false;
            break;
    }   
    user_nb_send_queue_to_task(NB_TRANS_TASK_RUN_TYPE_SETTING);
    return ret;
}
static uint32_t user_nb_caculate_next_idle_time(void)
{
    uint32_t timestamp;
    uint8_t pos;
    uint32_t ret;       
    
    switch(nb_commu_setting.mode)
    {
        case DRV_NB_COMMU_MODE_UNDEFINE:
            
            ret = DRV_NB_DEFAULT_UP_INTERVAL;
        
            break;
        case DRV_NB_COMMU_MODE_INTERVAL:
            
            ret = (nb_commu_setting.interval);
            
            break;
        case DRV_NB_COMMU_MODE_ONLINE: 
            ret = DRV_NB_DEFAULT_UP_INTERVAL;
            break;
        case DRV_NB_COMMU_MODE_TIMER:
            timestamp = hal_rtc2_get_unix_time();
            pos = (timestamp%86400) / 3600;
            if(pos >= 24)
                ret = 86400;    //if wrong, return 3600s for next time interval, but this is impossible
            for(uint8_t i=pos+1;i<24;i++)
            {
                if(nb_commu_setting.time_list[i] == '1')
                {
                    ret = (i *3600 - (timestamp%86400));
                    break;
                }
            }
            
            break;
        default:
            ret = false;
            break;
    } 
    return ret;
}
static void drv_nb_uart_config(void)
{
    static uart_attr    app_uart_attr ;

    app_uart_attr.uart_params_t.baud_rate     = UART_BAUDRATE_BAUDRATE_Baud9600;
    app_uart_attr.uart_params_t.flow_control  = APP_UART_FLOW_CONTROL_DISABLED;
    app_uart_attr.uart_params_t.rx_pin_no     = DRV_NB_TXD_UART_MCU_RXD_PIN;
    app_uart_attr.uart_params_t.tx_pin_no     = PIN_NB_RXD_UART_MCU_TXD_PIN;
   
    hal_uart_chnl_config(&app_uart_attr,UART_CHANNEL_NB);
    
}
static void drv_nb_uart_channel_select(void)
{
    drv_nb_uart_config();
    hal_uart_change_channel(UART_CHANNEL_NB);
    //vTaskDelay(200);
}

/**
* @function@name:    drv_nb_get_response
* @description  :    function for get at response from nb-iot module
* @input        :    str: must response string,   timeout: if exceed this time and no valid response, stop receive                   
* @output       :    none
* @return       :    true if receive string success
*/
static bool drv_nb_get_response(char const* const str,uint32_t timeout)
{
    //int8_t  dly_cnt = timeout/500;  //10ms receive once
    uint8_t res_buf[100] = {0};
    //do
    //{
        uart_receive(res_buf,sizeof(res_buf),timeout);
        //vTaskDelay(500);  
        //res_len += hal_uart_get_rx_fifo_datas(res_buf,sizeof(res_buf),UART_CHANNEL_NB);
        DBG_LOG("\r\nresponse: %s\r\n",res_buf);
        if(strstr((char *)res_buf, str) != NULL)
        {            
            return true;
        }
        //memset(res_buf, 0, sizeof(res_buf));
        //res_len = 0;
    //}while(dly_cnt--);
    return false;
}
/**
* @function@name:    drv_nb_at_cmd_trans
* @description  :    function for transmit an at command, and set timeout and retry
* @input        :    timeout: if exceed this time and no valid response, stop receive
*                    retry: retry times if failed
* @output       :    none
* @return       :    true if transmit success
*/
static bool drv_nb_at_cmd_trans(const char *p_send, uint32_t timeout, int8_t retry)
{
    if(p_send == NULL)
    {
        DBG_LOG("\r\nat cmd is null\r\n");
        return false;
    }
    //char res_buf[50];
//    uint8_t res_len = 0;
//    bool ret;
    for(uint8_t i=0; i<retry; i++)
    {
        hal_uart_send_string(p_send);
        DBG_LOG("\r\nsend cmd: %s\r\n", p_send);
        if(drv_nb_get_response("\r\nOK\r\n",timeout))
        {
            return true;
        }       
    }
    DBG_LOG("\r\n at cmd failed: %s\r\n",p_send);
    return false;
}
/**
* @function@name:    drv_nb_init_module
* @description  :    function for initializing the NB-IOT module, prepare to transmit data
* @input        :    none
* @output       :    none
* @return       :    true if success
*/
static bool drv_nb_init_module(void)
{
    uint8_t cmd_nums = sizeof(nb_at_command_t) / sizeof(nb_at_command_t[0]);
    for(uint8_t i=0; i<cmd_nums; i++)
    {
        
        if(drv_nb_at_cmd_trans(nb_at_command_t[i],2000,2) == false)
        {
            return false;
        }
    }
    return true;
}
/**
* @function@name:    drv_nb_set_min_func
* @description  :    function for reboot nb device
* @input        :    none:                     
* @output       :    none
* @return       :    none

*/
static bool drv_nb_reboot(void)
{
    char res_buf[100] = {0};
//    uint8_t res_len = 0;
    DBG_LOG("\r\nsend cmd: AT+NRB\r\n");
    hal_uart_send_string("AT+NRB\r\0");    
    //vTaskDelay(10000);     
    bool ret = false;    
    do
    {
        vTaskDelay(5000);        
        hal_uart_get_rx_fifo_datas((uint8_t *)res_buf,sizeof(res_buf),UART_CHANNEL_NB);
        DBG_LOG("\r\nreboot cmd response: %s",res_buf);
        if(strstr(res_buf, "REBOOT_CAUSE") && strstr(res_buf, "Neul \r\nOK\r\n"))
        //if(strstr(res_buf, "+CSCON:1"))
        {
            ret = true;            
        }
        memset(res_buf, 0, sizeof(res_buf));
        //res_len = 0;
    }while(!ret);
    vTaskDelay(5000);
    DBG_LOG("\r\n%s\r\n",res_buf);
    return true;
}


/**
* @function@name:    drv_nb_check_register_status
* @description  :    function for chech Message Registration Status,that Report the current registration status when connected to the CDP server.
* @input        :    none:                     
* @output       :    none
* @return       :    none
*/
static bool drv_nb_check_register_status(void)
{
    char res_buf[50] = {0};
    //uint8_t res_len;
    //int32_t retry = 0;
    
//    bool ret;
    for(uint8_t i=0;i<60;i++)
    {
        hal_uart_send_string("AT+NMSTATUS?\r\0");
        DBG_LOG("\r\nsend cmd: AT+NMSTATUS?\r\n");
        //vTaskDelay(3000);
        //hal_uart_get_rx_fifo_datas((uint8_t *)res_buf,sizeof(res_buf),UART_CHANNEL_NB);
        uart_receive(res_buf,sizeof(res_buf),1000);
        DBG_LOG("\r\nresponse: %s",res_buf);        
        //ret = strstr(res_buf, "+NMSTATUS:REGISTERED\r\nOK");+NMSTATUS:REGISTERED\r\nOK
        //if(strstr(res_buf, "+NMSTATUS:INIITIALISED\r\n\r\nOK\r\n") || strstr(res_buf, "+NMSTATUS:REGISTERED\r\n\r\nOK\r\n"))
        if(strstr(res_buf, "+NMSTATUS:MO_DATA_ENABLED\r\n\r\nOK\r\n") )   
        {
            return true;
        }
        memset(res_buf, 0, sizeof(res_buf));
    }
    return false;
}
/**
* @function@name:    drv_nb_establish_socket
* @description  :    function for establish the socket, must do it before transmit data
* @input        :    none:                     
* @output       :    none
* @return       :    return ture if established successful
*/
static bool drv_nb_establish_socket(void)
{
    DBG_LOG("\r\n********establish socket...*************\r\n");
    char res_buf[30] = {0};
    //int32_t res_len = 0;        
    for(uint8_t i=0;i<2;i++)
    {
        hal_uart_send_string(socket);      //send data command
        DBG_LOG("\r\nnb send command is:\r\n %s\r\n",socket);
        uart_receive(res_buf,sizeof(res_buf),500);
        //vTaskDelay(1000);
        //hal_uart_get_rx_fifo_datas((uint8_t *)res_buf,sizeof(res_buf),UART_CHANNEL_NB);
        DBG_LOG("\r\nsocket response: %s",res_buf);
        if(strstr(res_buf, "OK\r\n") != NULL)
        {
            DBG_LOG("\r\n********establish socket success*********\r\n");
            return true;
        }
        memset(res_buf,0,sizeof(res_buf));
        //res_len = 0;
    }
    DBG_LOG("\r\n********establish socket failed*********\r\n");
    return false;
}
static bool drv_nb_close_socket(void)
{
    char res_buf[30] = {0};
    ;
    for(uint8_t i=0;i<1;i++)
    {
    DBG_LOG("\r\nsend cmd: AT+NSOCL=0\r\n");
    hal_uart_send_string("AT+NSOCL=0\r\0");
    uart_receive(res_buf,sizeof(res_buf),500);
    //vTaskDelay(1000);
    //uint8_t res_len = hal_uart_get_rx_fifo_datas((uint8_t *)res_buf,sizeof(res_buf),UART_CHANNEL_NB);
    DBG_LOG("\r\nresponse: %s",res_buf);
    if(strstr(res_buf, "OK\r\n") != NULL)
    {            
        return true;
    }
    }
    return false;
}
bool drv_nb_check_attached(void)
{
    char res_buf[50] = {0};

    for(uint8_t i=0;i<10;i++)
    {
        hal_uart_send_string("AT+CGATT?\r\0");
        DBG_LOG("\r\nsend cmd: AT+CGATT?\r\n");

        uart_receive(res_buf,sizeof(res_buf),1000);
        DBG_LOG("\r\nresponse: %s",res_buf);        
        if(strstr(res_buf, "+CGATT:1\r\n\r\nOK\r\n") )   
        {
            return true;
        }
        memset(res_buf, 0, sizeof(res_buf));
        vTaskDelay(3000);
    }
    return false;
}
bool drv_nb_register_status(void)
{
    char res_buf[50] = {0};
    for(uint8_t i=0;i<10;i++)
    {
        hal_uart_send_string("AT+CEREG?\r\0");
        DBG_LOG("\r\nsend cmd: AT+CEREG?\r\n");

        uart_receive(res_buf,sizeof(res_buf),1000);
        DBG_LOG("\r\nresponse: %s",res_buf);        
        if(strstr(res_buf, "+CEREG:1,1\r\n\r\nOK\r\n") )   
        {
            return true;
        }
        memset(res_buf, 0, sizeof(res_buf));
        vTaskDelay(3000);
    }
    return false;
}
int32_t drv_nb_get_imsi(char * p_imsi)
{
    char res_buf[100] = {0};
    for(uint8_t i=0;i<3;i++)
    { 
        DBG_LOG("\r\nsend cmd: AT+CIMI\r\n");
        hal_uart_send_string("AT+CIMI\r\0");
        uart_receive(res_buf,sizeof(res_buf),1000);
        //vTaskDelay(1000);
        //uint8_t res_len = hal_uart_get_rx_fifo_datas((uint8_t *)res_buf,sizeof(res_buf),UART_CHANNEL_NB);
        DBG_LOG("\r\nresponse: %s",res_buf);
           
        if(strstr(res_buf, "OK\r\n") != NULL)
        {      
            memcpy(p_imsi,res_buf+2,15);
            DBG_LOG("\r\nIMSI: %s\r\n",p_imsi);
            return true;
        }
        memset(res_buf,0,sizeof(res_buf));
    }
    return false;
}
bool drv_nb_get_imei(char * p_imei)
{
    char res_buf[100] = {0};
    char *p_ret;
    for(uint8_t i=0;i<3;i++)
    { 
        DBG_LOG("\r\nsend cmd: AT+CGSN=1\r\n");
        hal_uart_send_string("AT+CGSN=1\r\0");
        uart_receive(res_buf,sizeof(res_buf),1000);
        //vTaskDelay(1000);
        //uint8_t res_len = hal_uart_get_rx_fifo_datas((uint8_t *)res_buf,sizeof(res_buf),UART_CHANNEL_NB);
        DBG_LOG("\r\nresponse: %s",res_buf);
         
        if(strstr(res_buf, "OK\r\n") != NULL)
        {      
            p_ret = strstr(res_buf, "+CGSN:");
            memcpy(p_imei,p_ret+6,14);
            DBG_LOG("\r\nIMEI: %s\r\n",p_imei);
            return true;
        }
        memset(res_buf,0,sizeof(res_buf));
    }
    return false;
}
static bool drv_nb_module_init(void)
{
       
    drv_nb_at_cmd_trans("AT+CFUN=1\r\0",1000,2);

    drv_nb_at_cmd_trans("AT+CEREG=1\r\0",1000,2);
 
    drv_nb_at_cmd_trans("AT+CGATT=1\r\0",1000,2);
 
    drv_nb_at_cmd_trans("AT+CPSMS=1,,,01011111,00100001\r\0",1000,2);

    //drv_nb_check_register_status();
    drv_nb_register_status();
    drv_nb_check_attached();
    return true;
}
static void drv_nb_pwr_on(void)
{
    nrf_gpio_cfg_output(DRV_NB_PWR_PIN);
    nrf_gpio_pin_set(DRV_NB_PWR_PIN);
    vTaskDelay(5000);  
    char res_buf[100] = {0};
    uint8_t res_len = 0; 
    do
    {
        res_len = uart_receive(res_buf,sizeof(res_buf),500);
    }while(res_len);
    //hal_uart_get_rx_fifo_datas((uint8_t *)res_buf,sizeof(res_buf),UART_CHANNEL_NB);
    DBG_LOG("\r\npower on response: %s",res_buf);
}

/**
* @functionname : drv_nb_get_response_valid_string 
* @description  : function for getting data field in response
*               response:has socket, IP, PORT, length field, data filed
*               1,10.11.12.13,32701,8,616172647661726B,0 \r\n
*               \r\n
*               OK \r\n                
* @input        : p_src_str: source string pointer, size: max space of bytes  
* @output       : p_dst_str: valid data will be copy to here 
* @return       : string length obtained
*/
int32_t drv_nb_get_response_valid_string(char * const p_dst_str,char * const p_src_str,int16_t size)
{
    uint16_t cnt = 0;
    char *p_src = p_src_str;
    char *p_dst = p_dst_str;
    if(p_dst_str == NULL || p_src_str == NULL)
    {
        return 0;
    }
    while(*p_src)
    {
        if((p_dst - p_dst_str) > size || (p_src - p_src_str) > size)
        {
            return 0;
        }
        if((*p_src >= '0' && *p_src <= '9') || \
           (*p_src >= 'A' && *p_src <= 'Z') || \
           (*p_src >= 'a' && *p_src <= 'z') 
        )
        {
            *p_dst++ = *p_src++;
            cnt++;
        }
        else
        {
            //return i;
            break;
        }        
    }
    
    *p_dst = 0;
    return cnt;
}
/**
* @functionname : drv_nb_get_response_valid_length 
* @description  : function for get the given hex length in response message by command "AT+NSORF=0,256"
*               response:has socket, IP, PORT, length field, data filed
*               1,10.11.12.13,32701,8,616172647661726B,0 \r\n
*               \r\n
*               OK \r\n  
* @input        : p_src: received response message  
* @output       : len_field: the given length, char to int 
* @return       : position of next field
*/
char * drv_nb_get_response_valid_length(char * const p_src, uint8_t * len_field)
{
    char *p;
    //char size_buf[3];
    int16_t size = 0;
//    uint8_t res_valid_len = 0;
    p = strstr(p_src, nb_udp_ip);
    if(p == NULL)
    {
        DBG_LOG("\r\nnot find udp ip\r\n");
        return 0;
    }
    DBG_LOG("\r\nfind udp ip position = 0x%x \r\n",(uint32_t)p);
    p += strlen(nb_udp_ip); //point to length field
    DBG_LOG("\r\nfind length field position = 0x%x \r\n",(uint32_t)p);
    while(*p && *p != ',')
    {
        size = size*10 + *p - '0';
        p++;        
    }
    DBG_LOG("\r\nsize = %d \r\n",size);
    if(size > 256)
    {
        size = 0;
        return 0;
    }
    *len_field = size;
    p++;        //over ','
    return p;   //return valid data position
}
/**
* @functionname : drv_nb_abtain_data 
* @description  : function for get valid message and transmit to command analysis
*               response:has socket, IP, PORT, length field, data filed
*               1,10.11.12.13,32701,8,616172647661726B,0 \r\n
*               \r\n
*               OK \r\n  
* @input        : p_str: received response message  , size: max space of bytes  
* @output       : none
* @return       : length of data field
*/
int32_t drv_nb_abtain_data(char * const p_str,int16_t size)
{
    /*abtain valid data, behined of ip, for example:
    1,10.11.12.13,32701,8,616172647661726B,0
    
    OK
    */
    DBG_LOG("\r\nobtain data: p_str= 0x%x, max space = %d\r\n",p_str,size);
    uint8_t hex_buf[256] = {0};
    uint8_t len_field = 0;      //give length, hex length
    uint16_t str_len = 0;       //the string length, one hex has two char
    char *pos;
    pos = drv_nb_get_response_valid_length(p_str,&len_field);
    if(pos == NULL)
    {
        return 0;
    }
    DBG_LOG("\r\nnb_abtain_data:get valid length return pos = 0x%x \r\n",pos);
    DBG_LOG("\r\nlen_field = %d\r\n",len_field);
    if(pos == NULL)
    {
        return 0;
    }
    str_len = drv_nb_get_response_valid_string(p_str,pos,size-(pos-p_str));
    DBG_LOG("\r\nnb_abtain_data:drv_nb_get_response_valid_string str_len = %d \r\n",str_len);
    
    if(str_len != len_field * 2 || str_len == 0)
    {
        DBG_LOG("\r\nnb_abtain_data:invalid length, len_field = %d, count len = %d\r\n",len_field*2,str_len);
        return 0;
    }
    StrToHex(hex_buf,p_str,len_field);
    //DBG_LOG("\r\nnb_abtain_data:getted valid hex data:\r\n");
    //for(uint8_t i=0;i<len_field;i++)
    {
        //DBG_LOG("%2x ",hex_buf[i]);
    }    
    user_parse_send_queue(hex_buf,len_field,USER_MSG_CHANNLE_NB);
    DBG_LOG("\r\nnb_abtain_data:send queue to instructin parse over\r\n",p_str);
    return len_field;
}
/**
* @functionname : drv_nb_receive_response 
* @description  : function for receive response from nb-iot interface
* @input        : rec_buf: pointer to out buf of hex type, buf_size: the out buf max capacity  
* @output       : none 
* @return       : hex length
*/
static int32_t drv_nb_receive_response(void)
{
    DBG_LOG("\r\n**************drv_nb_receive_response*****************\r\n");
    //char res_buf[600] = {0};
    int32_t res_len = 0;   
    /*uint32_t retry = 10;
    bool sta = false;;
    memset(trans_rev_buf,0,sizeof(trans_rev_buf));
    
    do
    {
        uart_receive(trans_rev_buf,sizeof(trans_rev_buf),300);
        if(strstr(trans_rev_buf, "+NSONMI:") != NULL)
        {
            sta = 1;
            break;
        }
    }while(retry--);
    if(!sta)
    {
        return 0;
    }
    */
    //uart_receive(trans_rev_buf,sizeof(trans_rev_buf),10);
    memset(trans_rev_buf,0,sizeof(trans_rev_buf));
    //DBG_LOG("\r\nnb_receive_response:send command: AT+NSORF=0,256\r\n");
    hal_uart_send_string("AT+NSORF=0,256\r");  
    
    vTaskDelay(1000);
    DBG_LOG("\r\nnb_receive_response:get rx fifo, after command: AT+NSORF=0,256\r\n");
    //res_len= hal_uart_get_rx_fifo_datas((uint8_t *)trans_rev_buf,sizeof(trans_rev_buf),UART_CHANNEL_NB);
    res_len = uart_receive(trans_rev_buf,sizeof(trans_rev_buf),2000);
    if(res_len < 10)
    {
        res_len = uart_receive(trans_rev_buf,sizeof(trans_rev_buf),2000);
    }
    DBG_LOG("\r\nnb_receive_response:received data:%s\r\n",trans_rev_buf);    
    int32_t ret = 0;
    if(res_len > 0 && strstr(trans_rev_buf, "0\r\n\r\nOK\r\n") != NULL)
    {
        ret = drv_nb_abtain_data(trans_rev_buf,sizeof(trans_rev_buf));                      
    } 
    else
    {
        ret = 0;
    }
    memset(trans_rev_buf,0,sizeof(trans_rev_buf));
    return ret;    
    
}
/**
* @functionname : drv_nb_wait_response 
* @description  : function for waiting response, timeout = 20s @ref WAIT_SERVER_RESPONSE
* @input        : none  
* @output       : none 
* @return       : none
*/
static void drv_nb_wait_response(void)
{
    int32_t ret;
    uint32_t time = hal_rtc2_get_unix_time();
    int32_t wait_cnt = 0;
    do
    {
        ret = drv_nb_receive_response();
        wait_cnt++;
        DBG_LOG("\r\nwaiting nb response count = %d.....................\r\n",wait_cnt);
    }while(!ret && ((hal_rtc2_get_unix_time() - time) < (WAIT_SERVER_RESPONSE - 4)));
}
int32_t  drv_nb_package_hex_to_string(uint8_t * const hex_buf,uint8_t const hex_len, char * const str_buf, int16_t const str_buf_len)
{
    if(str_buf_len < (hex_len*2 +1))
    {
        DBG_LOG("\r\nstring buffer is not enough space\r\n");
        return 0;
    }
    if(hex_buf == NULL || str_buf == NULL)
    {
        DBG_LOG("\r\nnb package hex to string pointer is NULL\r\n");
        return 0;
    }
    HexToStr(str_buf,hex_buf,hex_len);
    return hex_len;
}

/**
* @function@name:    drv_nb_trans_data
* @description  :    function for transmit data througth nb-IOT channel
* @input        :    none:                     
* @output       :    none
* @return       :    return ture if send successful
*/
bool drv_nb_trans_data(uint8_t *dt, uint8_t len)
{
    if(dt == NULL)
    {
        DBG_LOG("\r\nnb send data pointer is NULL\r\n");
        return 0;
    }
    if(len == 0)
    {
        DBG_LOG("\r\nnb_send data length = 0\r\n");
        return 0;
    }
    DBG_LOG("\r\ntransmit data via nb\r\n");
    drv_nb_close_socket();
    if(drv_nb_establish_socket() == false)
    {
        DBG_LOG("\r\n!!!!!!!!!!!!!!!establish_socket failed!!!!!!!!!!!!!!!!!!!!!!\r\n");
        return 0;
    }
    uint32_t retry = false;
    uint32_t rx_len = 0;
    do
    {
    memset(trans_rev_buf,0,sizeof(trans_rev_buf));
    DBG_LOG("\r\nfill command and data\r\n");
    strcat(trans_rev_buf,cmd_udp_tx);    
    sprintf(&trans_rev_buf[strlen(cmd_udp_tx)],"%d",len);        //convert total message length(decimal) to char
    DBG_LOG("\r\nwill send total len = %d\r\n",len);
    strcat(trans_rev_buf,",");
    uint8_t offset = digit(len) + strlen(cmd_udp_tx)+1;      //+1 is ','
    drv_nb_package_hex_to_string(dt,len,&trans_rev_buf[offset],sizeof(trans_rev_buf)-offset);   //convert valid data filed hex to string
    strcat(trans_rev_buf,"\r\0");    
    hal_uart_send_string(trans_rev_buf);      //send data filed
    DBG_LOG("\r\nnb send data is:\r\n %s\r\n",trans_rev_buf);
    memset(trans_rev_buf,0,sizeof(trans_rev_buf));
        //for(uint8_t i=0;i<3;i++)
        {
        
        rx_len = uart_receive(&trans_rev_buf[rx_len],sizeof(trans_rev_buf),2000);
        hal_uart_get_rx_fifo_datas((uint8_t *)trans_rev_buf,sizeof(trans_rev_buf),UART_CHANNEL_NB);
        DBG_LOG("\r\nsend data response: %s",trans_rev_buf);
        if(strstr(trans_rev_buf, "OK") != NULL || strstr(trans_rev_buf, "+NSONMI:") != NULL)
        {
            //user_resume_nb_receive_handler_task();  //transmit successful, resume receive handler task             
            memset(trans_rev_buf,0,sizeof(trans_rev_buf));
            xSemaphoreGive(binary_semaphore_nb_rx);
            DBG_LOG("nb-iot send data successful\r\n"); 
            return true;
        }
        }
        retry++;
    }while(retry < 3);
    //drv_nb_close_socket();        //don't close at here, because need to receive response, close module after receive end
    memset(trans_rev_buf,0,sizeof(trans_rev_buf));
    DBG_LOG("nb-iot send data failed\r\n");
    return false;
}

/**
* @function@name:    drv_nb_restart
* @description  :    function for restrating the nb module
* @input        :    none:                     
* @output       :    none
* @return       :    return ture if send successful
*/
bool drv_nb_restart(void)
{
    DBG_LOG("\r\nconfig nb uart\r\n");
    drv_nb_uart_channel_select();
    DBG_LOG("\r\npower on and reboot nb module\r\n");
    //drv_nb_pwr_on();   
    drv_nb_reboot();
    DBG_LOG("\r\ninit nb module\r\n");
    drv_nb_module_init(); 
    
    return true;
}
static void drv_nb_pwr_off(void)
{
    nrf_gpio_cfg_output(DRV_NB_PWR_PIN);
    nrf_gpio_pin_clear(DRV_NB_PWR_PIN);
    //vTaskDelay(2000);
}
/**
* @function@name:    drv_nb_receive_handler_task
* @description  :    function for performace task of response message handler 
* @input        :    none:                     
* @output       :    none
* @return       :    none
*/
static void drv_nb_receive_handler_task(void *arg)
{
//    portTickType xLastWakeTime;
    //xLastWakeTime = xTaskGetTickCount();
    //vTaskDelay(20000);
    DBG_LOG("\r\nreceive_handler_task start\r\n");
    int32_t cnt = 0;
    xSemaphoreTake( binary_semaphore_nb_rx, 100 );
    while(1)
    {
        //vTaskDelayUntil( &xLastWakeTime, 5*S );
        if(xSemaphoreTake( binary_semaphore_nb_rx, portMAX_DELAY) == pdPASS)
        {
        //vTaskDelay(500);
        DBG_LOG("\r\nreceive_handler_task run cnt = %d\r\n",cnt++);
        //drv_nb_receive_response();
        drv_nb_wait_response();
        drv_nb_close_socket();
        }
    }
}
#define     test_rt 0
#if test_rt
static void drv_nb_timer_transmit_task(void)
{
    user_app_send_led_queue(1);
    DBG_LOG("\r\nconfig nb uart\r\n");
    drv_nb_uart_channel_select();
    DBG_LOG("\r\npower on and reboot nb module\r\n");    
    drv_nb_pwr_on();
    drv_nb_module_init(); 
    drv_nb_get_imsi(nb_imsi);
    drv_nb_get_imei(nb_imei);        
    user_app_send_led_queue(0);
    hal_uart_close();
    drv_nb_pwr_off();
    //user_app_init();
    //vTaskDelay(2000);
    //vTaskDelete(nb_tx_handle);
    while(1)
    {
        vTaskDelay(3600000);
    }
    /*
    nrf_gpio_cfg_input(DRV_NB_TXD_UART_MCU_RXD_PIN,NRF_GPIO_PIN_NOPULL );
    nrf_gpio_cfg_output(PIN_NB_RXD_UART_MCU_TXD_PIN);

    nrf_gpio_cfg_output(UART_DEBUG_PIN_TXD);
    nrf_gpio_cfg_input(UART_DEBUG_PIN_RXD, NRF_GPIO_PIN_NOPULL);
    drv_nb_pwr_on();
    while(1)
    {
        if(nrf_gpio_pin_read(DRV_NB_TXD_UART_MCU_RXD_PIN))
        {
            nrf_gpio_pin_set(UART_DEBUG_PIN_TXD);
        }
        else
        {
            nrf_gpio_pin_clear(UART_DEBUG_PIN_TXD);
        }

        if(nrf_gpio_pin_read(UART_DEBUG_PIN_RXD))
        {
            nrf_gpio_pin_set(PIN_NB_RXD_UART_MCU_TXD_PIN);
        }
        else
        {
            nrf_gpio_pin_clear(PIN_NB_RXD_UART_MCU_TXD_PIN);
        }
    }
    */
}
#else
/**
* @function@name:    drv_nb_timer_transmit_task
* @description  :    function for performace task of timer to transmit data to server
* @input        :    none:                     
* @output       :    none
* @return       :    none
*/
static void drv_nb_timer_transmit_task(void *arg)
{            
    //vTaskDelay(1000);
    static uint32_t run_cnt = 0;
    static uint32_t interval;
    static uint8_t queue;
    static uint8_t encode_buf[256];
    static uint8_t send_buf[256];
    user_app_send_led_queue(500);
    DBG_LOG("\r\nconfig nb uart\r\n");
    drv_nb_uart_channel_select();
    DBG_LOG("\r\npower on and reboot nb module\r\n");    
    drv_nb_pwr_on();
    //drv_nb_module_init(); 
    drv_nb_get_imsi(nb_imsi);
    drv_nb_get_imei(nb_imei);        
    user_app_send_led_queue(0);
    hal_uart_close();
    drv_nb_pwr_off();
    //user_app_init();
    //hal_uart_close(); 
    xQueueReset(queue_nb_mode);
    while(1)
    {                
        //interval = 60*S;
        //nb_commu_setting.mode = DRV_NB_COMMU_MODE_TIMER;        //for test
        interval = user_nb_caculate_next_idle_time() * S; 
        DBG_LOG("\r\nnb_timer_transmit_task = 0x%x\r\n\0",nb_tx_handle);
        DBG_LOG("\r\nnb module communication interval = %dS\r\n",interval/1000);
        if(xQueueReceive(queue_nb_mode, &queue , interval) == pdPASS)
        {                     
            switch(queue)
            {
                case NB_TRANS_TASK_RUN_TYPE_SETTING:
                    DBG_LOG("\r\nnb communication setting\r\n"); 
                    break;
                case NB_TRANS_TASK_RUN_TYPE_WARNING:
                    user_app_send_led_queue(2000);
                    user_suspend_ble_trans_task();
                    drv_nb_uart_channel_select();
                    drv_nb_pwr_on();
                    drv_nb_module_init();
                    drv_nb_get_imsi(nb_imsi);
                    drv_nb_get_imei(nb_imei);
                    //no warninig message, normal upload                    
                    DBG_LOG("\r\n**********************it's time to upload register message through nb**************************\r\n"); 
                    if(app_register(encode_buf,sizeof(encode_buf), send_buf, sizeof(send_buf),COMMUNICATION_INTERFACE_TYPE_NB) == true)
                    {
                        DBG_LOG("\r\n*****************it's time to upload warning data through nb****************************\r\n"); 
                        user_warning_infor_upload(encode_buf,sizeof(encode_buf), send_buf, sizeof(send_buf),COMMUNICATION_INTERFACE_TYPE_NB);
                    }
                    user_app_send_led_queue(0);
                    drv_nb_pwr_off();
                    hal_uart_close();
                    user_resume_ble_trans_task();
                    
                    DBG_LOG("\r\n++++++++++++++++++++++++++upload over via nb! cnt = %d++++++++++++++++++++++++++\r\n",run_cnt); 
                    break;
                case NB_TRANS_TASK_RUN_TYPE_NORMAL:
                    
                    break;
                default:
                    DBG_LOG("\r\nnb transmition - invalide run type queue\r\n"); 
                    break;
            }           
        } 
        else
        {
            if(nb_commu_setting.mode != DRV_NB_COMMU_MODE_UNDEFINE)
            {
                    user_app_send_led_queue(2000);
                    user_suspend_ble_trans_task();
                    drv_nb_uart_channel_select();
                    drv_nb_pwr_on();
                    drv_nb_module_init();
                    //drv_nb_get_imsi(nb_imsi);
                    //drv_nb_get_imei(nb_imei);

                    DBG_LOG("\r\n**********************it's time to upload register message through nb**************************\r\n"); 
                    if(app_register(encode_buf,sizeof(encode_buf), send_buf, sizeof(send_buf),COMMUNICATION_INTERFACE_TYPE_NB) == true)
                    {
                        DBG_LOG("*********************it's time to setting require**************************\r\n");
                        user_app_setting_require(encode_buf,sizeof(encode_buf), send_buf, sizeof(send_buf),COMMUNICATION_INTERFACE_TYPE_NB);
                        DBG_LOG("\r\n***********************it's time to upload environment data through nb**************************\r\n");        
                        user_env_upload_data(encode_buf,sizeof(encode_buf), send_buf, sizeof(send_buf),COMMUNICATION_INTERFACE_TYPE_NB); 
                        //DBG_LOG("\r\n***********************it's time to upload behavior data through nb********************\r\n");
                        //user_bhv_upload_data(encode_buf,sizeof(encode_buf), send_buf, sizeof(send_buf),COMMUNICATION_INTERFACE_TYPE_NB);
                        //DBG_LOG("\r\n*****************it's time to upload estrus data through nb****************************\r\n"); 
                        //user_estrus_upload_data(encode_buf,sizeof(encode_buf), send_buf, sizeof(send_buf),COMMUNICATION_INTERFACE_TYPE_NB);
                        DBG_LOG("\r\n*****************it's time to upload warning data through nb****************************\r\n"); 
                        user_warning_infor_upload(encode_buf,sizeof(encode_buf), send_buf, sizeof(send_buf),COMMUNICATION_INTERFACE_TYPE_NB);
                        
                    }
                    drv_nb_pwr_off();
                    hal_uart_close();
                    user_resume_ble_trans_task();
                    DBG_LOG("\r\n++++++++++++++++++++++++++upload over via nb! cnt = %d++++++++++++++++++++++++++\r\n",run_cnt);
                    user_app_send_led_queue(0);
            }                    
        }
    }
}
#endif  //test_rt
void user_creat_nb_timer_transmit_task(void)
{
    //binary_semaphore_nb_tx = xSemaphoreCreateBinary();
    queue_nb_mode = xQueueCreate( 1, sizeof(uint8_t) );
    BaseType_t ret;    
    ret = xTaskCreate(drv_nb_timer_transmit_task, "nbtx", 768, NULL, 1, &nb_tx_handle );
    if(pdPASS != ret)
    {
        APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
    }  
}
void user_creat_nb_receive_handler_task(void)
{
    binary_semaphore_nb_rx = xSemaphoreCreateBinary();
    BaseType_t ret;    
    ret = xTaskCreate(drv_nb_receive_handler_task, "nbrx", 512, NULL, 1, &nb_rx_handle );
    if(pdPASS != ret)
    {
        APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
    }  
}
void user_nb_send_queue_to_task(uint8_t type)
{
    xQueueSendToBack(queue_nb_mode, &type, 10);
}
uint32_t drv_get_nb_max_send_len(void)
{
    return DRV_NB_UNIT_SEND_DATA_LEN;
}
void user_nb_send_semphore_to_rx_task(void)
{
    xSemaphoreGive(binary_semaphore_nb_rx);
}
void user_nb_send_semphore_to_tx_task(void)
{
    xSemaphoreGive(binary_semaphore_nb_tx);
}
//suspend the timer to transmit task
void user_suspend_nb_timer_transmit_task(void)
{
    DBG_LOG("\r\nsuspend_nb_timer_transmit_task\r\n");
    vTaskSuspend(nb_tx_handle);
    DBG_LOG("\r\nsuspend task ok\r\n");
}
//resume the timer to transmit task
void user_resume_nb_timer_transmit_task(void)
{
    DBG_LOG("\r\nresume_nb_timer_transmit_task\r\n");
    vTaskResume(nb_tx_handle);
    DBG_LOG("\r\nresume task ok\r\n");
}
//suspend the receive handler task
void user_suspend_nb_receive_handler_task(void)
{
    DBG_LOG("\r\nsuspend_nb_receive_handler_task\r\n");
    vTaskSuspend(nb_rx_handle);
    DBG_LOG("\r\nsuspend task ok\r\n");
}
//resume the timer to transmit task
void user_resume_nb_receive_handler_task(void)
{
    DBG_LOG("\r\nresume_nb_receive_handler_task\r\n");
    vTaskResume(nb_rx_handle);
    DBG_LOG("\r\nresume task ok\r\n");
}
void user_get_nb_imei(char * const imei)
{
    strcpy(imei,nb_imei);
}
void user_get_nb_imsi(char * const imsi)
{
    strcpy(imsi,nb_imsi);
}
//end
