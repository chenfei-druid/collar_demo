#include <stdbool.h>
#include <stdint.h>

#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "app_error.h"
#include "FreeRTOS.h"
#include "nrf_sdh_freertos.h"
#include "semphr.h"
#include "task.h"
#include "nrf.h"
#include "nrf_drv_wdt.h"
#include "app_util_platform.h"
#include "user_app.h"
#include "hal_nrf_wdt.h"
#include "nrf_nvmc.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "hal_rtc.h"
#include "nrf_wdt.h"
#include "user_app_log.h"
#include "user_app_set.h"
//#include "user_task.h"


nrf_drv_wdt_channel_id m_channel_id;

static      TaskHandle_t        wdt_handle;
//static      bool                wdt_event_flag = 0;
//static      uint8_t            *p_ram = (uint8_t *)(USER_RAM_PARAM_START_ADDR);
//static      uint32_t            *p_reset_flag = (uint32_t *)(USER_RAM_PARAM_START_ADDR+4);
//static      uint8_t             reset_cnt = 0;
//static      perform_status      per_sta_t;
static      uint32_t            timstamp;

extern      bool get_dfu_flag(void);
extern      void clear_dft_flag(void);

/**
 * @brief WDT events handler.
 */
void wdt_event_handler(void)
{    
    DBG_LOG("\r\nRebooting...\r\n"); 

    NVIC_SystemReset();   
}

void user_system_reset(void)
{
    //save parameter;
    //uint8_t len = sizeof(per_sta_t);
    //user_set_param_for_reset();
    //user_save_reset_param_into_flash();
    //reset
    NVIC_SystemReset(); 
}
/**
 * @brief WDT config.
 */
void hal_config_watch_dog(void)
{
    uint32_t err_code = NRF_SUCCESS;
    //Configure WDT.
    nrf_drv_wdt_config_t config = NRF_DRV_WDT_DEAFULT_CONFIG;
    config.reload_value = RELOAD_VALUE;
    err_code = nrf_drv_wdt_init(&config, wdt_event_handler);
    APP_ERROR_CHECK(err_code);
    err_code = nrf_drv_wdt_channel_alloc(&m_channel_id);
    APP_ERROR_CHECK(err_code);
    nrf_drv_wdt_enable();
}
void hal_wdt_disable(void)
{
    nrf_wdt_int_disable(NRF_WDT_INT_TIMEOUT_MASK);
}
void hal_wdt_feed(void)
{
    nrf_drv_wdt_channel_feed(m_channel_id);
}
void hal_wdt_task(void *arg)
{
    static uint8_t reset_cnt;
    hal_config_watch_dog();
    uint32_t feed_wdt_interval = 60000;
    DBG_LOG("\r\ninit wdt\r\n"); 
    nrf_drv_wdt_channel_feed(m_channel_id); 
    while(1)
    {
        vTaskDelay(feed_wdt_interval);
        user_set_rewrite_timestamp();
        nrf_drv_wdt_channel_feed(m_channel_id); 
        DBG_LOG("\r\nfeed watch dog\r\n");
        timstamp = hal_rtc2_get_unix_time();              
        //if(((timstamp+28800) % 86400) - 16200 <= (feed_wdt_interval/1000))//16200))     //4:30:00 every day
        if(((timstamp+28800) % 86400) - 12600 <= (feed_wdt_interval/1000))       //10:20
        {
            reset_cnt++;
            if(reset_cnt >= 3)
            {
                reset_cnt = 0;
                user_system_reset();
            }
        }                
    }
}

void hal_create_wdt_task(void)
{
    BaseType_t ret;
    ret = xTaskCreate( hal_wdt_task, "wdt", 192, NULL, 1, &wdt_handle );
    if(pdPASS != ret)
    {
        APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
    }  
}

