/** 
 * user_acc_hal.c
 *
 * @group:       neck strap project
 * @author:      Chenfei
 * @create time: 2017/9/29
 * @version:     V0.0
 *
 */
#include <math.h>
#include "string.h"

#include "user_ble_application.h"
//#include "nrf_delay.h"
#include "nrf_drv_gpiote.h"
#include "hal_acc.h"
#include "SEGGER_RTT.h"
#include "SEGGER_RTT_Conf.h"
#include "behavior.h"
#include "Behavior2.pb.h"
#include "time.h"
#include "user_data_struct.h"
#include "Record.h"
#include "user_task.h"
/***********************************************user defined variable************************************************/
#define                 ODBA_COUNT_MINUTE           60/(100/25)  //second_in_unit_minute/(acc_points_once/acc_data_rate)
#define                 ODBA_COUNT_HOUR_MINUTE      2                         //minutes in unit hour
uint8_t xyz_buff[256];
//Behavior_Record_t       *m_bhv_record_t[ODBA_COUNT_MINUTE];         //odba every 4S

//static  xSemaphoreHandle    binary_semaphore_acc_collect;
/***********************************************defined functions**********************************************/
/*
static bool hal_acc_read_fifo_data(hal_snr_data * p_drv_acc_fifo_data)
{
   
    uint8_t i;
    uint8_t int_flag;
    uint8_t data_numbers;
    int_flag = drv_acc_kx022_read_int_flag();
    if(int_flag & 0x20)
    {
        data_numbers = drv_acc_kx022_read_fifo_data_sample_numbers();
        DBG_LOG("read fifo data numbers = %d\r\n",data_numbers);
        drv_kx022_get_fifo_data(p_drv_acc_fifo_data->data_s,data_numbers);
        p_drv_acc_fifo_data->data_len = data_numbers;
    }
    drv_acc_kx022_clear_interrupt_source();    
}
*/
void hal_acc_test(hal_snr_data *acc_buffer)
{
    int8_t j = 0;
    for(int8_t i=0; i<25; i++)
    {        
        acc_buffer->data_s[i*6]   = 5;
        acc_buffer->data_s[i*6+1] = 0;
        acc_buffer->data_s[i*6+2] = 5;
        acc_buffer->data_s[i*6+3] = 0;
        acc_buffer->data_s[i*6+4] = 5;
        acc_buffer->data_s[i*6+5] = 0;
    }
    acc_buffer->data_len = 6*25;
}
void hal_acc_odba_alg_init(void)
{   
    Behavior_Init(51,60);   
}
void hal_acc_init(void)
{
    drv_acc_kx022_init();
    hal_acc_odba_alg_init();
}
void hal_acc_start_collect(void)
{
    drv_acc_kx022_measure_fifo_start();
}

/**
* @function@name:    hal_acc_odba_minute
* @description  :    function for compulate odba data, and abtain a odba result every one minute
* @input        :    time: unix timestamp
* @output       :    none
* @return       :    none
*/
void hal_acc_odba_minute(uint32_t time)
{
    
}
/**
* @function@name:    hal_acc_odba_hour
* @description  :    function for compulate odba data, and abtain a odba result every one hour
* @input        :    time: unix timestamp
* @output       :    none
* @return       :    none
*/
void hal_acc_odba_hour(uint32_t time)
{
    static Behavior_Record_t       m_hour_record_t;
    static int16_t                 m_record_cnt = 0;
    static DataPoint32_t           m_sum_odba_t = {0,0,0};
    static DataPoint32_t           m_sum_mdl_t = {0,0,0};
    hal_snr_data                drv_acc_fifo_data_t;
    //g_sensor_xyz_s      g_sensor_xyz_t;
    Behavior_Record_t           *bhv_rec_t;
    uint32_t            temp_sum = 0;
    //hal_acc_read_fifo_data(&drv_acc_fifo_data_t);
    hal_acc_test(&drv_acc_fifo_data_t);
    //user_org_data_to_xyz_data(drv_acc_fifo_data_t,&g_sensor_xyz_t);
    //user_sensor_data_compute(g_sensor_xyz_t, &temp_sum);
    bhv_rec_t = Behavior_Update((DataPoint_t *)drv_acc_fifo_data_t.data_s, drv_acc_fifo_data_t.data_len/6, time);
    if(bhv_rec_t != NULL)
    {
        m_sum_mdl_t.x += bhv_rec_t->meandl.x;
        m_sum_mdl_t.y += bhv_rec_t->meandl.y;
        m_sum_mdl_t.z += bhv_rec_t->meandl.z;
            
        m_sum_odba_t.x   += bhv_rec_t->odba.x;
        m_sum_odba_t.y   += bhv_rec_t->odba.y;
        m_sum_odba_t.z   += bhv_rec_t->odba.z;
        m_record_cnt++;

        DBG_LOG("\r\nbhv_rec_t->meandl.x=%d\r\n, bhv_rec_t->meandl.y=%d\r\n, bhv_rec_t->meandl.z=%d\r\n\r\n",\
        bhv_rec_t->meandl.x, bhv_rec_t->meandl.y, bhv_rec_t->meandl.z);
        DBG_LOG("m_sum_odba_t.x=%d\r\n, m_sum_odba_t.y=%d\r\n, m_sum_odba_t.z=%d\r\n\r\n",\
        m_sum_mdl_t.x, m_sum_mdl_t.y, m_sum_mdl_t.z);

        if(m_record_cnt >= ODBA_COUNT_HOUR_MINUTE)
        {
            
            m_hour_record_t.odba.x  =   m_sum_odba_t.x / ODBA_COUNT_HOUR_MINUTE;
            m_hour_record_t.odba.y  =   m_sum_odba_t.z / ODBA_COUNT_HOUR_MINUTE;
            m_hour_record_t.odba.z  =   m_sum_odba_t.z / ODBA_COUNT_HOUR_MINUTE; 
            
            m_hour_record_t.meandl.x=   m_sum_mdl_t.x / ODBA_COUNT_HOUR_MINUTE;
            m_hour_record_t.meandl.y=   m_sum_mdl_t.y / ODBA_COUNT_HOUR_MINUTE;
            m_hour_record_t.meandl.z=   m_sum_mdl_t.z / ODBA_COUNT_HOUR_MINUTE;
            
            m_hour_record_t.time    =   time;                            
            
            Record_Write(RECORD_TYPE_BEHAVIOR, (uint8_t *)&m_hour_record_t);
            
            m_sum_mdl_t.x = 0;
            m_sum_mdl_t.y = 0;
            m_sum_mdl_t.z = 0;
        
            m_sum_odba_t.x = 0;
            m_sum_odba_t.y = 0;
            m_sum_odba_t.z = 0;
        
            m_record_cnt = 0;
            
            DBG_LOG("\r\none hour record result: time = %d\r\n",time);
            DBG_LOG("\r\nm_record_t->meandl.x=%d, m_record_t->meandl.y=%d, m_record_t->meandl.z=%d\r\n\r\n",
                    m_hour_record_t.meandl.x, 
                    m_hour_record_t.meandl.y, 
                    m_hour_record_t.meandl.z);
        
            DBG_LOG("\r\nm_record_t->odba.x=%d, m_record_t->odba.y=%d, m_record_t->odba.z=%d\r\n\r\n",
                    m_hour_record_t.odba.x, 
                    m_hour_record_t.odba.y, 
                    m_hour_record_t.odba.z);
        }
    }
}



//end functions

