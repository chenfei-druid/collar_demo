/** 
 * hal_nrf_uart.c
 *
 * this is uart hal for application, user must set parameter for one of channels firstly, then call the function @ref user_uart_chnl_config 
 * to config the uart channel according to app interface. when app need to use a channel, call function @ref user_uart_change_channel to 
 * switch uart channel.
 * @author:      Chenfei
 * @create time: 2017/9/26
 * @modify time: 2018/2/27
 * @version:     V0.0
 *
 */
/*********************************includes*************************************/
#include <stdint.h>
#include <string.h>
#include "app_uart.h"
#include "app_util_platform.h"
#include "nrf_drv_gpiote.h"
//#include "nrf_delay.h"
#include "hal_nrf_uart.h"
//#include "nrf_uart.h"
#include "nrf_uarte.h"
#include "user_task.h"
#include "FreeRTOS.h"
#include "task.h"
#include "user_app_log.h"

/*****************************user defined***************************************/        
static uart_msg         uart_msg_t;//  =UART_DEFAULT_CONFIG;
static uint8_t          uart_data_array[UART_RX_BUF_SIZE];  
static uint8_t          uart_data_cnt = 0;   
static bool             hal_uart_lock_flag = 0;
static uart_conf        m_uart_conf = {.channel_nums = 0,
                                       .uart_attr_t = {0},
                                        }; 
static uart_rx_t        m_uart_rx_t[UART_CHANNEL_MAX-1];
/*************************defined functions*************************************/
#ifdef FREERTOS
#include "FreeRTOS.h"
#include "nrf_sdh_freertos.h"
#include "semphr.h"
                                        
xSemaphoreHandle    binary_semaphore_uart_rx;
QueueHandle_t       queue_uart_rx;
QueueHandle_t       queue_uart_tx;  
TaskHandle_t        app_uart_rx_handle;
TaskHandle_t        app_uart_tx_handle;   
//static QueueHandle_t        buart_queue = NULL;                                        
/**
* @function@name:    user_uart_rx_task
* @description  :    task for uart rx handler
* @input        :    *arg:                     
* @output       :    none
* @return       :    none
*/
/*
static void hal_uart_rx_task(void *arg)
{
    uint8_t uart_buff[UART_RX_BUF_SIZE] = {0};
    uint8_t len = 0;
    xSemaphoreTake( binary_semaphore_uart_rx, 200 );
    while(1)
    {     
        //receive datas       
		if(xSemaphoreTake( binary_semaphore_uart_rx, portMAX_DELAY ) ==pdPASS)
		{			
			if(hal_uart_check_rx(uart_buff,&len) == true)
			{
				//hal_uart_message_process(uart_buff, len, hal_uart_channel_check());
                for(uint8_t i=0; i<len; i++)
                {
                    while(xQueueSendToBack(queue_uart_rx, &uart_buff[i], 0) != pdPASS);
                }
				hal_clear_uart_rx_buf();
			}         					
		}
       //vTaskDelay( 100 );
    }
}
*/
void hal_give_uart_rx_semaphore(void)
{
    BaseType_t yield_req = pdFALSE;
	static portBASE_TYPE pxHigherPriorityTaskWoken = pdFALSE ;
    xSemaphoreGiveFromISR(binary_semaphore_uart_rx, &pxHigherPriorityTaskWoken);
    if(pxHigherPriorityTaskWoken == pdTRUE)
    {
		/* Switch the task if required. */
        portYIELD_FROM_ISR(yield_req);	
    }
}

static void hal_uart_tx_task(void *arg)
{
    char p_uart_tx;
    xQueueReceive(queue_uart_tx, &p_uart_tx , 200);
    while(true)
    {
         //tranmit datas   
        while(xQueueReceive(queue_uart_tx, &p_uart_tx , portMAX_DELAY) != errQUEUE_EMPTY)
        {
            //send tx data over uarte dma
            hal_uart_send_ch(p_uart_tx);
        }		
        //vTaskDelay( 100 );
    }
}

void hal_creat_uart_handler_task(void)
{
    vSemaphoreCreateBinary( binary_semaphore_uart_rx );
    //vSemaphoreCreateBinary( binary_semaphore_uart_tx );
    //queue_uart_rx   = xQueueCreate( 256, sizeof(char) );
    queue_uart_tx   = xQueueCreate( 256, sizeof(char) );
    BaseType_t ret;
    /*
    ret = xTaskCreate( hal_uart_rx_task, "rx", 192, NULL, 1, &app_uart_rx_handle );
        if(pdPASS != ret)
        {
            APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
        } */ 
    ret = xTaskCreate( hal_uart_tx_task, "tx", 128, NULL, 1, &app_uart_tx_handle );
        if(pdPASS != ret)
        {
            APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
        }    
         
}
#endif                                        
/**
* @function@name:    uart_data_read
* @description  :    Function for save uart data                                       
* @input        :    temp: a char data from uart interface
* @output       :    none
* @return       :    none
*/
static void uart_data_read(char temp)
{      
    //DBG_LOG("%c ",temp);    
    static portBASE_TYPE TaskWoken = pdFALSE;
     if(m_uart_conf.channel_nums == UART_CHANNEL_NB)
    {
        if(m_uart_rx_t[UART_CHANNEL_NB].data_cnt >= UART_RX_BUF_SIZE)
        {
            m_uart_rx_t[UART_CHANNEL_NB].data_cnt = 0;
        }
        m_uart_rx_t[UART_CHANNEL_NB].buffer[m_uart_rx_t[UART_CHANNEL_NB].data_cnt++] = temp;
        if(temp == '\n')
        {
            uart_msg_t.rx.length = m_uart_rx_t[UART_CHANNEL_NB].data_cnt;
            memcpy(uart_msg_t.rx.buff,m_uart_rx_t[UART_CHANNEL_NB].buffer,uart_msg_t.rx.length);
            memset(m_uart_rx_t[UART_CHANNEL_NB].buffer,0,sizeof(m_uart_rx_t[UART_CHANNEL_NB].buffer));
            m_uart_rx_t[UART_CHANNEL_NB].data_cnt = 0;
            //hal_give_uart_rx_semaphore();
            xQueueSendToBackFromISR(queue_uart_rx, &uart_msg_t.rx.length, &TaskWoken);
            uint32_t buff_addr = (uint32_t)uart_msg_t.rx.buff;
            xQueueSendToBackFromISR(queue_uart_rx, &buff_addr, &TaskWoken); 
            //DBG_LOG("\r\nrx addr = %x, buff_addr = %x\r\n",uart_msg_t.rx.buff,buff_addr);
        }
    }
    
    else if(m_uart_conf.channel_nums == UART_CHANNEL_DEBUG)
    {
        //DBG_LOG("\r\nlora chn rx_cnt= %d \r\n",m_uart_rx_t[UART_CHANNEL_LORA].data_cnt);
        if(m_uart_rx_t[UART_CHANNEL_DEBUG].data_cnt >= UART_RX_BUF_SIZE)
        {
            m_uart_rx_t[UART_CHANNEL_DEBUG].data_cnt = 0;
            //DBG_LOG("rx cnt clear \r\n");
        }
        m_uart_rx_t[UART_CHANNEL_DEBUG].buffer[m_uart_rx_t[UART_CHANNEL_DEBUG].data_cnt++] = temp;
        //hal_give_uart_rx_semaphore(); 
        
    } 
}
/**
* @function@name:    hal_csc_check_sum
* @description  :    Function for checking sum for verify the uart message is valid or not
*                                                           
* @input        :    p_umessage, the pointor to message buffer, length,message length will be verified, csc, the csc filed value in message
* @output       :    none
* @return       :    true, verify result is true, otherwise is false
*/
static bool hal_csc_check_sum(uint8_t const * p_umessage, const uint8_t length, uint8_t csc)
{
    char sum = 0;
    for(uint8_t i=0;i<length;i++)
    {
        sum += *p_umessage++;
    }
    return ((csc == sum)? (true): (false));
}

/**
* @function@name:    hal_uart_judge_start_end
* @description  :    Function for judging identifier of end and start, @m_uart_conf.uart_attr_t[m_uart_conf.channel_nums].uart_flag
*                                                           
* @input        :    none
* @output       :    none
* @return       :    none
*/
static bool hal_uart_judge_identifier(void)
{
    uint32_t p_cnt;
    if(uart_data_cnt >= m_uart_conf.uart_attr_t[m_uart_conf.channel_nums].uart_flag.flag_end_length)                //end identifier
    {
        uint8_t end_len = m_uart_conf.uart_attr_t[m_uart_conf.channel_nums].uart_flag.flag_end_length;
        int8_t i = 0;
        for( i=0; i<end_len; i++)
        {
            //#uart_data_cnt - end_len#, because this is end transmit, the received datas length total is must more than end identifier length
            if(uart_data_array[uart_data_cnt - end_len + i] != m_uart_conf.uart_attr_t[m_uart_conf.channel_nums].uart_flag.flag_end[i])
            {
                break;
            }
        }
        if(i == end_len)        // the length = end identifier length
        {
            if(uart_data_cnt == end_len)
            {
                //the end flag is send first for example  GSM, NOIOT module
                return false;
            }
            uint8_t *p_satrt = uart_data_array;
            uint8_t start_len = m_uart_conf.uart_attr_t[m_uart_conf.channel_nums].uart_flag.flag_start_length;
            int8_t j;
            if(start_len > 0)       //must start len>0
            {
                //search start identifier            
                                           
                for(i=uart_data_cnt-end_len - 1 - start_len; i>= 0; i--)
                {
                    for(j=0; j<start_len; j++)
                    {
                        if(uart_data_array[i + j] != m_uart_conf.uart_attr_t[m_uart_conf.channel_nums].uart_flag.flag_start[j])
                        {
                            break;
                        }                   
                    }
                    if(j == start_len)
                    {
                        //the valid start identifier is found, then get the start position, and valid data length
                        p_satrt = &uart_data_array[i];
                        p_cnt = uart_data_cnt - i;      
                        break;
                    }
                }
            //not found the start identifier, but the end is found, that means this message is unnecessary, delete the buff and return fail;
                if(i == uart_data_cnt-end_len)
                {
                return false;
                }
            }
            //check CSC            
            if(m_uart_conf.uart_attr_t[m_uart_conf.channel_nums].csc_mode == true)
            {
                //if(p_satrt[uart_data_cnt-end_len-1] != hal_csc_check_sum(p_satrt,uart_data_cnt-end_len-1))
                if(true != hal_csc_check_sum(&p_satrt[m_uart_conf.uart_attr_t[m_uart_conf.channel_nums].csc_start],p_cnt-end_len-1-m_uart_conf.uart_attr_t[m_uart_conf.channel_nums].csc_start, p_satrt[p_cnt-end_len-1]))
                { 
                    return false;
                }
            }            
            //copy uart buff 
            memset(uart_msg_t.rx.buff,0,sizeof(uart_msg_t.rx.buff));
            for(i=0; i< p_cnt; i++)
            {
                uart_msg_t.rx.buff[i] = p_satrt[i];      //保存接收到的数据
            }
            uart_msg_t.rx.length = p_cnt;  
            uart_msg_t.rx.flag = 1;                
            memset(uart_data_array,0,sizeof(uart_data_array));
            uart_data_cnt = 0;  
            return true;                       
        }
    }
    return false;
}
/**@brief   Function for handling app_uart events.
 *
 * @details This function will receive a single character from the app_uart module and append it to
 *          a string. The string will be be sent over BLE when the last character received was a
 *          'new line' '\n' (hex 0x0A) or if the string has reached the maximum data length.
 */
/**@snippet [Handling the data received over UART] */
void uart_event_handle(app_uart_evt_t * p_event)
{
    uint8_t ch;
    //static portBASE_TYPE xHigherPriorityTaskWoken;
    switch (p_event->evt_type)
    {
        case APP_UART_DATA:                 //for uart single
            //app_uart_get(&ch);       
            //uart_data_read(ch);
            break;
        
        case APP_UART_DATA_READY:           //for uart_fifo
            app_uart_get(&ch);
            //uart_data_read(ch);
            xQueueSendToBackFromISR(queue_uart_rx, &ch, NULL);
            break;
        case APP_UART_TX_EMPTY:
            uart_msg_t.tx.flag = 1;
            //DBG_LOG("\r\nuart_tx_flag = %d \r\n",uart_msg_t.tx.flag); 
            break;
        
        case APP_UART_COMMUNICATION_ERROR:
            //nrf_uart_event_clear(NRF_UART0, NRF_UART_EVENT_ERROR);    
            nrf_uarte_event_clear(NRF_UARTE0, NRF_UARTE_EVENT_ERROR);   //for uart easyDMA
            //APP_ERROR_HANDLER(p_event->data.error_communication);
            break;

        case APP_UART_FIFO_ERROR:
            //nrf_uart_event_clear(NRF_UART0, NRF_UART_EVENT_ERROR);
            APP_ERROR_HANDLER(p_event->data.error_code);
            break;

        default:
            break;
    }
}
/**
* @function@name:    hal_uart_close
* @description  :    Founction for closing the uart interface
* @input        :    none
* @output       :    none
* @return       :    none
*/
void hal_uart_close(void)
{
    app_uart_close();
    m_uart_conf.channel_nums = UART_CHN_NOT_USE;
    //hal_uart_lock_channel();

}
/**
* @function@name:    hal_uart_send_ch
* @description  :    send a char or one byte data over UART
* @input        :    ch: the char will be send
* @output       :    none
* @return       :    ture, send successed, false is not successed
*/
static uint32_t send_cnt = 0;
bool hal_uart_send_ch(uint8_t ch)
{
    uint32_t retry = 2;
    uint32_t res;
    do
    {
        send_cnt++;
        res = app_uart_put(ch);  
        if(res == NRF_ERROR_NO_MEM)
        {
            while(!uart_msg_t.tx.flag);
            uart_msg_t.tx.flag = 0;
        }
    }while (res != NRF_SUCCESS && retry-- > 0);   
    if(res != NRF_SUCCESS)
    {
        return false;
    }        
    return true;
}
/**
* @function@name:   hal_uart_send_string
* @description  :   send a string data over UART
* @input        :   str,   pointer to the string will be send
* @output       :   none
* @return       :   ture, send successed, false is not successed
*/
bool hal_uart_send_string(const char *str)
{
    if(str == NULL)
    {
        return false;
    }
    //uart_msg_t.tx.flag = 0;
    while(*str)
    {
        while(xQueueSendToBack(queue_uart_tx, str, 10) != pdPASS);

        str++;
    }
    //DBG_LOG("\r\nstr = 0x%x\r\n",str);
    //DBG_LOG("\r\nuart_msg_t.tx.flag = %d\r\n",uart_msg_t.tx.flag);
    //while(uart_msg_t.tx.flag == 0);
    return true;   
}
/**
* @function@name:   hal_uart_send_data
* @description  :   send some hex datas over UART
* @input        :   dt: pointor to sent data, len: send data len
* @output       :   none
* @return       :   ture, send successed, false is not successed
*/

bool hal_uart_send_data(const uint8_t *dt, int32_t len)
{
    if(len <= 0 || dt ==NULL)
        return false;
    uart_msg_t.tx.flag = 0;
    for(uint32_t i = 0; i<len; i++)
    {
        while(xQueueSendToBack(queue_uart_tx, &dt[i], 10) != pdPASS);
        //dt++;
    }      
    //wait 
    while(uart_msg_t.tx.flag == 0);
    return true;
}

/**@brief  Function for openning and initializing the UART module.
 */
/**@snippet [UART Initialization] */

static void uart_open(app_uart_comm_params_t  *p_uart_attr)
{
    uint32_t                     err_code;
    app_uart_comm_params_t const comm_params =
    {
        .rx_pin_no    = p_uart_attr->rx_pin_no,
        .tx_pin_no    = p_uart_attr->tx_pin_no,
        //.rts_pin_no   = RTS_PIN_NUMBER,
        //.cts_pin_no   = CTS_PIN_NUMBER,
        .flow_control = p_uart_attr->flow_control,
        .use_parity   = p_uart_attr->use_parity,
        .baud_rate    = p_uart_attr->baud_rate,
    };
    //uart for not use easyDMA
    //APP_UART_INIT(&comm_params,uart_event_handle,APP_IRQ_PRIORITY_LOWEST,err_code);
    /**/
        //uart for using easyDMA
    APP_UART_FIFO_INIT(&comm_params,
                       UART_RX_BUF_SIZE,
                       UART_TX_BUF_SIZE,
                       uart_event_handle,
                       APP_IRQ_PRIORITY_LOWEST,
                       err_code);    
    
    APP_ERROR_CHECK(err_code);
    //nrf_uart_int_disable(NRF_UART0, NRF_UART_INT_MASK_ERROR);   //屏蔽错误中断
    nrf_uarte_int_disable(NRF_UARTE0, NRF_UARTE_INT_ERROR_MASK);   //屏蔽错误中断
    
    if(queue_uart_rx == NULL)
    {
        queue_uart_rx   = xQueueCreate( UART_RX_BUF_SIZE, sizeof(char) );
    }
    if(queue_uart_rx != NULL)
    {
        xQueueReset(queue_uart_rx);
    }
}
/**
* @function@name:   hal_uart_change_channel
* @description  :   Fuction for uart interface config ,it can be configed to more channels, @ref UART_CHANNEL_MAX_NUMS
* @input        :   p_uart_attr: pointor to attribute for uart of current channel, chn: uart interface channel will be used
* @output       :   none
* @return       :   true: config successed, false is not successed
*/
//bool hal_uart_channel_config(uart_start_end_flag * p_m_uart_flag, app_uart_comm_params_t * p_uart_attr)  
bool hal_uart_chnl_config(uart_attr * p_uart_attr, uint8_t chn)    
{
    uint8_t i = 0;
    //UART receiving identifier must be not more than default value @ref UART_IDEN_MAX_LEN
    if(p_uart_attr->uart_flag.flag_start_length > UART_IDEN_MAX_LEN || p_uart_attr->uart_flag.flag_end_length > UART_IDEN_MAX_LEN)
    {
        return false;
    }
    //uart channel reuired must not be more than default value @ref UART_CHANNEL_MAX_NUMS
    if(chn >= UART_CHANNEL_MAX_NUMS)
    {
        //m_uart_conf.channel_nums = 0;
        return false;
    }
    m_uart_conf.channel_nums = chn; //get channel interface
    m_uart_conf.uart_attr_t[m_uart_conf.channel_nums].uart_flag.flag_start_length = p_uart_attr->uart_flag.flag_start_length;
    m_uart_conf.uart_attr_t[m_uart_conf.channel_nums].uart_flag.flag_end_length   = p_uart_attr->uart_flag.flag_end_length;
    //save start identifier
    for(i=0; i<p_uart_attr->uart_flag.flag_start_length; i++)
    {        
        m_uart_conf.uart_attr_t[m_uart_conf.channel_nums].uart_flag.flag_start[i]  = p_uart_attr->uart_flag.flag_start[i];
    }

    //save end identifier
    for(i=0; i<p_uart_attr->uart_flag.flag_end_length; i++)
    {
        m_uart_conf.uart_attr_t[m_uart_conf.channel_nums].uart_flag.flag_end[i]    =   p_uart_attr->uart_flag.flag_end[i];
    }

    //save uart channel attribute
    
    m_uart_conf.uart_attr_t[m_uart_conf.channel_nums].uart_params_t.rx_pin_no      = p_uart_attr->uart_params_t.rx_pin_no;
    m_uart_conf.uart_attr_t[m_uart_conf.channel_nums].uart_params_t.tx_pin_no      = p_uart_attr->uart_params_t.tx_pin_no;
    m_uart_conf.uart_attr_t[m_uart_conf.channel_nums].uart_params_t.baud_rate      = p_uart_attr->uart_params_t.baud_rate;
    m_uart_conf.uart_attr_t[m_uart_conf.channel_nums].uart_params_t.use_parity     = p_uart_attr->uart_params_t.use_parity;
    m_uart_conf.uart_attr_t[m_uart_conf.channel_nums].uart_params_t.flow_control   = p_uart_attr->uart_params_t.flow_control;
    m_uart_conf.uart_attr_t[m_uart_conf.channel_nums].csc_mode                     = p_uart_attr->csc_mode;
    m_uart_conf.uart_attr_t[m_uart_conf.channel_nums].csc_start                    = p_uart_attr->csc_start;
    //m_uart_conf.channel_nums++;
    return true;
}
void hal_uart_chn_release(uint8_t chn)
{
    memset(&m_uart_conf.uart_attr_t[chn], 0, sizeof((uint8_t *)&m_uart_conf.uart_attr_t[chn]));
}
/**
* @function@name:   hal_uart_change_channel
* @description  :   Fuction for switching uart channel to use
* @input        :   chn, channel for uart will be used
* @output       :   none
* @return       :   none
*/
void hal_uart_change_channel(uint8_t chn)
{
    app_uart_comm_params_t  uart_params_t;
    /*
    while(hal_uart_lock_flag)
    {
        vTaskDelay(10);
    }
    */
    hal_uart_close();
    
    m_uart_conf.channel_nums    =   chn;
    uart_params_t.rx_pin_no     =   m_uart_conf.uart_attr_t[m_uart_conf.channel_nums].uart_params_t.rx_pin_no;
    uart_params_t.tx_pin_no     =   m_uart_conf.uart_attr_t[m_uart_conf.channel_nums].uart_params_t.tx_pin_no;
    uart_params_t.baud_rate     =   m_uart_conf.uart_attr_t[m_uart_conf.channel_nums].uart_params_t.baud_rate;
    uart_params_t.use_parity    =   m_uart_conf.uart_attr_t[m_uart_conf.channel_nums].uart_params_t.use_parity;
    uart_params_t.flow_control  =   m_uart_conf.uart_attr_t[m_uart_conf.channel_nums].uart_params_t.flow_control;
    
    uart_open(&uart_params_t);
    //hal_uart_lock_channel();
    vTaskDelay(50);
}
void hal_uart_lock_channel(void)
{
    hal_uart_lock_flag = true;
}
void hal_uart_unlock_channel(void)
{
    hal_uart_lock_flag = false;
}
/**
* @function@name:   hal_uart_set_buad_rate
* @description  :   Fuction for setting uart baud rate, @ref app_uart_comm_params_t
* @input        :   baud_rate, uart baud rate; chn, channel for uart used
* @output       :   none
* @return       :   none
*/
void hal_uart_set_buad_rate(uint32_t baud_rate, uint8_t chn)
{
    m_uart_conf.uart_attr_t[chn].uart_params_t.baud_rate =  baud_rate;
}
/**
* @function@name:   hal_uart_set_parity
* @description  :   Fuction for setting uart parity, @ref app_uart_comm_params_t
* @input        :   parity, even parity if TRUE, no parity if FALSE; chn: channel for uart used
* @output       :   none
* @return       :   none
*/
void hal_uart_set_parity(bool parity, uint8_t chn)
{
    m_uart_conf.uart_attr_t[chn].uart_params_t.use_parity =  parity;
}
/**
* @function@name:   hal_uart_check_rx
* @description  :   Fuction for checking the datas have been received over uart, and get the valid data
* @input        :   none
* @output       :   p_buf: pointor to data buffer, len: the received data length
* @return       :   true: received data successful, false: otherwise
*/
bool hal_uart_check_rx(uint8_t *p_buf, uint8_t *len)
{
    uint8_t i = 0;
    hal_uart_judge_identifier();
    if(uart_msg_t.rx.flag == 0)
    {
        return false;
    }
    *len = uart_msg_t.rx.length;
    for(i = 0; i<uart_msg_t.rx.length; i++)
    {
        p_buf[i] = uart_msg_t.rx.buff[i];
    }
    uart_msg_t.rx.flag = 0;
    return true;
}
/**
* @function@name:   hal_clear_uart_rx_buf
* @description  :   Fuction for clear uart data buff
* @input        :   none
* @output       :   none
* @return       :   true: successful, false: failed
*/
bool hal_clear_uart_rx_buf(void)
{
    //uart_msg_t.rx.buff = 0;
    //uart_msg_t.rx.flag = 0;
    //uart_msg_t.rx.length = 0;
    memset(&uart_msg_t.rx,0, sizeof(uart_msg_t.rx));
    return true;
}
/**
* @function@name:   hal_uart_channel_check
* @description  :   Fuction for checking the uart channel
* @input        :   none
* @output       :   none
* @return       :   uart_channel
*/
uint8_t hal_uart_channel_check(void)
{
    return m_uart_conf.channel_nums;
}

/*
uint32_t uart_receive(void *buf, uint32_t max_size,uint32_t timeout)
{
    if(buf == NULL || max_size == 0)
    {
        DBG_LOG("uart receive pointer is null\r\n");
        return 0;
    }
    //uint32_t queue = 0;
    //uint8_t *p_buf = (uint8_t *)queue;
    uint8_t * p_buf = NULL;
    uint32_t rx_len = 0;
    if(queue_uart_rx == NULL)
    {
        return 0;
    }
    //DBG_LOG("ble_max_len = %d",ble_max_len);
    if(xQueueReceive(queue_uart_rx, &rx_len , timeout) != pdPASS)
    {
        //DBG_LOG("uart receive timeout");
        return 0;
    } 
    if(rx_len > 0 && rx_len <= max_size)
    {
        DBG_LOG("uart rx len = %d\r\n",rx_len);
        //p_buf = (uint8_t *)malloc(give_len * sizeof(uint8_t));
        xQueueReceive(queue_uart_rx, (uint32_t *)&p_buf , 0);
        //DBG_LOG("rec address = 0x%x\r\n",p_buf);    
        memcpy(buf,p_buf,rx_len);
        return rx_len;
    }
    else
    {
        if(rx_len > max_size)
        {
            DBG_LOG("rx_len = %d > max size = %d\r\n",rx_len,max_size);
        }
        else
        {
            DBG_LOG("rx_len = 0\r\n");
        }
        return 0;
    } 
}
*/

static uint32_t receive(void *buf, uint32_t max_size,uint32_t timeout)
{
    if(buf == NULL || max_size == 0)
    {
        DBG_LOG("uart receive pointer is null\r\n");
        return 0;
    }
    uint8_t queue = 0;
    uint8_t * p_buf = buf;
    uint32_t rx_len = 0;    
    if(queue_uart_rx == NULL)
    {
        return 0;
    }
    while(xQueueReceive(queue_uart_rx, &queue , timeout) == pdPASS)
    {
        p_buf[rx_len++] = queue;
        if(rx_len >= max_size)
        {
            DBG_LOG("uart receive len larger\r\n");
            return rx_len;
        }
    }
    return rx_len;
}
uint32_t uart_receive(void *buf, uint32_t max_size,uint32_t timeout)
{
    uint32_t len = 0;
    uint32_t count = 0;
    char *p = (char *)buf;
    portTickType start_time;
    portTickType now_time;
    start_time = xTaskGetTickCount();
    now_time = start_time;
    do
    {
        len = receive(&p[count],max_size-len,100);
        count += len;
        now_time = xTaskGetTickCount();
        
    }while(((len > 0) || (count == 0)) && (now_time - start_time) < timeout);
    
    return count;
}
/*uint32_t uart_receive(void *buf, uint32_t max_size,uint32_t timeout)
{
    if(buf == NULL || max_size == 0)
    {
        DBG_LOG("uart receive pointer is null\r\n");
        return 0;
    }
    uint8_t queue = 0;
    uint8_t * p_buf = buf;
    uint32_t rx_len = 0;    
    if(queue_uart_rx == NULL)
    {
        return 0;
    }
    portTickType start_time;
    portTickType now_time;
    start_time = xTaskGetTickCount();
    now_time = start_time;
    for(;(now_time-start_time) < timeout;)
    {
        if(xQueueReceive(queue_uart_rx, &queue , 50) == pdPASS)
        {
            p_buf[rx_len++] = queue;
            if(rx_len >= max_size)
            {
                DBG_LOG("uart receive len larger\r\n");
                //return rx_len;
                break;
            }
        }
        else
        {
            if(rx_len > 0)
            {
                break;
            }
        }
        now_time = xTaskGetTickCount();
    }
    return rx_len;
}*/
/**
* @function@name:   hal_uart_get_rx_fifo_datas
* @description  :   Fuction for getting data from uart dma fifo
* @input        :   p_buf: pointor to dest buffer will be filled, 
*                   max_len: the dest buffer max len can be used
*                   chn_type: uart channel type @ref uart_channel
* @output       :   none
* @return       :   valid data received length
*/
uint32_t hal_uart_get_rx_fifo_datas(uint8_t *p_buf, uint16_t max_len, uart_channel chn_type)
{
    
    if(chn_type > UART_CHANNEL_MAX)
    {
        return 0;
    }
    if(p_buf == NULL)
    {
        return 0;
    }
//    uint8_t i;
    uint16_t valid_len;
    uint8_t p_start ;
    switch(chn_type)
    {
        /*
        case UART_CHANNEL_DEBUG:
            if(m_uart_rx_t[UART_CHANNEL_DEBUG].data_cnt > max_len)
            {
                DBG_LOG("\r\nthe actual len of debug msg is more than max space\r\n");
                p_start = m_uart_rx_t[UART_CHANNEL_DEBUG].data_cnt - max_len;
                valid_len = max_len;                
            }   
            else
            {
                p_start = 0;
                valid_len = m_uart_rx_t[UART_CHANNEL_DEBUG].data_cnt;
            }
            memcpy(p_buf,&m_uart_rx_t[UART_CHANNEL_DEBUG].buffer[p_start],valid_len);
            m_uart_rx_t[UART_CHANNEL_DEBUG].data_cnt = 0;
            break;
            */
        case UART_CHANNEL_NB:             
            //DBG_LOG("\r\nget nb chn rx_cnt= %d \r\n",m_uart_rx_t[UART_CHANNEL_NB].data_cnt);
            if(m_uart_rx_t[UART_CHANNEL_NB].data_cnt > max_len)
            {
                DBG_LOG("\r\nthe actual len of nb msg is more than max space %d\r\n",max_len);
                p_start = m_uart_rx_t[UART_CHANNEL_NB].data_cnt - max_len;
                valid_len = max_len;
            }   
            else
            {
                p_start = 0;
                valid_len = m_uart_rx_t[UART_CHANNEL_NB].data_cnt;
            }
            memcpy(p_buf,m_uart_rx_t[UART_CHANNEL_NB].buffer,valid_len);
            m_uart_rx_t[UART_CHANNEL_NB].data_cnt = 0;
            memset(m_uart_rx_t[UART_CHANNEL_NB].buffer,0, sizeof(m_uart_rx_t[UART_CHANNEL_NB]));
            //DBG_LOG("\r\nget nb data and clear cnt\r\n");
            break;
        
        default:break;
    }
    return valid_len;
}


